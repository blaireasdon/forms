app.directive('fluroPreloader', function() {
    return {
        restrict: 'E',
        replace:true,
        scope:{},
        templateUrl:'fluro-preloader/fluro-preloader.html',
        controller:'FluroPreloaderController',
        link: function(scope, element, attrs) {
            
        }
    };
});

app.controller('FluroPreloaderController', function($scope, $urlRouter, $state, $rootScope, $timeout) {
    

    //////////////////////////////////////////////////////////////////

    var preloadTimer;

    $scope.preloader = {
        class:'reset'
    }

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, error) {

        //If we are actually moving to the state
        if($rootScope.stateChangeBypass) {
            $rootScope.stateChangeBypass = false;
            // console.log('GOT THERE')
            return;
        } else {


            //Stop the normal functionality
            event.preventDefault();
            $rootScope.stateChangeBypass = true;

            //////////////////////////////////////////////////////

            //Add the preloader class
            $scope.preloader.class = 'reset';
            preloadTimer = $timeout(function() {
                $scope.preloader.class = 'loading';
            });


            //////////////////////////////////////////////////////

            console.log('WAIT');

            //Wati a few seconds before actually transitioning
            $timeout(function(){

                console.log('GO')
                $state.go(toState, toParams);
            }, 500);
        }
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

        ////////////////////////////////////////////////////
        //If the preloader wasn't fast enough to start cancel it
        if (preloadTimer) {
            $timeout.cancel(preloadTimer);
            preloadTimer = null;
        }

        //Reset Preloader
        $scope.preloader.class = 'reset';

    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$preloaderHide', hidePreloader);
    $rootScope.$on('$stateChangeSuccess', hidePreloader);

    //Hide the preloader
    function hidePreloader(event, toState, toParams, fromState, fromParams, error) {
            // console.log('hide preloader')
        ////////////////////////////////////////////////////

        //If the preloader wasn't fast enough to start cancel it
        if (preloadTimer) {
            $timeout.cancel(preloadTimer);
            preloadTimer = null;
        }

        //If the preloader did show
        if ($scope.preloader.class == 'loading') {
            //Wait a little bit then hide the preloader
            $timeout(function() {
                // console.log('preloader has loaded');
                $scope.preloader.class = 'loaded';
            }, 600)
        }

    };


    //////////////////////////////////////////////////////////////////

    // $scope.clicked = function($event) {
    //     if($scope.asLink) {
    //         $state.go('watchVideo',{id:$scope.model._id, from:$scope.fromProduct})
    //     }
    // }


});