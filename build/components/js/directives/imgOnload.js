app.directive('imgOnload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                scope.$eval(attrs.imgOnload);
            });
        }
    };
});
