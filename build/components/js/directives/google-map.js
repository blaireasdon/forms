app.directive('googleMap', function($window, $timeout) {
    return {
        restrict: "E",
        replace: true,
        template: '<div class="map-outer"><div class="map-inner"></div></div>',
        scope: {
            model: "=ngModel",
            selectCallback: "=ngSelectCallback",
            styleOptions: "=ngStyleOptions",
        },
        link: function($scope, $element, $attrs) {

            var mapOptions;
            var map;
            var marker;
            var geocoder;
            var markers = [];

            ////////////////////////////////////////////////////////////
            
            styleOptions = [];
            
            if($scope.styleOptions) {
                styleOptions = $scope.styleOptions;
            } 
            
            ////////////////////////////////////////////////////////////
            
            //Nullify all markers
            function clearMarkers() {
                console.log('clear all markers');
                _.each(markers, function(marker) {
                    marker.setMap(null);
                });
              
                markers = [];
            }
            
            ////////////////////////////////////////////////////////////
            
            function initialize() {
                console.log('Map starting')
                
                mapOptions = {
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: false,
                    maxZoom: 15,
                    scrollwheel: false,
                    draggable: true,
                    styles: styleOptions,
                };
                
                //Create the map itself
                map = new google.maps.Map($element.find('.map-inner').get(0), mapOptions);
                
                //Create the Geocoder
                geocoder = new google.maps.Geocoder();
                
                //refit the map whenever the window is resized
                angular.element($window).bind('resize', fitMap);
            };
            
            ////////////////////////////////////////////////////////////////

            function fitMap() {

                //Create a bounds object
                var bounds = new google.maps.LatLngBounds();

                //Loop through each marker and expand the boundaries
                _.each(markers, function(marker) {

                    var lat = marker.position.lat();
                    var lng = marker.position.lng();

                    var myLatLng = new google.maps.LatLng(lat, lng);
                    bounds.extend(myLatLng);
                });
                
                //Fit the bounds
                map.fitBounds(bounds);
            
            }
            
            ////////////////////////////////////////////////////////////////
            
            var openWindow;
            
            function createInfoWindow(map, infowindow, contentString, marker) {
            
                if(openWindow) {
                    openWindow.close();
                }
                
                  google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                    openWindow = infowindow;
                  });
            }
            
            ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////
            
            //Create a marker representing the location
            var createMarker = function(location) {
                if (location.latitude && location.longitude) {
                    
                    var position = new google.maps.LatLng(location.latitude, location.longitude);
                        
                    map.setCenter(position);
                        
                    marker = new google.maps.Marker({
                        map: map,
                        position: position,
                        title: location.title
                    });
                    
                    google.maps.event.addListener(marker, 'click', function() {
                    
                        $timeout(function() {
                            if ($scope.selectCallback) {
                                $scope.selectCallback(location)
                            }
                        })
                    
                    });
                    
                    //Add the marker to the array
                    markers.push(marker);
                }
            };
            
            /////////////////////////////////////
            
            $scope.$watch("model", function(model) {
                // if (model) {


                //     _.each(model, function(model) {
                        createMarker(model)
                    // });
                // }

                fitMap();
            });

            ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////
            
            //Initialize the map
            initialize();
        },
    };
});