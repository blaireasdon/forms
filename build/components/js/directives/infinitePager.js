/**
 * sample usage 
<any-tag infinite-pager items="items" per-page="18" ng-params="{nextPageOnScroll:true}">
    <any-tag ng-repeat="page in pages"></any-tag>

    <div ng-if="pages.length <= totalPages">
        <i class="fa fa-spin fa-spinner"></i>
        <button ng-click="nextPage()">next page</button>
    </div>

</any-tag>
 */

app.directive('infinitePager', function($timeout, $sessionStorage, $state, $window) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attr) {
            var perPage = 16;
            
            if($attr.perPage) {
                perPage = parseInt($attr.perPage);
            }

            if($attr.ngParams) {
                var ngParams = $scope.$eval($attr.ngParams);
                if(ngParams.nextPageOnScroll) {
                    bindNextPageOnScroll();
                }
            }
            
            $scope.pager = {
                currentPage: 0,
                limit: perPage,
            };

            $scope.pages = [];

            // session storage to identify previous page
            if ($sessionStorage.currentPage) {
                if($sessionStorage.currentPage[$state.current.name]) {
                    // make sure pages is created after back button is pressed, before scrolling down
                    $scope.$on('back-resume-scroll', function(event, args) {
                        // load pages
                        var start = $scope.pager.limit;
                        var end = ($sessionStorage.currentPage[$state.current.name] + 1) * $scope.pager.limit;
                        // console.log('on-before-scroll', start, end);
                        
                        //Slice into seperate chunks
                        var sliced = _.slice($scope.allItems, start, end);
                        $scope.pages.push(sliced);

                        // only scroll and assign page when element is ready
                        $element.ready(function(){
                            document.body.scrollTop = args.previousScrollPosition;
                            $scope.pager.currentPage = $sessionStorage.currentPage[$state.current.name];
                            // console.log('scrolled and currentPage updated');
                        })
                    });
                }
            } else {
                $sessionStorage.currentPage = {};
            }

            /////////////////////////////////////////////////////
            
            $scope.$watch($attr.items, function(items) {
                $scope.allItems = items;
                
                if($scope.allItems) {
                    $scope.pages.length = 0;
                    $scope.pager.currentPage = 0;

                    $scope.totalPages = Math.ceil($scope.allItems.length / $scope.pager.limit) - 1;

                    $scope.updateCurrentPage();                    
                }
            });
    
            /////////////////////////////////////////////////////
           
            //Update the current page
            $scope.updateCurrentPage = function() {

                if ($scope.allItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
                    $scope.pager.currentPage = 0;
                }
                
                var start = ($scope.pager.currentPage * $scope.pager.limit);
                var end = start + $scope.pager.limit;

                // console.log('page updated', start, end);

                //Slice into seperate chunks
                var sliced = _.slice($scope.allItems, start, end);
                $scope.pages.push(sliced);
            }
    
            /////////////////////////////////////////////////////
            var timer;
            
            $scope.nextPage = function() {
                if ($scope.pager.currentPage < $scope.totalPages) {
                    $timeout.cancel(timer);
                    timer = $timeout(function() {
                        $scope.pager.currentPage = ($scope.pager.currentPage + 1);
                        $sessionStorage.currentPage[$state.current.name] = $scope.pager.currentPage;
                        $scope.updateCurrentPage();
                    });
                } else {
                    // $scope.updateCurrentPage();
                }
            }
            
            /////////////////////////////////////////////////////

            function bindNextPageOnScroll() {
                angular.element($window).bind("scroll", function() {
                    var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
                    var body = document.body, html = document.documentElement;
                    var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
                    windowBottom = windowHeight + window.pageYOffset;
                    if (windowBottom >= docHeight) {
                        $scope.nextPage();
                    }
                });
            }


        }
    };
    
})