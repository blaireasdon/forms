app.directive('ontaphold', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {

				var timer;
				var touchduration = 500; //default touch duration

				element[0].addEventListener('touchstart', function() {
				    timer = $timeout(onlongtouch, touchduration); 
				}, false);

				element[0].addEventListener('touchend', function() {
				    //stops short touches from firing the event
				    if (timer) {
				        return $timeout.cancel(timer);
				    } else {
				    	return onlongtouch();
				    }
				}, false);

				function onlongtouch() { 
					scope.$eval(attrs.ontaphold);
				};
                
            });
        }
    };
});