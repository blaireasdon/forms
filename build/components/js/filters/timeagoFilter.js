
app.filter('timeago', function(){
  return function(date){

  	if(_.isString(date)) {
  		date = new Date(date);
  	}
    return moment(date).fromNow();
  };
});
