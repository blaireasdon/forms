app.service('pastTimeService', function(){
	
	var list = [
		{ label: 'Last hour', value: 'hours' },
		{ label: 'Last 24 hours', value: 'days' },
		{ label: 'Last week', value: 'weeks' },
		{ label: 'Last month', value: 'months' },
		{ label: 'Last year', value: 'years' },
	];
    var service = {
        list: list,
        getMoment: getMoment,
        getLabel: getLabel
    };

    return service;

    ////////////

    function getMoment(listValue) {
    	return moment().subtract(1, listValue);
    };

    function getLabel(listValue) {
    	var found = _.find(list, { value: listValue });
    	if(!found) { 
    		return; 
    	}
    	return found.label;
    };


})