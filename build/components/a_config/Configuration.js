
_.contains = _.includes;

/////////////////////////////////////////////////////////////////////

var app = angular.module('fluro', [
    'ngAnimate',
    'ngResource',
    'ui.router',
    'ngTouch',
    'fluro.config',
    'fluro.content',
    'fluro.asset',
    'fluro.access',
    'fluro.validate',
    'fluro.interactions',
    'formly',
    'formlyBootstrap',
    'angulartics',
    'angulartics.google.analytics',
    'signature',
])


/////////////////////////////////////////////////////////////////////

function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName('meta');

    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == stringKey) {
            return metas[i].getAttribute("content");
        }
    }
    return "";
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.config(function($stateProvider, $httpProvider, $compileProvider, FluroProvider, $urlRouterProvider, $locationProvider, $analyticsProvider) {

    // make sure include default tracker and fluro tracker
    $analyticsProvider.settings.ga.additionalAccountNames = ['fluro'];

    // send user properties as well
    $analyticsProvider.settings.ga.additionalAccountHitTypes.setUserProperties = true;

    //Dont include all the angular comments and ng-scope classes
    $compileProvider.debugInfoEnabled(false);


    ///////////////////////////////////////////////////

    //Get the access token and the API URL
    var accessToken = getMetaKey('fluro_application_key');
    var apiURL = getMetaKey('fluro_url');

    /////////////////////////////////////////////// ////

    //Create the initial config setup
    var initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: false, //Change this if you want to use local storage  or session storage
        backupToken: accessToken,
    }

    ///////////////////////////////////////////

    //Check if we are developing an app locally
    var appDevelopmentURL = getMetaKey('app_dev_url');

    //If an app development url is set then we know where to login etc
    if (appDevelopmentURL && appDevelopmentURL.length) {
        initialConfig.appDevelopmentURL = appDevelopmentURL;
    } else {
        //Set HTML5 mode to true when we deploy
        //otherwise live reloading will break in local dev environment
        $locationProvider.html5Mode(true);
    }

    ///////////////////////////////////////////

    //Set the initial config
    FluroProvider.set(initialConfig);

    ///////////////////////////////////////////

    //Http Intercpetor to check auth failures for xhr requests
    if (!accessToken) {
        $httpProvider.defaults.withCredentials = true;
    }

    $httpProvider.interceptors.push('FluroAuthentication');

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    //////////////////////////////////////////



    $stateProvider.state('form', {
        url: '/form/:id?process',
        templateUrl: 'routes/form/form-view.html',
        controller: FormViewController,
        resolve: FormViewController.resolve,
    });

    ///////////////////////////////////////////

    $stateProvider.state('event', {
        url: '/event/:id',
        templateUrl: 'routes/event/event.html',
        controller: EventFormController,
        resolve: EventFormController.resolve,
    });

    ///////////////////////////////////////////

    $urlRouterProvider.otherwise("/");


});

/////////////////////////////////////////////////////////////////////

app.run(function($rootScope, $timeout, ShareModalService, $sessionStorage, DateTools, Asset, FluroSEOService, FluroContent, FluroBreadcrumbService, FluroScrollService, $location, $timeout, $state, $analytics) {

    
    $rootScope.avatarCache = {
        buster: 1,
    }

    //////////////////////////////////////////////////////////////////

    $rootScope.asset = Asset;
    $rootScope.$state = $state;
    $rootScope.session = $sessionStorage;
    $rootScope.breadcrumb = FluroBreadcrumbService;
    $rootScope.scroll = FluroScrollService;
    $rootScope.seo = FluroSEOService;
    $rootScope.modal = ShareModalService;
    $rootScope.date = DateTools;

    
    //////////////////////////////////////////////////////////////////

    if (!applicationData) {
        return ////console.log('No application data has been specified');
    }

    //Add the application data to the $rootScope
    $rootScope.applicationData = applicationData;

    if(applicationData.sandbox) {
        console.log('ENVIRONMENT: Sandbox');
    }

    if(applicationData.staging) {
        console.log('ENVIRONMENT: Staging');
    }

    //////////////////////////////////////////////////////////////////

    if (applicationUser && applicationUser._id) {
        $rootScope.user = applicationUser;

        $analytics.setUsername(applicationUser._id); // fluro id
        $analytics.setAlias(applicationUser.email); // user email
        $analytics.setUserProperties({
            dimension1: applicationUser.account._id // fluro account id
        });
    }

    //////////////////////////////////////////////////////////////////

    //Add the site name as the seo
    // var siteName = _.get(applicationData, '_application.title');
    // if (siteName && siteName.length) {
    //     FluroSEOService.siteTitle = siteName;
    // }

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    //Set your default SEO image here
    var defaultImageID = null; //_.get(applicationData, 'publicData.seoImage');
    var applicationID = _.get(applicationData, '_application._id');

    if (defaultImageID) {
        if (defaultImageID._id) {
            defaultImageID = defaultImageID._id;
        }

        FluroSEOService.defaultImageURL = Asset.imageUrl(defaultImageID, 640, null, {
            from: applicationID,
            extension: 'jpg'
        });
    }

    /////////////////////////////////////////////////////////////

    //Set your default SEO meta description here
    var defaultSEODescription = null; //_.get(applicationData, 'publicData.seoDescription');
    FluroSEOService.defaultDescription = defaultSEODescription;

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        console.log('State change error', event);
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {
        $rootScope.currentState = toState.name;
    });

    //////////////////////////////////////////////////////////////////


    $rootScope.getTypeOrDefinition = function(item, defaultIfNoneProvided) {
        if (item.definition && item.definition.length) {
            return item.definition;
        }

        if (!item._type && defaultIfNoneProvided) {
            return defaultIfNoneProvided;
        }


        return item._type;
    }

    //////////////////////////////////////////////////////////////////

    //Make touch devices more responsive
    FastClick.attach(document.body);

});