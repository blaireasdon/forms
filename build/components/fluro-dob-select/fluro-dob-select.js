app.factory('dateutils', function() {

    ////console.log('date.utils')
    // validate if entered values are a real date
    function validateDate(date) {


        // store as a UTC date as we do not want changes with timezones
        var d = new Date(Date.UTC(date.year, date.month, date.day));
        var result = d && (d.getMonth() === date.month && d.getDate() === Number(date.day));

        // ////console.log('Valid date', result, date);
        return result;
    }

    // reduce the day count if not a valid date (e.g. 30 february)
    function changeDate(date) {
        if (date.day > 28) {
            date.day--;
            return date;
        }
        // this case should not exist with a restricted input
        // if a month larger than 11 is entered
        else if (date.month > 11) {
            date.day = 31;
            date.month--;
            return date;
        }
    }

    var self = this;
    this.days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
    this.months = [{
        value: 0,
        name: 'January'
    }, {
        value: 1,
        name: 'February'
    }, {
        value: 2,
        name: 'March'
    }, {
        value: 3,
        name: 'April'
    }, {
        value: 4,
        name: 'May'
    }, {
        value: 5,
        name: 'June'
    }, {
        value: 6,
        name: 'July'
    }, {
        value: 7,
        name: 'August'
    }, {
        value: 8,
        name: 'September'
    }, {
        value: 9,
        name: 'October'
    }, {
        value: 10,
        name: 'November'
    }, {
        value: 11,
        name: 'December'
    }];

    return {
        checkDate: function(date) {

            if (!date.day) {
                return false;
            }

            if (date.month === 0) {

            } else {
                if (!date.month) {
                    return false;
                }
            }

            if (!date.year) {
                return false;
            }

            if (validateDate(date)) {
                // update the model when the date is correct
                return date;
            } else {
                // change the date on the scope and try again if invalid
                return this.checkDate(changeDate(date));
            }
        },
        get: function(name) {
            return self[name];
        }
    };
});

app.directive('dobSelect', ['dateutils',
    function(dateutils) {
        return {
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                hideAge: '=?',
                hideDates: '=?',
            },
            controller: ['$scope', 'dateutils',
                function($scope, dateutils) {


                    ////console.log('dob.select')


                    // set up arrays of values
                    $scope.days = dateutils.get('days');

                    $scope.months = dateutils.get('months');


                    //////////////////////////////////////////////////////////


                        // set the years drop down from attributes or defaults
                        var currentYear = parseInt(new Date().getFullYear());
                        var numYearsInPast = 100;
                        var oldestYear = currentYear - numYearsInPast;

                        // parseInt(attrs.startingYear, 10) || new Date().getFullYear();
                        // var numYears = parseInt(attrs.numYears, 10) || 100;
                        // var oldestYear = currentYear - numYears;

                       

                        var yearOptions = [];

                        for (var i = currentYear; i >= oldestYear; i--) {
                            yearOptions.push(i);
                        }

                        $scope.years = yearOptions;



                    /////////////////////////////////////////////////////

                    if (!$scope.model) {
                        //$scope.model = new Date();
                       // ////console.log('No way man')
                    } else {

                        //////console.log('TESTING', $scope.model, _.isDate($scope.model))
                        if (!_.isDate($scope.model)) {
                            $scope.model = new Date($scope.model);
                        }
                    }

                    /////////////////////////////////////////////////////

                    // split the current date into sections
                    var dateFields = {};

                    if ($scope.model) {
                        //Update the date fields
                        dateFields.day = new Date($scope.model).getDate();
                        dateFields.month = new Date($scope.model).getMonth();
                        dateFields.year = new Date($scope.model).getFullYear();

                        $scope.dateFields = dateFields;


                        /////////////////////////////////////////////////////

                        //Age
                        $scope.age = moment().diff($scope.model, 'years');

                    }

                    /////////////////////////////////////////////////////

                    // validate that the date selected is accurate
                    $scope.checkDate = function() {
                        // update the date or return false if not all date fields entered.
                        var date = dateutils.checkDate($scope.dateFields);
                        if (date) {
                            $scope.dateFields = date;
                        }
                    };

                    /////////////////////////////////////////////////////

                    function dateFieldsChanged(newDate, oldDate) {

                        //////console.log('Date fields Changed', newDate);
                        if (newDate == oldDate) {
                            return;
                        }

                        //Stop Watching Age
                        stopWatchingAge();


                        /////////////////////////////////

                        if (!$scope.dateFields) {
                            return ////console.log('No datefields object');
                        }

                        if (!$scope.dateFields.year) {
                            return ////console.log('No Year');
                        }

                        /////////////////////////////////

                        if ($scope.dateFields.month === 0) {

                        } else {
                            if (!$scope.dateFields.month) {
                                return ////console.log('No Month');
                            }
                        }

                        /////////////////////////////////

                        if (!$scope.dateFields.day) {
                            return ////console.log('No day');
                        }

                        /////////////////////////////////

                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        date.setHours(0, 0, 0, 0);

                        if (!$scope.model) {
                            $scope.model = new Date();
                        }


                        //Set the models details
                        $scope.model.setDate(date.getDate());
                        $scope.model.setMonth(date.getMonth());
                        $scope.model.setFullYear(date.getFullYear());

                        //Get the difference
                        var diff = moment().diff($scope.model, 'years');

                        //Update the age
                        $scope.age = parseInt(diff);


                        //Start Watching Age
                        startWatchingAge();
                    }


                    //Age was changed
                    function ageChanged(newAge, oldAge) {

                        if (newAge == oldAge) {
                            return;
                        }
                        ////console.log('Age changed', newAge, oldAge);

                        var today = new Date();
                        today.setHours(0, 0, 0, 0);

                        var currentYear = today.getFullYear();
                        var diff = parseInt(currentYear) - parseInt(newAge);

                        if (diff) {

                            //Stop Watching the Date
                            stopWatchingDate();

                            if (!$scope.dateFields) {

                                // if(!$scope.model) {
                                //     $scope.model = new Date();
                                // }

                                var dateFields = {};
                                //Update the date fields
                                //dateFields.day = new Date($scope.model).getDate();
                                //dateFields.month = new Date($scope.model).getMonth();
                                dateFields.year = diff; //new Date($scope.model).getFullYear();
                                $scope.dateFields = dateFields;
                            } else {
                                $scope.dateFields.year = diff;
                            }

                            //Set the year
                            // if ($scope.dateFields.year != diff) {
                            //     $scope.dateFields.year = diff;
                            // }

                            //Start Watching the date again
                            startWatchingDate();


                        } else {
                            ////console.log('NOPE', newAge, currentYear, diff);
                        }


                    }

                    /////////////////////////////////////////////////////

                    var ageWatcher;

                    function startWatchingAge() {
                        ageWatcher = $scope.$watch('age', ageChanged);
                    }

                    function stopWatchingAge() {
                        if (ageWatcher) {
                            ageWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////

                    var dateWatcher;

                    function startWatchingDate() {
                        dateWatcher = $scope.$watch('dateFields', dateFieldsChanged, true);
                    }

                    function stopWatchingDate() {
                        if (dateWatcher) {
                            dateWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////


                    //Start watching for changes to either
                    startWatchingAge();
                    startWatchingDate();
                   


                }
            ],
            templateUrl: 'fluro-dob-select/fluro-dob-select.html',
            link: function(scope, element, attrs, ctrl) {
                if (attrs.yearText) {
                    scope.yearText = true;
                }
                // allow overwriting of the
                if (attrs.dayDivClass) {
                    angular.element(element[0].children[0]).removeClass('form-group col-xs-3');
                    angular.element(element[0].children[0]).addClass(attrs.dayDivClass);
                }
                if (attrs.dayClass) {
                    angular.element(element[0].children[0].children[0]).removeClass('form-control');
                    angular.element(element[0].children[0].children[0]).addClass(attrs.dayClass);
                }
                if (attrs.monthDivClass) {
                    angular.element(element[0].children[1]).removeClass('form-group col-xs-5');
                    angular.element(element[0].children[1]).addClass(attrs.monthDivClass);
                }
                if (attrs.monthClass) {
                    angular.element(element[0].children[1].children[0]).removeClass('form-control');
                    angular.element(element[0].children[1].children[0]).addClass(attrs.monthClass);
                }
                if (attrs.yearDivClass) {
                    angular.element(element[0].children[2]).removeClass('form-group col-xs-4');
                    angular.element(element[0].children[2]).addClass(attrs.yearDivClass);
                }
                if (attrs.yearClass) {
                    angular.element(element[0].children[2].children[0]).removeClass('form-control');
                    angular.element(element[0].children[2].children[0]).addClass(attrs.yearClass);
                }

                

                // $scope.years = _.reverse(yearOptions);



                // pass down the ng-disabled property
                scope.$parent.$watch(attrs.ngDisabled, function(newVal) {
                    scope.disableFields = newVal;
                });

            }
        };
    }
]);

