app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'signature',
        templateUrl: 'fluro-interaction-form/signature/signature.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        // defaultOptions: {
        //     noFormControl: true,
        //     wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        //     templateOptions: {
        //         inputOptions: {
        //             wrapper: null
        //         }
        //     }
        // },
    });

});