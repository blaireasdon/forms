////////////////////////////////////////////////////////////////////////

var _nowYear = new Date().getFullYear();

var _defaultYears = _.chain(_.range(-100, 12))
    .map(function(y) {
        var actual = _nowYear + y;

        return {
            name:actual,
            value:actual,
        }
    })
    .reverse()
    .value();



app.directive('fluroDateSelect', [function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'fluro-interaction-form/date-select/date-selector.html',
        controller: ['$scope', 'dateutils', function($scope, dateutils) {



            //Get days and months
            $scope.days = dateutils.get('days');
            $scope.months = dateutils.get('months');
            $scope.years = _defaultYears;



            /////////////////////////////////////////////////////

            // split the current date into sections
            var dateFields = {};

            /////////////////////////////////////////////////////

            $scope.$watch('dateFields', dateFieldsChanged, true);
            $scope.$watch('model', modelChanged);

            /////////////////////////////////////////////////////

            function modelChanged() {

                if ($scope.model) {
                    if (!_.isDate($scope.model)) {
                        $scope.model = new Date($scope.model);
                    }

                    //Update the date fields
                    dateFields.day = new Date($scope.model).getDate();
                    dateFields.month = new Date($scope.model).getMonth();
                    dateFields.year = new Date($scope.model).getFullYear();
                    $scope.dateFields = dateFields;
                }
            }

            /////////////////////////////////////////////////////

            function dateFieldsChanged(newDate, oldDate) {

                //////console.log('Date fields Changed', newDate);
                if (newDate == oldDate) {
                    return;
                }

                /////////////////////////////////

                if (!$scope.dateFields) {
                    return ////console.log('No datefields object');
                }

                if (!$scope.dateFields.year) {
                    return ////console.log('No Year');
                }

                /////////////////////////////////

                if ($scope.dateFields.month === 0) {

                } else {
                    if (!$scope.dateFields.month) {
                        return ////console.log('No Month');
                    }
                }

                /////////////////////////////////

                if (!$scope.dateFields.day) {
                    return ////console.log('No day');
                }

                /////////////////////////////////

                var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                date.setHours(0, 0, 0, 0);

                if (!$scope.model) {
                    $scope.model = new Date();
                }


                //Set the models details
                $scope.model.setDate(date.getDate());
                $scope.model.setMonth(date.getMonth());
                $scope.model.setFullYear(date.getFullYear());
            }

            /////////////////////////////////////////////////////

            // validate that the date selected is accurate
            $scope.checkDate = function() {
                // update the date or return false if not all date fields entered.
                var date = dateutils.checkDate($scope.dateFields);
                if (date) {
                    $scope.dateFields = date;
                }
            };

            


            // $validate();

            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////

            
            /**
             ////console.log('FluroDateSelectController')
            $scope.today = function() {
                $scope.model[$scope.options.key] = new Date();
            };


            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd/MM/yyyy'];
            $scope.format = $scope.formats[0];

            /**/
        }]
    }

}]);