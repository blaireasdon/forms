app.directive('interactionForm', function($compile) {

    //////console.log('interaction form controller directive')
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            linkedProcess: '=?linkedProcess',
            vm: '=?config',
            callback: '=?callback',
        },
        transclude: true,
        controller: 'InteractionFormController',
        template: '<div class="fluro-interaction-form" ng-transclude-here />',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };


});

////////////////////////////////////////////////////////////////////////

app.directive('webform', function($compile) {

    //////console.log('web form controller directive')

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            vm: '=?config',
            debugMode: '=?debugMode',
            callback: '=?callback',
            linkedEvent: '=?linkedEvent',
            linkedProcess: '=?linkedProcess',
        },
        transclude: true,
        controller: 'InteractionFormController',
        templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
                // console.log('Transclude content', clone)
                //$element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };
});


app.config(function(formlyConfigProvider) {


    formlyConfigProvider.setType({
        name: 'currency',
        extends: 'input',
        overwriteOk: true,
        controller: ['$scope', function($scope) {
            /*
            ////////console.log('CURRENCY SCOPE', $scope);

            $scope.$watch('model[options.key]', function(val) {

                if(!$scope.model[$scope.options.key] && $scope.model[$scope.options.key] != 0 ) {
                    ////////console.log('Set!')
                    $scope.model[$scope.options.key] = 0;
                }
            })
            /**/
        }],

        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        defaultOptions: {
            ngModelAttrs: {
                currencyDirective: {
                    attribute: 'ng-currency'
                },
                fractionValue: {
                    attribute: 'fraction',
                    bound: 'fraction'
                },
                minimum: {
                    attribute: 'min',
                    bound: 'min'
                },
                maximum: {
                    attribute: 'max',
                    bound: 'max'
                }
            },
            templateOptions: {
                customAttrVal: '',
                required: true,
                fraction: 2,
            },
            validators: {
                validInput: {
                    expression: function($viewValue, $modelValue, scope) {

                        var numericValue = Number($modelValue);

                        if (isNaN(numericValue)) {
                            return false;
                        }

                        //Get Minimum and Maximum Amounts
                        var minimumAmount = scope.options.data.minimumAmount;
                        var maximumAmount = scope.options.data.maximumAmount;

                        if (minimumAmount && numericValue < minimumAmount) {
                            return false;
                        }

                        if (maximumAmount && numericValue > maximumAmount) {
                            return false;
                        }

                        return true;
                    }
                }
            }
        }
    });


})

////////////////////////////////////////////////////////////////////////

app.run(function(formlyConfig, $templateCache) {

    //////console.log('web form run')
    formlyConfig.templateManipulators.postWrapper.push(function(template, options, scope) {

        if (!scope.to.errorMessage || !scope.to.errorMessage.length) {



            var viewValue = _.get(scope, 'fc.$viewValue');
            var fieldDefinition = _.get(options, 'templateOptions.definition');

            // console.log('SCOPE OPTIONS', viewValue);

            if (!viewValue || _.isArray(viewValue)) {
                // if(!viewValue.length) {
                // scope.to.errorMessage = 'Please enter at least';
                // } else {

                if (fieldDefinition && (fieldDefinition.maximum == 1)) {
                    scope.to.errorMessage = 'This field is required';
                } else {
                    if (fieldDefinition && fieldDefinition.minimum != 1) {
                        scope.to.errorMessage = 'Please select at least ' + fieldDefinition.minimum + ' options';
                    } else {
                        scope.to.errorMessage = 'Please select at least 1 option';
                    }

                }

            } else {
                // console.log('VIEW FALUE IS NOT VALID', viewValue);
                scope.to.errorMessage = "'" + viewValue + "' is not a valid answer";
            }
        }

        var fluroErrorTemplate = $templateCache.get('fluro-interaction-form/field-errors.html');
        return '<div>' + template + fluroErrorTemplate + '</div>';
    });

    //////////////////////////////////




    //////////////////////////////////

    formlyConfig.setType({
        name: 'multiInput',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/multi.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: ['$scope', function($scope) {
            $scope.copyItemOptions = copyItemOptions;

            function copyItemOptions() {
                return angular.copy($scope.to.inputOptions);
            }
        }]
    });

    //////////////////////////////////

    // formlyConfig.setType({
    //     name: 'payment',
    //     overwriteOk: true,
    //     templateUrl: 'fluro-interaction-form/payment/payment.html',
    //     //controller: 'FluroPaymentController',
    //     defaultOptions: {
    //         noFormControl: true,
    //     },
    // });

    formlyConfig.setType({
        name: 'custom',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/custom.html',
        controller: 'CustomInteractionFieldController',
        wrapper: ['bootstrapHasError']
    });

    formlyConfig.setType({
        name: 'button-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/button-select/fluro-button-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });



    formlyConfig.setType({
        name: 'date-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/date-select/fluro-date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope, FluroValidate) {

            $scope.$watch('model', checkValidity, true);

            var opts = $scope.options;
            var definition = opts.templateOptions.definition;

            /////////////////////////////////////////////////////

            function checkValidity() {

                if (!$scope.model) {
                    $scope.model = {};
                }

                var validRequired;
                var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

                //Check if multiple
                if ($scope.multiple) {
                    if ($scope.to.required) {
                        validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
                    }
                } else {
                    if ($scope.to.required) {
                        if ($scope.model[opts.key]) {
                            validRequired = true;
                        }
                    }
                }

                // if(!validInput) {
                //    console.log('CHECK VALIDITY', definition.title, validRequired, validInput);
                // }


                if ($scope.fc) {

                    var formControl = $scope.fc;
                    if (formControl.length) {
                        formControl = formControl[0];
                    }

                    // console.log('FC', $scope.fc);
                    formControl.$setValidity('required', validRequired);
                    formControl.$setValidity('validInput', validInput);
                }
            }
        }
    });



    formlyConfig.setType({
        name: 'terms',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/fluro-terms.html',
        wrapper: ['bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'order-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/order-select/fluro-order-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    /**/

});

////////////////////////////////////////////////////////////////////////

app.controller('CustomInteractionFieldController', function($scope, FluroValidate) {
    $scope.$watch('model[options.key]', function(value) {
        if (value) {
            if ($scope.fc) {
                $scope.fc.$setTouched();
            }
        }
    }, true);
});

////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

app.controller('InteractionFormController', function($scope, $q, $timeout, $rootScope, DateTools, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {


    var tithelyController;


    /////////////////////////////////////////////

    function getAge(dateInput, timezone) {
        var today = new Date();
        var birthDate = new Date(dateInput);
        var age = today.getFullYear() - birthDate.getFullYear();

        var m = today.getMonth() - birthDate.getMonth();

        //If the date is on the cusp of the new year
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

        return age;
    }

    /////////////////////////////////////////////////////////////////

    function matchInArray(array, key, value, operator) {

        //Filter the array options by a certain value and operator
        var matches = _.filter(array, function(entry) {

            //Get the value from the object
            var retrievedValue = _.get(entry, key);
            var isMatch;

            ///////////////////////

            //Check how to operate
            switch (operator) {
                case '>':
                    isMatch = (retrievedValue > value);
                    break;
                case '<':
                    isMatch = (retrievedValue < value);
                    break;
                case '>=':
                    isMatch = (retrievedValue >= value);
                    break;
                case '<=':
                    isMatch = (retrievedValue <= value);
                    break;
                case 'in':
                    isMatch = _.includes(retrievedValue, value);
                    break;
                default:
                    //operator is strict equals
                    if (value === undefined) {
                        isMatch = retrievedValue;
                    } else {
                        isMatch = (retrievedValue == value);
                    }
                    break;
            }

            ///////////////////////

            return isMatch;
        })

        return matches;
    }


    /////////////////////////////////////////////////////////////////


    // $scope.debugMode = true;

    //If we are in staging environment then set debugMode to true
    if (_.get(window, 'applicationData.staging')) {
        $scope.debugMode = true;
    }



    /////////////////////////////////////////////////////////////////

    if (!$scope.vm) {
        $scope.vm = {}
    }

    /////////////////////////////////////////////////////////////////

    //Keep this available to all field scopes
    //this is used so that other fields can get calculated total
    //not just the payment summary field
    $formScope = $scope;

    /////////////////////////////////////////////////////////////////

    function debugLog() {
        // return;
        if ($scope.debugMode) {
            console.log(_.map(arguments, function(v) { return v }));
        }
    }


    /////////////////////////////////////////////////////////////////

    // The model object that we reference
    // on the  element in index.html
    if ($scope.vm.defaultModel) {
        $scope.vm.model = angular.copy($scope.vm.defaultModel);
    } else {
        $scope.vm.model = {};
    }

    /////////////////////////////////////////////////////////////////

    // An array of our form fields with configuration
    // and options set. We make reference to this in
    // the 'fields' attribute on the  element
    $scope.vm.modelFields = [];

    /////////////////////////////////////////////////////////////////

    //Keep track of the state of the form
    $scope.vm.state = 'ready';





    /////////////////////////////////////////////////////////////////

    $scope.correctPermissions = true;

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////


    $scope.scrollToField = function(field) {
        if (field.field) {
            field = field.field;
        }
        var element = _.get(field, 'formControl.$$element[0]');

        if (!element) {
            element = document.querySelector('#' + field.$name);
            // document.getElementById('#'+field.$name);   
            console.log('ID', field, field.$name, element)
        }

        if (element) {
            if (element.scrollIntoView) {
                element.scrollIntoView();
            }
        }



    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.readyToSubmit = false;

    // $scope.$watch('vm.modelForm.$invalid', updateErrorList)
    $scope.$watch('vm.modelFields', validateAllFields, true)
    $scope.$watch('vm.model', validateAllFields, true)

    $scope.$watch('model', definitionModelChanged);
    $scope.$watch('integration', integrationChanged);

    // $scope.$watch('vm.modelForm.$touched', updateErrorList)
    // $scope.$watch('vm.modelForm.$error.required', updateErrorList)
    // $scope.$watch('vm.modelForm.$error.validInput', updateErrorList)

    var timer;


    function validateAllFields() {

        //Debounce the validation
        if (timer) {
            $timeout.cancel(timer);
            timer = null;
        }

        timer = $timeout(function() {

            function setValid(ready) {
                // console.log('SET VALID', ready)
                $scope.readyToSubmit = ready;
            }

            /////////////////////////////////////

            function mapRecursiveField(field) {
                if (!field) {
                    return;
                }

                var output = [field];

                /////////////////////////////////////////////////////

                if (field.key == '_paymentCardDetails') {
                    var calculatedAmount = _.get($formScope, 'vm.model.calculatedTotal');
                    var donationAmount = _.get($formScope, 'vm.model._paymentAmount');


                    //If there is no cost
                    //then we don't need to validate
                    //the card fields
                    if (!calculatedAmount && !donationAmount) {
                        console.log('No calculated total');
                        return;
                    }

                    //Get the payment method
                    var paymentMethod = _.get($formScope, 'vm.model._paymentMethod');
                    if (paymentMethod && paymentMethod.length && paymentMethod != 'card') {
                        console.log('Alternative payment method');
                        return;
                    }
                }

                /////////////////////////////////////////////////////

                var isDisabled = _.get(field, 'templateOptions.definition.disableValidation');

                if (isDisabled) {
                    // console.log('FAILED', field.key, isDisabled)
                    return;
                }


                /////////////////////////////////////////////////////


                if (!field.hide && field.fields && field.fields.length) {
                    output.push(_.map(field.fields, mapRecursiveField));
                }

                if (!field.hide && field.data && field.data.fields && field.data.fields.length) {
                    output.push(_.map(field.data.fields, mapRecursiveField));
                }

                if (!field.hide && field.fieldGroup && field.fieldGroup.length) {
                    // console.log('IS REQUIRED?', field);
                    output.push(_.map(field.fieldGroup, mapRecursiveField));
                }

                if (!field.hide && field.data && field.data.replicatedFields && field.data.replicatedFields.length) {

                    // console.log('REPLICATED FIELDS')
                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                }



                if (!field.hide && field.data && field.data.replicatedDataFields && field.data.replicatedDataFields.length) {

                    // console.log('REPLICATED FIELDS')
                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedDataFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                }

                if (!field.hide && field.data && field.data.replicatedDetailFields && field.data.replicatedDetailFields.length) {

                    // console.log('REPLICATED FIELDS')
                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedDetailFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                }



                //  else {
                //     if (field.data && field.data.fields && field.data.fields.length) {
                //         output.push(_.map(field.data.fields, mapRecursiveField));
                //     }
                // }

                return output;
            }



            ////////////////////////////////////////////

            // console.log('ERROR LIST CHECKING', $scope.vm.modelFields);

            $scope.errorList = _.chain($scope.vm.modelFields)
                .map(mapRecursiveField)
                .flattenDeep()
                .compact()
                .filter(function(field) {

                    /////////////////////////////////////////////////////

                    var required = _.get(field, 'formControl.$error.required');
                    var dateInvalid = _.get(field, 'formControl.$error.date');
                    var invalid = _.get(field, 'formControl.$error.validInput');
                    var uploadInProgress = _.get(field, 'formControl.$error.uploadInProgress');
                    // var errorExistsAndShouldBeVisible = _.get(field, 'validation.errorExistsAndShouldBeVisible');

                    /////////////////////////////////////////////////////

                    var shouldwarning = (required || invalid || dateInvalid || uploadInProgress);
                    if (shouldwarning) {
                        // console.log('Not valid > ', shouldwarning, field, required, invalid)
                    }



                    return shouldwarning;
                })
                .value();


            ////////////////////////////////////////////

            //Interaction Form
            var interactionForm = $scope.vm.modelForm;

            if (!interactionForm) {
                console.log('INVALID - no form')
                return setValid(false);
            }

            ////////////////////////////////////////////

            // if (interactionForm.$invalid) {

            // console.log($scope.vm);


            if (!$scope.errorList || !$scope.errorList.length) {



                var invalidValues = _.get(interactionForm, '$error.validInput');
                var requiredValues = _.get(interactionForm, '$error.required');

                //We need to display something to the user as to why the form is not valid
                $scope.errorList = _.chain([invalidValues, requiredValues])
                    .flatten()
                    .compact()
                    .map(function(f, index) {
                        return _.chain(f)
                            .keys()
                            .map(function(key) {

                                if (!_.startsWith(key, 'formly_')) {
                                    return;
                                }

                                var field = f[key];


                                // console.log('CHECK', f, key, field);
                                if (field.$invalid || field.$error) {
                                    return field;
                                }

                                return;
                            })
                            .value()
                    })
                    .flatten()
                    .compact()
                    .map(function(f, index) {




                        var readableName = f.$name;
                        if (!f.$viewValue) {
                            // console.log(f, readableName);



                            var message = 'Please check ' + readableName;

                            if (_.startsWith(readableName, 'formly_')) {
                                if (f.$error.date) {
                                    message = 'Invalid date provided'
                                }
                            }

                            return {
                                id: index,
                                field: f,
                                message: message,
                            }
                        }
                        return {
                            id: index,
                            field: f,
                            message: f.$viewValue + ' is an invalid value for ' + readableName,
                        }
                        // }



                        // return f;
                    })
                    .compact()
                    .value();


                // console.log('INVALID NESTED FORMS - ', $scope.errorList)
                // } else {
                //     console.log('HAS ERROR LIST ALREADY', $scope.errorList);

                if ($scope.errorList.length) {
                    console.log('INVALID We have errors so set to false')
                    return setValid(false);
                }
            }



            // }

            ////////////////////////////////////////////

            if (interactionForm.$error) {

                console.log('Errors', interactionForm.$error);

                if (interactionForm.$error.required && interactionForm.$error.required.length) {
                    // console.log('INVALID $error.required - ', interactionForm.$error.required);
                    return setValid(false);
                }



                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) {


                    // console.log('INVALID $error.validInput  - ', interactionForm.$error.validInput);

                    console.log('INVALID valid input not provided', interactionForm.$error.validInput);
                    return setValid(false);
                }

                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) {

                    // console.log('INVALID $error.uploadInProgress - Upload still in progress');
                    console.log('INVALID Upload has not finished');
                    return setValid(false);
                }
            }

            // ////////console.log('Form should be good to go')

            //It all worked so set to true
            setValid(true);
        }, 200)

    }

    /**
     /////////////////////////////////////////////////////////////////

    $scope.$watch('vm.modelFields', function(fields) {
        //////////console.log('Interaction Fields changed')
        $scope.errorList = getAllErrorFields(fields);

        console.log('Error List', $scope.errorList);
    }, true)

    /**/


    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    formlyValidationMessages.addStringMessage('required', 'This field is required');

    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid value';
    }


    formlyValidationMessages.messages.uploadInProgress = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is still processing';
    }

    formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid date';
    }

    /////////////////////////////////////////////////////////////////

    $scope.reset = function() {

        //Reset
        if ($scope.vm.defaultModel) {
            //console.log('Reset the default model')
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }
        $scope.vm.modelForm.$setPristine();
        $scope.vm.options.resetModel();


        //Act as if a new definition was changed
        definitionModelChanged();




        //Clear the response from previous submission
        $scope.response = null;
        $scope.vm.state = 'ready';


        //Reset after state change
        ////////console.log('Broadcast reset')
        $scope.$broadcast('form-reset');

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    // //Function to run on permissions
    // function checkPermissions() {
    //     if ($rootScope.user) {
    //         //Check if we have the correct permissions
    //         var canCreate = FluroAccess.can('create', $scope.model.definitionName);
    //         var canSubmit = FluroAccess.can('submit', $scope.model.definitionName);

    //         //Allow if the user can create or submit
    //         $scope.correctPermissions = (canCreate | canSubmit);
    //     } else {
    //         //Just do this by default
    //         $scope.correctPermissions = true;
    //     }
    // }

    /////////////////////////////////////////////////////////////////

    // //Watch if user login changes
    // $scope.$watch(function() {
    //     return $rootScope.user;
    // }, checkPermissions)

    /////////////////////////////////////////////////////////////////

    var injectedScripts = {};


    function injectScript(scriptURL, callback) {

        if (injectedScripts[scriptURL]) {
            return callback();
        }

        //Keep note so we don't inject twice
        injectedScripts[scriptURL] = true;

        //////////////////////////////////////

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.onload = callback;
        script.src = scriptURL;
        document.getElementsByTagName('head')[0].appendChild(script);

        ////////////////////////////////////

        console.log('Appended script dependency', scriptURL);
    }

    /////////////////////////////////////////////////////////////////

    function integrationChanged(integration) {

        if (!integration) {
            return;
        }

        ////////////////////////////////////////

        //Check if the environment is a sandbox environment or if the gateway is defined as a sandbox gateway
        var SANDBOX_ENV = _.get(integration, 'publicDetails.sandbox') || _.get(window, 'applicationData.sandbox');

        ////////////////////////////////////////

        //Check to see if we need to inject any dependencies
        switch (integration.module) {
            case 'stripe':
                injectScript('https://js.stripe.com/v2/', function() {
                    console.log('Stripe has been included on page')
                });
                break;
            case 'eway':
                injectScript('https://secure.ewaypayments.com/scripts/eCrypt.js', function() {
                    console.log('Eway has been included on page')
                });
                break;
            case 'tithely':


                if (SANDBOX_ENV) {
                    console.log('Inject SANDBOX token script')
                    injectScript('https://tithelydev.com/api-js/v1/tithely.js', tithelyLoaded);
                } else {
                    console.log('Inject LIVE token script')
                    injectScript('https://tithe.ly/api-js/v1/tithely.js', tithelyLoaded);
                }

                //////////////////////////////////////

                function tithelyLoaded() {
                    console.log('TITHELY HAS LOADED')
                    //Get encrypted token from Stripe
                    var key;
                    var liveKey = integration.publicDetails.publicKey;
                    var sandboxKey = integration.publicDetails.publicKey;

                    /////////////////////////////////////////////

                    if (SANDBOX_ENV) {
                        key = sandboxKey;
                    } else {
                        key = liveKey;
                    }

                    /////////////////////////////////////////////

                    //Set the tithely key
                    tithelyController = new Tithely(key);
                }


                break;
        }

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function definitionModelChanged() {

        // ////////console.log('Model changed');
        if (!$scope.model || $scope.model.parentType != 'interaction') {
            console.log('Model is not an interaction'); //$scope.model = {};
            return
        }

        /////////////////////////////////////////////////////////

        //Check if we need a payment gateway
        var paymentSettings = $scope.model.paymentDetails;

        //If we do need a payment gateway
        if (paymentSettings && (paymentSettings.required || paymentSettings.allow)) {



            var hasPublicDetails = _.get($scope, 'integration.publicDetails');
            var hasModule = _.get($scope, 'integration.module');

            if (!hasPublicDetails || !hasModule) {
                console.log('Form Requires Payment, but no payment gateway was provided');
                alert('This form has not been configured properly. And no payment gateway was defined. Please notify the administrator of this website immediately.')
                return;
            }
        }



        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        // The model object that we reference
        // on the  element in index.html
        // $scope.vm.model = {};
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }


        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        $scope.vm.modelFields = [];

        /////////////////////////////////////////////////////////////////

        //Keep track of the state of the form
        $scope.vm.state = 'ready';

        /////////////////////////////////////////////////////////////////

        //Add the submit function
        $scope.vm.onSubmit = submitInteraction;

        /////////////////////////////////////////////////////////////////

        //Keep track of any async promises we need to wait for
        $scope.promises = [];

        /////////////////////////////////////////////////////////////////

        //Submit is finished
        $scope.submitLabel = 'Submit';

        if ($scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length) {
            $scope.submitLabel = $scope.model.data.submitLabel;
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details
        var interactionFormSettings = $scope.model.data;

        if (!interactionFormSettings) {
            interactionFormSettings = {};
        }

        if (!interactionFormSettings.allowAnonymous && !interactionFormSettings.disableDefaultFields) {
            interactionFormSettings.requireFirstName = true;
            interactionFormSettings.requireLastName = true;
            interactionFormSettings.requireGender = true;
            interactionFormSettings.requireEmail = true;

            switch (interactionFormSettings.identifier) {
                case 'both':
                    interactionFormSettings.requireEmail =
                        interactionFormSettings.requirePhone = true;
                    break;
                case 'email':
                    interactionFormSettings.requireEmail = true;
                    break;
                case 'phone':
                    interactionFormSettings.requirePhone = true;
                    break;
                case 'either':
                    interactionFormSettings.askPhone = true;
                    interactionFormSettings.askEmail = true;
                    break;
            }
        }


        /////////////////////////////////////////////////////////////////

        var firstNameField;
        var lastNameField;
        var genderField;

        /////////////////////////////////////////////////////////////////

        //Gender
        if (interactionFormSettings.askGender || interactionFormSettings.requireGender) {
            genderField = {
                key: '_gender',
                type: 'select',
                templateOptions: {
                    type: 'email',
                    label: 'Title',
                    placeholder: interactionFormSettings.placeholderGender, //'Please select a title',
                    options: [{
                        name: 'Mr',
                        value: 'male'
                    }, {
                        name: 'Ms / Mrs',
                        value: 'female'
                    }],
                    required: interactionFormSettings.requireGender,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return (value == 'male' || value == 'female');
                    }
                }
            }
            //$scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //First Name
        if (interactionFormSettings.askFirstName || interactionFormSettings.requireFirstName) {
            firstNameField = {
                key: '_firstName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'First Name',
                    placeholder: interactionFormSettings.placeholderFirstName, //'Please enter your first name',
                    required: interactionFormSettings.requireFirstName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////

        //Last Name
        if (interactionFormSettings.askLastName || interactionFormSettings.requireLastName) {
            lastNameField = {
                key: '_lastName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'Last Name',
                    placeholder: interactionFormSettings.placeholderLastName, //'Please enter your last name',
                    required: interactionFormSettings.requireLastName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        if (genderField && firstNameField && lastNameField) {

            genderField.className = 'col-sm-2';

            firstNameField.className =
                lastNameField.className = 'col-sm-5';

            $scope.vm.modelFields.push({
                fieldGroup: [genderField, firstNameField, lastNameField],
                className: 'row',
                // name:'First and Last Name',
            });
        } else if (firstNameField && lastNameField && !genderField) {
            firstNameField.className =
                lastNameField.className = 'col-sm-6';

            $scope.vm.modelFields.push({
                fieldGroup: [firstNameField, lastNameField],
                className: 'row',
                // name:'First and Last Name',
            });
        } else {
            if (genderField) {
                $scope.vm.modelFields.push(genderField);
            }

            if (firstNameField) {
                $scope.vm.modelFields.push(firstNameField);
            }

            if (lastNameField) {
                $scope.vm.modelFields.push(lastNameField);
            }
        }



        /////////////////////////////////////////////////////////////////

        //Email Address
        if (interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {

            // console.log('REQUIRE EMAIL???', interactionFormSettings);
            var newField = {
                key: '_email',
                type: 'input',
                templateOptions: {
                    type: 'email',
                    label: 'Email Address',
                    placeholder: interactionFormSettings.placeholderEmail, //'Please enter a valid email address',
                    required: interactionFormSettings.requireEmail,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {



                        var value = $modelValue || $viewValue;

                        if (!value || !value.length) {
                            return true;
                        }

                        return validator.isEmail(value);
                    }
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._phoneNumber || !scope.model._phoneNumber.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }

            $scope.vm.modelFields.push(newField);
        }


        /////////////////////////////////////////////////////////////////

        //Ask Phone Number
        if (interactionFormSettings.askPhone || interactionFormSettings.requirePhone) {
            var newField = {
                key: '_phoneNumber',
                type: 'input',
                templateOptions: {
                    type: 'tel',
                    label: 'Contact Phone Number',
                    placeholder: interactionFormSettings.placeholderPhoneNumber, //'Please enter a contact phone number',
                    required: interactionFormSettings.requirePhone,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._email || !scope.model._email.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

            }


            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //Age / Date of birth
        if (interactionFormSettings.askDOB || interactionFormSettings.requireDOB) {
            var newField = {
                key: '_dob',
                type: 'dob-select',
                templateOptions: {
                    label: 'Date of birth',
                    placeholder: interactionFormSettings.placeholderDOB, //'Please provide your date of birth',
                    required: interactionFormSettings.requireDOB,
                    maxDate: new Date(),
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                    // params:{
                    //     hideAge:true,
                    // }

                }
            }

            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        function addFieldDefinition(array, fieldDefinition) {

            // switch(fieldDefinition.type) {
            //     case 'group':
            //     case 'string':
            //     break;
            //     default:
            //     return console.log('TYPE', fieldDefinition.type);
            //     break;
            // }

            if (fieldDefinition.params && fieldDefinition.params.disableWebform) {
                //If we are hiding this field then just do nothing and return here
                return;
            }




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Create a new field
            var newField = {};
            newField.key = fieldDefinition.key;
            // newField.name = _.startCase(fieldDefinition.title);

            /////////////////////////////

            //Ensure integers for min/max
            fieldDefinition.minimum = parseInt(fieldDefinition.minimum);
            fieldDefinition.maximum = parseInt(fieldDefinition.maximum);

            /////////////////////////////

            //Add the class name if applicable
            if (fieldDefinition.className) {
                newField.className = fieldDefinition.className;
            }

            /////////////////////////////

            //Template Options
            var templateOptions = {};
            templateOptions.type = 'text';
            templateOptions.label = fieldDefinition.title;
            templateOptions.description = fieldDefinition.description;
            templateOptions.params = fieldDefinition.params;


            //Attach a custom error message
            if (fieldDefinition.errorMessage) {
                templateOptions.errorMessage = fieldDefinition.errorMessage;
            }

            //Include the definition itself
            templateOptions.definition = fieldDefinition;

            /////////////////////////////

            //Add a placeholder
            if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                templateOptions.placeholder = fieldDefinition.placeholder;
            }

            //  else if (fieldDefinition.description && fieldDefinition.description.length) {
            //     templateOptions.placeholder = fieldDefinition.description;
            // } else {
            //     templateOptions.placeholder = fieldDefinition.title;
            // }

            /////////////////////////////

            //Require if minimum is greater than 1 and not a field group
            templateOptions.required = (fieldDefinition.minimum > 0);

            /////////////////////////////

            templateOptions.onBlur = 'to.focused=false';
            templateOptions.onFocus = 'to.focused=true';




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Allowed Options


            switch (fieldDefinition.type) {
                case 'reference':


                    //If we have allowed references specified
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                        templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id,
                            }
                        });
                    } else {
                        //We want to load all the options from the server
                        templateOptions.options = [];

                        if (fieldDefinition.sourceQuery) {

                            //We use the query to find all the references we can find
                            var queryId = fieldDefinition.sourceQuery;
                            if (queryId._id) {
                                queryId = queryId._id;
                            }

                            /////////////////////////

                            var options = {};

                            //If we need to template the query
                            if (fieldDefinition.queryTemplate) {
                                options.template = fieldDefinition.queryTemplate;
                                if (options.template._id) {
                                    options.template = options.template._id;
                                }
                            }

                            /////////////////////////

                            //Now retrieve the query
                            var promise = FluroContentRetrieval.getQuery(queryId, options);

                            //Now get the results from the query
                            promise.then(function(res) {
                                //////////console.log('Options', res);
                                templateOptions.options = _.map(res, function(ref) {
                                    return {
                                        name: ref.title,
                                        value: ref._id,
                                    }
                                })
                            });
                        } else {

                            if (fieldDefinition.directive != 'embedded') {
                                if (fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length) {
                                    //We want to load all the possible references we can select
                                    FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                        templateOptions.options = _.map(res, function(ref) {
                                            return {
                                                name: ref.title,
                                                value: ref._id,
                                            }
                                        })
                                    });
                                }
                            }
                        }
                    }
                    break;
                default:
                    //Just list the options specified
                    if (fieldDefinition.options && fieldDefinition.options.length) {
                        templateOptions.options = fieldDefinition.options;
                    } else {
                        templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                            return {
                                name: val,
                                value: val
                            }
                        });
                    }
                    break;
            }


            /////////////////////////////

            //Directive or widget
            switch (fieldDefinition.directive) {
                case 'reference-select':
                case 'value-select':

                    switch (window.DEVICE_PLATFORM) {
                        case 'ios':
                        case 'android':
                            //Use the 
                            newField.type = 'reference-select';
                            break;
                        default:

                            if (templateOptions.options && templateOptions.options.length > 10) {
                                newField.type = 'select';
                            } else {

                                //Detour here
                                newField.type = 'button-select';
                            }
                            break;
                    }

                    break;
                case 'select':
                    newField.type = 'select';
                    break;
                case 'wysiwyg':
                    newField.type = 'textarea';
                    break;
                default:
                    newField.type = fieldDefinition.directive || 'input';
                    break;
            }


            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //If there is custom attributes
            if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length) {
                newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = 'customAttr' + key;
                    results[customKey] = {
                        attribute: key
                    };

                    //Custom Key
                    templateOptions[customKey] = attr;

                    return results;
                }, {});
            }


            /////////////////////////////


            //Allow for multiple select
            if (newField.type == 'select') {
                if (!newField.ngModelAttrs) {
                    newField.ngModelAttrs = {};
                }

                if (fieldDefinition.maximum != 1) {
                    newField.ngModelAttrs.multiple = {
                        value: 'multiple'
                    };
                }
            }

            /////////////////////////////


            //Allow for multiple select
            if (newField.type == 'select') {
                if (!newField.ngModelAttrs) {
                    newField.ngModelAttrs = {};
                }

                if (fieldDefinition.maximum != 1) {
                    newField.ngModelAttrs.multiple = "true";
                }
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //What kind of data type, override for things like checkbox
            //if (fieldDefinition.type == 'boolean') {
            if (fieldDefinition.directive != 'custom' && fieldDefinition.directive != 'value') {
                switch (fieldDefinition.type) {
                    case 'boolean':

                        if (fieldDefinition.params && fieldDefinition.params.storeCopy) {
                            newField.type = 'terms';
                        } else {
                            newField.type = 'checkbox';
                        }

                        break;
                    case 'date':

                        if (newField.type != 'dob-select') {
                            newField.type = 'date-select';
                        }




                        break;
                    case 'number':
                    case 'float':
                    case 'integer':
                    case 'decimal':
                        templateOptions.type = 'input';
                        // templateOptions.step = 'any';

                        if (!newField.ngModelAttrs) {
                            newField.ngModelAttrs = {};
                        }

                        /////////////////////////////////////////////

                        newField.ngModelAttrs.customAttrpattern = {
                            attribute: 'pattern',
                        }

                        templateOptions.customAttrpattern = "[0-9.]*";

                        //Only do this if its an integer cos iOS SUCKS!
                        //iOS will show an alphabetical keyboard and allow bad entry of data anyway
                        //So no way to be smart here rught now
                        if (fieldDefinition.type == 'integer') {
                            // ////////console.log('Is integer');

                            templateOptions.type = 'number';
                            templateOptions.baseDefaultValue = 0;
                            templateOptions.customAttrpattern = "[0-9]*";
                            //Force numeric keyboard


                            newField.ngModelAttrs.customAttrinputmode = {
                                attribute: 'inputmode',
                            }

                            //Force numeric keyboard

                            templateOptions.customAttrinputmode = "numeric"


                            /////////////////////////////////////////////

                            // ////////console.log('SET NUMERICINPUT')

                            if (fieldDefinition.params) {
                                if (parseInt(fieldDefinition.params.maxValue) !== 0) {
                                    templateOptions.max = fieldDefinition.params.maxValue;
                                }

                                if (parseInt(fieldDefinition.params.minValue) !== 0) {
                                    templateOptions.min = fieldDefinition.params.minValue;
                                } else {
                                    templateOptions.min = 0;
                                }
                            }

                        }
                        break;
                }

            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Default Options

            if (fieldDefinition.maximum == 1) {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0];
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id;
                        }
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]);
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0];
                        }
                    }
                }
            } else {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {
                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences;
                        } else {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                return ref._id;
                            });
                        }
                    } else {
                        templateOptions.baseDefaultValue = [];
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                                return Number(val);
                            });
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues;
                        }
                    }
                }
            }


            /////////////////////////////

            //Append the template options
            newField.templateOptions = templateOptions;

            /////////////////////////////


            /////////////////////////////
            /////////////////////////////

            newField.validators = {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;

                    // console.log('VALID IS', $viewValue, $modelValue, scope, fieldDefinition)


                    if (!value) {
                        return true;
                    }



                    var valid = FluroValidate.validate(value, fieldDefinition);

                    if (!valid) {
                        // console.log('CHECKING VALID', value, valid, fieldDefinition)
                    }

                    return valid;
                }
            }

            ////////////////////////////////////////

            //If it's a date then we need to validate the date
            if (fieldDefinition.type == 'date') {


                //Validate the datr
                newField.validators.date = function($viewValue, $modelValue, scope) {

                    var value = $modelValue || $viewValue;
                    if (!value) {
                        if (fieldDefinition.minimum) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    var parsedDate = Date.parse(value);
                    var invalid = isNaN(parsedDate)

                    // console.log('PARSED DATE', parsedDate);

                    return !invalid;

                    // return validator.isDate(value);
                }
            }

            /////////////////////////////
            /////////////////////////////

            /**
            if (fieldDefinition.directive == 'upload') {
                newField.validators.uploadInProgress = function($viewValue, $modelValue, scope) {

                    var value = $modelValue || $viewValue;
                    var isArray = _.isArray(value);

                    if (isArray) {
                        var values = value;
                        var hasErrors = _.includes(values, 'error');
                        var hasProcessing = _.includes(values, 'processing');

                        if (hasErrors || hasProcessing) {
                            console.log('VALLLLLL', hasErrors, hasProcessing)
                            return false;
                        } else {
                            console.log('VAL GOOD');
                            return true;
                        }
                    } else {
                        return (value == 'error' || value == 'processing');
                    }
                }
            }
            /**/

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////




            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////


            if (fieldDefinition.directive == 'embedded') {
                newField.type = 'embedded';

                //Check if its an array or an object
                if (fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                    templateOptions.baseDefaultValue = {
                        data: {}
                    };
                } else {

                    var askCount = 0;

                    if (fieldDefinition.askCount) {
                        askCount = fieldDefinition.askCount;
                    }

                    //////////console.log('ASK COUNT PLEASE', askCount);

                    //////////////////////////////////////

                    if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                        askCount = fieldDefinition.minimum;
                    }

                    if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                        askCount = fieldDefinition.maximum;
                    }

                    //////////////////////////////////////

                    var initialArray = [];

                    //Fill with the asking amount of objects
                    if (askCount) {
                        _.times(askCount, function() {
                            initialArray.push({});
                        });
                    }

                    //////////console.log('initial array', initialArray);
                    //Now set the default value
                    templateOptions.baseDefaultValue = initialArray;
                }

                //////////////////////////////////////////

                //Create the new data object to store the fields
                newField.data = {
                    fields: [],
                    dataFields: [],
                    detailFields: [],
                    replicatedFields: [],
                    replicatedDataFields: [],
                    replicatedDetailFields: [],
                }

                //////////////////////////////////////////

                //Link to the definition of this nested object
                var fieldContainer = newField.data.fields;
                var dataFieldContainer = newField.data.dataFields;
                var detailFieldContainer = newField.data.detailFields;


                //////////////////////////////////////////

                //Loop through each sub field inside a group
                if (fieldDefinition.fields && fieldDefinition.fields.length) {
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }

                //////////////////////////////////////////

                var embeddedContactDetails = _.get(fieldDefinition, 'params.targetDetails');
                _.each(embeddedContactDetails, function(contactDetailName) {

                    var formID = $formScope.model._id;
                    var detailPromise = FluroContent.endpoint('defined/' + contactDetailName + '?form=' + formID).get().$promise;

                    detailPromise.then(function(detailSheetDefinition) {


                        var detailDataObject = {
                            type: 'group',
                            asObject: true,
                            minimum: 1,
                            maximum: 1,
                            key: 'data',
                            fields: detailSheetDefinition.fields,
                        }

                        ///////////////////////////////

                        var sheet = {
                            type: 'group',
                            asObject: true,
                            minimum: 1,
                            maximum: 1,
                            key: detailSheetDefinition.definitionName,
                            fields: [detailDataObject],
                        }



                        // detailFieldContainer[detailSheetDefinition.definitionName];
                        // if(!sheet) {
                        //     sheet = detailFieldContainer[detailSheetDefinition.definitionName] = [];
                        // }

                        // //Now loop through and all all the embedded definition fields
                        // if (detailSheetDefinition && detailSheetDefinition.fields && detailSheetDefinition.fields.length) {
                        //     var childFields = detailSheetDefinition.fields;

                        //     //Loop through each sub field inside a group
                        //     _.each(childFields, function(sub) {
                        //         addFieldDefinition(sheet.fields, sub);
                        //     })
                        // }

                        addFieldDefinition(detailFieldContainer, sheet);
                    });

                    $scope.promises.push(detailPromise);
                })


                //////////////////////////////////////////

                var promise = FluroContent.endpoint('defined/' + fieldDefinition.params.restrictType).get().$promise;

                promise.then(function(embeddedDefinition) {

                    //Now loop through and all all the embedded definition fields
                    if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                        var childFields = embeddedDefinition.fields;

                        //Exclude all specified fields
                        if (fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length) {
                            childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            });
                        }


                        ////////console.log('EXCLUSIONS', fieldDefinition.params.excludeKeys, childFields);

                        //Loop through each sub field inside a group
                        _.each(childFields, function(sub) {
                            addFieldDefinition(dataFieldContainer, sub);
                        })
                    }
                });

                //////////////////////////////////////////

                //Keep track of the promise
                $scope.promises.push(promise);

                //////////////////////////////////////////

                // //Need to keep it dynamic so we know when its done
                // newField.expressionProperties = {
                //     'templateOptions.embedded': function() {
                //         return promise;
                //     }
                // }



            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            if (fieldDefinition.type == 'group' && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {


                var fieldContainer;

                if (fieldDefinition.asObject) {

                    /*
                    newField = {
                        type: 'nested',
                        className: fieldDefinition.className,
                        data: {
                            fields: []
                        }
                    }
                    */
                    newField.type = 'nested';

                    //Check if its an array or an object
                    if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                        templateOptions.baseDefaultValue = {};
                    } else {

                        var askCount = 0;

                        if (fieldDefinition.askCount) {
                            askCount = fieldDefinition.askCount;
                        }

                        //////////////////////////////////////

                        if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                            askCount = fieldDefinition.minimum;
                        }

                        if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                            askCount = fieldDefinition.maximum;
                        }

                        //////////////////////////////////////

                        var initialArray = [];

                        //Fill with the asking amount of objects
                        if (askCount) {
                            _.times(askCount, function() {
                                initialArray.push({});
                            });
                        }

                        // ////////console.log('initial array', initialArray);
                        //Now set the default value
                        templateOptions.baseDefaultValue = initialArray;
                    }

                    newField.data = {
                        fields: [],
                        replicatedFields: [],
                        replicatedDataFields: [],
                        replicatedDetailFields: [],
                    }

                    //Link to the definition of this nested object
                    fieldContainer = newField.data.fields;

                } else {
                    //Start again
                    newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className,
                    }

                    //Link to the sub fields
                    fieldContainer = newField.fieldGroup;
                }

                //Loop through each sub field inside a group
                _.each(fieldDefinition.fields, function(sub) {
                    addFieldDefinition(fieldContainer, sub);
                });
            }

            /////////////////////////////

            //Check if there are any expressions added to this field

            //Add the hide expression if added through another method
            if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {

                if (!fieldDefinition.expressions) {
                    fieldDefinition.expressions = {};
                }
                fieldDefinition.expressions.hide = fieldDefinition.hideExpression;
                console.log('HIDE EXPRESSION', fieldDefinition.expressions);
            }

            if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {



                //////////////////////////////////////////

                //Get all expressions and join them together so we just listen once
                var allExpressions = _.reduce(fieldDefinition.expressions, function(memo, string) {

                    if(string && string.length) {
                        memo += '(' + string + ')';
                    }

                    return memo;
                }, '');



                /////////////////////////////////////////////////////


                //Now create a watcher
                newField.watcher = {
                    expression: function(field, scope) {


                        var watchScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                            date: new Date(),
                            matchInArray: matchInArray,

                            dateTools: DateTools, //DEPRECATED SHOULD NOT BE USED

                            //Basic Bits
                            getAge: getAge,
                            Date: Date,
                            Math: Math,
                            String: String,
                            Date: Date,
                            parseInt: parseInt,
                            parseFloat: parseFloat,
                            Boolean: Boolean,
                            Number: Number,
                        }



                        //Return the result
                        var val = $parse(allExpressions)(watchScope);


                        console.log('EXPRESSION CHANGED', allExpressions, val);
                        // debugLog('PARSE EXPRESSIONS', allExpressions, val);

                        return val;

                    },
                    listener: function(field, newValue, oldValue, scope, stopWatching) {

                        // debugLog('Expression Changed');

                        //Create a new scope object
                        var checkScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                            date: new Date(),
                            matchInArray: matchInArray,
                            dateTools: DateTools, //DEPRECATED SHOULD NOT BE USED

                            //Basic Bits
                            getAge: getAge,
                            Date: Date,
                            Math: Math,
                            String: String,
                            Date: Date,
                            parseInt: parseInt,
                            parseFloat: parseFloat,
                            Boolean: Boolean,
                            Number: Number,
                        }

                        //Loop through each expression that needs to be evaluated
                        _.each(fieldDefinition.expressions, function(expression, key) {

                            //Get the value
                            var retrievedValue = $parse(expression)(checkScope);



                            console.log('Expression changed', expression, retrievedValue);
                            debugLog('expression:', retrievedValue, checkScope);

                            //Get the field key
                            var fieldKey = field.key;

                            ///////////////////////////////////////

                            switch (key) {
                                case 'defaultValue':
                                    if (!field.formControl || !field.formControl.$dirty) {
                                        return scope.model[fieldKey] = retrievedValue;
                                    }
                                    break;
                                case 'value':
                                    return scope.model[fieldKey] = retrievedValue;
                                    break;
                                case 'required':
                                    return field.templateOptions.required = Boolean(retrievedValue);
                                    break;
                                case 'hide':
                                    return field.hide = Boolean(retrievedValue);
                                    break;
                                    // case 'label':
                                    //     if(retrievedValue) {
                                    //         var string = String(retrievedValue);
                                    //         return field.templateOptions.label = String(retrievedValue);
                                    //     }
                                    //     break;
                            }

                        });

                    }



                    ///////////////////////////////////////
                }
            }

            /////////////////////////////
            /**
                        if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {

                            

                             
                            newField.hideExpression = function($viewValue, $modelValue, scope) {

                                //Create a new scope object
                                var checkScope = {
                                    date: new Date(),
                                    model: scope.model,
                                    data: $scope.vm.model,
                                    interaction: $scope.vm.model,
                                    date:new Date(),
                                    matchInArray: matchInArray,
                                    getAge:getAge,
                                    Math:Math,
                                    Date:Date,
                                    dateTools: DateTools, //DEPRECATED SHOULD NOT BE USED
                                }

                                var parsedValue = $parse(fieldDefinition.hideExpression)(checkScope);

                                debugLog('Hide Expression', fieldDefinition.hideExpression, parsedValue);


                                // _.set(newField, 'templateOptions.hiddenByExpression', parsedValue);

                                console.log('hide expression parsed value', fieldDefinition.hideExpression, checkScope, parsedValue);
                                return Boolean(parsedValue);
                            }

                           


                        }
                         /**/

            /////////////////////////////

            if (!newField.fieldGroup) {
                //Create a copy of the default value
                newField.defaultValue = angular.copy(templateOptions.baseDefaultValue);
            }


            /////////////////////////////

            if (newField.type == 'pathlink') {
                return;
            }

            /////////////////////////////
            //Push our new field into the array
            array.push(newField);


        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Loop through each defined field and add it to our form
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        });

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details

        if (!$scope.model.paymentDetails) {
            $scope.model.paymentDetails = {};
        }

        var paymentSettings = $scope.model.paymentDetails;


        // //console.log('SCOPE MODEL', $scope.model);

        /////////////////////////////////////////////////////////////////

        //Credit Card Details
        if (paymentSettings.required || paymentSettings.allow) {

            //Setup the wrapper fields
            var paymentWrapperFields = [];
            var paymentCardFields = [];


            //console.log('PAYMENT SETTINGS REQUIRED', paymentSettings.required)

            // paymentWrapperFields.push({
            //     template: '<h4><i class="fa fa-credit-card"></i> Payment details</h4>'
            // });


            // ////////console.log('BEFORE SCOPE', $scope);

            if (paymentSettings.required) {



                debugLog('-- DEBUG -- Payment Settings Required')



                //Add the payment summary
                paymentWrapperFields.push({
                    templateUrl: 'fluro-interaction-form/payment/payment-summary.html',
                    controller: ['$scope', '$parse', function($scope, $parse) {


                        $scope.currencySymbol = '$';

                        switch (paymentSettings.currency) {
                            case 'gbp':
                                $scope.currencySymbol = "£";
                                break;

                        }

                        //Add the payment details to the scope
                        $scope.paymentDetails = paymentSettings;


                        //Start with the required amount
                        var requiredAmount = paymentSettings.amount;

                        //Store the calculatedAmount on the scope
                        $formScope.vm.model.calculatedTotal = requiredAmount;
                        $scope.calculatedTotal = requiredAmount;

                        /////////////////////////////////////////////////////


                        var watchString = '';


                        /////////////////////////////////////////////////////

                        //Add this to the scope so we can recalculate when things change and the watcher sees
                        //the function
                        $scope.matchInArray = matchInArray;

                        // function(array, key, value, operator) {

                        //     if(!operator) {
                        //         operator '=';
                        //     }

                        //     var matches = _.filter(array, function(entry) {
                        //         var retrievedValue = _.get(entry, key);

                        //         if (value === undefined) {
                        //             return retrievedValue;
                        //         } else {
                        //             return retrievedValue == value;
                        //         }
                        //     })

                        //     return matches;
                        // }

                        //For the watcher to see the 'data' object we need to add it to the scope too
                        $scope.data = $formScope.vm.model;

                        ////////////////////////////////////////
                        ////////////////////////////////////////

                        //Map each modifier to a property string and combine them all at once
                        var modelVariables = _.chain(paymentSettings.modifiers)
                            .map(function(paymentModifier) {

                                var string = '';

                                if (paymentModifier.condition && paymentModifier.condition.length) {
                                    string = '(' + paymentModifier.expression + ') + (' + paymentModifier.condition + ')';
                                } else {
                                    string = '(' + paymentModifier.expression + ')';
                                }
                                return string;
                            })
                            .flatten()
                            .compact()
                            .uniq()
                            .value();


                        if (modelVariables.length) {
                            watchString = modelVariables.join(' + "a" + ');
                        }

                        ////////////////////////////////////////

                        // console.log('WATCH STRING', watchString);

                        if (watchString.length) {


                            debugLog('Watching changes', watchString);
                            $scope.$watch(watchString, calculateTotal);
                        } else {

                            // debugLog('No watch string set', paymentSettings);

                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;
                            $scope.modifications = [];
                        }

                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////

                        function calculateTotal(watchString) {

                            console.log('Recalculate!!', watchString)
                            debugLog('Recalculate total', watchString);


                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;

                            var date = new Date();
                            $scope.date = date.getTime();

                            $scope.modifications = [];




                            /////////////////////////////////////////////////////


                            if (!paymentSettings.modifiers || !paymentSettings.modifiers.length) {
                                debugLog('No payment modifiers set');
                                return;
                            }

                            //Loop through each modifier
                            var modificationList = _.chain(paymentSettings.modifiers)
                                .map(function(modifier) {




                                    /////////////////////////////////////////////////////

                                    var context = {
                                        date: date,
                                        calculatedTotal: $scope.calculatedTotal,
                                        model: $scope.model,
                                        data: $scope.data,
                                        matchInArray: matchInArray,


                                        //Basic Bits
                                        getAge: getAge,
                                        Date: Date,
                                        Math: Math,
                                        String: String,
                                        Date: Date,
                                        parseInt: parseInt,
                                        parseFloat: parseFloat,
                                        Boolean: Boolean,
                                        Number: Number,
                                    }

                                    // console.log('PARSE ON CONTEXT');
                                    /////////////////////////////////////////////////////



                                    var parsedValue = $parse(modifier.expression)(context);
                                    parsedValue = Number(parsedValue);

                                    // console.log('ParsedValue', $scope, modifier.expression, parsedValue)

                                    if (isNaN(parsedValue)) {
                                        debugLog('Payment modifier error', modifier.title, parsedValue);
                                        //throw Error('Invalid or non-numeric pricing modifier ' + modifier.title);
                                        return;
                                    }

                                    /////////////////////////////////////////

                                    var parsedCondition = true;

                                    if (modifier.condition && String(modifier.condition).length) {
                                        parsedCondition = $parse(modifier.condition)(context);
                                    }

                                    /////////////////////////////////////////

                                    //If the condition returns false then just stop here and go to the next modifier
                                    if (!parsedCondition) {
                                        debugLog('inactive', modifier.title, modifier, $scope);
                                        return
                                    }

                                    /////////////////////////////////////////

                                    var currencySymbol = '$';

                                    switch (paymentSettings.currency) {
                                        case 'gbp':
                                            currencySymbol = "£";
                                            break;

                                    }

                                    /////////////////////////////////////////

                                    var operator = '';
                                    var operatingValue = currencySymbol + parseFloat(parsedValue / 100).toFixed(2);

                                    switch (modifier.operation) {
                                        case 'add':
                                            operator = '+';
                                            $scope.calculatedTotal = $scope.calculatedTotal + parsedValue;
                                            break;
                                        case 'subtract':
                                            operator = '-';
                                            $scope.calculatedTotal = $scope.calculatedTotal - parsedValue;
                                            break;
                                        case 'divide':
                                            operator = '/';
                                            operatingValue = parsedValue;
                                            $scope.calculatedTotal = $scope.calculatedTotal / parsedValue;
                                            break;
                                        case 'multiply':
                                            operator = 'x';
                                            operatingValue = parsedValue;
                                            $scope.calculatedTotal = $scope.calculatedTotal * parsedValue;
                                            break;
                                        case 'set':
                                            $scope.calculatedTotal = parsedValue;
                                            break;
                                    }

                                    var readableOperator = operator + ' ' + operatingValue;

                                    if (!parsedValue) {
                                        readableOperator = '';
                                    }

                                    var resultModified = {
                                        title: modifier.title,
                                        total: $scope.calculatedTotal,
                                        description: readableOperator,
                                        operation: modifier.operation,
                                    }

                                    return resultModified;

                                    // //Let the front end know that this modification was made
                                    // $scope.modifications.push({
                                    //     title: modifier.title,
                                    //     total: $scope.calculatedTotal,
                                    //     description: operator + ' ' + operatingValue,
                                    //     operation: modifier.operation,
                                    // });
                                })
                                .compact()
                                .value();


                            if (modificationList && modificationList.length) {


                                var lastSetIndex = _.findLastIndex(modificationList, function(mod) {
                                    return mod.operation == 'set';
                                });

                                //////////////////////////////////////

                                if (lastSetIndex == -1) {
                                    $scope.hasSetModification = false;
                                } else {
                                    $scope.hasSetModification = true;
                                    // console.log('HAS SET MODIFICATION');
                                }

                                //////////////////////////////////////

                                //A Set was used
                                if (lastSetIndex && lastSetIndex != -1) {
                                    $scope.modifications = _.slice(modificationList, lastSetIndex)
                                } else {

                                    $scope.modifications = modificationList

                                }

                            }



                            debugLog('Visible Modifications', modificationList);
                            // debugLog('CALCULATED TOTAL', $scope.calculatedTotal);

                            //If the modifiers change the price below 0 then change the total back to 0
                            if (!$scope.calculatedTotal || isNaN($scope.calculatedTotal) || $scope.calculatedTotal < 0) {
                                $scope.calculatedTotal = 0;
                            }



                            //Add the calculated total to the rootscope

                            $timeout(function() {
                                //console.log('Set calculated total', $scope.calculatedTotal)
                                $formScope.vm.model.calculatedTotal = $scope.calculatedTotal;
                            })

                        }

                        /**/
                    }],
                });
            } else {

                var amountDescription = 'Please enter an amount (' + String(paymentSettings.currency).toUpperCase() + ') ';


                //Limits of amount
                var minimum = paymentSettings.minAmount;
                var maximum = paymentSettings.maxAmount;
                var defaultAmount = paymentSettings.amount;

                ///////////////////////////////////////////

                var paymentErrorMessage = 'Invalid amount';

                ///////////////////////////////////////////

                if (minimum) {
                    minimum = (parseInt(minimum) / 100);
                    paymentErrorMessage = 'Amount must be a number at least ' + $filter('currency')(minimum, '$');

                    amountDescription += 'Enter at least ' + $filter('currency')(minimum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();
                }

                if (maximum) {
                    maximum = (parseInt(maximum) / 100);
                    paymentErrorMessage = 'Amount must be a number less than ' + $filter('currency')(maximum, '$');

                    amountDescription += 'Enter up to ' + $filter('currency')(maximum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();;
                }


                if (minimum && maximum) {
                    amountDescription = 'Enter a numeric amount between ' + $filter('currency')(minimum) + ' and  ' + $filter('currency')(maximum) + ' ' + String(paymentSettings.currency).toUpperCase();;
                    paymentErrorMessage = 'Amount must be a number between ' + $filter('currency')(minimum) + ' and ' + $filter('currency')(maximum);
                }

                ///////////////////////////////////////////

                //Add the option for putting in a custom amount of money
                var fieldConfig = {
                    key: '_paymentAmount',
                    type: 'currency',
                    //defaultValue: 'Cade Embery',
                    templateOptions: {
                        type: 'text',
                        label: 'Amount',
                        description: amountDescription,
                        placeholder: '0.00',
                        required: true,
                        errorMessage: paymentErrorMessage,
                        min: minimum,
                        max: maximum,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    data: {
                        customMaxLength: 8,
                        minimumAmount: minimum,
                        maximumAmount: maximum,
                    },
                };

                if (minimum) {
                    fieldConfig.defaultValue = minimum;
                }

                paymentWrapperFields.push({
                    'template': '<hr/><h3>Payment Details</h3>'
                });
                paymentWrapperFields.push(fieldConfig);
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Setup debug card details
            var defaultCardName;
            var defaultCardNumber;
            var defaultCardExpMonth;
            var defaultCardExpYear;
            var defaultCardCVN;

            //If testing mode
            if ($scope.debugMode) {
                defaultCardName = 'John Citizen';
                defaultCardNumber = '4242424242424242';
                defaultCardExpMonth = '05';
                defaultCardExpYear = '2020';
                defaultCardCVN = '123';
            }

            //////////////////////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardName',
                type: 'input',
                defaultValue: defaultCardName,
                templateOptions: {
                    type: 'text',
                    label: 'Name on Card',
                    placeholder: 'eg. (John Smith)',
                    // required: paymentSettings.required,
                    errorMessage: 'Card Name is required',
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return String(value).length > 0;
                    }
                },
            });




            /////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardNumber',
                type: 'input',
                defaultValue: defaultCardNumber,
                name: 'PaymentCardNumber',
                templateOptions: {
                    type: 'text',
                    label: 'Card Number',
                    placeholder: 'Card Number (No dashes or spaces)',
                    // required: paymentSettings.required,
                    errorMessage: 'Invalid Card Number',
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {

                        /////////////////////////////////////////////
                        var luhnChk = function(a) {
                            return function(c) {

                                if (!c) {
                                    return false;
                                }
                                for (var l = c.length, b = 1, s = 0, v; l;) v = parseInt(c.charAt(--l), 10), s += (b ^= 1) ? a[v] : v;
                                return s && 0 === s % 10
                            }
                        }([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]);

                        /////////////////////////////////////////////

                        var value = $modelValue || $viewValue;
                        var valid = luhnChk(value) && String(value).length > 15;
                        return valid;
                    }
                },
            });

            paymentCardFields.push({
                className: 'row clearfix',
                fieldGroup: [{
                    key: '_paymentCardExpMonth',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpMonth,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Month',
                        placeholder: 'MM',
                        // required: paymentSettings.required,
                        errorMessage: 'Invalid Card Expiry Month',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return String(value).length == 2;

                        }
                    },
                }, {
                    key: '_paymentCardExpYear',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpYear,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Year',
                        placeholder: 'YYYY',
                        // required: paymentSettings.required,
                        errorMessage: 'Invalid Card Expiry Year',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return String(value).length == 4;

                        }
                    },
                }, {
                    key: '_paymentCardCVN',
                    className: "col-xs-4 col-sm-2",
                    type: 'input',
                    defaultValue: defaultCardCVN,
                    templateOptions: {
                        type: 'text',
                        label: 'CVN',
                        placeholder: 'CVN',
                        // required: paymentSettings.required,
                        errorMessage: 'Invalid Card CVN',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return String(value).length > 0;

                        }
                    },
                }]
            });



            //////////////////////////////////////////////////////////

            //If the form is set to allow anonymous or has been set to ask for a specific receipt
            //email then prompt the submitter to type it in
            if (interactionFormSettings.allowAnonymous || interactionFormSettings.askReceipt) {
                paymentCardFields.push({
                    key: '_paymentEmail',
                    type: 'input',
                    templateOptions: {
                        type: 'email',
                        label: 'Receipt Email Address',
                        placeholder: 'Enter an email address for transaction receipt',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                });
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Create the credit card field group
            var cardDetailsField = {
                className: "payment-details",
                key: '_paymentCardDetails',
                fieldGroup: paymentCardFields,
                hideExpression: function($viewValue, $modelValue, scope) {

                    // console.log('Recalculate hide expression');

                    //If the calculated total is exactly 0
                    if ($formScope.vm.model.calculatedTotal === 0) {
                        debugLog('HIDE PAYMENT NO AMOUNT DUE', $formScope.vm.model.calculatedTotal)
                        return true;
                    }

                    if (paymentSettings.allowAlternativePayments && $formScope.vm.model._paymentMethod != 'card') {
                        debugLog('HIDE PAYMENT NOT CARD', $formScope.vm.model._paymentMethod)
                        return true;
                    }
                }
            }

            //////////////////////////////////////////////////////////

            // console.log('SHOW PAYMENT METHODS', paymentSettings.allowAlternativePayments && paymentSettings.paymentMethods && paymentSettings.paymentMethods.length)

            var alternativeMethodsAvailable = _.get(paymentSettings, 'paymentMethods.length');

            if (paymentSettings.allowAlternativePayments && alternativeMethodsAvailable) {


                //Create a method selection widget
                var methodSelection = {
                    className: "payment-method-select",
                    //defaultValue:{},
                    // key:'_paymentMethod',
                    // fieldGroup:cardDetailsField,
                    data: {
                        fields: [cardDetailsField],
                        settings: paymentSettings,
                    },
                    controller: ['$scope', function($scope) {

                        //Payment Settings on scope
                        $scope.settings = paymentSettings;

                        //Options
                        $scope.methodOptions = _.map(paymentSettings.paymentMethods, function(method) {
                            return method;
                        });

                        //Add card at the start
                        $scope.methodOptions.unshift({
                            title: 'Pay with Card',
                            key: 'card',
                        });

                        ////////////////////////////////////////

                        if (!$scope.model._paymentMethod) {
                            console.log('NO PAYMENT METHOD SO SET TO CARD')
                            $scope.model._paymentMethod = 'card';
                        }

                        //Select the first method by default
                        $scope.selected = {
                            method: $scope.methodOptions[0]
                        };

                        $scope.selectMethod = function(method) {

                            console.log('SELECT METHOD', method);
                            $scope.settings.showOptions = false;
                            $scope.selected.method = method;
                            $scope.model._paymentMethod = method.key;

                            // ngModel.$validate();
                            // console.log('SET METHOD', method, $scope.options.runExpressions())
                            // $scope.fc.$setTouched();
                            // $scope.fc.$setValidity('required', validRequired);


                        }
                    }],
                    templateUrl: 'fluro-interaction-form/payment/payment-method.html',
                    hideExpression: function($viewValue, $modelValue, scope) {

                        //If the calculated total is exactly 0
                        if ($formScope.vm.model.calculatedTotal === 0) {
                            debugLog('HIDE BECAUSE CALCULATED TOTAL IS 0');
                            return true;
                        }
                    }
                };

                paymentWrapperFields.push(methodSelection);

            } else {
                //Push the card details
                paymentWrapperFields.push(cardDetailsField);
            }

            //////////////////////////////////////////////////////////

            // $scope.vm.modelFields = $scope.vm.modelFields.concat(paymentWrapperFields);

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            $scope.vm.modelFields.push({
                fieldGroup: paymentWrapperFields,

            });
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Wait for all async promises to resolve

        if (!$scope.promises.length) {
            // console.log('NO PROMISES');
            $scope.promisesResolved = true;
        } else {

            $scope.promisesResolved = false;

            $q.all($scope.promises).then(function() {
                // console.log('Promises have been resolved');
                $scope.promisesResolved = true;

                //updateErrorList();
                //Update the error list
                // $scope.errorList = getAllErrorFields($scope.vm.modelFields);
                // ////////console.log('All promises resolved', $scope.errorList);

                // _.each($scope.errorList, function(field) {
                //     ////////console.log('FIELD', field.templateOptions.label, field.formControl)
                // })

            });
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function getAllErrorFields(array) {


        // field.formControl.$invalid


        return _.chain(array)
            .map(function(field) {
                if (field.fieldGroup && field.fieldGroup.length) {
                    //Get the error fields in a field group
                    return getAllErrorFields(field.fieldGroup);

                } else if (field.data && ((field.data.fields && field.data.fields.length) || (field.data.dataFields && field.data.dataFields) || (field.data.detailFields && field.data.detailFields) || (field.data.replicatedFields && field.data.replicatedFields))) {

                    var combined = [];

                    // field.data.detailFields, //TODO INCLUDE THIS
                    combined = combined.concat(field.data.fields, field.data.dataFields, field.data.detailFields, field.data.replicatedFields);
                    combined = _.compact(combined);

                    //Get the error fields in the combined extra data
                    return getAllErrorFields(combined);
                } else {
                    return field;
                }
            })
            .flatten()
            .value();
    }



    /////////////////////////////////////////////////////////////////


    function submitInteraction() {

        console.log('FORM -> Submit Interaction');

        //Sending
        $scope.vm.state = 'sending';
        // console.log('state.sending');

        var interactionKey = $scope.model.definitionName;
        var interactionDetails = angular.copy($scope.vm.model);


        //console.log('____________________________________');
        //console.log('SUBMIT', interactionKey, interactionDetails)

        /////////////////////////////////////////////////////////

        //Asking for Payment
        var requiresPayment;
        var allowsPayment;

        /////////////////////////////////////////////////////////


        var paymentConfiguration = $scope.model.paymentDetails;


        //Check if we have supplied payment details
        if (paymentConfiguration) {
            requiresPayment = paymentConfiguration.required;
            allowsPayment = paymentConfiguration.allow;
        }

        // ////////console.log('PAYMENT CONFIG', paymentConfiguration)
        // ////////console.log('PAYMENT ALLOWED REQUIRED', requiresPayment, allowsPayment)

        /////////////////////////////////////////////////////////

        //Check if we need a payment
        if (requiresPayment || allowsPayment) {

            console.log('FORM -> Requires Payment', requiresPayment, allowsPayment)

            ////////////////////////////////////

            var paymentDetails = {};

            ////////////////////////////////////

            //Check if we can use alternative payment methods
            if (paymentConfiguration.allowAlternativePayments && paymentConfiguration.paymentMethods) {

                var selectedMethod = interactionDetails._paymentMethod;
                //If the user chose an alternative payment
                if (selectedMethod && selectedMethod != 'card') {

                    //Mark which method we are using as an alternative method
                    paymentDetails.method = selectedMethod;

                    console.log('FORM -> Alternative Payment Method')
                    //////console.log('Have selected payment method other than card')
                    //Skip straight through to process the request
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Only stop here if we REQUIRE payment
            if (requiresPayment) {
                ////////console.log('TESTING REQUIRES PAYMENT', $formScope);
                //If payment modifiers have removed the need for charging a payment
                if (!$formScope.vm.model.calculatedTotal) {

                    console.log('FORM -> No Payment Total Due')
                    //////console.log('Calculated total is ', 0);
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Get the payment integration 
            var paymentIntegration = $scope.integration;



            /////////////////////////////////////////////////////////

            if (!paymentIntegration || !paymentIntegration.publicDetails) {

                // if (paymentConfiguration.required) {
                //     //////console.log('No payment integration was supplied for this interaction but payment is required');
                // } else {
                //     //////console.log('No payment integration was supplied for this interaction but payments are set to be allowed');
                // }

                alert('This form has not been configured properly. And no payment gateway was defined. Please notify the administrator of this website immediately.')
                $scope.vm.state = 'ready';
                // console.log('state.ready');
                return;
            }

            /////////////////////////////////////////////////////////


            //Ensure we tell the server which integration to use to process payment
            paymentDetails.integration = paymentIntegration._id;


            //Get the credit card details
            var cardDetails = interactionDetails._paymentCardDetails;
            // console.log('Credit Card Details are', cardDetails);

            /////////////////////////////////////////////////////////

            //Check if we are running in a sandbox environment
            var SANDBOX_ENV = _.get(paymentIntegration, 'publicDetails.sandbox') || _.get(window, 'applicationData.sandbox');

            //Add sandbox to the interaction submission
            paymentDetails.sandbox = SANDBOX_ENV;

            /////////////////////////////////////////////////////////

            console.log('CHECK PAYMENT MODULE', paymentIntegration);
            //Now get the required details for making the transaction
            switch (paymentIntegration.module) {
                case 'eway':

                    //console.log('EWAY PAYMENT')

                    if (!window.eCrypt) {
                        console.log('ERROR: Eway is selected for payment but the eCrypt script has not been included in this application visit https://eway.io/api-v3/#encrypt-function for more information');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    //Get encrypted token from eWay
                    //var liveUrl = 'https://api.ewaypayments.com/DirectPayment.json';
                    //var sandboxUrl = 'https://api.sandbox.ewaypayments.com/DirectPayment.json';

                    /////////////////////////////////////////////

                    //Get the Public Encryption Key
                    var key = paymentIntegration.publicDetails.publicKey;

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var ewayCardDetails = {};
                    ewayCardDetails.name = cardDetails._paymentCardName;
                    ewayCardDetails.number = eCrypt.encryptValue(cardDetails._paymentCardNumber, key);
                    ewayCardDetails.cvc = eCrypt.encryptValue(cardDetails._paymentCardCVN, key);

                    var expiryMonth = String(cardDetails._paymentCardExpMonth);
                    var expiryYear = String(cardDetails._paymentCardExpYear);

                    if (expiryMonth.length < 1) {
                        expiryMonth = '0' + expiryMonth;
                    }
                    ewayCardDetails.exp_month = expiryMonth;
                    ewayCardDetails.exp_year = expiryYear.slice(-2);

                    //Send encrypted details to the server
                    paymentDetails.details = ewayCardDetails;

                    //Process the request
                    return processRequest();

                    break;

                case 'tithely':


                    //console.log('STRIPE PAYMENT')

                    if (!window.Tithely) {
                        console.log('ERROR: Tithely is selected for payment but the Tithly API has not been included in this application');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    // console.log('PAYMENT INTEGRATION', paymentIntegration);

                    // //Get encrypted token from Stripe
                    // var liveKey = paymentIntegration.publicDetails.publicKey;
                    // var sandboxKey = paymentIntegration.publicDetails.publicKey;

                    // /////////////////////////////////////////////

                    // if (paymentIntegration.publicDetails.sandbox) {
                    //     key = sandboxKey;
                    // } else {
                    //     key = liveKey;
                    // }

                    // /////////////////////////////////////////////

                    // //Set the stripe key

                    // var tithely = new Tithely(key);

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var tithelyCardDetails = {};
                    tithelyCardDetails.card_name = cardDetails._paymentCardName;
                    tithelyCardDetails.card_number = cardDetails._paymentCardNumber;
                    tithelyCardDetails.card_cvc = cardDetails._paymentCardCVN;
                    tithelyCardDetails.card_expiry_month = cardDetails._paymentCardExpMonth;
                    tithelyCardDetails.card_expiry_year = cardDetails._paymentCardExpYear;

                    /////////////////////////////////////////////


                    //console.log('Tithely card', tithelyCardDetails);
                    /////////////////////////////////////////////

                    // Tokenize card
                    tithelyController.add_payment_method(tithelyCardDetails, tokenCreated);

                    /////////////////////////////////////////////

                    function tokenCreated(status, response) {
                        $timeout(function() {
                            if (response.error) {
                                // Error handling here
                                console.log('Tithely token error', response);
                                $scope.processErrorMessages = [response.error.message];
                                $scope.vm.state = 'error';
                                // console.warn(response.error.message);
                            } else {

                                console.log('Got tithely token!', response);
                                paymentDetails.details = response;
                                return processRequest();
                            }
                        })
                    }
                    break;
                case 'stripe':




                    if (!window.Stripe) {
                        console.log('ERROR: Stripe is selected for payment but the Stripe API has not been included in this application');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    console.log('FORM -> Send Request to Stripe');

                    //Get encrypted token from Stripe
                    var liveKey = paymentIntegration.publicDetails.livePublicKey;
                    var sandboxKey = paymentIntegration.publicDetails.testPublicKey;

                    //Use the Live key by default
                    var key = liveKey;

                    /////////////////////////////////////////////


                    if (SANDBOX_ENV) {
                        key = sandboxKey;
                        console.log('FORM -> Tokenizing using test key')
                    } else {
                        console.log('FORM -> Tokenizing using live key')
                    }

                    /////////////////////////////////////////////

                    //Set the stripe key
                    Stripe.setPublishableKey(key);

                    /////////////////////////////////////////////


                    //Get the card details from our form
                    var stripeCardDetails = {};
                    stripeCardDetails.name = cardDetails._paymentCardName;
                    stripeCardDetails.number = cardDetails._paymentCardNumber;
                    stripeCardDetails.cvc = cardDetails._paymentCardCVN;
                    stripeCardDetails.exp_month = cardDetails._paymentCardExpMonth;
                    stripeCardDetails.exp_year = cardDetails._paymentCardExpYear;

                    /////////////////////////////////////////////

                    Stripe.card.createToken(stripeCardDetails, function(status, response) {

                        $timeout(function() {
                            // console.log('TESTING', status, response);
                            if (response.error) {
                                //Error creating token
                                // Notifications.error(response.error);
                                console.log('FORM -> Stripe token error', response);
                                $scope.processErrorMessages = [response.error.message];




                                $scope.vm.state = 'error';
                                // console.log('state.error');


                            } else {
                                //Include the payment details
                                console.log('FORM -> Stripe tokenized')
                                paymentDetails.details = response;
                                return processRequest();
                            }

                        })
                    });
                    break;
                default:
                    console.log('FORM -> NO VALID PAYMENT MODULE PROVIDED')
                    break;
            }
        } else {
            console.log('FORM -> No payment required')
            return processRequest();
        }


        ///////////////////////////////////////////////////////////////////////

        function processRequest() {
            console.log('FORM -> Process Request')

            /////////////////////////////////////////////////////////


            //Add the transaction email  details
            var receiptEmail = _.get(interactionDetails, '_paymentCardDetails._paymentEmail');

            if (receiptEmail && receiptEmail.length) {
                console.log('FORM -> Setting payment receipt email address', receiptEmail);
                paymentDetails.email = receiptEmail;
            }

            /////////////////////////////////////////////////////////

            //Delete payment details (we don't send these to fluro)
            delete interactionDetails._paymentCardDetails;
            // console.log('Delete payment details', interactionDetails)

            /////////////////////////////////////////////////////////

            //Log the request
            //////////console.log('Process request', interactionKey, interactionDetails, paymentDetails);

            /////////////////////////////////////////////////////////

            //Allow user specified payment
            if (interactionDetails._paymentAmount) {
                paymentDetails.amount = (parseFloat(interactionDetails._paymentAmount) * 100);
            }

            /////////////////////////////////////////////////////////

            //Explicitly pass through the account we want to use the definition for
            var definitionID = $scope.model._id;
            if (definitionID._id) {
                //ensure its just a simple string and not an object
                definitionID = definitionID._id;
            }

            /////////////////////////////////////////////////////////

            //Query string parameters
            var params = {
                definition: definitionID,
            }
            // console.log('PAYMENT DETAILS', paymentDetails);

            if ($scope.linkedProcess) {
                params.process = $scope.linkedProcess;
            }
            // return submissionFail('On purpose');

            //Attempt to send information to interact endpoint

            // console.log('SEND THE THING', interactionDetails);
            // return submissionFail('On purpose');
            
            var request = FluroInteraction.interact($scope.model.title, interactionKey, interactionDetails, paymentDetails, $scope.linkedEvent, params);


            //////////////////////////////////

            //When the promise results fire the callbacks
            request.then(submissionSuccess, submissionFail)

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////

            function submissionSuccess(res) {

                console.log('FORM -> Submission Success');
                /**
                //TESTING
                $scope.vm.state = 'ready';
                return ////////console.log('RES TEST', res);
                /**/

                ///////////////////////////////////

                if ($scope.callback) {
                    $scope.callback(res);
                }

                ///////////////////////////////////

                //Reset
                if ($scope.vm.defaultModel) {
                    $scope.vm.model = angular.copy($scope.vm.defaultModel);
                } else {
                    $scope.vm.model = {};
                }


                $scope.vm.modelForm.$setPristine();
                $scope.vm.options.resetModel();

                //Reset the form scope
                $formScope = $scope;

                // $scope.vm.model = {}
                // $scope.vm.modelForm.$setPristine();
                // $scope.vm.options.resetModel();

                //Response from server incase we want to use it on the thank you page
                $scope.response = res;

                //Change state
                $scope.vm.state = 'complete';
                // console.log('state.complete');
            }

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////


            function submissionFail(res) {

                console.log('FORM -> Submission Failed', res);
                ////////console.log('Interaction Failed', res);
                // Notifications.error(res.data);

                $scope.vm.state = 'error';
                // console.log('state.error');

                if (!res.data) {
                    return $scope.processErrorMessages = ['Error: ' + res];
                }

                if (res.data.error) {
                    if (res.data.error.message) {
                        return $scope.processErrorMessages = [res.error.message];
                    } else {
                        return $scope.processErrorMessages = [res.error];
                    }
                }

                if (res.data.errors) {
                    return $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                        return error.message;
                    });
                }

                if (_.isArray(res.data)) {
                    return $scope.processErrorMessages = res.data;
                } else {
                    $scope.processErrorMessages = [res.data];
                }



                //$scope.vm.state = 'ready';
            }


        }
    }

});