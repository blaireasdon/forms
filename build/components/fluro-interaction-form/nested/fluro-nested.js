/**/
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'nested',
        templateUrl: 'fluro-interaction-form/nested/fluro-nested.html',
        controller: 'FluroInteractionNestedController',
    });

});

//////////////////////////////////////////////////////////

app.controller('FluroInteractionNestedController', function($scope, $timeout) {


    //Definition
    var def = $scope.to.definition;

    ////////////////////////////////////

    var minimum = def.minimum;
    var maximum = def.maximum;


    ////////////////////////////////////

    $scope.$watch('model[options.key]', function(model) {
        if (!model) {
            //console.log('Reset Model cos no value!')
            resetDefaultValue();
        }
    });

    ////////////////////////////////////


    function resetDefaultValue() {

        var defaultValue = angular.copy($scope.to.baseDefaultValue);
        if(!$scope.model) {
            // console.log('NO RESET Reset Model Values', $scope.options.key, defaultValue);
            return;
        }
        $scope.model[$scope.options.key] = defaultValue;
    }

    ////////////////////////////////////

    //Listen for a reset event
    $scope.$on('form-reset', resetDefaultValue);


    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }


    ////////////////////////////////////

    $scope.addAnother = function() {

        console.log('Add another')
        $scope.model[$scope.options.key].push({id:guid()});
    }


    $scope.removeEntry = function($index) {

        var modelEntries = $scope.model[$scope.options.key];
        // var fieldEntries =  _.get($scope.options, 'data.fields');
        var replicatedEntries =  _.get($scope.options, 'data.replicatedFields');
        var replicatedDataEntries =  _.get($scope.options, 'data.replicatedDataFields');
        var replicatedDetailEntries =  _.get($scope.options, 'data.replicatedDetailFields');

        

        // delete $scope.keepTrack[uuid];


        

        modelEntries.splice($index, 1);
        // fieldEntries.splice($index, 1);
        replicatedEntries.splice($index, 1);
        replicatedDataEntries.splice($index, 1);
        replicatedDetailEntries.splice($index, 1);


        console.log('REMOVE', $index, replicatedEntries);
    }

    ////////////////////////////////////

    $scope.canRemove = function() {
        if (minimum) {
            if ($scope.model[$scope.options.key].length > minimum) {
                return true;
            }
        } else {
            return true;
        }
    }

    ////////////////////////////////////

    $scope.canAdd = function() {
        if (maximum) {
            if ($scope.model[$scope.options.key].length < maximum) {
                return true;
            }
        } else {
            return true;
        }
    }


    $scope.copyFields = function() {

        var copiedFields = angular.copy($scope.options.data.fields);
        $scope.options.data.replicatedFields.push(copiedFields);

        return copiedFields;
    }

    $scope.copyDataFields = function() {
        var copiedFields = angular.copy($scope.options.data.dataFields);
        $scope.options.data.replicatedDataFields.push(copiedFields);
        return copiedFields;
    }

    $scope.copyDetailFields = function() {
        var copiedFields = angular.copy($scope.options.data.detailFields);
        $scope.options.data.replicatedDetailFields.push(copiedFields);
        return copiedFields;
    }
});
/**/