app.run(function(formlyConfig, $templateCache) {

formlyConfig.setType({
        name: 'upload',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/upload/upload.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            },
            // validators: {
            //     validInput: {
            //         expression: function($viewValue, $modelValue, scope) {

            //             return false;
            //         }
            //     }
            // },
        },
        
        controller: 'FluroInteractionFormUploadController',
    });

});



app.controller('FluroInteractionFormUploadController', function($scope, $http, FluroValidate) {

    $scope.ctrl = {
        fileArray: [],
    };

    /////////////////////////////////////////////////////////////////////////

    var to = $scope.to;
    var opts = $scope.options;

    /////////////////////////////////////////////////////////////////////////

    //Get the definition
    var definition = $scope.to.definition;

    //Minimum and maximum
    var minimum = definition.minimum;
    var maximum = definition.maximum;

    if (!minimum) {
        minimum = 0;
    }

    if (!maximum) {
        maximim = 0;
    }

    $scope.multiple = (maximum != 1);

    //////////////////////////////

    var definition = _.get($scope, 'to.definition');

    /////////////////////////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.ctrl.fileArray, item);

        if (!$scope.ctrl.fileArray || !$scope.ctrl.fileArray.length) {
            var fileElement = angular.element('#' + $scope.options.id + ' input');
            angular.element(fileElement).val(null);
        }

        $scope.ctrl.refresh();
    }

    //////////////////////////////

    //Helper function to refresh and check validity
    $scope.ctrl.refresh = function() {

        /////////////////////////////////////

        var maximum = definition.maximum;

        /////////////////////////////////////

        if (maximum == 1) {


            if ($scope.ctrl.fileArray.length) {

                var firstFile = $scope.ctrl.fileArray[0];
                if (firstFile.state == 'complete') {
                    $scope.model[$scope.options.key] = firstFile.attachmentID;
                } else {
                    $scope.model[$scope.options.key] = firstFile.state;
                }
            } else {
                $scope.model[$scope.options.key] = null;
            }
        } else {

            $scope.model[$scope.options.key] = _.map($scope.ctrl.fileArray, function(fileItem) {

                if (fileItem.state == 'complete') {
                    return fileItem.attachmentID;
                }
                return fileItem.state;
            })
        }


        if ($scope.fc) {
            $scope.fc.$setTouched();
        }

        checkValidity();
    }

    /////////////////////////////////////////////////////////////////////////

    if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
        $scope.$watch(function() {
            return $scope.to.required;
        }, function(newValue) {
            checkValidity();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    if ($scope.to.required) {
        var unwatchFormControl = $scope.$watch('fc', function(newValue) {
            if (!newValue) {
                return;
            }
            checkValidity();
            unwatchFormControl();
        });
    }
    
    //////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////

    function checkValidity() {

        console.log('CHECK VALIDTIY!')
        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        


        //Check if multiple
        if ($scope.multiple) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        // ///////////////////////////////////////////////////

        // var values = $scope.model[opts.key];
        // var hasErrors = _.includes(values, 'error');
        // var hasProcessing = _.includes(values, 'processing');

        // if (hasErrors || hasProcessing) {
        //     validInput = false;
        // }

        ///////////////////////////////////////////////////

        // console.log('CHECK VALID HERE?', $scope.to.required, validInput);


        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }



        /**
    function checkValidity() {



        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        /////////////////////////////////////

        var values = $scope.model[$scope.options.key];

        var hasErrors = _.some(values, 'error');
        var hasProcessing = _.some(values, 'processing');

        if (hasErrors || hasProcessing) {
            validInput = false;
        }

        /////////////////////////////////////

        //Check if multiple
        if (definition.maximum != 1) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        /////////////////////////////////////

        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }
    /**/

    //////////////////////////////

    $scope.filesize = function(fileSizeInBytes) {
        var i = -1;
        var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
    };
});

/////////////////////////////////////

app.directive("fluroFileInput", function(Fluro, $http) {
    return {
        scope: {
            model: "=fileArray",
            definition: '=',
            callback: '=',
        },
        link: function($scope, $element, $attrs) {



            if (!$scope.model) {
                $scope.model = [];
            }

            ///////////////////////////////////////

            $element.bind('change', filesSelected)

            ///////////////////////////////////////

            function filesSelected(changeEvent) {



                //Get files that were selected
                var files = changeEvent.target.files;
                var realmID = _.get($scope.definition, 'params.realm');


                if(realmID) {


                    if(_.isArray(realmID)) {
                        realmID = realmID[0];
                    }

                    if(realmID._id) {
                        realmID = realmID._id;
                    }
                }
                
                console.log('FILE SELECTED', realmID);

                var uploadURL = Fluro.apiURL + '/file/attach/' + realmID;

                ///////////////////////////////

                // $scope.model = _.map(files, function(file) {
                //     return {
                //         lastModified: file.lastModified,
                //         lastModifiedDate: file.lastModifiedDate,
                //         name: file.name,
                //         size: file.size,
                //         type: file.type,
                //     }

                // });


                // if (datas.length === files.length) {
                //         scope.$apply(function() {
                //             scope.fileread = datas;
                //         });
                //     }

                ///////////////////////////////

                $scope.model = _.map(files, function(file) {

                    var output = {}

                    ////////////////////////////////

                    output.state = 'processing';

                    ////////////////////////////////

                    //Create the form data
                    var formData = new FormData();
                    formData.append('file', file);

                    ////////////////////////////////

                    var request = $http({
                        url: uploadURL,
                        method: "POST",
                        data: formData,
                        headers: {
                            'Content-Type': undefined
                        },
                        uploadEventHandlers: {
                            progress: function(e) {
                                $scope.callback();
                                if (e.lengthComputable) {
                                    output.progress = Math.round((e.loaded / e.total) * 100);
                                }
                            }
                        },
                    });

                    ////////////////////////////////

                    output.file = file;
                    output.request = request;

                    ////////////////////////////////

                    request.then(function(res) {
                        output.state = 'complete';
                        output.attachmentID = res.data._id;
                        $scope.callback();
                    }, function(err) {
                        output.state = 'error';

                        $scope.callback();
                    })

                    ////////////////////////////////

                    return output;
                })

                ///////////////////////////////

                $scope.callback();

            }

            ///////////////////////////////////////


            /**
            element.bind("change", function(changeEvent) {

                // console.log('FILE', changeEvent);

                var readers = [];
                var files = changeEvent.target.files;
                var datas = [];

                /////////////////////////////////////////

                if (!files.length) {
                    scope.$apply(function() {
                        scope.fileread = [];
                    });
                    return;
                }


                for (var i = 0; i < files.length; i++) {
                    readers[i] = new FileReader();
                    readers[i].index = i;

                    readers[i].onload = function(loadEvent) {

                        var index = loadEvent.target.index;

                        datas.push({
                            lastModified: files[index].lastModified,
                            lastModifiedDate: files[index].lastModifiedDate,
                            name: files[index].name,
                            size: files[index].size,
                            type: files[index].type,
                            data: loadEvent.target.result
                        });

                        if (datas.length === files.length) {
                            scope.$apply(function() {
                                scope.fileread = datas;
                            });
                        }

                    };
                    readers[i].readAsDataURL(files[i]);
                }
            });
            /**/

        }
    }
});