app.directive('realmFolders', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '@ngType',
            definition: '@?ngDefinition',
        },
        templateUrl: 'admin-realm-folders/admin-realm-folders.html',
        controller: 'RealmFoldersController',
    };
});




app.directive('realmFolderItem', function($compile, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            realm: '=ngModel',
            selection: '=ngSelection',
            searchTerms: '=ngSearchTerms',
        },
        templateUrl: 'admin-realm-folders/admin-realm-folders-item.html',
        link: function($scope, $element, $attrs) {



            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    if (_.isObject(i)) {
                        return i._id == realm._id;
                    } else {
                        return i == realm._id;
                    }
                });
            }

            $scope.toggle = function(realm) {

                var matches = _.filter($scope.selection, function(existingRealmID) {

                    var searchRealmID = realm._id;
                    if (existingRealmID._id) {
                        existingRealmID = existingRealmID._id;
                    }

                    return (searchRealmID == existingRealmID);
                })

                ////////////////////////////////

                if (matches.length) {

                    _.each(matches, function(match) {
                        _.pull($scope.selection, match);
                    })
                } else {
                    $scope.selection.push(realm);
                }

            }

            ///////////////////////////////////////////////////

            function hasActiveChild(realm) {

                var active = $scope.contains(realm);

                if (active) {
                    return true;
                }

                if (realm.children && realm.children.length) {
                    return _.some(realm.children, hasActiveChild);
                }
            }

            ///////////////////////////////////////////////////

            $scope.isExpanded = function(realm) {
                if (realm.expanded) {
                    return realm.expanded;
                }

                return $scope.activeTrail(realm);
            }

            ///////////////////////////////////////////////////

            $scope.activeTrail = function(realm) {
                if (!realm.children) {
                    return;
                }

                return _.some(realm.children, hasActiveChild);
            }



            var template = '<realm-folder-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:' + "'title'" + ' track by subRealm._id"></realm-folder-item>';

            var newElement = angular.element(template);
            $compile(newElement)($scope);
            $element.find('.realm-select-children').append(newElement);
        }
    };
});



app.controller('RealmFoldersController', function($scope, $filter, $rootScope, FluroContent, FluroAccess) {

    $scope.settings = {
        show: true,
    };

    ////////////////////////////////////////////////////

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    ////////////////////////////////////////////////////

    if (!$scope.definition || !$scope.definition.length) {
        $scope.definition = $scope.type;
    }

    ////////////////////////////////////////////////////


    // if($rootScope.user.accountType == 'administrator') {
    //     FluroContent.resource('realm').query().$promise.then(function(res) {
    //         $scope.realms = res;
    //     }, function(err) {
    //         console.log('Realm Select ERROR', err);
    //     });
    // } else {

    //     //Now we get the realms from FluroAccess
    //     $scope.realms =  FluroAccess.retrieveSelectableRealms('create', $scope.definition, $scope.type);
    // }

    // //////////////////////////////////


    $scope.tree = $rootScope.user.permissionSets;
    // //Now we get the realm tree from FluroAccess service
    // FluroAccess.retrieveSelectableRealms('create', $scope.definition, $scope.type)
    //     .then(function(realmTree) {

    //         //Add the tree to the scope
    //         $scope.tree = realmTree;

    //         ////////////////////////////////////////////////////////

    //         //If only one realm is available select it by default
    //         if (realmTree.length == 1 && !$scope.model.length) {
    //             //If there are no children select this only realm by default
    //             if(!realmTree.children || !realmTree.children.length) {
    //                 $scope.model = [realmTree[0]];
    //             }
    //         }
    //     });



    //////////////////////////////////

    $scope.selected = function(realm) {

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        return _.some($scope.model, function(r) {

            if (r._id) {
                r = r._id;
            }

            return r == realmID;
        });
    }


    $scope.toggle = function(realm) {

        var realmID = realm._id;
        if (realmID._id) {
            realmID = realmID._id;
        }

        //////////////////////////////////////

        var selected = $scope.selected(realm);

        if (selected) {
            $scope.model = _.reject($scope.model, function(rid) {
                if (rid._id) {
                    rid = rid._id;
                }
                return (rid == realmID);
            });
        } else {
            $scope.model.push(realm);
        }
    }

    // //////////////////////////////////


    // $scope.includes = function(realm) {
    //     return _.some($scope.model, function(i) {
    //         if (_.isObject(i)) {
    //             return i._id == realm._id;
    //         } else {
    //             return i == realm._id;
    //         }
    //     });
    // }

    // $scope.toggle = function(realm) {

    //     var matches = _.filter($scope.model, function(r) {
    //         var id = r;
    //         if (_.isObject(r)) {
    //             id = r._id;
    //         }

    //         return (id == realm._id);
    //     })

    //     ////////////////////////////////

    //     if (matches.length) {
    //         $scope.model = _.reject($scope.model, function(r) {
    //             var id = r;
    //             if (_.isObject(r)) {
    //                 id = r._id;
    //             }

    //             return (id == realm._id);
    //         });
    //     } else {
    //         $scope.model.push(realm);
    //     }

    // }



});