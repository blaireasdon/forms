EventFormController.resolve = {
	event: function($http, $stateParams, Fluro) {
		var url = Fluro.apiURL + '/content/_query/5987dc3dcf544d199f545833?variables[slugID]=' + $stateParams.id;
    
    	return $http.get(url);
	},
	
	seo: function(FluroSEOService, Asset, $filter, event) {

		event = _.first(event.data);

        FluroSEOService.pageTitle = event.title;


        var imageID = _.get(event, 'mainImage._id');

        if(imageID) {
        	//Use the poster of the video
        	FluroSEOService.imageURL = Asset.imageUrl(imageID, 640, null);
        } else {
        	FluroSEOService.imageURL = null;
        }
        


        // var shortDescription = _.get(event, 'data.publicData.body');


        // shortDescription = shortDescription.replace(/<[^>]+>/g, '').slice(0, 100);
        // if (shortDescription.length == 100) {
        //     shortDescription += '...';
        // }

        FluroSEOService.description = $filter('readableDate')(event);//shortDescription;


        return true;
    },
}

function EventFormController($scope, event, $http, Fluro) {

// 	
	// console.log('TESTING', event);
	
	$scope.event = _.get(event, 'data[0]');
	$scope.form = _.get(event, 'data[0].forms[0]');


	////////////////////////////////

	// function populateForms() {

	// 	// run through each of the forms on the event  
	// 	_.each($scope.event.forms, function(form) {

	// 		// then go get the form from the server
	// 		var url = Fluro.apiURL + '/content/_query/5892a5369985ce5ee279eeb6?variables[slugID]=' + form._id;
	    
	//     	$http.get(url).then(function success(res) {
	//     		console.log('Got form',res.data[0].title)

	//     		// then put into $scope.forms[].
	//     		$scope.forms.push(res.data[0])

	//     		console.log('forms', $scope.forms)
	//     	});

    		
	// 	});
	// }
	// populateForms();
}

