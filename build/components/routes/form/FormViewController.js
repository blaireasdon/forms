FormViewController.resolve = {
    // publicForms: function($stateParams, FluroContentRetrieval) {
    // 	return FluroContentRetrieval.query(null, null, 5892a5369985ce5ee279eeb6, null, {
    // 		slugID: 
    // 	};
    // },
    form: function($http, Fluro, $stateParams, $q) {

        var deferred = $q.defer();

        // var url = Fluro.apiURL + '/content/_query/5892a5369985ce5ee279eeb6?variables[slugID]=' + $stateParams.id; // + formID;
        var url = Fluro.apiURL + '/form/' + $stateParams.id; // + formID;

        $http.get(url).then(function(res) {
            console.log('GOT FORM', res.data)
            deferred.resolve(res.data);
        }, function(err) {
            console.log('ERROR',err);
            deferred.reject(err);
        });


        return deferred.promise;
    },
    seo: function(FluroSEOService, Asset, $filter, form) {

    	// console.log('FORM DATA', _.first(form.data));

        console.log('FORM', form);

    	// form = _.first(form.data);

        var displayTitle = _.get(form, 'data.publicData.title');
        if (!displayTitle) {
            displayTitle = form.title;
        }

        FluroSEOService.pageTitle = displayTitle;

        // console.log('SEO', form)

        var imageID = _.get(form, 'data.publicData.mainImage');

        if(imageID) {
        	//Use the poster of the video
        	FluroSEOService.imageURL = Asset.imageUrl(imageID, 640, null);
        } else {
        	FluroSEOService.imageURL = null;
        }


        var shortDescription = _.get(form, 'data.publicData.body');

        if(!shortDescription) {
        	shortDescription = '';
        }
        shortDescription = shortDescription.replace(/<[^>]+>/g, '').slice(0, 100);
        if (shortDescription.length == 100) {
            shortDescription += '...';
        }

        FluroSEOService.description = shortDescription;


        return true;
    },
}

function FormViewController($scope, $stateParams, form) {

    // console.log('form', form)
    $scope.form = form;

    $scope.linkedProcess = $stateParams.process;
    console.log('Linked Process', $scope.linkedProcess);

    $scope.done = function(response) {
        console.log('Callback done');
    }

}