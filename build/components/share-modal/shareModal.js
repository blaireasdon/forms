app.directive('shortUrl', function(FluroContent, $location, ShareModalService) {

    return {
        restrict: 'E',
        replace: true,
        scope:{
            form:'=ngModel',
        },
        template: '<a class="btn btn-sm btn-outline" ng-click="open()"><span>Share</span><i class="fa fa-share"></i></a>', 
        link: function($scope, $element, $attr) {


            $scope.$watch('form', function(form) {
                if(!form) {
                    return;
                }
                
                if(form && form._id) {

                    var longURL = $location.absUrl();
                    FluroContent.endpoint('url/shorten').save({url:longURL}).$promise.then(function(res) {

                        console.log('Got short url', res);
                        var shortURL = res.url;
                        $scope.shortURL = 'http://' + shortURL;
                    })
                }
            }) 

            ///////////////////////////////////////////////

            $scope.open = function() {
                console.log('happy days')
                ShareModalService.form = $scope.form;
            }
        }
    };
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////

app.service('ShareModalService', function() {
    var service = {};

    service.remove = function() {
        service.form = null;
        service.copied = false;
    }

    return service;
})


///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////


function clipboard(shortURL){

    var element = angular.element('<input id="clipper">');
    element.css({
        position:'fixed',
        top:999999999,
        left:99999999999,
        'z-index':-999,
    });
    element[0].value = shortURL;
    // console.log(shortURL, element[0].value);
    angular.element('body').append(element);
    element[0].select();

    var copied;

    try {
        copied = document.execCommand('copy');

        if(!copied) {
            console.error("Cannot copy text");
        } else {
            console.log("Copied text", copied);
        }
    } catch (err) {
        console.log('Unable to copy', err);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

app.directive('shareModal', function($location, FluroContent) {

	return {
        restrict: 'E',
        replace: true,
        scope:true,
        templateUrl: 'share-modal/share-modal.html', 
        controller: function($scope, ShareModalService) {

	    	// $scope.$watch(function() {
	    	// 	return ShareModalService.form;
	    	// }, function(form) {
	    	// 	// console.log('Formy form', form)
	    	// })

            $scope.service = ShareModalService;
            console.log('Service', $scope.service)

            $scope.processing = true;



            var longURL = $location.absUrl();

            FluroContent.endpoint('url/shorten').save({url:longURL}).$promise.then(function(res) {

                $scope.processing = false;

                $scope.url = 'http://' + res.url;

            })

            $scope.close = function() {

                angular.element('#clipper').remove();
                ShareModalService.remove()
            }

            $scope.copyUrl = function() {
                console.log('starting to copy');
                clipboard($scope.url);

                

                ShareModalService.copied = true;
               
                
            }



        }
    };
})
