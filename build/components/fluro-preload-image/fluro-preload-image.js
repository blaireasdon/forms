app.directive('preloadImage', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.aspect = angular.isDefined(attrs.aspect) ? scope.$parent.$eval(attrs.aspect) : 0;

            if (scope.aspect) {
                element.wrap('<div class="preload-image-outer aspect-ratio" style="padding-bottom:' + scope.aspect + '%"></div>');
            } else {
                element.wrap('<div class="preload-image-outer"></div>');
            }

            var preloader = angular.element('<span class="image-preloader"><i class="fa fa-spinner fa-spin"/></span>');

            element.on('load', function() {
                element.removeClass('preload-hide');
                element.addClass('preload-show');

                preloader.remove();
            });

            element.on('error', function(err) {
                element.removeClass('preload-hide');
                element.addClass('preload-show');

                preloader.remove();
            });

            if (attrs.ngSrc && attrs.ngSrc.length) {
                element.addClass('preload-hide');
                element.parent().append(preloader);
            }
        }
    };
});


app.directive('preloadBackground', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            //Create a new image element
            var image = new Image();

            //Create the preloader
            var preloader = angular.element('<span class="image-preloader"><i class="fa fa-spinner fa-spin"/></span>');

            //Create the inner image div
            var div = angular.element('<div class="preload-bg-image"></div>');
            
            //Append and setup the original element
            element
            .addClass('preload-bg-outer')
            .append(div);

            //////////////////////////////////////////////

            //Get the background image url
            if (attrs.preloadBackground && attrs.preloadBackground.length) {
                // console.log('PReload background', attrs.preloadBackground);
                image.src = attrs.preloadBackground;
                element.addClass('preload-hide');
                element.append(preloader);
            }

            //////////////////////////////////////////////

            //Once the image has loaded
            image.onload = function(){

                div.css({'backgroundImage': 'url(' + image.src + ')'});
                element.addClass('preload-show');
                element.removeClass('preload-hide');
                preloader.remove();
         };
     }
 };
});
