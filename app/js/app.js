
_.contains = _.includes;

/////////////////////////////////////////////////////////////////////

var app = angular.module('fluro', [
    'ngAnimate',
    'ngResource',
    'ui.router',
    'ngTouch',
    'fluro.config',
    'fluro.content',
    'fluro.asset',
    'fluro.access',
    'fluro.validate',
    'fluro.interactions',
    'formly',
    'formlyBootstrap',
    'angulartics',
    'angulartics.google.analytics',
    'signature',
])


/////////////////////////////////////////////////////////////////////

function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName('meta');

    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == stringKey) {
            return metas[i].getAttribute("content");
        }
    }
    return "";
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.config(function($stateProvider, $httpProvider, $compileProvider, FluroProvider, $urlRouterProvider, $locationProvider, $analyticsProvider) {

    // make sure include default tracker and fluro tracker
    $analyticsProvider.settings.ga.additionalAccountNames = ['fluro'];

    // send user properties as well
    $analyticsProvider.settings.ga.additionalAccountHitTypes.setUserProperties = true;

    //Dont include all the angular comments and ng-scope classes
    $compileProvider.debugInfoEnabled(false);


    ///////////////////////////////////////////////////

    //Get the access token and the API URL
    var accessToken = getMetaKey('fluro_application_key');
    var apiURL = getMetaKey('fluro_url');

    /////////////////////////////////////////////// ////

    //Create the initial config setup
    var initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: false, //Change this if you want to use local storage  or session storage
        backupToken: accessToken,
    }

    ///////////////////////////////////////////

    //Check if we are developing an app locally
    var appDevelopmentURL = getMetaKey('app_dev_url');

    //If an app development url is set then we know where to login etc
    if (appDevelopmentURL && appDevelopmentURL.length) {
        initialConfig.appDevelopmentURL = appDevelopmentURL;
    } else {
        //Set HTML5 mode to true when we deploy
        //otherwise live reloading will break in local dev environment
        $locationProvider.html5Mode(true);
    }

    ///////////////////////////////////////////

    //Set the initial config
    FluroProvider.set(initialConfig);

    ///////////////////////////////////////////

    //Http Intercpetor to check auth failures for xhr requests
    if (!accessToken) {
        $httpProvider.defaults.withCredentials = true;
    }

    $httpProvider.interceptors.push('FluroAuthentication');

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    //////////////////////////////////////////



    $stateProvider.state('form', {
        url: '/form/:id?process',
        templateUrl: 'routes/form/form-view.html',
        controller: FormViewController,
        resolve: FormViewController.resolve,
    });

    ///////////////////////////////////////////

    $stateProvider.state('event', {
        url: '/event/:id',
        templateUrl: 'routes/event/event.html',
        controller: EventFormController,
        resolve: EventFormController.resolve,
    });

    ///////////////////////////////////////////

    $urlRouterProvider.otherwise("/");


});

/////////////////////////////////////////////////////////////////////

app.run(function($rootScope, $timeout, ShareModalService, $sessionStorage, DateTools, Asset, FluroSEOService, FluroContent, FluroBreadcrumbService, FluroScrollService, $location, $timeout, $state, $analytics) {

    
    $rootScope.avatarCache = {
        buster: 1,
    }

    //////////////////////////////////////////////////////////////////

    $rootScope.asset = Asset;
    $rootScope.$state = $state;
    $rootScope.session = $sessionStorage;
    $rootScope.breadcrumb = FluroBreadcrumbService;
    $rootScope.scroll = FluroScrollService;
    $rootScope.seo = FluroSEOService;
    $rootScope.modal = ShareModalService;
    $rootScope.date = DateTools;

    
    //////////////////////////////////////////////////////////////////

    if (!applicationData) {
        return ////console.log('No application data has been specified');
    }

    //Add the application data to the $rootScope
    $rootScope.applicationData = applicationData;

    if(applicationData.sandbox) {
        console.log('ENVIRONMENT: Sandbox');
    }

    if(applicationData.staging) {
        console.log('ENVIRONMENT: Staging');
    }

    //////////////////////////////////////////////////////////////////

    if (applicationUser && applicationUser._id) {
        $rootScope.user = applicationUser;

        $analytics.setUsername(applicationUser._id); // fluro id
        $analytics.setAlias(applicationUser.email); // user email
        $analytics.setUserProperties({
            dimension1: applicationUser.account._id // fluro account id
        });
    }

    //////////////////////////////////////////////////////////////////

    //Add the site name as the seo
    // var siteName = _.get(applicationData, '_application.title');
    // if (siteName && siteName.length) {
    //     FluroSEOService.siteTitle = siteName;
    // }

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    //Set your default SEO image here
    var defaultImageID = null; //_.get(applicationData, 'publicData.seoImage');
    var applicationID = _.get(applicationData, '_application._id');

    if (defaultImageID) {
        if (defaultImageID._id) {
            defaultImageID = defaultImageID._id;
        }

        FluroSEOService.defaultImageURL = Asset.imageUrl(defaultImageID, 640, null, {
            from: applicationID,
            extension: 'jpg'
        });
    }

    /////////////////////////////////////////////////////////////

    //Set your default SEO meta description here
    var defaultSEODescription = null; //_.get(applicationData, 'publicData.seoDescription');
    FluroSEOService.defaultDescription = defaultSEODescription;

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        console.log('State change error', event);
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {
        $rootScope.currentState = toState.name;
    });

    //////////////////////////////////////////////////////////////////


    $rootScope.getTypeOrDefinition = function(item, defaultIfNoneProvided) {
        if (item.definition && item.definition.length) {
            return item.definition;
        }

        if (!item._type && defaultIfNoneProvided) {
            return defaultIfNoneProvided;
        }


        return item._type;
    }

    //////////////////////////////////////////////////////////////////

    //Make touch devices more responsive
    FastClick.attach(document.body);

});
app.directive('realmFolders', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '@ngType',
            definition: '@?ngDefinition',
        },
        templateUrl: 'admin-realm-folders/admin-realm-folders.html',
        controller: 'RealmFoldersController',
    };
});




app.directive('realmFolderItem', function($compile, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            realm: '=ngModel',
            selection: '=ngSelection',
            searchTerms: '=ngSearchTerms',
        },
        templateUrl: 'admin-realm-folders/admin-realm-folders-item.html',
        link: function($scope, $element, $attrs) {



            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    if (_.isObject(i)) {
                        return i._id == realm._id;
                    } else {
                        return i == realm._id;
                    }
                });
            }

            $scope.toggle = function(realm) {

                var matches = _.filter($scope.selection, function(existingRealmID) {

                    var searchRealmID = realm._id;
                    if (existingRealmID._id) {
                        existingRealmID = existingRealmID._id;
                    }

                    return (searchRealmID == existingRealmID);
                })

                ////////////////////////////////

                if (matches.length) {

                    _.each(matches, function(match) {
                        _.pull($scope.selection, match);
                    })
                } else {
                    $scope.selection.push(realm);
                }

            }

            ///////////////////////////////////////////////////

            function hasActiveChild(realm) {

                var active = $scope.contains(realm);

                if (active) {
                    return true;
                }

                if (realm.children && realm.children.length) {
                    return _.some(realm.children, hasActiveChild);
                }
            }

            ///////////////////////////////////////////////////

            $scope.isExpanded = function(realm) {
                if (realm.expanded) {
                    return realm.expanded;
                }

                return $scope.activeTrail(realm);
            }

            ///////////////////////////////////////////////////

            $scope.activeTrail = function(realm) {
                if (!realm.children) {
                    return;
                }

                return _.some(realm.children, hasActiveChild);
            }



            var template = '<realm-folder-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:' + "'title'" + ' track by subRealm._id"></realm-folder-item>';

            var newElement = angular.element(template);
            $compile(newElement)($scope);
            $element.find('.realm-select-children').append(newElement);
        }
    };
});



app.controller('RealmFoldersController', function($scope, $filter, $rootScope, FluroContent, FluroAccess) {

    $scope.settings = {
        show: true,
    };

    ////////////////////////////////////////////////////

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    ////////////////////////////////////////////////////

    if (!$scope.definition || !$scope.definition.length) {
        $scope.definition = $scope.type;
    }

    ////////////////////////////////////////////////////


    // if($rootScope.user.accountType == 'administrator') {
    //     FluroContent.resource('realm').query().$promise.then(function(res) {
    //         $scope.realms = res;
    //     }, function(err) {
    //         console.log('Realm Select ERROR', err);
    //     });
    // } else {

    //     //Now we get the realms from FluroAccess
    //     $scope.realms =  FluroAccess.retrieveSelectableRealms('create', $scope.definition, $scope.type);
    // }

    // //////////////////////////////////


    $scope.tree = $rootScope.user.permissionSets;
    // //Now we get the realm tree from FluroAccess service
    // FluroAccess.retrieveSelectableRealms('create', $scope.definition, $scope.type)
    //     .then(function(realmTree) {

    //         //Add the tree to the scope
    //         $scope.tree = realmTree;

    //         ////////////////////////////////////////////////////////

    //         //If only one realm is available select it by default
    //         if (realmTree.length == 1 && !$scope.model.length) {
    //             //If there are no children select this only realm by default
    //             if(!realmTree.children || !realmTree.children.length) {
    //                 $scope.model = [realmTree[0]];
    //             }
    //         }
    //     });



    //////////////////////////////////

    $scope.selected = function(realm) {

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        return _.some($scope.model, function(r) {

            if (r._id) {
                r = r._id;
            }

            return r == realmID;
        });
    }


    $scope.toggle = function(realm) {

        var realmID = realm._id;
        if (realmID._id) {
            realmID = realmID._id;
        }

        //////////////////////////////////////

        var selected = $scope.selected(realm);

        if (selected) {
            $scope.model = _.reject($scope.model, function(rid) {
                if (rid._id) {
                    rid = rid._id;
                }
                return (rid == realmID);
            });
        } else {
            $scope.model.push(realm);
        }
    }

    // //////////////////////////////////


    // $scope.includes = function(realm) {
    //     return _.some($scope.model, function(i) {
    //         if (_.isObject(i)) {
    //             return i._id == realm._id;
    //         } else {
    //             return i == realm._id;
    //         }
    //     });
    // }

    // $scope.toggle = function(realm) {

    //     var matches = _.filter($scope.model, function(r) {
    //         var id = r;
    //         if (_.isObject(r)) {
    //             id = r._id;
    //         }

    //         return (id == realm._id);
    //     })

    //     ////////////////////////////////

    //     if (matches.length) {
    //         $scope.model = _.reject($scope.model, function(r) {
    //             var id = r;
    //             if (_.isObject(r)) {
    //                 id = r._id;
    //             }

    //             return (id == realm._id);
    //         });
    //     } else {
    //         $scope.model.push(realm);
    //     }

    // }



});
app.directive('realmFolders', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '@ngType',
            definition: '@?ngDefinition',
        },
        templateUrl: 'admin-realm-folders/admin-realm-folders.html',
        controller: 'RealmFoldersController',
    };
});




app.directive('realmFolderItem', function($compile, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            realm: '=ngModel',
            selection: '=ngSelection',
            searchTerms: '=ngSearchTerms',
        },
        templateUrl: 'admin-realm-folders/admin-realm-folders-item.html',
        link: function($scope, $element, $attrs) {



            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    if (_.isObject(i)) {
                        return i._id == realm._id;
                    } else {
                        return i == realm._id;
                    }
                });
            }

            $scope.toggle = function(realm) {

                var matches = _.filter($scope.selection, function(existingRealmID) {

                    var searchRealmID = realm._id;
                    if (existingRealmID._id) {
                        existingRealmID = existingRealmID._id;
                    }

                    return (searchRealmID == existingRealmID);
                })

                ////////////////////////////////

                if (matches.length) {

                    _.each(matches, function(match) {
                        _.pull($scope.selection, match);
                    })
                } else {
                    $scope.selection.push(realm);
                }

            }

            ///////////////////////////////////////////////////

            function hasActiveChild(realm) {

                var active = $scope.contains(realm);

                if (active) {
                    return true;
                }

                if (realm.children && realm.children.length) {
                    return _.some(realm.children, hasActiveChild);
                }
            }

            ///////////////////////////////////////////////////

            $scope.isExpanded = function(realm) {
                if (realm.expanded) {
                    return realm.expanded;
                }

                return $scope.activeTrail(realm);
            }

            ///////////////////////////////////////////////////

            $scope.activeTrail = function(realm) {
                if (!realm.children) {
                    return;
                }

                return _.some(realm.children, hasActiveChild);
            }



            var template = '<realm-select-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:' + "'title'" + ' track by subRealm._id"></realm-select-item>';

            var newElement = angular.element(template);
            $compile(newElement)($scope);
            $element.find('.realm-select-children').append(newElement);
        }
    };
});



app.controller('RealmFoldersController', function($scope, $filter, $rootScope, FluroContent, FluroAccess) {

    $scope.settings = {
        show: true,
    };

    ////////////////////////////////////////////////////

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    ////////////////////////////////////////////////////

    if (!$scope.definition || !$scope.definition.length) {
        $scope.definition = $scope.type;
    }

    ////////////////////////////////////////////////////


    // if($rootScope.user.accountType == 'administrator') {
    //     FluroContent.resource('realm').query().$promise.then(function(res) {
    //         $scope.realms = res;
    //     }, function(err) {
    //         console.log('Realm Select ERROR', err);
    //     });
    // } else {

    //     //Now we get the realms from FluroAccess
    //     $scope.realms =  FluroAccess.retrieveSelectableRealms('create', $scope.definition, $scope.type);
    // }

    // //////////////////////////////////



    // //////////////////////////////////

    
    //Now we get the realm tree from FluroAccess service
    FluroAccess.retrieveSelectableRealms('create', $scope.definition, $scope.type)
        .then(function(realmTree) {

            //Add the tree to the scope
            $scope.tree = realmTree;

            ////////////////////////////////////////////////////////

            //If only one realm is available select it by default
            if (realmTree.length == 1 && !$scope.model.length) {
                //If there are no children select this only realm by default
                if(!realmTree.children || !realmTree.children.length) {
                    $scope.model = [realmTree[0]];
                }
            }
        });



    //////////////////////////////////

    $scope.selected = function(realm) {

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        return _.some($scope.model, function(r) {

            if (r._id) {
                r = r._id;
            }

            return r == realmID;
        });
    }


    $scope.toggle = function(realm) {

        var realmID = realm._id;
        if (realmID._id) {
            realmID = realmID._id;
        }

        //////////////////////////////////////

        var selected = $scope.selected(realm);

        if (selected) {
            $scope.model = _.reject($scope.model, function(rid) {
                if (rid._id) {
                    rid = rid._id;
                }
                return (rid == realmID);
            });
        } else {
            $scope.model.push(realm);
        }
    }

    // //////////////////////////////////


    // $scope.includes = function(realm) {
    //     return _.some($scope.model, function(i) {
    //         if (_.isObject(i)) {
    //             return i._id == realm._id;
    //         } else {
    //             return i == realm._id;
    //         }
    //     });
    // }

    // $scope.toggle = function(realm) {

    //     var matches = _.filter($scope.model, function(r) {
    //         var id = r;
    //         if (_.isObject(r)) {
    //             id = r._id;
    //         }

    //         return (id == realm._id);
    //     })

    //     ////////////////////////////////

    //     if (matches.length) {
    //         $scope.model = _.reject($scope.model, function(r) {
    //             var id = r;
    //             if (_.isObject(r)) {
    //                 id = r._id;
    //             }

    //             return (id == realm._id);
    //         });
    //     } else {
    //         $scope.model.push(realm);
    //     }

    // }



});

(function() {
    
    Date.shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    Date.longMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    Date.shortDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    Date.longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    
    // defining patterns
    var replaceChars = {
        // Day
        d: function() { return (this.getDate() < 10 ? '0' : '') + this.getDate(); },
        D: function() { return Date.shortDays[this.getDay()]; },
        j: function() { return this.getDate(); },
        l: function() { return Date.longDays[this.getDay()]; },
        N: function() { return (this.getDay() == 0 ? 7 : this.getDay()); },
        S: function() { return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th'))); },
        w: function() { return this.getDay(); },
        z: function() { var d = new Date(this.getFullYear(),0,1); return Math.ceil((this - d) / 86400000); }, // Fixed now
        // Week
        W: function() { 
            var target = new Date(this.valueOf());
            var dayNr = (this.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var firstThursday = target.valueOf();
            target.setMonth(0, 1);
            if (target.getDay() !== 4) {
                target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
            }
            return 1 + Math.ceil((firstThursday - target) / 604800000);
        },
        // Month
        F: function() { return Date.longMonths[this.getMonth()]; },
        m: function() { return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1); },
        M: function() { return Date.shortMonths[this.getMonth()]; },
        n: function() { return this.getMonth() + 1; },
        t: function() { var d = new Date(); return new Date(d.getFullYear(), d.getMonth(), 0).getDate() }, // Fixed now, gets #days of date
        // Year
        L: function() { var year = this.getFullYear(); return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)); },   // Fixed now
        o: function() { var d  = new Date(this.valueOf());  d.setDate(d.getDate() - ((this.getDay() + 6) % 7) + 3); return d.getFullYear();}, //Fixed now
        Y: function() { return this.getFullYear(); },
        y: function() { return ('' + this.getFullYear()).substr(2); },
        // Time
        a: function() { return this.getHours() < 12 ? 'am' : 'pm'; },
        A: function() { return this.getHours() < 12 ? 'AM' : 'PM'; },
        B: function() { return Math.floor((((this.getUTCHours() + 1) % 24) + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) * 1000 / 24); }, // Fixed now
        g: function() { return this.getHours() % 12 || 12; },
        G: function() { return this.getHours(); },
        h: function() { return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12); },
        H: function() { return (this.getHours() < 10 ? '0' : '') + this.getHours(); },
        i: function() { return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes(); },
        s: function() { return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds(); },
        u: function() { var m = this.getMilliseconds(); return (m < 10 ? '00' : (m < 100 ?
    '0' : '')) + m; },
        // Timezone
        e: function() { return "Not Yet Supported"; },
        I: function() {
            var DST = null;
                for (var i = 0; i < 12; ++i) {
                        var d = new Date(this.getFullYear(), i, 1);
                        var offset = d.getTimezoneOffset();
    
                        if (DST === null) DST = offset;
                        else if (offset < DST) { DST = offset; break; }                     else if (offset > DST) break;
                }
                return (this.getTimezoneOffset() == DST) | 0;
            },
        O: function() { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00'; },
        P: function() { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':00'; }, // Fixed now
        T: function() { return this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1'); },
        Z: function() { return -this.getTimezoneOffset() * 60; },
        // Full Date/Time
        c: function() { return this.format("Y-m-d\\TH:i:sP"); }, // Fixed now
        r: function() { return this.toString(); },
        U: function() { return this.getTime() / 1000; }
    };

    // Simulates PHP's date function
    Date.prototype.format = function(format) {
        var date = this;
        return format.replace(/(\\?)(.)/g, function(_, esc, chr) {
            return (esc === '' && replaceChars[chr]) ? replaceChars[chr].call(date) : chr;
        });
    };

}).call(this);

(function () {
	'use strict';
	
	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @version 1.0.3
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */
	
	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/
	
	
	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} options The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;
	
		options = options || {};
	
		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;
	
	
		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;
	
	
		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;
	
	
		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;
	
	
		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;
	
	
		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;
	
	
		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;
	
	
		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;
	
		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;
	
		if (FastClick.notNeeded(layer)) {
			return;
		}
	
		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}
	
	
		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}
	
		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}
	
		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);
	
		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};
	
			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}
	
		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {
	
			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}
	
	
	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0;
	
	
	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent);
	
	
	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);
	
	
	/**
	 * iOS 6.0(+?) requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS ([6-9]|\d{2})_\d/).test(navigator.userAgent);
	
	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;
	
	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {
	
		// Don't send a synthetic click to disabled inputs (issue #62)
		case 'button':
		case 'select':
		case 'textarea':
			if (target.disabled) {
				return true;
			}
	
			break;
		case 'input':
	
			// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
			if ((deviceIsIOS && target.type === 'file') || target.disabled) {
				return true;
			}
	
			break;
		case 'label':
		case 'video':
			return true;
		}
	
		return (/\bneedsclick\b/).test(target.className);
	};
	
	
	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
		case 'textarea':
			return true;
		case 'select':
			return !deviceIsAndroid;
		case 'input':
			switch (target.type) {
			case 'button':
			case 'checkbox':
			case 'file':
			case 'image':
			case 'radio':
			case 'submit':
				return false;
			}
	
			// No point in attempting to focus disabled inputs
			return !target.disabled && !target.readOnly;
		default:
			return (/\bneedsfocus\b/).test(target.className);
		}
	};
	
	
	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;
	
		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}
	
		touch = event.changedTouches[0];
	
		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};
	
	FastClick.prototype.determineEventType = function(targetElement) {
	
		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}
	
		return 'click';
	};
	
	
	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;
	
		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};
	
	
	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;
	
		scrollParent = targetElement.fastClickScrollParent;
	
		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}
	
				parentElement = parentElement.parentElement;
			} while (parentElement);
		}
	
		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};
	
	
	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {
	
		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}
	
		return eventTarget;
	};
	
	
	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;
	
		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}
	
		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];
	
		if (deviceIsIOS) {
	
			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}
	
			if (!deviceIsIOS4) {
	
				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}
	
				this.lastTouchIdentifier = touch.identifier;
	
				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}
	
		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;
	
		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;
	
		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}
	
		return true;
	};
	
	
	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;
	
		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}
	
		return false;
	};
	
	
	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}
	
		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}
	
		return true;
	};
	
	
	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {
	
		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}
	
		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}
	
		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};
	
	
	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;
	
		if (!this.trackingClick) {
			return true;
		}
	
		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}
	
		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;
	
		this.lastClickTime = event.timeStamp;
	
		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;
	
		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];
	
			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}
	
		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}
	
				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {
	
			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}
	
			this.focus(targetElement);
			this.sendClick(targetElement, event);
	
			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}
	
			return false;
		}
	
		if (deviceIsIOS && !deviceIsIOS4) {
	
			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}
	
		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}
	
		return false;
	};
	
	
	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};
	
	
	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {
	
		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}
	
		if (event.forwardedTouchEvent) {
			return true;
		}
	
		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}
	
		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {
	
			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {
	
				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}
	
			// Cancel the event
			event.stopPropagation();
			event.preventDefault();
	
			return false;
		}
	
		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};
	
	
	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;
	
		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}
	
		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}
	
		permitted = this.onMouse(event);
	
		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}
	
		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};
	
	
	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;
	
		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}
	
		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};
	
	
	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
	
		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}
	
		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];
	
		if (chromeVersion) {
	
			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');
	
				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
	
			// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}
	
		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);
	
			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');
	
				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}
	
		// IE10 with -ms-touch-action: none, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none') {
			return true;
		}
	
		return false;
	};
	
	
	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} options The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};
	
	
	if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
	
		// AMD. Register as an anonymous module.
		define(function() {
			return FastClick;
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());

app.service('FluroBreadcrumbService', function($rootScope, $timeout, $state) {

    var controller = {
        trail: [],
    };

    ///////////////////////////////////////

    //Private variables
    var backButtonPress;
    var scrollPositions = {};

    ///////////////////////////////////////

    //Initialize a breadcrumb trail
    controller.trail = [];

    ///////////////////////////////////////

    //Change of state started
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, error) {
        //Get the href for the url we're going to
        var path = $state.href(fromState, fromParams);

        //Store the scroll position of where we are currently
        var previousScrollPosition = document.body.scrollTop;
        scrollPositions[path] = previousScrollPosition;
        
    });

    ///////////////////////////////////////

    //Listen for a change to the current state
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {


        //Clear the breadcrumb if we are at the home page
        if(toState.name == 'home') {
            controller.trail.length =0;
        }



        //If the user is navigating back
        if (backButtonPress) {

            //Get the href for the url we're going to
            var path = $state.href(toState, toParams);

            //Get the last scroll position
            var previousScrollPosition = scrollPositions[path];

            //If there is a scroll position previously set
            if (previousScrollPosition) {
                //Move to that scroll position
                $timeout(function() {

                    
                    $rootScope.$broadcast('back-resume-scroll', {previousScrollPosition: previousScrollPosition});
                    document.body.scrollTop = previousScrollPosition;
                })
            } else {
                //Otherwise set scroll to 0
                document.body.scrollTop = 0;
            }

            //Remove the previous set breadcrumb
            controller.trail.pop();
            controller.trail.pop();

            //Reset the back button variable
            backButtonPress = false;
        } else {
            //New state so set scroll to 0
            document.body.scrollTop = 0;
        }

        // if(toState)
        //Override here to 
        // switch (toState.name) {
        //     case 'home':
        //     case 'songs':
        //     case 'events':
        //         controller.trail.length = 0;
        //         break;
        // }


        if(toParams.noBreadcrumb) {
            return;
        }
            
        
            //Add the current state with its parameters to the breadcrumb
            controller.trail.push({
                // title:
                name: toState.name,
                params: toParams
            });
        
    });




    ///////////////////////////////////////

    //Get the first defined state with a name from the router
    controller.topState = _.find($state.get(), function(state) {
        return (state.name && state.name.length);
    })

    ///////////////////////////////////////

    controller.top = function() {

        controller.trail.length = 0;

        if (controller.topState) {
            //Go to the home state
            $state.go(controller.topState);
        }
    }

    ///////////////////////////////////////

    controller.clear = function() {
        var last = controller.trail.pop();

        if(last) {
            controller.trail = [last];
        } else {
            controller.trail.length = 0;
        }
    }

    ///////////////////////////////////////

    controller.backTo = function(breadcrumbItem) {

        // console.log('Back to', breadcrumbItem);

        var index = controller.trail.indexOf(breadcrumbItem);
        if(index != -1) {
            controller.trail.length = index;
            $state.go(breadcrumbItem.name, breadcrumbItem.params);
            return;
        }
    }

    ///////////////////////////////////////

    controller.back = function() {

        //If we only have one breadcrumb in the list then we can't go back any further
        if (!controller.trail.length) {
            return;
        }

        /////////////////////////////

        //And get the last in the breadcrumb
        backButtonPress = true;

        var count = controller.trail.length;
        var previousState = controller.trail[count - 2];


        //If we have a state to go back to
        if (previousState) {
            //Go to the previous state
            $state.go(previousState.name, previousState.params);
        } else {
            //Go up a level if we can
            if ($state.$current.parent && $state.$current.parent.self.name.length) {
                $state.go('^');
            } else {
                //Otherwise just go to the top state
                $state.go(controller.topState);
            }
        }
    }

    ///////////////////////////////////////

    return controller;

});
app.factory('dateutils', function() {

    ////console.log('date.utils')
    // validate if entered values are a real date
    function validateDate(date) {


        // store as a UTC date as we do not want changes with timezones
        var d = new Date(Date.UTC(date.year, date.month, date.day));
        var result = d && (d.getMonth() === date.month && d.getDate() === Number(date.day));

        // ////console.log('Valid date', result, date);
        return result;
    }

    // reduce the day count if not a valid date (e.g. 30 february)
    function changeDate(date) {
        if (date.day > 28) {
            date.day--;
            return date;
        }
        // this case should not exist with a restricted input
        // if a month larger than 11 is entered
        else if (date.month > 11) {
            date.day = 31;
            date.month--;
            return date;
        }
    }

    var self = this;
    this.days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
    this.months = [{
        value: 0,
        name: 'January'
    }, {
        value: 1,
        name: 'February'
    }, {
        value: 2,
        name: 'March'
    }, {
        value: 3,
        name: 'April'
    }, {
        value: 4,
        name: 'May'
    }, {
        value: 5,
        name: 'June'
    }, {
        value: 6,
        name: 'July'
    }, {
        value: 7,
        name: 'August'
    }, {
        value: 8,
        name: 'September'
    }, {
        value: 9,
        name: 'October'
    }, {
        value: 10,
        name: 'November'
    }, {
        value: 11,
        name: 'December'
    }];

    return {
        checkDate: function(date) {

            if (!date.day) {
                return false;
            }

            if (date.month === 0) {

            } else {
                if (!date.month) {
                    return false;
                }
            }

            if (!date.year) {
                return false;
            }

            if (validateDate(date)) {
                // update the model when the date is correct
                return date;
            } else {
                // change the date on the scope and try again if invalid
                return this.checkDate(changeDate(date));
            }
        },
        get: function(name) {
            return self[name];
        }
    };
});

app.directive('dobSelect', ['dateutils',
    function(dateutils) {
        return {
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                hideAge: '=?',
                hideDates: '=?',
            },
            controller: ['$scope', 'dateutils',
                function($scope, dateutils) {


                    ////console.log('dob.select')


                    // set up arrays of values
                    $scope.days = dateutils.get('days');

                    $scope.months = dateutils.get('months');


                    //////////////////////////////////////////////////////////


                        // set the years drop down from attributes or defaults
                        var currentYear = parseInt(new Date().getFullYear());
                        var numYearsInPast = 100;
                        var oldestYear = currentYear - numYearsInPast;

                        // parseInt(attrs.startingYear, 10) || new Date().getFullYear();
                        // var numYears = parseInt(attrs.numYears, 10) || 100;
                        // var oldestYear = currentYear - numYears;

                       

                        var yearOptions = [];

                        for (var i = currentYear; i >= oldestYear; i--) {
                            yearOptions.push(i);
                        }

                        $scope.years = yearOptions;



                    /////////////////////////////////////////////////////

                    if (!$scope.model) {
                        //$scope.model = new Date();
                       // ////console.log('No way man')
                    } else {

                        //////console.log('TESTING', $scope.model, _.isDate($scope.model))
                        if (!_.isDate($scope.model)) {
                            $scope.model = new Date($scope.model);
                        }
                    }

                    /////////////////////////////////////////////////////

                    // split the current date into sections
                    var dateFields = {};

                    if ($scope.model) {
                        //Update the date fields
                        dateFields.day = new Date($scope.model).getDate();
                        dateFields.month = new Date($scope.model).getMonth();
                        dateFields.year = new Date($scope.model).getFullYear();

                        $scope.dateFields = dateFields;


                        /////////////////////////////////////////////////////

                        //Age
                        $scope.age = moment().diff($scope.model, 'years');

                    }

                    /////////////////////////////////////////////////////

                    // validate that the date selected is accurate
                    $scope.checkDate = function() {
                        // update the date or return false if not all date fields entered.
                        var date = dateutils.checkDate($scope.dateFields);
                        if (date) {
                            $scope.dateFields = date;
                        }
                    };

                    /////////////////////////////////////////////////////

                    function dateFieldsChanged(newDate, oldDate) {

                        //////console.log('Date fields Changed', newDate);
                        if (newDate == oldDate) {
                            return;
                        }

                        //Stop Watching Age
                        stopWatchingAge();


                        /////////////////////////////////

                        if (!$scope.dateFields) {
                            return ////console.log('No datefields object');
                        }

                        if (!$scope.dateFields.year) {
                            return ////console.log('No Year');
                        }

                        /////////////////////////////////

                        if ($scope.dateFields.month === 0) {

                        } else {
                            if (!$scope.dateFields.month) {
                                return ////console.log('No Month');
                            }
                        }

                        /////////////////////////////////

                        if (!$scope.dateFields.day) {
                            return ////console.log('No day');
                        }

                        /////////////////////////////////

                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        date.setHours(0, 0, 0, 0);

                        if (!$scope.model) {
                            $scope.model = new Date();
                        }


                        //Set the models details
                        $scope.model.setDate(date.getDate());
                        $scope.model.setMonth(date.getMonth());
                        $scope.model.setFullYear(date.getFullYear());

                        //Get the difference
                        var diff = moment().diff($scope.model, 'years');

                        //Update the age
                        $scope.age = parseInt(diff);


                        //Start Watching Age
                        startWatchingAge();
                    }


                    //Age was changed
                    function ageChanged(newAge, oldAge) {

                        if (newAge == oldAge) {
                            return;
                        }
                        ////console.log('Age changed', newAge, oldAge);

                        var today = new Date();
                        today.setHours(0, 0, 0, 0);

                        var currentYear = today.getFullYear();
                        var diff = parseInt(currentYear) - parseInt(newAge);

                        if (diff) {

                            //Stop Watching the Date
                            stopWatchingDate();

                            if (!$scope.dateFields) {

                                // if(!$scope.model) {
                                //     $scope.model = new Date();
                                // }

                                var dateFields = {};
                                //Update the date fields
                                //dateFields.day = new Date($scope.model).getDate();
                                //dateFields.month = new Date($scope.model).getMonth();
                                dateFields.year = diff; //new Date($scope.model).getFullYear();
                                $scope.dateFields = dateFields;
                            } else {
                                $scope.dateFields.year = diff;
                            }

                            //Set the year
                            // if ($scope.dateFields.year != diff) {
                            //     $scope.dateFields.year = diff;
                            // }

                            //Start Watching the date again
                            startWatchingDate();


                        } else {
                            ////console.log('NOPE', newAge, currentYear, diff);
                        }


                    }

                    /////////////////////////////////////////////////////

                    var ageWatcher;

                    function startWatchingAge() {
                        ageWatcher = $scope.$watch('age', ageChanged);
                    }

                    function stopWatchingAge() {
                        if (ageWatcher) {
                            ageWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////

                    var dateWatcher;

                    function startWatchingDate() {
                        dateWatcher = $scope.$watch('dateFields', dateFieldsChanged, true);
                    }

                    function stopWatchingDate() {
                        if (dateWatcher) {
                            dateWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////


                    //Start watching for changes to either
                    startWatchingAge();
                    startWatchingDate();
                   


                }
            ],
            templateUrl: 'fluro-dob-select/fluro-dob-select.html',
            link: function(scope, element, attrs, ctrl) {
                if (attrs.yearText) {
                    scope.yearText = true;
                }
                // allow overwriting of the
                if (attrs.dayDivClass) {
                    angular.element(element[0].children[0]).removeClass('form-group col-xs-3');
                    angular.element(element[0].children[0]).addClass(attrs.dayDivClass);
                }
                if (attrs.dayClass) {
                    angular.element(element[0].children[0].children[0]).removeClass('form-control');
                    angular.element(element[0].children[0].children[0]).addClass(attrs.dayClass);
                }
                if (attrs.monthDivClass) {
                    angular.element(element[0].children[1]).removeClass('form-group col-xs-5');
                    angular.element(element[0].children[1]).addClass(attrs.monthDivClass);
                }
                if (attrs.monthClass) {
                    angular.element(element[0].children[1].children[0]).removeClass('form-control');
                    angular.element(element[0].children[1].children[0]).addClass(attrs.monthClass);
                }
                if (attrs.yearDivClass) {
                    angular.element(element[0].children[2]).removeClass('form-group col-xs-4');
                    angular.element(element[0].children[2]).addClass(attrs.yearDivClass);
                }
                if (attrs.yearClass) {
                    angular.element(element[0].children[2].children[0]).removeClass('form-control');
                    angular.element(element[0].children[2].children[0]).addClass(attrs.yearClass);
                }

                

                // $scope.years = _.reverse(yearOptions);



                // pass down the ng-disabled property
                scope.$parent.$watch(attrs.ngDisabled, function(newVal) {
                    scope.disableFields = newVal;
                });

            }
        };
    }
]);


app.controller('FluroInteractionButtonSelectController', function($scope, FluroValidate) {


    /////////////////////////////////////////////////////////////////////////

    var to = $scope.to;
    var opts = $scope.options;

    $scope.selection = {
        values: [],
        value: null,
    }


    /////////////////////////////////////////////////////////////////////////

    //Get the definition
    var definition = $scope.to.definition;

    //Minimum and maximum
    var minimum = definition.minimum;
    var maximum = definition.maximum;

    if(!minimum) {
        minimum = 0;
    }

    if(!maximum) {
        maximim = 0;
    }

    $scope.multiple = (maximum != 1);


    /////////////////////////////////////////////////////////////////////////

    $scope.dragControlListeners = {
        //accept: function (sourceItemHandleScope, destSortableScope) {return boolean}//override to determine drag is allowed or not. default is true.
        //itemMoved: function (event) {//Do what you want},
        orderChanged: function(event) {
            //Do what you want
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        },
        //containment: '#board'//optional param.
        //clone: true //optional param for clone feature.
        //allowDuplicates: false //optional param allows duplicates to be dropped.
    };


    /////////////////////////////////////////////////////////////////////////

    $scope.selectBox = {}

    $scope.selectUpdate = function() {
        if(!$scope.selectBox.item) {
            return;
        }
        $scope.selection.values.push($scope.selectBox.item);
        $scope.model[opts.key] = angular.copy($scope.selection.values);
    }

    /////////////////////////////////////////////////////////////////////////




    $scope.canAddMore = function() {

        if(!maximum) {
            return true;
        }
       
        if($scope.multiple) {
            return ($scope.selection.values.length < maximum);
        } else {
            if(!$scope.selection.value) {
                return true;
            }
        }
        
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.contains = function(value) {
        if ($scope.multiple) {
            //Check if the values are selected
            return _.includes($scope.selection.values, value);
        } else {
            return $scope.selection.value == value;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.select = function(value) {

        if ($scope.multiple) {
            if (!$scope.canAddMore()) {
                return;
            }
            $scope.selection.values.push(value);
        } else {
            $scope.selection.value = value;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.deselect = function(value) {
        if ($scope.multiple) {
            _.pull($scope.selection.values, value);
        } else {
            $scope.selection.value = null;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.toggle = function(reference) {

        if ($scope.contains(reference)) {
            $scope.deselect(reference);
        } else {
            $scope.select(reference);
        }

        //Update model
        setModel();
    }


    /////////////////////////////////////////////////////////////////////////

    // initialize the checkboxes check property
    $scope.$watch('model', function(newModelValue, oldModelValue) {


        if (newModelValue != oldModelValue) {
            var modelValue;

            //If there is properties in the FORM model
            if (_.keys(newModelValue).length) {

                //Get the model for this particular field
                modelValue = newModelValue[opts.key];

                if ($scope.multiple) {
                    if (modelValue && _.isArray(modelValue)) {
                        $scope.selection.values = angular.copy(modelValue);
                    } else {
                        $scope.selection.values = [];
                    }
                } else {
                    $scope.selection.value = angular.copy(modelValue);
                }
            }
        }
    }, true);


    /////////////////////////////////////////////////////////////////////////

    function checkValidity() {

        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        //Check if multiple
        if ($scope.multiple) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }

    /////////////////////////////////////////////////////////////////////////

    function setModel() {
        if ($scope.multiple) {
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        } else {
            $scope.model[opts.key] = angular.copy($scope.selection.value);
        }

        if ($scope.fc) {
            $scope.fc.$setTouched();
        }

        //console.log('Model set!', $scope.model[opts.key]);
        checkValidity();
    }

    /////////////////////////////////////////////////////////////////////////

    if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
        $scope.$watch(function() {
            return $scope.to.required;
        }, function(newValue) {
            checkValidity();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    if ($scope.to.required) {
        var unwatchFormControl = $scope.$watch('fc', function(newValue) {
            if (!newValue) {
                return;
            }
            checkValidity();
            unwatchFormControl();
        });
    }

    /////////////////////////////////////////////////////////////////////////
})
////////////////////////////////////////////////////////////////////////

var _nowYear = new Date().getFullYear();

var _defaultYears = _.chain(_.range(-100, 12))
    .map(function(y) {
        var actual = _nowYear + y;

        return {
            name:actual,
            value:actual,
        }
    })
    .reverse()
    .value();



app.directive('fluroDateSelect', [function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'fluro-interaction-form/date-select/date-selector.html',
        controller: ['$scope', 'dateutils', function($scope, dateutils) {



            //Get days and months
            $scope.days = dateutils.get('days');
            $scope.months = dateutils.get('months');
            $scope.years = _defaultYears;



            /////////////////////////////////////////////////////

            // split the current date into sections
            var dateFields = {};

            /////////////////////////////////////////////////////

            $scope.$watch('dateFields', dateFieldsChanged, true);
            $scope.$watch('model', modelChanged);

            /////////////////////////////////////////////////////

            function modelChanged() {

                if ($scope.model) {
                    if (!_.isDate($scope.model)) {
                        $scope.model = new Date($scope.model);
                    }

                    //Update the date fields
                    dateFields.day = new Date($scope.model).getDate();
                    dateFields.month = new Date($scope.model).getMonth();
                    dateFields.year = new Date($scope.model).getFullYear();
                    $scope.dateFields = dateFields;
                }
            }

            /////////////////////////////////////////////////////

            function dateFieldsChanged(newDate, oldDate) {

                //////console.log('Date fields Changed', newDate);
                if (newDate == oldDate) {
                    return;
                }

                /////////////////////////////////

                if (!$scope.dateFields) {
                    return ////console.log('No datefields object');
                }

                if (!$scope.dateFields.year) {
                    return ////console.log('No Year');
                }

                /////////////////////////////////

                if ($scope.dateFields.month === 0) {

                } else {
                    if (!$scope.dateFields.month) {
                        return ////console.log('No Month');
                    }
                }

                /////////////////////////////////

                if (!$scope.dateFields.day) {
                    return ////console.log('No day');
                }

                /////////////////////////////////

                var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                date.setHours(0, 0, 0, 0);

                if (!$scope.model) {
                    $scope.model = new Date();
                }


                //Set the models details
                $scope.model.setDate(date.getDate());
                $scope.model.setMonth(date.getMonth());
                $scope.model.setFullYear(date.getFullYear());
            }

            /////////////////////////////////////////////////////

            // validate that the date selected is accurate
            $scope.checkDate = function() {
                // update the date or return false if not all date fields entered.
                var date = dateutils.checkDate($scope.dateFields);
                if (date) {
                    $scope.dateFields = date;
                }
            };

            


            // $validate();

            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////

            
            /**
             ////console.log('FluroDateSelectController')
            $scope.today = function() {
                $scope.model[$scope.options.key] = new Date();
            };


            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd/MM/yyyy'];
            $scope.format = $scope.formats[0];

            /**/
        }]
    }

}]);
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'dob-select',
        templateUrl: 'fluro-interaction-form/dob-select/fluro-dob-select.html',
        //controller: 'FluroInteractionDobSelectController',
        wrapper: ['bootstrapHasError'],
    });

});

app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'embedded',
        templateUrl: 'fluro-interaction-form/embedded/fluro-embedded.html',
        controller: 'FluroInteractionNestedController',
        wrapper: ['bootstrapHasError'],
    });

});

/**

app.controller('FluroEmbeddedDefinitionController', function($scope, $http, Fluro, $filter, FluroValidate) {


})

/**/
app.directive('interactionForm', function($compile) {

    //////console.log('interaction form controller directive')
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            linkedProcess: '=?linkedProcess',
            vm: '=?config',
            callback: '=?callback',
        },
        transclude: true,
        controller: 'InteractionFormController',
        template: '<div class="fluro-interaction-form" ng-transclude-here />',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };


});

////////////////////////////////////////////////////////////////////////

app.directive('webform', function($compile) {

    //////console.log('web form controller directive')

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            vm: '=?config',
            debugMode: '=?debugMode',
            callback: '=?callback',
            linkedEvent: '=?linkedEvent',
            linkedProcess: '=?linkedProcess',
        },
        transclude: true,
        controller: 'InteractionFormController',
        templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
                // console.log('Transclude content', clone)
                //$element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };
});


app.config(function(formlyConfigProvider) {


    formlyConfigProvider.setType({
        name: 'currency',
        extends: 'input',
        overwriteOk: true,
        controller: ['$scope', function($scope) {
            /*
            ////////console.log('CURRENCY SCOPE', $scope);

            $scope.$watch('model[options.key]', function(val) {

                if(!$scope.model[$scope.options.key] && $scope.model[$scope.options.key] != 0 ) {
                    ////////console.log('Set!')
                    $scope.model[$scope.options.key] = 0;
                }
            })
            /**/
        }],

        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        defaultOptions: {
            ngModelAttrs: {
                currencyDirective: {
                    attribute: 'ng-currency'
                },
                fractionValue: {
                    attribute: 'fraction',
                    bound: 'fraction'
                },
                minimum: {
                    attribute: 'min',
                    bound: 'min'
                },
                maximum: {
                    attribute: 'max',
                    bound: 'max'
                }
            },
            templateOptions: {
                customAttrVal: '',
                required: true,
                fraction: 2,
            },
            validators: {
                validInput: {
                    expression: function($viewValue, $modelValue, scope) {

                        var numericValue = Number($modelValue);

                        if (isNaN(numericValue)) {
                            return false;
                        }

                        //Get Minimum and Maximum Amounts
                        var minimumAmount = scope.options.data.minimumAmount;
                        var maximumAmount = scope.options.data.maximumAmount;

                        if (minimumAmount && numericValue < minimumAmount) {
                            return false;
                        }

                        if (maximumAmount && numericValue > maximumAmount) {
                            return false;
                        }

                        return true;
                    }
                }
            }
        }
    });


})

////////////////////////////////////////////////////////////////////////

app.run(function(formlyConfig, $templateCache) {

    //////console.log('web form run')
    formlyConfig.templateManipulators.postWrapper.push(function(template, options, scope) {

        if (!scope.to.errorMessage || !scope.to.errorMessage.length) {



            var viewValue = _.get(scope, 'fc.$viewValue');
            var fieldDefinition = _.get(options, 'templateOptions.definition');

            // console.log('SCOPE OPTIONS', viewValue);

            if (!viewValue || _.isArray(viewValue)) {
                // if(!viewValue.length) {
                // scope.to.errorMessage = 'Please enter at least';
                // } else {

                if (fieldDefinition && (fieldDefinition.maximum == 1)) {
                    scope.to.errorMessage = 'This field is required';
                } else {
                    if (fieldDefinition && fieldDefinition.minimum != 1) {
                        scope.to.errorMessage = 'Please select at least ' + fieldDefinition.minimum + ' options';
                    } else {
                        scope.to.errorMessage = 'Please select at least 1 option';
                    }

                }

            } else {
                // console.log('VIEW FALUE IS NOT VALID', viewValue);
                scope.to.errorMessage = "'" + viewValue + "' is not a valid answer";
            }
        }

        var fluroErrorTemplate = $templateCache.get('fluro-interaction-form/field-errors.html');
        return '<div>' + template + fluroErrorTemplate + '</div>';
    });

    //////////////////////////////////




    //////////////////////////////////

    formlyConfig.setType({
        name: 'multiInput',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/multi.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: ['$scope', function($scope) {
            $scope.copyItemOptions = copyItemOptions;

            function copyItemOptions() {
                return angular.copy($scope.to.inputOptions);
            }
        }]
    });

    //////////////////////////////////

    // formlyConfig.setType({
    //     name: 'payment',
    //     overwriteOk: true,
    //     templateUrl: 'fluro-interaction-form/payment/payment.html',
    //     //controller: 'FluroPaymentController',
    //     defaultOptions: {
    //         noFormControl: true,
    //     },
    // });

    formlyConfig.setType({
        name: 'custom',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/custom.html',
        controller: 'CustomInteractionFieldController',
        wrapper: ['bootstrapHasError']
    });

    formlyConfig.setType({
        name: 'button-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/button-select/fluro-button-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });



    formlyConfig.setType({
        name: 'date-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/date-select/fluro-date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope, FluroValidate) {

            $scope.$watch('model', checkValidity, true);

            var opts = $scope.options;
            var definition = opts.templateOptions.definition;

            /////////////////////////////////////////////////////

            function checkValidity() {

                if (!$scope.model) {
                    $scope.model = {};
                }

                var validRequired;
                var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

                //Check if multiple
                if ($scope.multiple) {
                    if ($scope.to.required) {
                        validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
                    }
                } else {
                    if ($scope.to.required) {
                        if ($scope.model[opts.key]) {
                            validRequired = true;
                        }
                    }
                }

                // if(!validInput) {
                //    console.log('CHECK VALIDITY', definition.title, validRequired, validInput);
                // }


                if ($scope.fc) {

                    var formControl = $scope.fc;
                    if (formControl.length) {
                        formControl = formControl[0];
                    }

                    // console.log('FC', $scope.fc);
                    formControl.$setValidity('required', validRequired);
                    formControl.$setValidity('validInput', validInput);
                }
            }
        }
    });



    formlyConfig.setType({
        name: 'terms',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/fluro-terms.html',
        wrapper: ['bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'order-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/order-select/fluro-order-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    /**/

});

////////////////////////////////////////////////////////////////////////

app.controller('CustomInteractionFieldController', function($scope, FluroValidate) {
    $scope.$watch('model[options.key]', function(value) {
        if (value) {
            if ($scope.fc) {
                $scope.fc.$setTouched();
            }
        }
    }, true);
});

////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

app.controller('InteractionFormController', function($scope, $q, $timeout, $rootScope, DateTools, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {


    var tithelyController;


    /////////////////////////////////////////////

    function getAge(dateInput, timezone) {
        var today = new Date();
        var birthDate = new Date(dateInput);
        var age = today.getFullYear() - birthDate.getFullYear();

        var m = today.getMonth() - birthDate.getMonth();

        //If the date is on the cusp of the new year
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

        return age;
    }

    /////////////////////////////////////////////////////////////////

    function matchInArray(array, key, value, operator) {

        //Filter the array options by a certain value and operator
        var matches = _.filter(array, function(entry) {

            //Get the value from the object
            var retrievedValue = _.get(entry, key);
            var isMatch;

            ///////////////////////

            //Check how to operate
            switch (operator) {
                case '>':
                    isMatch = (retrievedValue > value);
                    break;
                case '<':
                    isMatch = (retrievedValue < value);
                    break;
                case '>=':
                    isMatch = (retrievedValue >= value);
                    break;
                case '<=':
                    isMatch = (retrievedValue <= value);
                    break;
                case 'in':
                    isMatch = _.includes(retrievedValue, value);
                    break;
                default:
                    //operator is strict equals
                    if (value === undefined) {
                        isMatch = retrievedValue;
                    } else {
                        isMatch = (retrievedValue == value);
                    }
                    break;
            }

            ///////////////////////

            return isMatch;
        })

        return matches;
    }


    /////////////////////////////////////////////////////////////////


    // $scope.debugMode = true;

    //If we are in staging environment then set debugMode to true
    if (_.get(window, 'applicationData.staging')) {
        $scope.debugMode = true;
    }



    /////////////////////////////////////////////////////////////////

    if (!$scope.vm) {
        $scope.vm = {}
    }

    /////////////////////////////////////////////////////////////////

    //Keep this available to all field scopes
    //this is used so that other fields can get calculated total
    //not just the payment summary field
    $formScope = $scope;

    /////////////////////////////////////////////////////////////////

    function debugLog() {
        // return;
        if ($scope.debugMode) {
            console.log(_.map(arguments, function(v) { return v }));
        }
    }


    /////////////////////////////////////////////////////////////////

    // The model object that we reference
    // on the  element in index.html
    if ($scope.vm.defaultModel) {
        $scope.vm.model = angular.copy($scope.vm.defaultModel);
    } else {
        $scope.vm.model = {};
    }

    /////////////////////////////////////////////////////////////////

    // An array of our form fields with configuration
    // and options set. We make reference to this in
    // the 'fields' attribute on the  element
    $scope.vm.modelFields = [];

    /////////////////////////////////////////////////////////////////

    //Keep track of the state of the form
    $scope.vm.state = 'ready';





    /////////////////////////////////////////////////////////////////

    $scope.correctPermissions = true;

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////


    $scope.scrollToField = function(field) {
        if (field.field) {
            field = field.field;
        }
        var element = _.get(field, 'formControl.$$element[0]');

        if (!element) {
            element = document.querySelector('#' + field.$name);
            // document.getElementById('#'+field.$name);   
            console.log('ID', field, field.$name, element)
        }

        if (element) {
            if (element.scrollIntoView) {
                element.scrollIntoView();
            }
        }



    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.readyToSubmit = false;

    // $scope.$watch('vm.modelForm.$invalid', updateErrorList)
    $scope.$watch('vm.modelFields', validateAllFields, true)
    $scope.$watch('vm.model', validateAllFields, true)

    $scope.$watch('model', definitionModelChanged);
    $scope.$watch('integration', integrationChanged);

    // $scope.$watch('vm.modelForm.$touched', updateErrorList)
    // $scope.$watch('vm.modelForm.$error.required', updateErrorList)
    // $scope.$watch('vm.modelForm.$error.validInput', updateErrorList)

    var timer;


    function validateAllFields() {

        //Debounce the validation
        if (timer) {
            $timeout.cancel(timer);
            timer = null;
        }

        timer = $timeout(function() {

            function setValid(ready) {
                // console.log('SET VALID', ready)
                $scope.readyToSubmit = ready;
            }

            /////////////////////////////////////

            function mapRecursiveField(field) {
                if (!field) {
                    return;
                }

                var output = [field];

                /////////////////////////////////////////////////////

                if (field.key == '_paymentCardDetails') {
                    var calculatedAmount = _.get($formScope, 'vm.model.calculatedTotal');
                    var donationAmount = _.get($formScope, 'vm.model._paymentAmount');


                    //If there is no cost
                    //then we don't need to validate
                    //the card fields
                    if (!calculatedAmount && !donationAmount) {
                        console.log('No calculated total');
                        return;
                    }

                    //Get the payment method
                    var paymentMethod = _.get($formScope, 'vm.model._paymentMethod');
                    if (paymentMethod && paymentMethod.length && paymentMethod != 'card') {
                        console.log('Alternative payment method');
                        return;
                    }
                }

                /////////////////////////////////////////////////////

                var isDisabled = _.get(field, 'templateOptions.definition.disableValidation');

                if (isDisabled) {
                    // console.log('FAILED', field.key, isDisabled)
                    return;
                }


                /////////////////////////////////////////////////////


                if (!field.hide && field.fields && field.fields.length) {
                    output.push(_.map(field.fields, mapRecursiveField));
                }

                if (!field.hide && field.data && field.data.fields && field.data.fields.length) {
                    output.push(_.map(field.data.fields, mapRecursiveField));
                }

                if (!field.hide && field.fieldGroup && field.fieldGroup.length) {
                    // console.log('IS REQUIRED?', field);
                    output.push(_.map(field.fieldGroup, mapRecursiveField));
                }

                if (!field.hide && field.data && field.data.replicatedFields && field.data.replicatedFields.length) {

                    // console.log('REPLICATED FIELDS')
                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                }



                if (!field.hide && field.data && field.data.replicatedDataFields && field.data.replicatedDataFields.length) {

                    // console.log('REPLICATED FIELDS')
                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedDataFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                }

                if (!field.hide && field.data && field.data.replicatedDetailFields && field.data.replicatedDetailFields.length) {

                    // console.log('REPLICATED FIELDS')
                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedDetailFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                }



                //  else {
                //     if (field.data && field.data.fields && field.data.fields.length) {
                //         output.push(_.map(field.data.fields, mapRecursiveField));
                //     }
                // }

                return output;
            }



            ////////////////////////////////////////////

            // console.log('ERROR LIST CHECKING', $scope.vm.modelFields);

            $scope.errorList = _.chain($scope.vm.modelFields)
                .map(mapRecursiveField)
                .flattenDeep()
                .compact()
                .filter(function(field) {

                    /////////////////////////////////////////////////////

                    var required = _.get(field, 'formControl.$error.required');
                    var dateInvalid = _.get(field, 'formControl.$error.date');
                    var invalid = _.get(field, 'formControl.$error.validInput');
                    var uploadInProgress = _.get(field, 'formControl.$error.uploadInProgress');
                    // var errorExistsAndShouldBeVisible = _.get(field, 'validation.errorExistsAndShouldBeVisible');

                    /////////////////////////////////////////////////////

                    var shouldwarning = (required || invalid || dateInvalid || uploadInProgress);
                    if (shouldwarning) {
                        // console.log('Not valid > ', shouldwarning, field, required, invalid)
                    }



                    return shouldwarning;
                })
                .value();


            ////////////////////////////////////////////

            //Interaction Form
            var interactionForm = $scope.vm.modelForm;

            if (!interactionForm) {
                console.log('INVALID - no form')
                return setValid(false);
            }

            ////////////////////////////////////////////

            // if (interactionForm.$invalid) {

            // console.log($scope.vm);


            if (!$scope.errorList || !$scope.errorList.length) {



                var invalidValues = _.get(interactionForm, '$error.validInput');
                var requiredValues = _.get(interactionForm, '$error.required');

                //We need to display something to the user as to why the form is not valid
                $scope.errorList = _.chain([invalidValues, requiredValues])
                    .flatten()
                    .compact()
                    .map(function(f, index) {
                        return _.chain(f)
                            .keys()
                            .map(function(key) {

                                if (!_.startsWith(key, 'formly_')) {
                                    return;
                                }

                                var field = f[key];


                                // console.log('CHECK', f, key, field);
                                if (field.$invalid || field.$error) {
                                    return field;
                                }

                                return;
                            })
                            .value()
                    })
                    .flatten()
                    .compact()
                    .map(function(f, index) {




                        var readableName = f.$name;
                        if (!f.$viewValue) {
                            // console.log(f, readableName);



                            var message = 'Please check ' + readableName;

                            if (_.startsWith(readableName, 'formly_')) {
                                if (f.$error.date) {
                                    message = 'Invalid date provided'
                                }
                            }

                            return {
                                id: index,
                                field: f,
                                message: message,
                            }
                        }
                        return {
                            id: index,
                            field: f,
                            message: f.$viewValue + ' is an invalid value for ' + readableName,
                        }
                        // }



                        // return f;
                    })
                    .compact()
                    .value();


                // console.log('INVALID NESTED FORMS - ', $scope.errorList)
                // } else {
                //     console.log('HAS ERROR LIST ALREADY', $scope.errorList);

                if ($scope.errorList.length) {
                    console.log('INVALID We have errors so set to false')
                    return setValid(false);
                }
            }



            // }

            ////////////////////////////////////////////

            if (interactionForm.$error) {

                console.log('Errors', interactionForm.$error);

                if (interactionForm.$error.required && interactionForm.$error.required.length) {
                    // console.log('INVALID $error.required - ', interactionForm.$error.required);
                    return setValid(false);
                }



                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) {


                    // console.log('INVALID $error.validInput  - ', interactionForm.$error.validInput);

                    console.log('INVALID valid input not provided', interactionForm.$error.validInput);
                    return setValid(false);
                }

                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) {

                    // console.log('INVALID $error.uploadInProgress - Upload still in progress');
                    console.log('INVALID Upload has not finished');
                    return setValid(false);
                }
            }

            // ////////console.log('Form should be good to go')

            //It all worked so set to true
            setValid(true);
        }, 200)

    }

    /**
     /////////////////////////////////////////////////////////////////

    $scope.$watch('vm.modelFields', function(fields) {
        //////////console.log('Interaction Fields changed')
        $scope.errorList = getAllErrorFields(fields);

        console.log('Error List', $scope.errorList);
    }, true)

    /**/


    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    formlyValidationMessages.addStringMessage('required', 'This field is required');

    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid value';
    }


    formlyValidationMessages.messages.uploadInProgress = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is still processing';
    }

    formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid date';
    }

    /////////////////////////////////////////////////////////////////

    $scope.reset = function() {

        //Reset
        if ($scope.vm.defaultModel) {
            //console.log('Reset the default model')
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }
        $scope.vm.modelForm.$setPristine();
        $scope.vm.options.resetModel();


        //Act as if a new definition was changed
        definitionModelChanged();




        //Clear the response from previous submission
        $scope.response = null;
        $scope.vm.state = 'ready';


        //Reset after state change
        ////////console.log('Broadcast reset')
        $scope.$broadcast('form-reset');

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    // //Function to run on permissions
    // function checkPermissions() {
    //     if ($rootScope.user) {
    //         //Check if we have the correct permissions
    //         var canCreate = FluroAccess.can('create', $scope.model.definitionName);
    //         var canSubmit = FluroAccess.can('submit', $scope.model.definitionName);

    //         //Allow if the user can create or submit
    //         $scope.correctPermissions = (canCreate | canSubmit);
    //     } else {
    //         //Just do this by default
    //         $scope.correctPermissions = true;
    //     }
    // }

    /////////////////////////////////////////////////////////////////

    // //Watch if user login changes
    // $scope.$watch(function() {
    //     return $rootScope.user;
    // }, checkPermissions)

    /////////////////////////////////////////////////////////////////

    var injectedScripts = {};


    function injectScript(scriptURL, callback) {

        if (injectedScripts[scriptURL]) {
            return callback();
        }

        //Keep note so we don't inject twice
        injectedScripts[scriptURL] = true;

        //////////////////////////////////////

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.onload = callback;
        script.src = scriptURL;
        document.getElementsByTagName('head')[0].appendChild(script);

        ////////////////////////////////////

        console.log('Appended script dependency', scriptURL);
    }

    /////////////////////////////////////////////////////////////////

    function integrationChanged(integration) {

        if (!integration) {
            return;
        }

        ////////////////////////////////////////

        //Check if the environment is a sandbox environment or if the gateway is defined as a sandbox gateway
        var SANDBOX_ENV = _.get(integration, 'publicDetails.sandbox') || _.get(window, 'applicationData.sandbox');

        ////////////////////////////////////////

        //Check to see if we need to inject any dependencies
        switch (integration.module) {
            case 'stripe':
                injectScript('https://js.stripe.com/v2/', function() {
                    console.log('Stripe has been included on page')
                });
                break;
            case 'eway':
                injectScript('https://secure.ewaypayments.com/scripts/eCrypt.js', function() {
                    console.log('Eway has been included on page')
                });
                break;
            case 'tithely':


                if (SANDBOX_ENV) {
                    console.log('Inject SANDBOX token script')
                    injectScript('https://tithelydev.com/api-js/v1/tithely.js', tithelyLoaded);
                } else {
                    console.log('Inject LIVE token script')
                    injectScript('https://tithe.ly/api-js/v1/tithely.js', tithelyLoaded);
                }

                //////////////////////////////////////

                function tithelyLoaded() {
                    console.log('TITHELY HAS LOADED')
                    //Get encrypted token from Stripe
                    var key;
                    var liveKey = integration.publicDetails.publicKey;
                    var sandboxKey = integration.publicDetails.publicKey;

                    /////////////////////////////////////////////

                    if (SANDBOX_ENV) {
                        key = sandboxKey;
                    } else {
                        key = liveKey;
                    }

                    /////////////////////////////////////////////

                    //Set the tithely key
                    tithelyController = new Tithely(key);
                }


                break;
        }

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function definitionModelChanged() {

        // ////////console.log('Model changed');
        if (!$scope.model || $scope.model.parentType != 'interaction') {
            console.log('Model is not an interaction'); //$scope.model = {};
            return
        }

        /////////////////////////////////////////////////////////

        //Check if we need a payment gateway
        var paymentSettings = $scope.model.paymentDetails;

        //If we do need a payment gateway
        if (paymentSettings && (paymentSettings.required || paymentSettings.allow)) {



            var hasPublicDetails = _.get($scope, 'integration.publicDetails');
            var hasModule = _.get($scope, 'integration.module');

            if (!hasPublicDetails || !hasModule) {
                console.log('Form Requires Payment, but no payment gateway was provided');
                alert('This form has not been configured properly. And no payment gateway was defined. Please notify the administrator of this website immediately.')
                return;
            }
        }



        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        // The model object that we reference
        // on the  element in index.html
        // $scope.vm.model = {};
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }


        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        $scope.vm.modelFields = [];

        /////////////////////////////////////////////////////////////////

        //Keep track of the state of the form
        $scope.vm.state = 'ready';

        /////////////////////////////////////////////////////////////////

        //Add the submit function
        $scope.vm.onSubmit = submitInteraction;

        /////////////////////////////////////////////////////////////////

        //Keep track of any async promises we need to wait for
        $scope.promises = [];

        /////////////////////////////////////////////////////////////////

        //Submit is finished
        $scope.submitLabel = 'Submit';

        if ($scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length) {
            $scope.submitLabel = $scope.model.data.submitLabel;
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details
        var interactionFormSettings = $scope.model.data;

        if (!interactionFormSettings) {
            interactionFormSettings = {};
        }

        if (!interactionFormSettings.allowAnonymous && !interactionFormSettings.disableDefaultFields) {
            interactionFormSettings.requireFirstName = true;
            interactionFormSettings.requireLastName = true;
            interactionFormSettings.requireGender = true;
            interactionFormSettings.requireEmail = true;

            switch (interactionFormSettings.identifier) {
                case 'both':
                    interactionFormSettings.requireEmail =
                        interactionFormSettings.requirePhone = true;
                    break;
                case 'email':
                    interactionFormSettings.requireEmail = true;
                    break;
                case 'phone':
                    interactionFormSettings.requirePhone = true;
                    break;
                case 'either':
                    interactionFormSettings.askPhone = true;
                    interactionFormSettings.askEmail = true;
                    break;
            }
        }


        /////////////////////////////////////////////////////////////////

        var firstNameField;
        var lastNameField;
        var genderField;

        /////////////////////////////////////////////////////////////////

        //Gender
        if (interactionFormSettings.askGender || interactionFormSettings.requireGender) {
            genderField = {
                key: '_gender',
                type: 'select',
                templateOptions: {
                    type: 'email',
                    label: 'Title',
                    placeholder: interactionFormSettings.placeholderGender, //'Please select a title',
                    options: [{
                        name: 'Mr',
                        value: 'male'
                    }, {
                        name: 'Ms / Mrs',
                        value: 'female'
                    }],
                    required: interactionFormSettings.requireGender,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return (value == 'male' || value == 'female');
                    }
                }
            }
            //$scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //First Name
        if (interactionFormSettings.askFirstName || interactionFormSettings.requireFirstName) {
            firstNameField = {
                key: '_firstName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'First Name',
                    placeholder: interactionFormSettings.placeholderFirstName, //'Please enter your first name',
                    required: interactionFormSettings.requireFirstName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////

        //Last Name
        if (interactionFormSettings.askLastName || interactionFormSettings.requireLastName) {
            lastNameField = {
                key: '_lastName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'Last Name',
                    placeholder: interactionFormSettings.placeholderLastName, //'Please enter your last name',
                    required: interactionFormSettings.requireLastName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        if (genderField && firstNameField && lastNameField) {

            genderField.className = 'col-sm-2';

            firstNameField.className =
                lastNameField.className = 'col-sm-5';

            $scope.vm.modelFields.push({
                fieldGroup: [genderField, firstNameField, lastNameField],
                className: 'row',
                // name:'First and Last Name',
            });
        } else if (firstNameField && lastNameField && !genderField) {
            firstNameField.className =
                lastNameField.className = 'col-sm-6';

            $scope.vm.modelFields.push({
                fieldGroup: [firstNameField, lastNameField],
                className: 'row',
                // name:'First and Last Name',
            });
        } else {
            if (genderField) {
                $scope.vm.modelFields.push(genderField);
            }

            if (firstNameField) {
                $scope.vm.modelFields.push(firstNameField);
            }

            if (lastNameField) {
                $scope.vm.modelFields.push(lastNameField);
            }
        }



        /////////////////////////////////////////////////////////////////

        //Email Address
        if (interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {

            // console.log('REQUIRE EMAIL???', interactionFormSettings);
            var newField = {
                key: '_email',
                type: 'input',
                templateOptions: {
                    type: 'email',
                    label: 'Email Address',
                    placeholder: interactionFormSettings.placeholderEmail, //'Please enter a valid email address',
                    required: interactionFormSettings.requireEmail,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {



                        var value = $modelValue || $viewValue;

                        if (!value || !value.length) {
                            return true;
                        }

                        return validator.isEmail(value);
                    }
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._phoneNumber || !scope.model._phoneNumber.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }

            $scope.vm.modelFields.push(newField);
        }


        /////////////////////////////////////////////////////////////////

        //Ask Phone Number
        if (interactionFormSettings.askPhone || interactionFormSettings.requirePhone) {
            var newField = {
                key: '_phoneNumber',
                type: 'input',
                templateOptions: {
                    type: 'tel',
                    label: 'Contact Phone Number',
                    placeholder: interactionFormSettings.placeholderPhoneNumber, //'Please enter a contact phone number',
                    required: interactionFormSettings.requirePhone,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._email || !scope.model._email.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

            }


            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //Age / Date of birth
        if (interactionFormSettings.askDOB || interactionFormSettings.requireDOB) {
            var newField = {
                key: '_dob',
                type: 'dob-select',
                templateOptions: {
                    label: 'Date of birth',
                    placeholder: interactionFormSettings.placeholderDOB, //'Please provide your date of birth',
                    required: interactionFormSettings.requireDOB,
                    maxDate: new Date(),
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                    // params:{
                    //     hideAge:true,
                    // }

                }
            }

            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        function addFieldDefinition(array, fieldDefinition) {

            // switch(fieldDefinition.type) {
            //     case 'group':
            //     case 'string':
            //     break;
            //     default:
            //     return console.log('TYPE', fieldDefinition.type);
            //     break;
            // }

            if (fieldDefinition.params && fieldDefinition.params.disableWebform) {
                //If we are hiding this field then just do nothing and return here
                return;
            }




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Create a new field
            var newField = {};
            newField.key = fieldDefinition.key;
            // newField.name = _.startCase(fieldDefinition.title);

            /////////////////////////////

            //Ensure integers for min/max
            fieldDefinition.minimum = parseInt(fieldDefinition.minimum);
            fieldDefinition.maximum = parseInt(fieldDefinition.maximum);

            /////////////////////////////

            //Add the class name if applicable
            if (fieldDefinition.className) {
                newField.className = fieldDefinition.className;
            }

            /////////////////////////////

            //Template Options
            var templateOptions = {};
            templateOptions.type = 'text';
            templateOptions.label = fieldDefinition.title;
            templateOptions.description = fieldDefinition.description;
            templateOptions.params = fieldDefinition.params;


            //Attach a custom error message
            if (fieldDefinition.errorMessage) {
                templateOptions.errorMessage = fieldDefinition.errorMessage;
            }

            //Include the definition itself
            templateOptions.definition = fieldDefinition;

            /////////////////////////////

            //Add a placeholder
            if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                templateOptions.placeholder = fieldDefinition.placeholder;
            }

            //  else if (fieldDefinition.description && fieldDefinition.description.length) {
            //     templateOptions.placeholder = fieldDefinition.description;
            // } else {
            //     templateOptions.placeholder = fieldDefinition.title;
            // }

            /////////////////////////////

            //Require if minimum is greater than 1 and not a field group
            templateOptions.required = (fieldDefinition.minimum > 0);

            /////////////////////////////

            templateOptions.onBlur = 'to.focused=false';
            templateOptions.onFocus = 'to.focused=true';




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Allowed Options


            switch (fieldDefinition.type) {
                case 'reference':


                    //If we have allowed references specified
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                        templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id,
                            }
                        });
                    } else {
                        //We want to load all the options from the server
                        templateOptions.options = [];

                        if (fieldDefinition.sourceQuery) {

                            //We use the query to find all the references we can find
                            var queryId = fieldDefinition.sourceQuery;
                            if (queryId._id) {
                                queryId = queryId._id;
                            }

                            /////////////////////////

                            var options = {};

                            //If we need to template the query
                            if (fieldDefinition.queryTemplate) {
                                options.template = fieldDefinition.queryTemplate;
                                if (options.template._id) {
                                    options.template = options.template._id;
                                }
                            }

                            /////////////////////////

                            //Now retrieve the query
                            var promise = FluroContentRetrieval.getQuery(queryId, options);

                            //Now get the results from the query
                            promise.then(function(res) {
                                //////////console.log('Options', res);
                                templateOptions.options = _.map(res, function(ref) {
                                    return {
                                        name: ref.title,
                                        value: ref._id,
                                    }
                                })
                            });
                        } else {

                            if (fieldDefinition.directive != 'embedded') {
                                if (fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length) {
                                    //We want to load all the possible references we can select
                                    FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                        templateOptions.options = _.map(res, function(ref) {
                                            return {
                                                name: ref.title,
                                                value: ref._id,
                                            }
                                        })
                                    });
                                }
                            }
                        }
                    }
                    break;
                default:
                    //Just list the options specified
                    if (fieldDefinition.options && fieldDefinition.options.length) {
                        templateOptions.options = fieldDefinition.options;
                    } else {
                        templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                            return {
                                name: val,
                                value: val
                            }
                        });
                    }
                    break;
            }


            /////////////////////////////

            //Directive or widget
            switch (fieldDefinition.directive) {
                case 'reference-select':
                case 'value-select':

                    switch (window.DEVICE_PLATFORM) {
                        case 'ios':
                        case 'android':
                            //Use the 
                            newField.type = 'reference-select';
                            break;
                        default:

                            if (templateOptions.options && templateOptions.options.length > 10) {
                                newField.type = 'select';
                            } else {

                                //Detour here
                                newField.type = 'button-select';
                            }
                            break;
                    }

                    break;
                case 'select':
                    newField.type = 'select';
                    break;
                case 'wysiwyg':
                    newField.type = 'textarea';
                    break;
                default:
                    newField.type = fieldDefinition.directive || 'input';
                    break;
            }


            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //If there is custom attributes
            if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length) {
                newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = 'customAttr' + key;
                    results[customKey] = {
                        attribute: key
                    };

                    //Custom Key
                    templateOptions[customKey] = attr;

                    return results;
                }, {});
            }


            /////////////////////////////


            //Allow for multiple select
            if (newField.type == 'select') {
                if (!newField.ngModelAttrs) {
                    newField.ngModelAttrs = {};
                }

                if (fieldDefinition.maximum != 1) {
                    newField.ngModelAttrs.multiple = {
                        value: 'multiple'
                    };
                }
            }

            /////////////////////////////


            //Allow for multiple select
            if (newField.type == 'select') {
                if (!newField.ngModelAttrs) {
                    newField.ngModelAttrs = {};
                }

                if (fieldDefinition.maximum != 1) {
                    newField.ngModelAttrs.multiple = "true";
                }
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //What kind of data type, override for things like checkbox
            //if (fieldDefinition.type == 'boolean') {
            if (fieldDefinition.directive != 'custom' && fieldDefinition.directive != 'value') {
                switch (fieldDefinition.type) {
                    case 'boolean':

                        if (fieldDefinition.params && fieldDefinition.params.storeCopy) {
                            newField.type = 'terms';
                        } else {
                            newField.type = 'checkbox';
                        }

                        break;
                    case 'date':

                        if (newField.type != 'dob-select') {
                            newField.type = 'date-select';
                        }




                        break;
                    case 'number':
                    case 'float':
                    case 'integer':
                    case 'decimal':
                        templateOptions.type = 'input';
                        // templateOptions.step = 'any';

                        if (!newField.ngModelAttrs) {
                            newField.ngModelAttrs = {};
                        }

                        /////////////////////////////////////////////

                        newField.ngModelAttrs.customAttrpattern = {
                            attribute: 'pattern',
                        }

                        templateOptions.customAttrpattern = "[0-9.]*";

                        //Only do this if its an integer cos iOS SUCKS!
                        //iOS will show an alphabetical keyboard and allow bad entry of data anyway
                        //So no way to be smart here rught now
                        if (fieldDefinition.type == 'integer') {
                            // ////////console.log('Is integer');

                            templateOptions.type = 'number';
                            templateOptions.baseDefaultValue = 0;
                            templateOptions.customAttrpattern = "[0-9]*";
                            //Force numeric keyboard


                            newField.ngModelAttrs.customAttrinputmode = {
                                attribute: 'inputmode',
                            }

                            //Force numeric keyboard

                            templateOptions.customAttrinputmode = "numeric"


                            /////////////////////////////////////////////

                            // ////////console.log('SET NUMERICINPUT')

                            if (fieldDefinition.params) {
                                if (parseInt(fieldDefinition.params.maxValue) !== 0) {
                                    templateOptions.max = fieldDefinition.params.maxValue;
                                }

                                if (parseInt(fieldDefinition.params.minValue) !== 0) {
                                    templateOptions.min = fieldDefinition.params.minValue;
                                } else {
                                    templateOptions.min = 0;
                                }
                            }

                        }
                        break;
                }

            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Default Options

            if (fieldDefinition.maximum == 1) {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0];
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id;
                        }
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]);
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0];
                        }
                    }
                }
            } else {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {
                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences;
                        } else {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                return ref._id;
                            });
                        }
                    } else {
                        templateOptions.baseDefaultValue = [];
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                                return Number(val);
                            });
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues;
                        }
                    }
                }
            }


            /////////////////////////////

            //Append the template options
            newField.templateOptions = templateOptions;

            /////////////////////////////


            /////////////////////////////
            /////////////////////////////

            newField.validators = {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;

                    // console.log('VALID IS', $viewValue, $modelValue, scope, fieldDefinition)


                    if (!value) {
                        return true;
                    }



                    var valid = FluroValidate.validate(value, fieldDefinition);

                    if (!valid) {
                        // console.log('CHECKING VALID', value, valid, fieldDefinition)
                    }

                    return valid;
                }
            }

            ////////////////////////////////////////

            //If it's a date then we need to validate the date
            if (fieldDefinition.type == 'date') {


                //Validate the datr
                newField.validators.date = function($viewValue, $modelValue, scope) {

                    var value = $modelValue || $viewValue;
                    if (!value) {
                        if (fieldDefinition.minimum) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    var parsedDate = Date.parse(value);
                    var invalid = isNaN(parsedDate)

                    // console.log('PARSED DATE', parsedDate);

                    return !invalid;

                    // return validator.isDate(value);
                }
            }

            /////////////////////////////
            /////////////////////////////

            /**
            if (fieldDefinition.directive == 'upload') {
                newField.validators.uploadInProgress = function($viewValue, $modelValue, scope) {

                    var value = $modelValue || $viewValue;
                    var isArray = _.isArray(value);

                    if (isArray) {
                        var values = value;
                        var hasErrors = _.includes(values, 'error');
                        var hasProcessing = _.includes(values, 'processing');

                        if (hasErrors || hasProcessing) {
                            console.log('VALLLLLL', hasErrors, hasProcessing)
                            return false;
                        } else {
                            console.log('VAL GOOD');
                            return true;
                        }
                    } else {
                        return (value == 'error' || value == 'processing');
                    }
                }
            }
            /**/

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////




            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////


            if (fieldDefinition.directive == 'embedded') {
                newField.type = 'embedded';

                //Check if its an array or an object
                if (fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                    templateOptions.baseDefaultValue = {
                        data: {}
                    };
                } else {

                    var askCount = 0;

                    if (fieldDefinition.askCount) {
                        askCount = fieldDefinition.askCount;
                    }

                    //////////console.log('ASK COUNT PLEASE', askCount);

                    //////////////////////////////////////

                    if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                        askCount = fieldDefinition.minimum;
                    }

                    if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                        askCount = fieldDefinition.maximum;
                    }

                    //////////////////////////////////////

                    var initialArray = [];

                    //Fill with the asking amount of objects
                    if (askCount) {
                        _.times(askCount, function() {
                            initialArray.push({});
                        });
                    }

                    //////////console.log('initial array', initialArray);
                    //Now set the default value
                    templateOptions.baseDefaultValue = initialArray;
                }

                //////////////////////////////////////////

                //Create the new data object to store the fields
                newField.data = {
                    fields: [],
                    dataFields: [],
                    detailFields: [],
                    replicatedFields: [],
                    replicatedDataFields: [],
                    replicatedDetailFields: [],
                }

                //////////////////////////////////////////

                //Link to the definition of this nested object
                var fieldContainer = newField.data.fields;
                var dataFieldContainer = newField.data.dataFields;
                var detailFieldContainer = newField.data.detailFields;


                //////////////////////////////////////////

                //Loop through each sub field inside a group
                if (fieldDefinition.fields && fieldDefinition.fields.length) {
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }

                //////////////////////////////////////////

                var embeddedContactDetails = _.get(fieldDefinition, 'params.targetDetails');
                _.each(embeddedContactDetails, function(contactDetailName) {

                    var formID = $formScope.model._id;
                    var detailPromise = FluroContent.endpoint('defined/' + contactDetailName + '?form=' + formID).get().$promise;

                    detailPromise.then(function(detailSheetDefinition) {


                        var detailDataObject = {
                            type: 'group',
                            asObject: true,
                            minimum: 1,
                            maximum: 1,
                            key: 'data',
                            fields: detailSheetDefinition.fields,
                        }

                        ///////////////////////////////

                        var sheet = {
                            type: 'group',
                            asObject: true,
                            minimum: 1,
                            maximum: 1,
                            key: detailSheetDefinition.definitionName,
                            fields: [detailDataObject],
                        }



                        // detailFieldContainer[detailSheetDefinition.definitionName];
                        // if(!sheet) {
                        //     sheet = detailFieldContainer[detailSheetDefinition.definitionName] = [];
                        // }

                        // //Now loop through and all all the embedded definition fields
                        // if (detailSheetDefinition && detailSheetDefinition.fields && detailSheetDefinition.fields.length) {
                        //     var childFields = detailSheetDefinition.fields;

                        //     //Loop through each sub field inside a group
                        //     _.each(childFields, function(sub) {
                        //         addFieldDefinition(sheet.fields, sub);
                        //     })
                        // }

                        addFieldDefinition(detailFieldContainer, sheet);
                    });

                    $scope.promises.push(detailPromise);
                })


                //////////////////////////////////////////

                var promise = FluroContent.endpoint('defined/' + fieldDefinition.params.restrictType).get().$promise;

                promise.then(function(embeddedDefinition) {

                    //Now loop through and all all the embedded definition fields
                    if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                        var childFields = embeddedDefinition.fields;

                        //Exclude all specified fields
                        if (fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length) {
                            childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            });
                        }


                        ////////console.log('EXCLUSIONS', fieldDefinition.params.excludeKeys, childFields);

                        //Loop through each sub field inside a group
                        _.each(childFields, function(sub) {
                            addFieldDefinition(dataFieldContainer, sub);
                        })
                    }
                });

                //////////////////////////////////////////

                //Keep track of the promise
                $scope.promises.push(promise);

                //////////////////////////////////////////

                // //Need to keep it dynamic so we know when its done
                // newField.expressionProperties = {
                //     'templateOptions.embedded': function() {
                //         return promise;
                //     }
                // }



            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            if (fieldDefinition.type == 'group' && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {


                var fieldContainer;

                if (fieldDefinition.asObject) {

                    /*
                    newField = {
                        type: 'nested',
                        className: fieldDefinition.className,
                        data: {
                            fields: []
                        }
                    }
                    */
                    newField.type = 'nested';

                    //Check if its an array or an object
                    if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                        templateOptions.baseDefaultValue = {};
                    } else {

                        var askCount = 0;

                        if (fieldDefinition.askCount) {
                            askCount = fieldDefinition.askCount;
                        }

                        //////////////////////////////////////

                        if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                            askCount = fieldDefinition.minimum;
                        }

                        if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                            askCount = fieldDefinition.maximum;
                        }

                        //////////////////////////////////////

                        var initialArray = [];

                        //Fill with the asking amount of objects
                        if (askCount) {
                            _.times(askCount, function() {
                                initialArray.push({});
                            });
                        }

                        // ////////console.log('initial array', initialArray);
                        //Now set the default value
                        templateOptions.baseDefaultValue = initialArray;
                    }

                    newField.data = {
                        fields: [],
                        replicatedFields: [],
                        replicatedDataFields: [],
                        replicatedDetailFields: [],
                    }

                    //Link to the definition of this nested object
                    fieldContainer = newField.data.fields;

                } else {
                    //Start again
                    newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className,
                    }

                    //Link to the sub fields
                    fieldContainer = newField.fieldGroup;
                }

                //Loop through each sub field inside a group
                _.each(fieldDefinition.fields, function(sub) {
                    addFieldDefinition(fieldContainer, sub);
                });
            }

            /////////////////////////////

            //Check if there are any expressions added to this field

            //Add the hide expression if added through another method
            if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {

                if (!fieldDefinition.expressions) {
                    fieldDefinition.expressions = {};
                }
                fieldDefinition.expressions.hide = fieldDefinition.hideExpression;
                console.log('HIDE EXPRESSION', fieldDefinition.expressions);
            }

            if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {



                //////////////////////////////////////////

                //Get all expressions and join them together so we just listen once
                var allExpressions = _.reduce(fieldDefinition.expressions, function(memo, string) {

                    if(string && string.length) {
                        memo += '(' + string + ')';
                    }

                    return memo;
                }, '');



                /////////////////////////////////////////////////////


                //Now create a watcher
                newField.watcher = {
                    expression: function(field, scope) {


                        var watchScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                            date: new Date(),
                            matchInArray: matchInArray,

                            dateTools: DateTools, //DEPRECATED SHOULD NOT BE USED

                            //Basic Bits
                            getAge: getAge,
                            Date: Date,
                            Math: Math,
                            String: String,
                            Date: Date,
                            parseInt: parseInt,
                            parseFloat: parseFloat,
                            Boolean: Boolean,
                            Number: Number,
                        }



                        //Return the result
                        var val = $parse(allExpressions)(watchScope);


                        console.log('EXPRESSION CHANGED', allExpressions, val);
                        // debugLog('PARSE EXPRESSIONS', allExpressions, val);

                        return val;

                    },
                    listener: function(field, newValue, oldValue, scope, stopWatching) {

                        // debugLog('Expression Changed');

                        //Create a new scope object
                        var checkScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                            date: new Date(),
                            matchInArray: matchInArray,
                            dateTools: DateTools, //DEPRECATED SHOULD NOT BE USED

                            //Basic Bits
                            getAge: getAge,
                            Date: Date,
                            Math: Math,
                            String: String,
                            Date: Date,
                            parseInt: parseInt,
                            parseFloat: parseFloat,
                            Boolean: Boolean,
                            Number: Number,
                        }

                        //Loop through each expression that needs to be evaluated
                        _.each(fieldDefinition.expressions, function(expression, key) {

                            //Get the value
                            var retrievedValue = $parse(expression)(checkScope);



                            console.log('Expression changed', expression, retrievedValue);
                            debugLog('expression:', retrievedValue, checkScope);

                            //Get the field key
                            var fieldKey = field.key;

                            ///////////////////////////////////////

                            switch (key) {
                                case 'defaultValue':
                                    if (!field.formControl || !field.formControl.$dirty) {
                                        return scope.model[fieldKey] = retrievedValue;
                                    }
                                    break;
                                case 'value':
                                    return scope.model[fieldKey] = retrievedValue;
                                    break;
                                case 'required':
                                    return field.templateOptions.required = Boolean(retrievedValue);
                                    break;
                                case 'hide':
                                    return field.hide = Boolean(retrievedValue);
                                    break;
                                    // case 'label':
                                    //     if(retrievedValue) {
                                    //         var string = String(retrievedValue);
                                    //         return field.templateOptions.label = String(retrievedValue);
                                    //     }
                                    //     break;
                            }

                        });

                    }



                    ///////////////////////////////////////
                }
            }

            /////////////////////////////
            /**
                        if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {

                            

                             
                            newField.hideExpression = function($viewValue, $modelValue, scope) {

                                //Create a new scope object
                                var checkScope = {
                                    date: new Date(),
                                    model: scope.model,
                                    data: $scope.vm.model,
                                    interaction: $scope.vm.model,
                                    date:new Date(),
                                    matchInArray: matchInArray,
                                    getAge:getAge,
                                    Math:Math,
                                    Date:Date,
                                    dateTools: DateTools, //DEPRECATED SHOULD NOT BE USED
                                }

                                var parsedValue = $parse(fieldDefinition.hideExpression)(checkScope);

                                debugLog('Hide Expression', fieldDefinition.hideExpression, parsedValue);


                                // _.set(newField, 'templateOptions.hiddenByExpression', parsedValue);

                                console.log('hide expression parsed value', fieldDefinition.hideExpression, checkScope, parsedValue);
                                return Boolean(parsedValue);
                            }

                           


                        }
                         /**/

            /////////////////////////////

            if (!newField.fieldGroup) {
                //Create a copy of the default value
                newField.defaultValue = angular.copy(templateOptions.baseDefaultValue);
            }


            /////////////////////////////

            if (newField.type == 'pathlink') {
                return;
            }

            /////////////////////////////
            //Push our new field into the array
            array.push(newField);


        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Loop through each defined field and add it to our form
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        });

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details

        if (!$scope.model.paymentDetails) {
            $scope.model.paymentDetails = {};
        }

        var paymentSettings = $scope.model.paymentDetails;


        // //console.log('SCOPE MODEL', $scope.model);

        /////////////////////////////////////////////////////////////////

        //Credit Card Details
        if (paymentSettings.required || paymentSettings.allow) {

            //Setup the wrapper fields
            var paymentWrapperFields = [];
            var paymentCardFields = [];


            //console.log('PAYMENT SETTINGS REQUIRED', paymentSettings.required)

            // paymentWrapperFields.push({
            //     template: '<h4><i class="fa fa-credit-card"></i> Payment details</h4>'
            // });


            // ////////console.log('BEFORE SCOPE', $scope);

            if (paymentSettings.required) {



                debugLog('-- DEBUG -- Payment Settings Required')



                //Add the payment summary
                paymentWrapperFields.push({
                    templateUrl: 'fluro-interaction-form/payment/payment-summary.html',
                    controller: ['$scope', '$parse', function($scope, $parse) {


                        $scope.currencySymbol = '$';

                        switch (paymentSettings.currency) {
                            case 'gbp':
                                $scope.currencySymbol = "£";
                                break;

                        }

                        //Add the payment details to the scope
                        $scope.paymentDetails = paymentSettings;


                        //Start with the required amount
                        var requiredAmount = paymentSettings.amount;

                        //Store the calculatedAmount on the scope
                        $formScope.vm.model.calculatedTotal = requiredAmount;
                        $scope.calculatedTotal = requiredAmount;

                        /////////////////////////////////////////////////////


                        var watchString = '';


                        /////////////////////////////////////////////////////

                        //Add this to the scope so we can recalculate when things change and the watcher sees
                        //the function
                        $scope.matchInArray = matchInArray;

                        // function(array, key, value, operator) {

                        //     if(!operator) {
                        //         operator '=';
                        //     }

                        //     var matches = _.filter(array, function(entry) {
                        //         var retrievedValue = _.get(entry, key);

                        //         if (value === undefined) {
                        //             return retrievedValue;
                        //         } else {
                        //             return retrievedValue == value;
                        //         }
                        //     })

                        //     return matches;
                        // }

                        //For the watcher to see the 'data' object we need to add it to the scope too
                        $scope.data = $formScope.vm.model;

                        ////////////////////////////////////////
                        ////////////////////////////////////////

                        //Map each modifier to a property string and combine them all at once
                        var modelVariables = _.chain(paymentSettings.modifiers)
                            .map(function(paymentModifier) {

                                var string = '';

                                if (paymentModifier.condition && paymentModifier.condition.length) {
                                    string = '(' + paymentModifier.expression + ') + (' + paymentModifier.condition + ')';
                                } else {
                                    string = '(' + paymentModifier.expression + ')';
                                }
                                return string;
                            })
                            .flatten()
                            .compact()
                            .uniq()
                            .value();


                        if (modelVariables.length) {
                            watchString = modelVariables.join(' + "a" + ');
                        }

                        ////////////////////////////////////////

                        // console.log('WATCH STRING', watchString);

                        if (watchString.length) {


                            debugLog('Watching changes', watchString);
                            $scope.$watch(watchString, calculateTotal);
                        } else {

                            // debugLog('No watch string set', paymentSettings);

                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;
                            $scope.modifications = [];
                        }

                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////

                        function calculateTotal(watchString) {

                            console.log('Recalculate!!', watchString)
                            debugLog('Recalculate total', watchString);


                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;

                            var date = new Date();
                            $scope.date = date.getTime();

                            $scope.modifications = [];




                            /////////////////////////////////////////////////////


                            if (!paymentSettings.modifiers || !paymentSettings.modifiers.length) {
                                debugLog('No payment modifiers set');
                                return;
                            }

                            //Loop through each modifier
                            var modificationList = _.chain(paymentSettings.modifiers)
                                .map(function(modifier) {




                                    /////////////////////////////////////////////////////

                                    var context = {
                                        date: date,
                                        calculatedTotal: $scope.calculatedTotal,
                                        model: $scope.model,
                                        data: $scope.data,
                                        matchInArray: matchInArray,


                                        //Basic Bits
                                        getAge: getAge,
                                        Date: Date,
                                        Math: Math,
                                        String: String,
                                        Date: Date,
                                        parseInt: parseInt,
                                        parseFloat: parseFloat,
                                        Boolean: Boolean,
                                        Number: Number,
                                    }

                                    // console.log('PARSE ON CONTEXT');
                                    /////////////////////////////////////////////////////



                                    var parsedValue = $parse(modifier.expression)(context);
                                    parsedValue = Number(parsedValue);

                                    // console.log('ParsedValue', $scope, modifier.expression, parsedValue)

                                    if (isNaN(parsedValue)) {
                                        debugLog('Payment modifier error', modifier.title, parsedValue);
                                        //throw Error('Invalid or non-numeric pricing modifier ' + modifier.title);
                                        return;
                                    }

                                    /////////////////////////////////////////

                                    var parsedCondition = true;

                                    if (modifier.condition && String(modifier.condition).length) {
                                        parsedCondition = $parse(modifier.condition)(context);
                                    }

                                    /////////////////////////////////////////

                                    //If the condition returns false then just stop here and go to the next modifier
                                    if (!parsedCondition) {
                                        debugLog('inactive', modifier.title, modifier, $scope);
                                        return
                                    }

                                    /////////////////////////////////////////

                                    var currencySymbol = '$';

                                    switch (paymentSettings.currency) {
                                        case 'gbp':
                                            currencySymbol = "£";
                                            break;

                                    }

                                    /////////////////////////////////////////

                                    var operator = '';
                                    var operatingValue = currencySymbol + parseFloat(parsedValue / 100).toFixed(2);

                                    switch (modifier.operation) {
                                        case 'add':
                                            operator = '+';
                                            $scope.calculatedTotal = $scope.calculatedTotal + parsedValue;
                                            break;
                                        case 'subtract':
                                            operator = '-';
                                            $scope.calculatedTotal = $scope.calculatedTotal - parsedValue;
                                            break;
                                        case 'divide':
                                            operator = '/';
                                            operatingValue = parsedValue;
                                            $scope.calculatedTotal = $scope.calculatedTotal / parsedValue;
                                            break;
                                        case 'multiply':
                                            operator = 'x';
                                            operatingValue = parsedValue;
                                            $scope.calculatedTotal = $scope.calculatedTotal * parsedValue;
                                            break;
                                        case 'set':
                                            $scope.calculatedTotal = parsedValue;
                                            break;
                                    }

                                    var readableOperator = operator + ' ' + operatingValue;

                                    if (!parsedValue) {
                                        readableOperator = '';
                                    }

                                    var resultModified = {
                                        title: modifier.title,
                                        total: $scope.calculatedTotal,
                                        description: readableOperator,
                                        operation: modifier.operation,
                                    }

                                    return resultModified;

                                    // //Let the front end know that this modification was made
                                    // $scope.modifications.push({
                                    //     title: modifier.title,
                                    //     total: $scope.calculatedTotal,
                                    //     description: operator + ' ' + operatingValue,
                                    //     operation: modifier.operation,
                                    // });
                                })
                                .compact()
                                .value();


                            if (modificationList && modificationList.length) {


                                var lastSetIndex = _.findLastIndex(modificationList, function(mod) {
                                    return mod.operation == 'set';
                                });

                                //////////////////////////////////////

                                if (lastSetIndex == -1) {
                                    $scope.hasSetModification = false;
                                } else {
                                    $scope.hasSetModification = true;
                                    // console.log('HAS SET MODIFICATION');
                                }

                                //////////////////////////////////////

                                //A Set was used
                                if (lastSetIndex && lastSetIndex != -1) {
                                    $scope.modifications = _.slice(modificationList, lastSetIndex)
                                } else {

                                    $scope.modifications = modificationList

                                }

                            }



                            debugLog('Visible Modifications', modificationList);
                            // debugLog('CALCULATED TOTAL', $scope.calculatedTotal);

                            //If the modifiers change the price below 0 then change the total back to 0
                            if (!$scope.calculatedTotal || isNaN($scope.calculatedTotal) || $scope.calculatedTotal < 0) {
                                $scope.calculatedTotal = 0;
                            }



                            //Add the calculated total to the rootscope

                            $timeout(function() {
                                //console.log('Set calculated total', $scope.calculatedTotal)
                                $formScope.vm.model.calculatedTotal = $scope.calculatedTotal;
                            })

                        }

                        /**/
                    }],
                });
            } else {

                var amountDescription = 'Please enter an amount (' + String(paymentSettings.currency).toUpperCase() + ') ';


                //Limits of amount
                var minimum = paymentSettings.minAmount;
                var maximum = paymentSettings.maxAmount;
                var defaultAmount = paymentSettings.amount;

                ///////////////////////////////////////////

                var paymentErrorMessage = 'Invalid amount';

                ///////////////////////////////////////////

                if (minimum) {
                    minimum = (parseInt(minimum) / 100);
                    paymentErrorMessage = 'Amount must be a number at least ' + $filter('currency')(minimum, '$');

                    amountDescription += 'Enter at least ' + $filter('currency')(minimum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();
                }

                if (maximum) {
                    maximum = (parseInt(maximum) / 100);
                    paymentErrorMessage = 'Amount must be a number less than ' + $filter('currency')(maximum, '$');

                    amountDescription += 'Enter up to ' + $filter('currency')(maximum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();;
                }


                if (minimum && maximum) {
                    amountDescription = 'Enter a numeric amount between ' + $filter('currency')(minimum) + ' and  ' + $filter('currency')(maximum) + ' ' + String(paymentSettings.currency).toUpperCase();;
                    paymentErrorMessage = 'Amount must be a number between ' + $filter('currency')(minimum) + ' and ' + $filter('currency')(maximum);
                }

                ///////////////////////////////////////////

                //Add the option for putting in a custom amount of money
                var fieldConfig = {
                    key: '_paymentAmount',
                    type: 'currency',
                    //defaultValue: 'Cade Embery',
                    templateOptions: {
                        type: 'text',
                        label: 'Amount',
                        description: amountDescription,
                        placeholder: '0.00',
                        required: true,
                        errorMessage: paymentErrorMessage,
                        min: minimum,
                        max: maximum,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    data: {
                        customMaxLength: 8,
                        minimumAmount: minimum,
                        maximumAmount: maximum,
                    },
                };

                if (minimum) {
                    fieldConfig.defaultValue = minimum;
                }

                paymentWrapperFields.push({
                    'template': '<hr/><h3>Payment Details</h3>'
                });
                paymentWrapperFields.push(fieldConfig);
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Setup debug card details
            var defaultCardName;
            var defaultCardNumber;
            var defaultCardExpMonth;
            var defaultCardExpYear;
            var defaultCardCVN;

            //If testing mode
            if ($scope.debugMode) {
                defaultCardName = 'John Citizen';
                defaultCardNumber = '4242424242424242';
                defaultCardExpMonth = '05';
                defaultCardExpYear = '2020';
                defaultCardCVN = '123';
            }

            //////////////////////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardName',
                type: 'input',
                defaultValue: defaultCardName,
                templateOptions: {
                    type: 'text',
                    label: 'Name on Card',
                    placeholder: 'eg. (John Smith)',
                    // required: paymentSettings.required,
                    errorMessage: 'Card Name is required',
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return String(value).length > 0;
                    }
                },
            });




            /////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardNumber',
                type: 'input',
                defaultValue: defaultCardNumber,
                name: 'PaymentCardNumber',
                templateOptions: {
                    type: 'text',
                    label: 'Card Number',
                    placeholder: 'Card Number (No dashes or spaces)',
                    // required: paymentSettings.required,
                    errorMessage: 'Invalid Card Number',
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {

                        /////////////////////////////////////////////
                        var luhnChk = function(a) {
                            return function(c) {

                                if (!c) {
                                    return false;
                                }
                                for (var l = c.length, b = 1, s = 0, v; l;) v = parseInt(c.charAt(--l), 10), s += (b ^= 1) ? a[v] : v;
                                return s && 0 === s % 10
                            }
                        }([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]);

                        /////////////////////////////////////////////

                        var value = $modelValue || $viewValue;
                        var valid = luhnChk(value) && String(value).length > 15;
                        return valid;
                    }
                },
            });

            paymentCardFields.push({
                className: 'row clearfix',
                fieldGroup: [{
                    key: '_paymentCardExpMonth',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpMonth,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Month',
                        placeholder: 'MM',
                        // required: paymentSettings.required,
                        errorMessage: 'Invalid Card Expiry Month',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return String(value).length == 2;

                        }
                    },
                }, {
                    key: '_paymentCardExpYear',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpYear,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Year',
                        placeholder: 'YYYY',
                        // required: paymentSettings.required,
                        errorMessage: 'Invalid Card Expiry Year',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return String(value).length == 4;

                        }
                    },
                }, {
                    key: '_paymentCardCVN',
                    className: "col-xs-4 col-sm-2",
                    type: 'input',
                    defaultValue: defaultCardCVN,
                    templateOptions: {
                        type: 'text',
                        label: 'CVN',
                        placeholder: 'CVN',
                        // required: paymentSettings.required,
                        errorMessage: 'Invalid Card CVN',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return String(value).length > 0;

                        }
                    },
                }]
            });



            //////////////////////////////////////////////////////////

            //If the form is set to allow anonymous or has been set to ask for a specific receipt
            //email then prompt the submitter to type it in
            if (interactionFormSettings.allowAnonymous || interactionFormSettings.askReceipt) {
                paymentCardFields.push({
                    key: '_paymentEmail',
                    type: 'input',
                    templateOptions: {
                        type: 'email',
                        label: 'Receipt Email Address',
                        placeholder: 'Enter an email address for transaction receipt',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                });
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Create the credit card field group
            var cardDetailsField = {
                className: "payment-details",
                key: '_paymentCardDetails',
                fieldGroup: paymentCardFields,
                hideExpression: function($viewValue, $modelValue, scope) {

                    // console.log('Recalculate hide expression');

                    //If the calculated total is exactly 0
                    if ($formScope.vm.model.calculatedTotal === 0) {
                        debugLog('HIDE PAYMENT NO AMOUNT DUE', $formScope.vm.model.calculatedTotal)
                        return true;
                    }

                    if (paymentSettings.allowAlternativePayments && $formScope.vm.model._paymentMethod != 'card') {
                        debugLog('HIDE PAYMENT NOT CARD', $formScope.vm.model._paymentMethod)
                        return true;
                    }
                }
            }

            //////////////////////////////////////////////////////////

            // console.log('SHOW PAYMENT METHODS', paymentSettings.allowAlternativePayments && paymentSettings.paymentMethods && paymentSettings.paymentMethods.length)

            var alternativeMethodsAvailable = _.get(paymentSettings, 'paymentMethods.length');

            if (paymentSettings.allowAlternativePayments && alternativeMethodsAvailable) {


                //Create a method selection widget
                var methodSelection = {
                    className: "payment-method-select",
                    //defaultValue:{},
                    // key:'_paymentMethod',
                    // fieldGroup:cardDetailsField,
                    data: {
                        fields: [cardDetailsField],
                        settings: paymentSettings,
                    },
                    controller: ['$scope', function($scope) {

                        //Payment Settings on scope
                        $scope.settings = paymentSettings;

                        //Options
                        $scope.methodOptions = _.map(paymentSettings.paymentMethods, function(method) {
                            return method;
                        });

                        //Add card at the start
                        $scope.methodOptions.unshift({
                            title: 'Pay with Card',
                            key: 'card',
                        });

                        ////////////////////////////////////////

                        if (!$scope.model._paymentMethod) {
                            console.log('NO PAYMENT METHOD SO SET TO CARD')
                            $scope.model._paymentMethod = 'card';
                        }

                        //Select the first method by default
                        $scope.selected = {
                            method: $scope.methodOptions[0]
                        };

                        $scope.selectMethod = function(method) {

                            console.log('SELECT METHOD', method);
                            $scope.settings.showOptions = false;
                            $scope.selected.method = method;
                            $scope.model._paymentMethod = method.key;

                            // ngModel.$validate();
                            // console.log('SET METHOD', method, $scope.options.runExpressions())
                            // $scope.fc.$setTouched();
                            // $scope.fc.$setValidity('required', validRequired);


                        }
                    }],
                    templateUrl: 'fluro-interaction-form/payment/payment-method.html',
                    hideExpression: function($viewValue, $modelValue, scope) {

                        //If the calculated total is exactly 0
                        if ($formScope.vm.model.calculatedTotal === 0) {
                            debugLog('HIDE BECAUSE CALCULATED TOTAL IS 0');
                            return true;
                        }
                    }
                };

                paymentWrapperFields.push(methodSelection);

            } else {
                //Push the card details
                paymentWrapperFields.push(cardDetailsField);
            }

            //////////////////////////////////////////////////////////

            // $scope.vm.modelFields = $scope.vm.modelFields.concat(paymentWrapperFields);

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            $scope.vm.modelFields.push({
                fieldGroup: paymentWrapperFields,

            });
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Wait for all async promises to resolve

        if (!$scope.promises.length) {
            // console.log('NO PROMISES');
            $scope.promisesResolved = true;
        } else {

            $scope.promisesResolved = false;

            $q.all($scope.promises).then(function() {
                // console.log('Promises have been resolved');
                $scope.promisesResolved = true;

                //updateErrorList();
                //Update the error list
                // $scope.errorList = getAllErrorFields($scope.vm.modelFields);
                // ////////console.log('All promises resolved', $scope.errorList);

                // _.each($scope.errorList, function(field) {
                //     ////////console.log('FIELD', field.templateOptions.label, field.formControl)
                // })

            });
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function getAllErrorFields(array) {


        // field.formControl.$invalid


        return _.chain(array)
            .map(function(field) {
                if (field.fieldGroup && field.fieldGroup.length) {
                    //Get the error fields in a field group
                    return getAllErrorFields(field.fieldGroup);

                } else if (field.data && ((field.data.fields && field.data.fields.length) || (field.data.dataFields && field.data.dataFields) || (field.data.detailFields && field.data.detailFields) || (field.data.replicatedFields && field.data.replicatedFields))) {

                    var combined = [];

                    // field.data.detailFields, //TODO INCLUDE THIS
                    combined = combined.concat(field.data.fields, field.data.dataFields, field.data.detailFields, field.data.replicatedFields);
                    combined = _.compact(combined);

                    //Get the error fields in the combined extra data
                    return getAllErrorFields(combined);
                } else {
                    return field;
                }
            })
            .flatten()
            .value();
    }



    /////////////////////////////////////////////////////////////////


    function submitInteraction() {

        console.log('FORM -> Submit Interaction');

        //Sending
        $scope.vm.state = 'sending';
        // console.log('state.sending');

        var interactionKey = $scope.model.definitionName;
        var interactionDetails = angular.copy($scope.vm.model);


        //console.log('____________________________________');
        //console.log('SUBMIT', interactionKey, interactionDetails)

        /////////////////////////////////////////////////////////

        //Asking for Payment
        var requiresPayment;
        var allowsPayment;

        /////////////////////////////////////////////////////////


        var paymentConfiguration = $scope.model.paymentDetails;


        //Check if we have supplied payment details
        if (paymentConfiguration) {
            requiresPayment = paymentConfiguration.required;
            allowsPayment = paymentConfiguration.allow;
        }

        // ////////console.log('PAYMENT CONFIG', paymentConfiguration)
        // ////////console.log('PAYMENT ALLOWED REQUIRED', requiresPayment, allowsPayment)

        /////////////////////////////////////////////////////////

        //Check if we need a payment
        if (requiresPayment || allowsPayment) {

            console.log('FORM -> Requires Payment', requiresPayment, allowsPayment)

            ////////////////////////////////////

            var paymentDetails = {};

            ////////////////////////////////////

            //Check if we can use alternative payment methods
            if (paymentConfiguration.allowAlternativePayments && paymentConfiguration.paymentMethods) {

                var selectedMethod = interactionDetails._paymentMethod;
                //If the user chose an alternative payment
                if (selectedMethod && selectedMethod != 'card') {

                    //Mark which method we are using as an alternative method
                    paymentDetails.method = selectedMethod;

                    console.log('FORM -> Alternative Payment Method')
                    //////console.log('Have selected payment method other than card')
                    //Skip straight through to process the request
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Only stop here if we REQUIRE payment
            if (requiresPayment) {
                ////////console.log('TESTING REQUIRES PAYMENT', $formScope);
                //If payment modifiers have removed the need for charging a payment
                if (!$formScope.vm.model.calculatedTotal) {

                    console.log('FORM -> No Payment Total Due')
                    //////console.log('Calculated total is ', 0);
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Get the payment integration 
            var paymentIntegration = $scope.integration;



            /////////////////////////////////////////////////////////

            if (!paymentIntegration || !paymentIntegration.publicDetails) {

                // if (paymentConfiguration.required) {
                //     //////console.log('No payment integration was supplied for this interaction but payment is required');
                // } else {
                //     //////console.log('No payment integration was supplied for this interaction but payments are set to be allowed');
                // }

                alert('This form has not been configured properly. And no payment gateway was defined. Please notify the administrator of this website immediately.')
                $scope.vm.state = 'ready';
                // console.log('state.ready');
                return;
            }

            /////////////////////////////////////////////////////////


            //Ensure we tell the server which integration to use to process payment
            paymentDetails.integration = paymentIntegration._id;


            //Get the credit card details
            var cardDetails = interactionDetails._paymentCardDetails;
            // console.log('Credit Card Details are', cardDetails);

            /////////////////////////////////////////////////////////

            //Check if we are running in a sandbox environment
            var SANDBOX_ENV = _.get(paymentIntegration, 'publicDetails.sandbox') || _.get(window, 'applicationData.sandbox');

            //Add sandbox to the interaction submission
            paymentDetails.sandbox = SANDBOX_ENV;

            /////////////////////////////////////////////////////////

            console.log('CHECK PAYMENT MODULE', paymentIntegration);
            //Now get the required details for making the transaction
            switch (paymentIntegration.module) {
                case 'eway':

                    //console.log('EWAY PAYMENT')

                    if (!window.eCrypt) {
                        console.log('ERROR: Eway is selected for payment but the eCrypt script has not been included in this application visit https://eway.io/api-v3/#encrypt-function for more information');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    //Get encrypted token from eWay
                    //var liveUrl = 'https://api.ewaypayments.com/DirectPayment.json';
                    //var sandboxUrl = 'https://api.sandbox.ewaypayments.com/DirectPayment.json';

                    /////////////////////////////////////////////

                    //Get the Public Encryption Key
                    var key = paymentIntegration.publicDetails.publicKey;

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var ewayCardDetails = {};
                    ewayCardDetails.name = cardDetails._paymentCardName;
                    ewayCardDetails.number = eCrypt.encryptValue(cardDetails._paymentCardNumber, key);
                    ewayCardDetails.cvc = eCrypt.encryptValue(cardDetails._paymentCardCVN, key);

                    var expiryMonth = String(cardDetails._paymentCardExpMonth);
                    var expiryYear = String(cardDetails._paymentCardExpYear);

                    if (expiryMonth.length < 1) {
                        expiryMonth = '0' + expiryMonth;
                    }
                    ewayCardDetails.exp_month = expiryMonth;
                    ewayCardDetails.exp_year = expiryYear.slice(-2);

                    //Send encrypted details to the server
                    paymentDetails.details = ewayCardDetails;

                    //Process the request
                    return processRequest();

                    break;

                case 'tithely':


                    //console.log('STRIPE PAYMENT')

                    if (!window.Tithely) {
                        console.log('ERROR: Tithely is selected for payment but the Tithly API has not been included in this application');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    // console.log('PAYMENT INTEGRATION', paymentIntegration);

                    // //Get encrypted token from Stripe
                    // var liveKey = paymentIntegration.publicDetails.publicKey;
                    // var sandboxKey = paymentIntegration.publicDetails.publicKey;

                    // /////////////////////////////////////////////

                    // if (paymentIntegration.publicDetails.sandbox) {
                    //     key = sandboxKey;
                    // } else {
                    //     key = liveKey;
                    // }

                    // /////////////////////////////////////////////

                    // //Set the stripe key

                    // var tithely = new Tithely(key);

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var tithelyCardDetails = {};
                    tithelyCardDetails.card_name = cardDetails._paymentCardName;
                    tithelyCardDetails.card_number = cardDetails._paymentCardNumber;
                    tithelyCardDetails.card_cvc = cardDetails._paymentCardCVN;
                    tithelyCardDetails.card_expiry_month = cardDetails._paymentCardExpMonth;
                    tithelyCardDetails.card_expiry_year = cardDetails._paymentCardExpYear;

                    /////////////////////////////////////////////


                    //console.log('Tithely card', tithelyCardDetails);
                    /////////////////////////////////////////////

                    // Tokenize card
                    tithelyController.add_payment_method(tithelyCardDetails, tokenCreated);

                    /////////////////////////////////////////////

                    function tokenCreated(status, response) {
                        $timeout(function() {
                            if (response.error) {
                                // Error handling here
                                console.log('Tithely token error', response);
                                $scope.processErrorMessages = [response.error.message];
                                $scope.vm.state = 'error';
                                // console.warn(response.error.message);
                            } else {

                                console.log('Got tithely token!', response);
                                paymentDetails.details = response;
                                return processRequest();
                            }
                        })
                    }
                    break;
                case 'stripe':




                    if (!window.Stripe) {
                        console.log('ERROR: Stripe is selected for payment but the Stripe API has not been included in this application');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    console.log('FORM -> Send Request to Stripe');

                    //Get encrypted token from Stripe
                    var liveKey = paymentIntegration.publicDetails.livePublicKey;
                    var sandboxKey = paymentIntegration.publicDetails.testPublicKey;

                    //Use the Live key by default
                    var key = liveKey;

                    /////////////////////////////////////////////


                    if (SANDBOX_ENV) {
                        key = sandboxKey;
                        console.log('FORM -> Tokenizing using test key')
                    } else {
                        console.log('FORM -> Tokenizing using live key')
                    }

                    /////////////////////////////////////////////

                    //Set the stripe key
                    Stripe.setPublishableKey(key);

                    /////////////////////////////////////////////


                    //Get the card details from our form
                    var stripeCardDetails = {};
                    stripeCardDetails.name = cardDetails._paymentCardName;
                    stripeCardDetails.number = cardDetails._paymentCardNumber;
                    stripeCardDetails.cvc = cardDetails._paymentCardCVN;
                    stripeCardDetails.exp_month = cardDetails._paymentCardExpMonth;
                    stripeCardDetails.exp_year = cardDetails._paymentCardExpYear;

                    /////////////////////////////////////////////

                    Stripe.card.createToken(stripeCardDetails, function(status, response) {

                        $timeout(function() {
                            // console.log('TESTING', status, response);
                            if (response.error) {
                                //Error creating token
                                // Notifications.error(response.error);
                                console.log('FORM -> Stripe token error', response);
                                $scope.processErrorMessages = [response.error.message];




                                $scope.vm.state = 'error';
                                // console.log('state.error');


                            } else {
                                //Include the payment details
                                console.log('FORM -> Stripe tokenized')
                                paymentDetails.details = response;
                                return processRequest();
                            }

                        })
                    });
                    break;
                default:
                    console.log('FORM -> NO VALID PAYMENT MODULE PROVIDED')
                    break;
            }
        } else {
            console.log('FORM -> No payment required')
            return processRequest();
        }


        ///////////////////////////////////////////////////////////////////////

        function processRequest() {
            console.log('FORM -> Process Request')

            /////////////////////////////////////////////////////////


            //Add the transaction email  details
            var receiptEmail = _.get(interactionDetails, '_paymentCardDetails._paymentEmail');

            if (receiptEmail && receiptEmail.length) {
                console.log('FORM -> Setting payment receipt email address', receiptEmail);
                paymentDetails.email = receiptEmail;
            }

            /////////////////////////////////////////////////////////

            //Delete payment details (we don't send these to fluro)
            delete interactionDetails._paymentCardDetails;
            // console.log('Delete payment details', interactionDetails)

            /////////////////////////////////////////////////////////

            //Log the request
            //////////console.log('Process request', interactionKey, interactionDetails, paymentDetails);

            /////////////////////////////////////////////////////////

            //Allow user specified payment
            if (interactionDetails._paymentAmount) {
                paymentDetails.amount = (parseFloat(interactionDetails._paymentAmount) * 100);
            }

            /////////////////////////////////////////////////////////

            //Explicitly pass through the account we want to use the definition for
            var definitionID = $scope.model._id;
            if (definitionID._id) {
                //ensure its just a simple string and not an object
                definitionID = definitionID._id;
            }

            /////////////////////////////////////////////////////////

            //Query string parameters
            var params = {
                definition: definitionID,
            }
            // console.log('PAYMENT DETAILS', paymentDetails);

            if ($scope.linkedProcess) {
                params.process = $scope.linkedProcess;
            }
            // return submissionFail('On purpose');

            //Attempt to send information to interact endpoint

            // console.log('SEND THE THING', interactionDetails);
            // return submissionFail('On purpose');
            
            var request = FluroInteraction.interact($scope.model.title, interactionKey, interactionDetails, paymentDetails, $scope.linkedEvent, params);


            //////////////////////////////////

            //When the promise results fire the callbacks
            request.then(submissionSuccess, submissionFail)

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////

            function submissionSuccess(res) {

                console.log('FORM -> Submission Success');
                /**
                //TESTING
                $scope.vm.state = 'ready';
                return ////////console.log('RES TEST', res);
                /**/

                ///////////////////////////////////

                if ($scope.callback) {
                    $scope.callback(res);
                }

                ///////////////////////////////////

                //Reset
                if ($scope.vm.defaultModel) {
                    $scope.vm.model = angular.copy($scope.vm.defaultModel);
                } else {
                    $scope.vm.model = {};
                }


                $scope.vm.modelForm.$setPristine();
                $scope.vm.options.resetModel();

                //Reset the form scope
                $formScope = $scope;

                // $scope.vm.model = {}
                // $scope.vm.modelForm.$setPristine();
                // $scope.vm.options.resetModel();

                //Response from server incase we want to use it on the thank you page
                $scope.response = res;

                //Change state
                $scope.vm.state = 'complete';
                // console.log('state.complete');
            }

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////


            function submissionFail(res) {

                console.log('FORM -> Submission Failed', res);
                ////////console.log('Interaction Failed', res);
                // Notifications.error(res.data);

                $scope.vm.state = 'error';
                // console.log('state.error');

                if (!res.data) {
                    return $scope.processErrorMessages = ['Error: ' + res];
                }

                if (res.data.error) {
                    if (res.data.error.message) {
                        return $scope.processErrorMessages = [res.error.message];
                    } else {
                        return $scope.processErrorMessages = [res.error];
                    }
                }

                if (res.data.errors) {
                    return $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                        return error.message;
                    });
                }

                if (_.isArray(res.data)) {
                    return $scope.processErrorMessages = res.data;
                } else {
                    $scope.processErrorMessages = [res.data];
                }



                //$scope.vm.state = 'ready';
            }


        }
    }

});
////////////////////////////////////////////////////////////////////////

app.directive('postForm', function($compile) {
    return {
        restrict: 'E',
        //replace: true,
        scope: {
            model: '=ngModel',
            host: '=hostId',
            reply: '=?reply',
            thread: '=?thread',
            userStore: '=?user',
            vm: '=?config',
            debugMode: '=?debugMode',
            callback: '=?callback',
        },
        transclude: true,
        controller: 'PostFormController',
        templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        },
    };
});


app.directive('recaptchaRender', function($window) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attrs, $ctrl) {

            //Check if we need to use recaptcha
            if ($scope.model.data && $scope.model.data.recaptcha) {

                //Recaptcha
                var element = $element[0];

                ////////////////////////////////////////////////

                var cancelWatch;

                //If recaptcha hasn't loaded yet wait for it to load
                if (window.grecaptcha) {
                    activateRecaptcha(window.grecaptcha)
                } else {

                    //Listen for when recaptcha exists
                    cancelWatch = $scope.$watch(function() {
                        return window.grecaptcha;
                    }, activateRecaptcha);
                }

                ////////////////////////////////////////////////

                function activateRecaptcha(recaptcha) {

                    console.log('Activate recaptcha!!');
                    if (cancelWatch) {
                        cancelWatch();
                    }

                    if (recaptcha) {
                        $scope.vm.recaptchaID = recaptcha.render(element, {
                            sitekey: '6LelOyUTAAAAADSACojokFPhb9AIzvrbGXyd-33z'
                        });
                    }
                }
            }

            ////////////////////////////////////////////////

        },
    };
});

app.controller('PostFormController', function($scope, $rootScope, $timeout, $q, $http, Fluro, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {





    /////////////////////////////////////////////////////////////////

    if (!$scope.thread) {
        $scope.thread = [];
    }

    /////////////////////////////////////////////////////////////////

    if (!$scope.vm) {
        $scope.vm = {}
    }
    /////////////////////////////////////////////////////////////////
    //Attach unique ID of this forms scope
    // $scope.vm.formScopeID = $scope.$id;




    //Resolve promises by default
    $scope.promisesResolved = true;
    $scope.correctPermissions = true;

    /////////////////////////////////////////////////////////////////

    // The model object that we reference
    // on the  element in index.html
    if ($scope.vm.defaultModel) {
        $scope.vm.model = angular.copy($scope.vm.defaultModel);
    } else {
        $scope.vm.model = {};
    }

    /////////////////////////////////////////////////////////////////

    // An array of our form fields with configuration
    // and options set. We make reference to this in
    // the 'fields' attribute on the  element
    $scope.vm.modelFields = [];

    /////////////////////////////////////////////////////////////////

    //Keep track of the state of the form
    $scope.vm.state = 'ready';


    /////////////////////////////////////////////////////////////////

    //  $scope.$watch('vm.modelForm', function(form) {
    //     console.log('Form Validation', form);
    // }, true)

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.readyToSubmit = false;

    // $scope.$watch('vm.modelForm.$invalid', updateErrorList)
    $scope.$watch('vm.modelFields', validateAllFields, true)
    $scope.$watch('vm.model', validateAllFields, true)
    $scope.$watch('model', validateAllFields)


/////////////////////////////////////////////////////////////////

    // $scope.$watch('vm.modelFields', function(fields) {
    //     //console.log('Interaction Fields changed')
    //     $scope.errorList = getAllErrorFields(fields);

    //     console.log('Error List', $scope.errorList);
    // }, true)

    // $scope.$watch('vm.modelForm.$touched', updateErrorList)
    // $scope.$watch('vm.modelForm.$error.required', updateErrorList)
    // $scope.$watch('vm.modelForm.$error.validInput', updateErrorList)

    var timer;

    function validateAllFields() {

        //Debounce the validation
        if(timer) {
            $timeout.cancel(timer);
            timer = null;
        }

        //Create a timer
        timer = $timeout(function() {

            //Clear the timer
            timer = null;

            
            /////////////////////////////////////

            function setValid(ready, list) {
                $scope.readyToSubmit = ready;

                if(!ready) {
                    $scope.errorList = list;
                } else {
                    $scope.errorList = null;
                }
            }

            /////////////////////////////////////

            function mapRecursiveField(field) {
                if (!field) {
                    return;
                }

                var output = [field];
                /////////////////////////////////////////////////////

                var isDisabled = _.get(field, 'templateOptions.definition.disableValidation');

                if (isDisabled) {
                    // console.log('FAILED', field.key, isDisabled)
                    return;
                }


                /////////////////////////////////////////////////////


                if (field.fields && field.fields.length) {
                    output.push(_.map(field.fields, mapRecursiveField));
                }

                if (field.fieldGroup && field.fieldGroup.length) {
                    output.push(_.map(field.fieldGroup, mapRecursiveField));
                }

                if (field.data && field.data.replicatedFields && field.data.replicatedFields.length) {

                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                } else {
                    if (field.data && field.data.fields && field.data.fields.length) {
                        output.push(_.map(field.data.fields, mapRecursiveField));
                    }
                }

                return output;
            }

            ////////////////////////////////////////////

            //Set the error list
            var errorList = _.chain($scope.vm.modelFields)
                .map(mapRecursiveField)
                .flattenDeep()
                .compact()
                .filter(function(field) {

                    /////////////////////////////////////////////////////

                    var required = _.get(field, 'formControl.$error.required');
                    var invalid = _.get(field, 'formControl.$error.validInput');
                    var uploadInProgress = _.get(field, 'formControl.$error.uploadInProgress');

                    /////////////////////////////////////////////////////

                    var shouldwarning = (required || invalid || uploadInProgress);
                    // if (shouldwarning) {
                        // console.log('Not valid > ', shouldwarning, field)
                    // }

                    /////////////////////////////////////////////////////

                    return shouldwarning;
                })
                .value();

            ////////////////////////////////////////////

            //Interaction Form
            var interactionForm = $scope.vm.modelForm;

            if (!interactionForm) {
                // console.log('Invalid no form')
                return setValid(false, errorList);
            }

            //If the form is invalid but we dont have any errors in our list
            if (interactionForm.$invalid) {
                if (!errorList || !errorList.length) {
                    var requiredValues = _.get(interactionForm, '$error.validInput');
                    //We need to display something to the user as to why the form is not valid
                    errorList = _.map(requiredValues, function(f, index) {
                        return {
                            id: index,
                            message: f.$viewValue + ' is an invalid value for ' + f.$name,
                        }
                    })
                }
                
                return setValid(false, errorList);
            }

            if (interactionForm.$error) {
                if (interactionForm.$error.required && interactionForm.$error.required.length) {
                    // console.log('required input not provided');
                    return setValid(false, errorList);
                }

                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) {
                    // console.log('valid input not provided');
                    return setValid(false, errorList);
                }

                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) {
                    // console.log('Upload has not finished');
                    return setValid(false, errorList);
                }
            }

            //It all worked so set to true
            setValid(true, errorList);
        }, 200)

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    formlyValidationMessages.addStringMessage('required', 'This field is required');

    /*
    formlyValidationMessages.messages.required = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is required';
    }
    */

    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid value';
    }

    formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid date';
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function resetCaptcha() {


        //Recaptcha ID
        var recaptchaID = $scope.vm.recaptchaID;

        console.log('Reset Captcha', recaptchaID);

        if (window.grecaptcha && recaptchaID) {
            window.grecaptcha.reset(recaptchaID);
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.reset = function() {

        //Reset
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }
        $scope.vm.modelForm.$setPristine();
        $scope.vm.options.resetModel();

        //Reset the captcha
        resetCaptcha();


        //Clear the response from previous submission
        $scope.response = null;
        $scope.vm.state = 'ready';

        //Reset after state change
        console.log('Broadcast reset')
        $scope.$broadcast('form-reset');

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    //Function to run on permissions
    // function checkPermissions() {
    //     if ($rootScope.user) {
    //         //Check if we have the correct permissions
    //         var canCreate = FluroAccess.can('create', $scope.model.definitionName);
    //         var canSubmit = FluroAccess.can('submit', $scope.model.definitionName);

    //         //Allow if the user can create or submit
    //         $scope.correctPermissions = (canCreate | canSubmit);
    //     } else {
    //         //Just do this by default
    //         $scope.correctPermissions = true;
    //     }
    // }

    // /////////////////////////////////////////////////////////////////

    // //Watch if user login changes
    // $scope.$watch(function() {
    //     return $rootScope.user;
    // }, checkPermissions)

    /////////////////////////////////////////////////////////////////

    $scope.$watch('model', function(newData, oldData) {

        // console.log('Model changed');
        if (!$scope.model || $scope.model.parentType != 'post') {
            return; //$scope.model = {};
        }

        /////////////////////////////////////////////////////////////////

        //check if we have the correct permissions
        // checkPermissions();

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        // The model object that we reference
        // on the  element in index.html
        // $scope.vm.model = {};
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }


        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        $scope.vm.modelFields = [];

        /////////////////////////////////////////////////////////////////

        //Keep track of the state of the form
        $scope.vm.state = 'ready';

        /////////////////////////////////////////////////////////////////

        //Add the submit function
        $scope.vm.onSubmit = submitPost;

        /////////////////////////////////////////////////////////////////

        //Keep track of any async promises we need to wait for
        $scope.promises = [];

        /////////////////////////////////////////////////////////////////

        //Submit is finished
        $scope.submitLabel = 'Submit';

        if ($scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length) {
            $scope.submitLabel = $scope.model.data.submitLabel;
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details
        var interactionFormSettings = $scope.model.data;

        if (!interactionFormSettings) {
            interactionFormSettings = {};
        }

        /////////////////////////////////////////////////////////////////
        /**/
        //Email Address
        // // if (interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {
        // var newField = {
        //     key: 'body',
        //     type: 'textarea',
        //     templateOptions: {
        //         // type: 'email',
        //         label: 'Body',
        //         placeholder: 'Enter your comment here',
        //         required: true,
        //         // required: interactionFormSettings.requireEmail,
        //         onBlur: 'to.focused=false',
        //         onFocus: 'to.focused=true',
        //         rows: 4,
        //         cols: 15
        //     },
        // }

        //Push the body
        // $scope.vm.modelFields.push(newField);
        // }
        /**/

        /////////////////////////////////////////////////////////////////

        //Push the extra data object
        // var dataObject = {
        //     key: 'data',
        //     type:'nested',
        //     fieldGroup: [],
        //     // templateOptions:{
        //     //     baseDefaultValue:{}
        //     // }
        // }


        // $scope.vm.modelFields.push(dataObject);

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        function addFieldDefinition(array, fieldDefinition) {

            if (fieldDefinition.params && fieldDefinition.params.disableWebform) {
                //If we are hiding this field then just do nothing and return here
                return;
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Create a new field
            var newField = {};
            newField.key = fieldDefinition.key;

            /////////////////////////////

            //Add the class name if applicable
            if (fieldDefinition.className) {
                newField.className = fieldDefinition.className;
            }

            /////////////////////////////

            //Template Options
            var templateOptions = {};
            templateOptions.type = 'text';
            templateOptions.label = fieldDefinition.title;
            templateOptions.description = fieldDefinition.description;
            templateOptions.params = fieldDefinition.params;

            //Attach a custom error message
            if (fieldDefinition.errorMessage) {
                templateOptions.errorMessage = fieldDefinition.errorMessage;
            }

            //Include the definition itself
            templateOptions.definition = fieldDefinition;

            /////////////////////////////

            //Add a placeholder
            if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                templateOptions.placeholder = fieldDefinition.placeholder;
            } else if (fieldDefinition.description && fieldDefinition.description.length) {
                templateOptions.placeholder = fieldDefinition.description;
            } else {
                templateOptions.placeholder = fieldDefinition.title;
            }

            /////////////////////////////

            //Require if minimum is greater than 1 and not a field group
            templateOptions.required = (fieldDefinition.minimum > 0);

            
           
           

            /////////////////////////////

            templateOptions.onBlur = 'to.focused=false';
            templateOptions.onFocus = 'to.focused=true';




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Allowed Options

            switch (fieldDefinition.type) {

                case 'reference':
                    //If we have allowed references specified
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                        templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id,
                            }
                        });
                    } else {
                        //We want to load all the options from the server
                        templateOptions.options = [];

                        if (fieldDefinition.sourceQuery) {

                            //We use the query to find all the references we can find
                            var queryId = fieldDefinition.sourceQuery;
                            if (queryId._id) {
                                queryId = queryId._id;
                            }

                            /////////////////////////

                            var options = {};

                            //If we need to template the query
                            if (fieldDefinition.queryTemplate) {
                                options.template = fieldDefinition.queryTemplate;
                                if (options.template._id) {
                                    options.template = options.template._id;
                                }
                            }

                            /////////////////////////

                            //Now retrieve the query
                            var promise = FluroContentRetrieval.getQuery(queryId, options);

                            //Now get the results from the query
                            promise.then(function(res) {
                                //console.log('Options', res);
                                templateOptions.options = _.map(res, function(ref) {
                                    return {
                                        name: ref.title,
                                        value: ref._id,
                                    }
                                })
                            });
                        } else {

                            if (fieldDefinition.directive != 'embedded') {
                                if (fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length) {
                                    //We want to load all the possible references we can select
                                    FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                        templateOptions.options = _.map(res, function(ref) {
                                            return {
                                                name: ref.title,
                                                value: ref._id,
                                            }
                                        })
                                    });
                                }
                            }
                        }
                    }
                    break;
                default:
                    //Just list the options specified
                    if (fieldDefinition.options && fieldDefinition.options.length) {
                        templateOptions.options = fieldDefinition.options;
                    } else {
                        templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                            return {
                                name: val,
                                value: val
                            }
                        });
                    }
                    break;
            }



            /////////////////////////////

            //Directive or widget
            switch (fieldDefinition.directive) {
                case 'reference-select':

                    switch(window.DEVICE_PLATFORM) {
                        case 'ios':
                        case 'android':
                            //Use the reference selector
                            newField.type = 'reference-select';
                        break;
                        default:
                            
                            newField.type = 'select';
                        break;
                    }
                break;
                case 'value-select':
                    newField.type = 'button-select';
                break;
                case 'select':
                    newField.type = 'select';
                    break;
                case 'wysiwyg':
                    newField.type = 'textarea';
                    break;
                default:
                    newField.type = fieldDefinition.directive;
                    break;
            }




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //If there is custom attributes
            if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length) {
                newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = 'customAttr' + key;
                    results[customKey] = {
                        attribute: key
                    };

                    //Custom Key
                    templateOptions[customKey] = attr;

                    return results;
                }, {});
            }


            /////////////////////////////


            //Allow for multiple select
            if(newField.type == 'select') {
                if (!newField.ngModelAttrs) {
                    newField.ngModelAttrs = {};
                }

                if(fieldDefinition.maximum != 1) {
                    newField.ngModelAttrs.multiple = {value:'multiple'};
                }
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //What kind of data type, override for things like checkbox
            //if (fieldDefinition.type == 'boolean') {
            if (fieldDefinition.directive != 'custom') {
                switch (fieldDefinition.type) {
                    case 'boolean':
                        if (fieldDefinition.params && fieldDefinition.params.storeCopy) {
                            newField.type = 'terms';
                        } else {
                            newField.type = 'checkbox';
                        }

                        break;
                    break;
                    case 'number':
                    case 'float':
                    case 'integer':
                    case 'decimal':
                        templateOptions.type = 'input';
                        // templateOptions.step = 'any';

                        if (!newField.ngModelAttrs) {
                            newField.ngModelAttrs = {};
                        }

                        /////////////////////////////////////////////

                        //Only do this if its an integer cos iOS SUCKS!
                        if (fieldDefinition.type == 'integer') {
                            // console.log('Is integer');

                            templateOptions.type = 'number';
                            templateOptions.baseDefaultValue = 0;
                            //Force numeric keyboard
                            newField.ngModelAttrs.customAttrpattern = {
                                attribute: 'pattern',
                            }

                            newField.ngModelAttrs.customAttrinputmode = {
                                attribute: 'inputmode',
                            }

                            //Force numeric keyboard
                            templateOptions.customAttrpattern = "[0-9]*";
                            templateOptions.customAttrinputmode = "numeric"


                            /////////////////////////////////////////////

                            // console.log('SET NUMERICINPUT')

                            if (fieldDefinition.params) {
                                if (parseInt(fieldDefinition.params.maxValue) !== 0) {
                                    templateOptions.max = fieldDefinition.params.maxValue;
                                }

                                if (parseInt(fieldDefinition.params.minValue) !== 0) {
                                    templateOptions.min = fieldDefinition.params.minValue;
                                } else {
                                    templateOptions.min = 0;
                                }
                            }

                        }
                        break;
                }

            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Default Options

            if (fieldDefinition.maximum == 1) {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0];
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id;
                        }
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]);
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0];
                        }
                    }
                }
            } else {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {
                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences;
                        } else {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                return ref._id;
                            });
                        }
                    } else {
                        templateOptions.baseDefaultValue = [];
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                                return Number(val);
                            });
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues;
                        }
                    }
                }
            }


            /////////////////////////////

            //Append the template options
            newField.templateOptions = templateOptions;

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            newField.validators = {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;

                    if (!value) {
                        return true;
                    }


                    var valid = FluroValidate.validate(value, fieldDefinition);

                    if (!valid) {
                        //console.log('Check validation', fieldDefinition.title, value)
                    }
                    return valid;
                }
            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////


            if (fieldDefinition.directive == 'embedded') {
                newField.type = 'embedded';

                //Check if its an array or an object
                if (fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                    templateOptions.baseDefaultValue = {
                        data: {}
                    };
                } else {

                    var askCount = 0;

                    if (fieldDefinition.askCount) {
                        askCount = fieldDefinition.askCount;
                    }

                    //console.log('ASK COUNT PLEASE', askCount);

                    //////////////////////////////////////

                    if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                        askCount = fieldDefinition.minimum;
                    }

                    if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                        askCount = fieldDefinition.maximum;
                    }

                    //////////////////////////////////////

                    var initialArray = [];

                    //Fill with the asking amount of objects
                    if (askCount) {
                        _.times(askCount, function() {
                            initialArray.push({});
                        });
                    }

                    //console.log('initial array', initialArray);
                    //Now set the default value
                    templateOptions.baseDefaultValue = initialArray;
                }

                //////////////////////////////////////////

                //Create the new data object to store the fields
                newField.data = {
                    fields: [],
                    dataFields: [],
                    replicatedFields: []
                }

                //////////////////////////////////////////

                //Link to the definition of this nested object
                var fieldContainer = newField.data.fields;
                var dataFieldContainer = newField.data.dataFields;


                //////////////////////////////////////////

                //Loop through each sub field inside a group
                if (fieldDefinition.fields && fieldDefinition.fields.length) {
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }

                //////////////////////////////////////////

                var promise = FluroContent.endpoint('defined/' + fieldDefinition.params.restrictType).get().$promise;


                promise.then(function(embeddedDefinition) {

                    //Now loop through and all all the embedded definition fields
                    if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                        var childFields = embeddedDefinition.fields;

                        //Exclude all specified fields
                        if (fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length) {
                            childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            });
                        }

                        // console.log('EXCLUSIONS', fieldDefinition.params.excludeKeys, childFields);
                        //Loop through each sub field inside a group
                        _.each(childFields, function(sub) {
                            addFieldDefinition(dataFieldContainer, sub);
                        })
                    }
                });

                //////////////////////////////////////////

                //Keep track of the promise
                $scope.promises.push(promise);

                //////////////////////////////////////////

                // //Need to keep it dynamic so we know when its done
                // newField.expressionProperties = {
                //     'templateOptions.embedded': function() {
                //         return promise;
                //     }
                // }
            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            if (fieldDefinition.type == 'group' && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                var fieldContainer;

                if (fieldDefinition.asObject) {

                    /*
                    newField = {
                        type: 'nested',
                        className: fieldDefinition.className,
                        data: {
                            fields: []
                        }
                    }
                    */
                    newField.type = 'nested';

                    //Check if its an array or an object
                    if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                        templateOptions.baseDefaultValue = {};
                    } else {

                        var askCount = 0;

                        if (fieldDefinition.askCount) {
                            askCount = fieldDefinition.askCount;
                        }

                        //////////////////////////////////////

                        if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                            askCount = fieldDefinition.minimum;
                        }

                        if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                            askCount = fieldDefinition.maximum;
                        }

                        //////////////////////////////////////

                        var initialArray = [];

                        //Fill with the asking amount of objects
                        if (askCount) {
                            _.times(askCount, function() {
                                initialArray.push({});
                            });
                        }

                        // console.log('initial array', initialArray);
                        //Now set the default value
                        templateOptions.baseDefaultValue = initialArray;
                    }

                    newField.data = {
                        fields: [],
                        replicatedFields: [],
                    }

                    //Link to the definition of this nested object
                    fieldContainer = newField.data.fields;

                } else {
                    //Start again
                    newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className,
                    }

                    //Link to the sub fields
                    fieldContainer = newField.fieldGroup;
                }

                //Loop through each sub field inside a group
                _.each(fieldDefinition.fields, function(sub) {
                    addFieldDefinition(fieldContainer, sub);
                });
            }

            /////////////////////////////

            //Check if there are any expressions added to this field


            if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {

                //Include Expression Properties
                // if (!newField.expressionProperties) {
                //     newField.expressionProperties = {};
                // }

                //////////////////////////////////////////

                //Add the hide expression if added through another method
                if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {
                    fieldDefinition.expressions.hide = fieldDefinition.hideExpression;
                }

                //////////////////////////////////////////

                //Get all expressions and join them together so we just listen once
                var allExpressions = _.values(fieldDefinition.expressions).join('+');

                //////////////////////////////////////////

                //Now create a watcher
                newField.watcher = {
                    expression: function(field, scope) {
                        //Return the result
                        return $parse(allExpressions)(scope);
                    },
                    listener: function(field, newValue, oldValue, scope, stopWatching) {

                        //Parse the expression on the root scope vm
                        if (!scope.interaction) {
                            scope.interaction = $scope.vm.model;
                        }

                        //Loop through each expression that needs to be evaluated
                        _.each(fieldDefinition.expressions, function(expression, key) {

                            //Get the value
                            var retrievedValue = $parse(expression)(scope);

                            //Get the field key
                            var fieldKey = field.key;

                            ///////////////////////////////////////

                            switch (key) {
                                case 'defaultValue':
                                    if (!field.formControl || !field.formControl.$dirty) {
                                        return scope.model[fieldKey] = retrievedValue;
                                    }
                                    break;
                                case 'value':
                                    return scope.model[fieldKey] = retrievedValue;
                                    break;
                                case 'required':
                                    return field.templateOptions.required = retrievedValue;
                                    break;
                                case 'hide':
                                    return field.hide = retrievedValue;
                                    break;
                                    // case 'label':
                                    //     if(retrievedValue) {
                                    //         var string = String(retrievedValue);
                                    //         return field.templateOptions.label = String(retrievedValue);
                                    //     }
                                    //     break;
                            }

                        });

                    }



                    ///////////////////////////////////////
                }


                //Replace expression
                //var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');



                /*
                    //Add the expression properties
                    newField.expressionProperties[key] = function($viewValue, $modelValue, scope) {


                        //Replace expression
                        var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');

           


                       // var retrievedValue = $parse(replaceExpression)($scope);
                        var retrievedValue = _.get($scope, replaceExpression);

                         console.log('Testing retrieved value from GET', retrievedValue, replaceExpression);

                        ////////////////////////////////////////

                        

                        return retrievedValue;
                    }
                    /**/
                //});
            }

            /////////////////////////////

            if (fieldDefinition.hideExpression) {
                newField.hideExpression = fieldDefinition.hideExpression;
            }

            /////////////////////////////

            if (!newField.fieldGroup) {
                //Create a copy of the default value
                newField.defaultValue = angular.copy(templateOptions.baseDefaultValue);
            }


            /////////////////////////////

            if (newField.type == 'pathlink') {
                return;
            }

            /////////////////////////////
            //Push our new field into the array
            array.push(newField);


        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Loop through each defined field and add it to our form
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        });
    });

    /////////////////////////////////////////////////////////////////


    function getAllErrorFields(array) {
        return _.chain(array).map(function(field) {
                if (field.fieldGroup && field.fieldGroup.length) {

                    return getAllErrorFields(field.fieldGroup);

                } else if (field.data && ((field.data.fields && field.data.fields.length) || (field.data.dataFields && field.data.dataFields) || (field.data.replicatedFields && field.data.replicatedFields))) {
                    var combined = [];
                    combined = combined.concat(field.data.fields, field.data.dataFields, field.data.replicatedFields);
                    combined = _.compact(combined);
                    return getAllErrorFields(combined);
                } else {
                    return field;
                }
            })
            .flatten()
            .value();
    }

    


    /////////////////////////////////////////////////////////////////

    //Submit the
    function submitPost() {

        //Sending
        $scope.vm.state = 'sending';

        var submissionKey = $scope.model.definitionName;
        var submissionModel = {
            data: angular.copy($scope.vm.model)
        }

        /////////////////////////////////////////////////////////

        var hostID = $scope.host;

        /////////////////////////////////////////////////////////

        //If its a reply then mark it as such
        if ($scope.reply) {
            submissionModel.reply = $scope.reply;
        }

        /////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////

        //If we have a recaptcha id present then use it
        if (typeof $scope.vm.recaptchaID !== 'undefined') {
            var response = window.grecaptcha.getResponse($scope.vm.recaptchaID);
            submissionModel['g-recaptcha-response'] = response;
        }

        /////////////////////////////////////////////////////////


        var request;

        //If a user store has been provided
        if ($scope.userStore) {

            //Get the required config
            $scope.userStore.config().then(function(config) {


                var postURL = Fluro.apiURL + '/post/' + hostID + '/' + submissionKey;

                //Make the request using the user stores configuration
                request = $http.post(postURL, submissionModel, config)

                //When the promise results fire the callbacks
                request.then(function(res) {
                    return submissionSuccess(res.data);
                }, function(res) {
                    return submissionFail(res.data);
                })
            });

        } else {

            //Attempt to send information to post endpoint
            request = FluroContent.endpoint('post/' + hostID + '/' + submissionKey).save(submissionModel).$promise;

            //When the promise results fire the callbacks
            request.then(submissionSuccess, submissionFail)
        }

        //////////////////////////////////        

        function submissionSuccess(res) {


            ///////////////////////////////////

            if ($scope.callback) {
                $scope.callback(res);
            }
            ///////////////////////////////////

            //Reset
            if ($scope.vm.defaultModel) {
                $scope.vm.model = angular.copy($scope.vm.defaultModel);
            } else {
                $scope.vm.model = {
                    data: {}
                };
            }
            $scope.vm.modelForm.$setPristine();
            $scope.vm.options.resetModel();

            //Reset the captcha
            resetCaptcha();

            // $scope.vm.model = {}
            // $scope.vm.modelForm.$setPristine();
            // $scope.vm.options.resetModel();

            //Response from server incase we want to use it on the thank you page
            $scope.response = res;

            //If there is a thread push this into it
            if ($scope.thread) {
                $scope.thread.push(res);
            }

            //Change state
            $scope.vm.state = 'complete';
        }

        //////////////////////////////////
        //////////////////////////////////
        //////////////////////////////////
        //////////////////////////////////
        //////////////////////////////////

        function submissionFail(res) {
            $scope.vm.state = 'error';

            if (!res.data) {
                return $scope.processErrorMessages = ['Error: ' + res];
            }

            if (res.data.error) {
                if (res.data.error.message) {
                    return $scope.processErrorMessages = [res.error.message];
                } else {
                    return $scope.processErrorMessages = [res.error];
                }
            }

            if (res.data.errors) {
                return $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                    return error.message;
                });
            }

            if (_.isArray(res.data)) {
                return $scope.processErrorMessages = res.data;
            } else {
                $scope.processErrorMessages = [res.data];
            }
        }
    }

});
app.directive('postThread', function() {
    return {
        restrict: 'E',
        transclude:true,
        scope:{
          definitionName:"=?type",
          host:"=?hostId",
          thread:"=?thread",
        },
        // template:'<div class="post-thread" ng-transclude></div>',
        link:function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function (clone, $scope) {
                $element.replaceWith(clone); // <-- will transclude it's own scope
            });
        },
        controller:function($scope, $filter, $rootScope, FluroContent) {

        	$scope.outer = $scope.$parent;

            if(!$scope.thread) {
                $scope.thread = [];
            }

            //////////////////////////////////////////////////

            var hostID;
            var definitionName;
            
            //////////////////////////////////////////////////

            function reloadThread() {
                console.log('Reload thread', 'post.'+hostID+'.' + definitionName);
                return FluroContent.endpoint('post/' + hostID + '/' + definitionName, true, true)
                .query()
                .$promise
                .then(threadLoaded, threadError);
            }

            //////////////////////////////////////////////////

            function threadLoaded(res) {
                console.log('Thread reloaded', res);
                //All the posts
                var allPosts = res;

                //Break it up into nested threads
                $scope.thread = _.chain(res)
                .map(function(post) {
                    //Find all replies to this post
                    post.thread = _.filter(allPosts, function(sub) {
                        return (sub.reply == post._id);
                    });
                    //If it's a top level post then send it back
                    if(!post.reply) {
                        return post;
                    }
                })
                .compact()
                .value();
            }

            //////////////////////////////////////////////////

            function threadError(err) {
                console.log('Thread Error', err);
                $scope.thread = []
            }

            //////////////////////////////////////////////////
            //////////////////////////////////////////////////

            //Watch for when the host and the definition name is changed/set
        	$scope.$watch('host + definitionName', function() {

                hostID = $scope.host;
                definitionName = $scope.definitionName;

                if(!hostID || !definitionName) {
                    return;
                } 

                ////////////////////////////////////////////////////////////////////////

                //Get the thread refresh event
                var threadRefreshEvent = 'post.'+hostID+'.' + definitionName;

                console.log('LISTENING FOR', threadRefreshEvent);
                //When it's broadcast
                $rootScope.$on(threadRefreshEvent, reloadThread);

                ////////////////////////////////////////////////////////////////////////
                
                return reloadThread();

                ////////////////////////////////////////////////////////////////////////

                
        	})
        },
    }
});
/**/
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'nested',
        templateUrl: 'fluro-interaction-form/nested/fluro-nested.html',
        controller: 'FluroInteractionNestedController',
    });

});

//////////////////////////////////////////////////////////

app.controller('FluroInteractionNestedController', function($scope, $timeout) {


    //Definition
    var def = $scope.to.definition;

    ////////////////////////////////////

    var minimum = def.minimum;
    var maximum = def.maximum;


    ////////////////////////////////////

    $scope.$watch('model[options.key]', function(model) {
        if (!model) {
            //console.log('Reset Model cos no value!')
            resetDefaultValue();
        }
    });

    ////////////////////////////////////


    function resetDefaultValue() {

        var defaultValue = angular.copy($scope.to.baseDefaultValue);
        if(!$scope.model) {
            // console.log('NO RESET Reset Model Values', $scope.options.key, defaultValue);
            return;
        }
        $scope.model[$scope.options.key] = defaultValue;
    }

    ////////////////////////////////////

    //Listen for a reset event
    $scope.$on('form-reset', resetDefaultValue);


    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }


    ////////////////////////////////////

    $scope.addAnother = function() {

        console.log('Add another')
        $scope.model[$scope.options.key].push({id:guid()});
    }


    $scope.removeEntry = function($index) {

        var modelEntries = $scope.model[$scope.options.key];
        // var fieldEntries =  _.get($scope.options, 'data.fields');
        var replicatedEntries =  _.get($scope.options, 'data.replicatedFields');
        var replicatedDataEntries =  _.get($scope.options, 'data.replicatedDataFields');
        var replicatedDetailEntries =  _.get($scope.options, 'data.replicatedDetailFields');

        

        // delete $scope.keepTrack[uuid];


        

        modelEntries.splice($index, 1);
        // fieldEntries.splice($index, 1);
        replicatedEntries.splice($index, 1);
        replicatedDataEntries.splice($index, 1);
        replicatedDetailEntries.splice($index, 1);


        console.log('REMOVE', $index, replicatedEntries);
    }

    ////////////////////////////////////

    $scope.canRemove = function() {
        if (minimum) {
            if ($scope.model[$scope.options.key].length > minimum) {
                return true;
            }
        } else {
            return true;
        }
    }

    ////////////////////////////////////

    $scope.canAdd = function() {
        if (maximum) {
            if ($scope.model[$scope.options.key].length < maximum) {
                return true;
            }
        } else {
            return true;
        }
    }


    $scope.copyFields = function() {

        var copiedFields = angular.copy($scope.options.data.fields);
        $scope.options.data.replicatedFields.push(copiedFields);

        return copiedFields;
    }

    $scope.copyDataFields = function() {
        var copiedFields = angular.copy($scope.options.data.dataFields);
        $scope.options.data.replicatedDataFields.push(copiedFields);
        return copiedFields;
    }

    $scope.copyDetailFields = function() {
        var copiedFields = angular.copy($scope.options.data.detailFields);
        $scope.options.data.replicatedDetailFields.push(copiedFields);
        return copiedFields;
    }
});
/**/
// app.run(function(formlyConfig, $templateCache) {

//     formlyConfig.setType({
//         name: 'person',
//         templateUrl: 'fluro-interaction-form/person/person.html',
//         controller: 'FluroInteractionNestedController',
//         wrapper: ['bootstrapLabel', 'bootstrapHasError'],
//         // defaultOptions: {
//         //     noFormControl: true,
//         //     wrapper: ['bootstrapLabel', 'bootstrapHasError'],
//         //     templateOptions: {
//         //         inputOptions: {
//         //             wrapper: null
//         //         }
//         //     }
//         // },
//     });

// });
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'search-select',
        templateUrl: 'fluro-interaction-form/search-select/fluro-search-select.html',
        controller: 'FluroSearchSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

});

app.controller('FluroSearchSelectController', function($scope, $http, Fluro, $filter, FluroValidate) {


    /////////////////////////////////////////////////////////////////////////

    //Search Object
    $scope.search = {};

    //Proposed value
    $scope.proposed = {}

    /////////////////////////////////////////////////////////////////////////

    var to = $scope.to;
    var opts = $scope.options;

    //Selection Object
    $scope.selection = {};

    /////////////////////////////////////////////////////////////////////////

    //Get the definition
    var definition = $scope.to.definition;


    /////////////////////////////////////////////////////////////////////////

    if (!definition.params) {
        definition.params = {};
    }

    /////////////////////////////////////////////////////////////////////////

    var restrictType = definition.params.restrictType;

    //Add maximum search results
    var searchLimit = definition.params.searchLimit;
    if (!searchLimit) {
        searchLimit = 10;
    }

    /////////////////////////////////////////////////////////////////////////

    //console.log('DEFINITION', definition);

    //Minimum and maximum
    var minimum = definition.minimum;
    var maximum = definition.maximum;

    if (!minimum) {
        minimum = 0;
    }

    if (!maximum) {
        maximim = 0;
    }

    $scope.multiple = (maximum != 1);

    if($scope.multiple) {
        if($scope.model[opts.key] && _.isArray($scope.model[opts.key])) {
            $scope.selection.values = angular.copy($scope.model[opts.key]);
        }
    } else {
        if($scope.model[opts.key]) {
            $scope.selection.value = $scope.model[opts.key];
        }
    }

    /////////////////////////////////////////////////////////////////////////


    $scope.canAddMore = function() {

        if (!maximum) {
            return true;
        }

        if ($scope.multiple) {
            return ($scope.selection.values.length < maximum);
        } else {
            if (!$scope.selection.value) {
                return true;
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////

    $scope.contains = function(value) {
        if ($scope.multiple) {
            //Check if the values are selected
            return _.includes($scope.selection.values, value);
        } else {
            return $scope.selection.value == value;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.$watch('model', function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {

            var modelValue;


            //If there is properties in the FORM model
            if (_.keys(newModelValue).length) {

                //Get the model for this particular field
                modelValue = newModelValue[opts.key];


               

                if ($scope.multiple) {
                    if (modelValue && _.isArray(modelValue)) {
                        $scope.selection.values = angular.copy(modelValue);
                    } else {
                        $scope.selection.values = [];
                    }
                } else {
                    $scope.selection.value = angular.copy(modelValue);
                }
            }
        }
    }, true);

    /////////////////////////////////////////////////////////////////////////

    function setModel() {

        if ($scope.multiple) {
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        } else {
            $scope.model[opts.key] = angular.copy($scope.selection.value);
        }

        if ($scope.fc) {
            $scope.fc.$setTouched();
        }


        checkValidity();
    }

    /////////////////////////////////////////////////////////////////////////

    if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
        $scope.$watch(function() {
            return $scope.to.required;
        }, function(newValue) {
            checkValidity();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    if ($scope.to.required) {
        var unwatchFormControl = $scope.$watch('fc', function(newValue) {
            if (!newValue) {
                return;
            }
            checkValidity();
            unwatchFormControl();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    function checkValidity() {


        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        //Check if multiple
        if ($scope.multiple) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        if ($scope.fc) {

            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.select = function(value) {

        //console.log('SELECT', value)

        if ($scope.multiple) {
            if (!$scope.canAddMore()) {
                return;
            }
            $scope.selection.values.push(value);


        } else {
            $scope.selection.value = value;

        }

        //Clear proposed item
        $scope.proposed = {};

        //Set the model
        setModel();


    }

    /////////////////////////////////////////////////////////

    $scope.retrieveReferenceOptions = function(val) {


        ////////////////////////

        //Create Search Url
        var searchUrl = Fluro.apiURL + '/content';
        if (restrictType) {
            searchUrl += '/' + restrictType;
        }
        searchUrl += '/search';

        ////////////////////////

        return $http.get(searchUrl + '/' + val, {
            ignoreLoadingBar: true,
            params: {
                limit: searchLimit,
            }
        }).then(function(response) {

            //Got the results
            var results = response.data;

            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selection.values, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, []);

        });

    }

    ////////////////////////////////////////////////////////////

    $scope.getValueLabel = function(value) {
        if(definition.options && definition.options.length) {
            var match = _.find(definition.options, {value:value});
            if(match && match.name) {
                return match.name;
            }
        }

        return value;
    }

    ////////////////////////////////////////////////////////////

    $scope.retrieveValueOptions = function(val) {

        if (definition.options && definition.options.length) {

            var options = _.reduce(definition.options, function(results, item) {

                var exists;

                if ($scope.multiple) {
                    exists = _.includes($scope.selection.values, item.value);
                } else {
                    exists = $scope.selection.value == item.value;
                }

                if (!exists) {
                    results.push({
                        name:item.name,
                        value:item.value,
                    });
                }

                return results;
            }, []);


            return $filter('filter')(options, val);

        } else if (definition.allowedValues && definition.allowedValues.length) {

            var options = _.reduce(definition.allowedValues, function(results, allowedValue) {

                var exists;

                if ($scope.multiple) {
                    exists = _.includes($scope.selection.values, allowedValue);
                } else {
                    exists = $scope.selection.value == allowedValue;
                }

                if (!exists) {
                    results.push({
                        name:allowedValue,
                        value:allowedValue,
                    });
                }

                return results;
            }, []);

            console.log('Options', options)

            return $filter('filter')(options, val);
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.deselect = function(value) {
        if ($scope.multiple) {
            _.pull($scope.selection.values, value);
        } else {
            delete $scope.selection.value;
        }

        setModel();
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.toggle = function(reference) {
        if ($scope.contains(reference)) {
            $scope.deselect(reference);
        } else {
            $scope.select(reference);
        }

        //Update model
        //setModel();
    }

})
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'signature',
        templateUrl: 'fluro-interaction-form/signature/signature.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        // defaultOptions: {
        //     noFormControl: true,
        //     wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        //     templateOptions: {
        //         inputOptions: {
        //             wrapper: null
        //         }
        //     }
        // },
    });

});
app.run(function(formlyConfig, $templateCache) {

formlyConfig.setType({
        name: 'upload',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/upload/upload.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            },
            // validators: {
            //     validInput: {
            //         expression: function($viewValue, $modelValue, scope) {

            //             return false;
            //         }
            //     }
            // },
        },
        
        controller: 'FluroInteractionFormUploadController',
    });

});



app.controller('FluroInteractionFormUploadController', function($scope, $http, FluroValidate) {

    $scope.ctrl = {
        fileArray: [],
    };

    /////////////////////////////////////////////////////////////////////////

    var to = $scope.to;
    var opts = $scope.options;

    /////////////////////////////////////////////////////////////////////////

    //Get the definition
    var definition = $scope.to.definition;

    //Minimum and maximum
    var minimum = definition.minimum;
    var maximum = definition.maximum;

    if (!minimum) {
        minimum = 0;
    }

    if (!maximum) {
        maximim = 0;
    }

    $scope.multiple = (maximum != 1);

    //////////////////////////////

    var definition = _.get($scope, 'to.definition');

    /////////////////////////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.ctrl.fileArray, item);

        if (!$scope.ctrl.fileArray || !$scope.ctrl.fileArray.length) {
            var fileElement = angular.element('#' + $scope.options.id + ' input');
            angular.element(fileElement).val(null);
        }

        $scope.ctrl.refresh();
    }

    //////////////////////////////

    //Helper function to refresh and check validity
    $scope.ctrl.refresh = function() {

        /////////////////////////////////////

        var maximum = definition.maximum;

        /////////////////////////////////////

        if (maximum == 1) {


            if ($scope.ctrl.fileArray.length) {

                var firstFile = $scope.ctrl.fileArray[0];
                if (firstFile.state == 'complete') {
                    $scope.model[$scope.options.key] = firstFile.attachmentID;
                } else {
                    $scope.model[$scope.options.key] = firstFile.state;
                }
            } else {
                $scope.model[$scope.options.key] = null;
            }
        } else {

            $scope.model[$scope.options.key] = _.map($scope.ctrl.fileArray, function(fileItem) {

                if (fileItem.state == 'complete') {
                    return fileItem.attachmentID;
                }
                return fileItem.state;
            })
        }


        if ($scope.fc) {
            $scope.fc.$setTouched();
        }

        checkValidity();
    }

    /////////////////////////////////////////////////////////////////////////

    if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
        $scope.$watch(function() {
            return $scope.to.required;
        }, function(newValue) {
            checkValidity();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    if ($scope.to.required) {
        var unwatchFormControl = $scope.$watch('fc', function(newValue) {
            if (!newValue) {
                return;
            }
            checkValidity();
            unwatchFormControl();
        });
    }
    
    //////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////

    function checkValidity() {

        console.log('CHECK VALIDTIY!')
        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        


        //Check if multiple
        if ($scope.multiple) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        // ///////////////////////////////////////////////////

        // var values = $scope.model[opts.key];
        // var hasErrors = _.includes(values, 'error');
        // var hasProcessing = _.includes(values, 'processing');

        // if (hasErrors || hasProcessing) {
        //     validInput = false;
        // }

        ///////////////////////////////////////////////////

        // console.log('CHECK VALID HERE?', $scope.to.required, validInput);


        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }



        /**
    function checkValidity() {



        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        /////////////////////////////////////

        var values = $scope.model[$scope.options.key];

        var hasErrors = _.some(values, 'error');
        var hasProcessing = _.some(values, 'processing');

        if (hasErrors || hasProcessing) {
            validInput = false;
        }

        /////////////////////////////////////

        //Check if multiple
        if (definition.maximum != 1) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        /////////////////////////////////////

        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }
    /**/

    //////////////////////////////

    $scope.filesize = function(fileSizeInBytes) {
        var i = -1;
        var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
    };
});

/////////////////////////////////////

app.directive("fluroFileInput", function(Fluro, $http) {
    return {
        scope: {
            model: "=fileArray",
            definition: '=',
            callback: '=',
        },
        link: function($scope, $element, $attrs) {



            if (!$scope.model) {
                $scope.model = [];
            }

            ///////////////////////////////////////

            $element.bind('change', filesSelected)

            ///////////////////////////////////////

            function filesSelected(changeEvent) {



                //Get files that were selected
                var files = changeEvent.target.files;
                var realmID = _.get($scope.definition, 'params.realm');


                if(realmID) {


                    if(_.isArray(realmID)) {
                        realmID = realmID[0];
                    }

                    if(realmID._id) {
                        realmID = realmID._id;
                    }
                }
                
                console.log('FILE SELECTED', realmID);

                var uploadURL = Fluro.apiURL + '/file/attach/' + realmID;

                ///////////////////////////////

                // $scope.model = _.map(files, function(file) {
                //     return {
                //         lastModified: file.lastModified,
                //         lastModifiedDate: file.lastModifiedDate,
                //         name: file.name,
                //         size: file.size,
                //         type: file.type,
                //     }

                // });


                // if (datas.length === files.length) {
                //         scope.$apply(function() {
                //             scope.fileread = datas;
                //         });
                //     }

                ///////////////////////////////

                $scope.model = _.map(files, function(file) {

                    var output = {}

                    ////////////////////////////////

                    output.state = 'processing';

                    ////////////////////////////////

                    //Create the form data
                    var formData = new FormData();
                    formData.append('file', file);

                    ////////////////////////////////

                    var request = $http({
                        url: uploadURL,
                        method: "POST",
                        data: formData,
                        headers: {
                            'Content-Type': undefined
                        },
                        uploadEventHandlers: {
                            progress: function(e) {
                                $scope.callback();
                                if (e.lengthComputable) {
                                    output.progress = Math.round((e.loaded / e.total) * 100);
                                }
                            }
                        },
                    });

                    ////////////////////////////////

                    output.file = file;
                    output.request = request;

                    ////////////////////////////////

                    request.then(function(res) {
                        output.state = 'complete';
                        output.attachmentID = res.data._id;
                        $scope.callback();
                    }, function(err) {
                        output.state = 'error';

                        $scope.callback();
                    })

                    ////////////////////////////////

                    return output;
                })

                ///////////////////////////////

                $scope.callback();

            }

            ///////////////////////////////////////


            /**
            element.bind("change", function(changeEvent) {

                // console.log('FILE', changeEvent);

                var readers = [];
                var files = changeEvent.target.files;
                var datas = [];

                /////////////////////////////////////////

                if (!files.length) {
                    scope.$apply(function() {
                        scope.fileread = [];
                    });
                    return;
                }


                for (var i = 0; i < files.length; i++) {
                    readers[i] = new FileReader();
                    readers[i].index = i;

                    readers[i].onload = function(loadEvent) {

                        var index = loadEvent.target.index;

                        datas.push({
                            lastModified: files[index].lastModified,
                            lastModifiedDate: files[index].lastModifiedDate,
                            name: files[index].name,
                            size: files[index].size,
                            type: files[index].type,
                            data: loadEvent.target.result
                        });

                        if (datas.length === files.length) {
                            scope.$apply(function() {
                                scope.fileread = datas;
                            });
                        }

                    };
                    readers[i].readAsDataURL(files[i]);
                }
            });
            /**/

        }
    }
});
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'value',
        templateUrl: 'fluro-interaction-form/value/value.html',
        //controller: 'FluroInteractionDobSelectController',
        wrapper: ['bootstrapHasError'],
    });

});

app.directive('preloadImage', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.aspect = angular.isDefined(attrs.aspect) ? scope.$parent.$eval(attrs.aspect) : 0;

            if (scope.aspect) {
                element.wrap('<div class="preload-image-outer aspect-ratio" style="padding-bottom:' + scope.aspect + '%"></div>');
            } else {
                element.wrap('<div class="preload-image-outer"></div>');
            }

            var preloader = angular.element('<span class="image-preloader"><i class="fa fa-spinner fa-spin"/></span>');

            element.on('load', function() {
                element.removeClass('preload-hide');
                element.addClass('preload-show');

                preloader.remove();
            });

            element.on('error', function(err) {
                element.removeClass('preload-hide');
                element.addClass('preload-show');

                preloader.remove();
            });

            if (attrs.ngSrc && attrs.ngSrc.length) {
                element.addClass('preload-hide');
                element.parent().append(preloader);
            }
        }
    };
});


app.directive('preloadBackground', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            //Create a new image element
            var image = new Image();

            //Create the preloader
            var preloader = angular.element('<span class="image-preloader"><i class="fa fa-spinner fa-spin"/></span>');

            //Create the inner image div
            var div = angular.element('<div class="preload-bg-image"></div>');
            
            //Append and setup the original element
            element
            .addClass('preload-bg-outer')
            .append(div);

            //////////////////////////////////////////////

            //Get the background image url
            if (attrs.preloadBackground && attrs.preloadBackground.length) {
                // console.log('PReload background', attrs.preloadBackground);
                image.src = attrs.preloadBackground;
                element.addClass('preload-hide');
                element.append(preloader);
            }

            //////////////////////////////////////////////

            //Once the image has loaded
            image.onload = function(){

                div.css({'backgroundImage': 'url(' + image.src + ')'});
                element.addClass('preload-show');
                element.removeClass('preload-hide');
                preloader.remove();
         };
     }
 };
});

app.directive('fluroPreloader', function() {
    return {
        restrict: 'E',
        replace:true,
        scope:{},
        templateUrl:'fluro-preloader/fluro-preloader.html',
        controller:'FluroPreloaderController',
        link: function(scope, element, attrs) {
            
        }
    };
});

app.controller('FluroPreloaderController', function($scope, $urlRouter, $state, $rootScope, $timeout) {
    

    //////////////////////////////////////////////////////////////////

    var preloadTimer;

    $scope.preloader = {
        class:'reset'
    }

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, error) {

        //If we are actually moving to the state
        if($rootScope.stateChangeBypass) {
            $rootScope.stateChangeBypass = false;
            // console.log('GOT THERE')
            return;
        } else {


            //Stop the normal functionality
            event.preventDefault();
            $rootScope.stateChangeBypass = true;

            //////////////////////////////////////////////////////

            //Add the preloader class
            $scope.preloader.class = 'reset';
            preloadTimer = $timeout(function() {
                $scope.preloader.class = 'loading';
            });


            //////////////////////////////////////////////////////

            console.log('WAIT');

            //Wati a few seconds before actually transitioning
            $timeout(function(){

                console.log('GO')
                $state.go(toState, toParams);
            }, 500);
        }
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

        ////////////////////////////////////////////////////
        //If the preloader wasn't fast enough to start cancel it
        if (preloadTimer) {
            $timeout.cancel(preloadTimer);
            preloadTimer = null;
        }

        //Reset Preloader
        $scope.preloader.class = 'reset';

    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$preloaderHide', hidePreloader);
    $rootScope.$on('$stateChangeSuccess', hidePreloader);

    //Hide the preloader
    function hidePreloader(event, toState, toParams, fromState, fromParams, error) {
            // console.log('hide preloader')
        ////////////////////////////////////////////////////

        //If the preloader wasn't fast enough to start cancel it
        if (preloadTimer) {
            $timeout.cancel(preloadTimer);
            preloadTimer = null;
        }

        //If the preloader did show
        if ($scope.preloader.class == 'loading') {
            //Wait a little bit then hide the preloader
            $timeout(function() {
                // console.log('preloader has loaded');
                $scope.preloader.class = 'loaded';
            }, 600)
        }

    };


    //////////////////////////////////////////////////////////////////

    // $scope.clicked = function($event) {
    //     if($scope.asLink) {
    //         $state.go('watchVideo',{id:$scope.model._id, from:$scope.fromProduct})
    //     }
    // }


});
app.directive('scrollActive', function($compile, $timeout, $window, FluroScrollService) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attrs) {

            ////////////////////////////////////////////////

            var onActive;
            var onBefore;
            var onAfter;
            var onAnchor;

            ////////////////////////////////////////////////

            var currentContext = '';
            var anchored;

            ////////////////////////////////////////////////

            if ($attrs.onActive) {
                onActive = function() {
                    $scope.$eval($attrs.onActive);
                }
            }

            if ($attrs.onAnchor) {
                onAnchor = function() {
                    $scope.$eval($attrs.onAnchor);
                }
            }

            if ($attrs.onAfter) {
                onAfter = function() {
                    $scope.$eval($attrs.onAfter);
                }
            }

            if ($attrs.onBefore) {
                onBefore = function() {
                    $scope.$eval($attrs.onBefore);
                }
            }


            ////////////////////////////////////////////////


            //Check if there is a parent we should be looking at instead of the body
            var parent = $element.closest('[scroll-active-parent]');
            var body = angular.element('body');

            ////////////////////////////////////////////////
            ////////////////////////////////////////////////

            if (parent.length) {
                //Listen for the parent scroll value
                parent.bind("scroll", updateParentScroll);
                $timeout(updateParentScroll, 10);
            } else {
                //Watch for changes to the main scroll value
                $scope.$watch(function() {
                    return FluroScrollService.getScroll();
                }, updateFromMainScroll);

                //Fire one for good measure
                $timeout(updateFromMainScroll, 10);
            }

            ////////////////////////////////////////////////
            ////////////////////////////////////////////////
            ////////////////////////////////////////////////
            ////////////////////////////////////////////////
            ////////////////////////////////////////////////

            function setScrollContext(context) {
                if (currentContext != context) {
                    currentContext = context;

                    $timeout(function() {

                        switch (context) {
                            case 'active':
                                $element.removeClass('scroll-after');
                                $element.removeClass('scroll-before');
                                $element.addClass('scroll-active');
                                $scope.scrollActive = true;
                                $scope.scrollBefore = false;
                                $scope.scrollAfter = false;

                                if (onActive) {
                                    onActive();
                                }

                                break;
                            case 'before':
                                $element.removeClass('scroll-after');
                                $element.addClass('scroll-before');
                                $element.removeClass('scroll-active');
                                $scope.scrollActive = false;
                                $scope.scrollBefore = true;
                                $scope.scrollAfter = false;

                                if (onBefore) {
                                    onBefore();
                                }

                                break;
                            case 'after':
                                $element.addClass('scroll-after');
                                $element.removeClass('scroll-before');
                                $element.removeClass('scroll-active');
                                $scope.scrollActive = false;
                                $scope.scrollBefore = false;
                                $scope.scrollAfter = true;

                                if (onAfter) {
                                    onAfter();
                                }

                                break;
                        }
                    })
                }
            }



            //////////////////////////////////////////////
            //////////////////////////////////////////////

            function updateParentScroll() {


                //Get the scroll value
                var scrollValue = parent.scrollTop();

                /////////////////////////

                //constants
                var viewportHeight = parent.height();
                var contentHeight = parent.get(0).scrollHeight;

                //////////////////////////////////////////////

                //Limits and markers
                var viewportHalf = (viewportHeight / 2);
                var maxScroll = contentHeight - viewportHeight;

                //Scroll
                var startView = 0;
                var endView = startView + viewportHeight;
                var halfView = endView - (viewportHeight / 2);


                /////////////////////////

                //Element Dimensions
                var elementHeight = $element.outerHeight();
                var elementStart = $element.position().top;
                var elementEnd = elementStart + elementHeight;
                var elementHalf = elementStart + (elementHeight / 4);

                ///////////////////////////////////////////////////
                ///////////////////////////////////////////////////

                //If an anchor callback has been specified
                if (onAnchor) {
                    var start = parseInt(startView);
                    var rangeStart = parseInt(elementStart);
                    var rangeEnd = parseInt(elementHalf);
                    
                    console.log(rangeStart, start, rangeEnd);
                    
                    if (start >= rangeStart && start < rangeEnd) {
                        if(!anchored) {
                            anchored = true;
                            if (anchored) {
                                onAnchor();
                            }
                        }
                    } else {
                        anchored = false;
                    }
                }


                ///////////////////////////////////////////////////

                //Check if the entire element is viewable on screen
                var entirelyViewable = (elementStart >= startView) && (elementEnd <= endView);

                ///////////////////////////////////////////////////

                //console.log('Scroll Value', entirelyViewable, scrollValue, halfView);
                if (entirelyViewable) {
                    return setScrollContext('active');
                }

                //Scrolled past the content so set to after
                if (halfView >= elementEnd) {
                    return setScrollContext('after');
                }

                //If element reaches half of the screen viewport
                if (halfView >= elementStart) {
                    return setScrollContext('active');
                }

                //If we reach the end of the page
                if (startView >= (maxScroll - 200)) {
                    return setScrollContext('active');
                }

                //Otherwise we havent reached the element yet
                return setScrollContext('before');



            }

            //////////////////////////////////////////////
            //////////////////////////////////////////////

            function updateFromMainScroll(scrollValue) {


                //constants
                var windowHeight = $window.innerHeight;
                var documentHeight = body.height();

                //////////////////////////////////////////////

                //Limits and markers
                var windowHalf = (windowHeight / 2);
                var maxScroll = documentHeight - windowHeight;

                //Scroll
                var startView = scrollValue;
                if(!startView) {
                    startView = 0;
                }
                var endView = startView + windowHeight;
                var halfView = endView - (windowHeight / 2)

                ///////////////////////////////////////////////////

                //Element
                var elementHeight = $element.outerHeight();
                var elementStart = $element.offset().top;
                var elementEnd = elementStart + elementHeight;
                var elementHalf = elementStart + (elementHeight / 4);

                ///////////////////////////////////////////////////
                ///////////////////////////////////////////////////

                //If an anchor callback has been specified
                if (onAnchor) {
                    var start = parseInt(startView);
                    var rangeStart = parseInt(elementStart);
                    var rangeEnd = parseInt(elementHalf);

                    console.log(rangeStart, start, rangeEnd);
                    
                    if (start >= rangeStart && start < rangeEnd) {
                        if(!anchored) {
                            anchored = true;
                            if (anchored) {
                                onAnchor();
                            }
                        }
                    } else {
                        anchored = false;
                    }
                }
                

                ///////////////////////////////////////////////////

                //Check if the entire element is viewable on screen
                var entirelyViewable = (elementStart >= startView) && (elementEnd <= endView);

                ///////////////////////////////////////////////////

                //console.log('Scroll Value', entirelyViewable, scrollValue, halfView);
                if (entirelyViewable) {
                    return setScrollContext('active');
                }

                //Scrolled past the content so set to after
                if (halfView >= elementEnd) {
                    return setScrollContext('after');
                }

                //If element reaches half of the screen viewport
                if (halfView >= elementStart) {
                    return setScrollContext('active');
                }

                //If we reach the end of the page
                if (startView >= (maxScroll - 200)) {
                    return setScrollContext('active');
                }


                //Otherwise we havent reached the element yet
                return setScrollContext('before');

            }

        }
    };
});
app.service('FluroScrollService', function($window, $location, $timeout) {

    var controller = {};
    
    /////////////////////////////////////
    
    controller.cache = {}
    
    /////////////////////////////////////

    controller.direction = 'down';

    /////////////////////////////////////

    var _value = 0;
    var body = angular.element('html,body');

     /////////////////////////////////////
   
    /////////////////////////////////////

    // angular.element($window).on('hashchange', function (event) {
    //     console.log('Event', window.location.hash);
    //     //var previousValue = _value;

    //     // Do what you need to do here like... getting imageId from #
    //     //var currentImageId = $location.search().imageId;
    //    // event.preventDefault();
    //    // event.stopPropagation();

    //     //Set scroll to previous value
    //     body.scrollTop(_value);

    //     var hash = $location.hash();
    //     controller.scrollToId(hash, 'slow');
    //     console.log('Hashchanged', hash, 'slow');


    //     //return false;
    // });


    /////////////////////////////////////

    controller.setAnchor = function(id) {
        $location.hash('jump-to-' + id);
    }

    /////////////////////////////////////

    controller.getAnchor = function() {
        var hash = $location.hash();
        if (_.startsWith(hash, 'jump-to-')) {
            return hash.substring(8);
        } else {
            return hash;
        }
    }

    /////////////////////////////////////

    function updateScroll() {
        var v = this.pageYOffset;

        if (_value != this.pageYOffset) {
            if (v < _value) {
                controller.direction = 'up';
            } else {
                controller.direction = 'down';
            }


            $timeout(function() {
                _value = this.pageYOffset;
            });
        }
    }

    /////////////////////////////////////

    controller.scrollToID =
        controller.scrollToId = function(id, speed, selector, offset) {

            if (speed != 0 && !speed) {
                speed = 'fast';
            }

            var $target = angular.element('#' + id);

            if ($target && $target.offset && $target.offset()) {
                if (!selector) {
                    selector = 'body,html';
                }


                var pos = $target.offset().top;

                //If theres an offset
                if(offset) {
                    pos += Number(offset);
                }

                angular.element(selector).animate({
                    scrollTop: pos
                }, speed);
            }

    }

    /////////////////////////////////////

    controller.scrollToPosition =
        controller.scrollTo = function(pos, speed, selector, offset) {

            if (speed != 0 && !speed) {
                speed = 'fast';
            }

            if (!selector) {
                selector = 'body,html';
            }


             //If theres an offset
            if(offset) {
                pos += Number(offset);
            }

            angular.element(selector).animate({
                scrollTop: pos
            }, speed);
    }

    /////////////////////////////////////

    controller.get =
    controller.getScroll = function() {
        return _value;
    }

    /////////////////////////////////////

    controller.getMax = function(selector) {

        if (!selector) {
            selector = 'body,html';
        }

        var bodyHeight = angular.element(selector).height();
        var windowHeight = $window.innerHeight;

        return (bodyHeight - windowHeight);
    }


    controller.getHalfPoint = function() {
        return ($window.innerHeight / 2);
    }

    controller.getWindowHeight = function() {
        return $window.innerHeight;
    }

    /////////////////////////////////////

    angular.element($window).bind("scroll", updateScroll);

    //Update the scroll on init
    updateScroll();

    /////////////////////////////////////

    return controller;

});
app.service('FluroSEOService', function($rootScope, $location) {

    var controller = {
    }

    ///////////////////////////////////////
    ///////////////////////////////////////
    ///////////////////////////////////////
    ///////////////////////////////////////

    $rootScope.$watch(function() {
        return controller.siteTitle + ' | ' + controller.pageTitle;
    }, function() {
        controller.headTitle = '';


        if(controller.siteTitle && controller.siteTitle.length) {
            controller.headTitle += controller.siteTitle;

            if(controller.pageTitle && controller.pageTitle.length) {
                controller.headTitle += ' | ' + controller.pageTitle;
            }
        } else {
            if(controller.pageTitle && controller.pageTitle.length) {
                controller.headTitle = controller.pageTitle;
            }
        }
    });

    ///////////////////////////////////////
    ///////////////////////////////////////

    controller.getImageURL = function() {

        //Get the default image URL
        var url = controller.defaultImageURL;

        if(controller.imageURL && controller.imageURL.length) {
            url = controller.imageURL;
        }

        return url;
    }

    ///////////////////////////////////////
    ///////////////////////////////////////

    controller.getDescription = function() {

        //Get the default image URL
        var description = controller.defaultDescription;

        if(controller.description) {
            description = controller.description;
        }

        if(description && description.length) {
            return description;
        } else {
            return '';
        }
    }


    //////////////////////////////////////////////////////////////////

    //Get the default site wide Social sharing image
    
    


    ///////////////////////////////////////
    ///////////////////////////////////////

    //Listen to change in the state
    $rootScope.$on('$stateChangeSuccess', function() {
        controller.url = $location.$$absUrl;
    });

    //Listen to change in the state
    $rootScope.$on('$stateChangeStart', function() {

        //Reset SEO stuff
        controller.description = null;
        controller.imageURL = null;

        console.log('REset SEO');
    });


    ///////////////////////////////////////

    return controller;

});
app.directive('statWidget', function() {
    return {
        restrict: 'E',
        // replace: true,
        // template:'<span ng-transclude></span>',
        scope: {
            item: '=',
            stat: '@',
            myStats: '=?myStats',
        },
        controller: 'FluroStatWidgetController',
        transclude: true,
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.replaceWith(clone);
            });
        },
    };
});

/////////////////////////////////////////////////////////////////////////

app.controller('FluroStatWidgetController', function($scope, $timeout, FluroContent) {

    if (!$scope.myStats) {
        $scope.myStats = {};
    }

    //////////////////////////////////////////////////

    $scope.processing = false;

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    //Watch for stat changes
    $scope.$watch(function() {

        var myStatValue = $scope.myStats['_' + $scope.stat];
        var totalStatValue = 0;

        if ($scope.item.stats && $scope.item.stats['_' + $scope.stat]) {
            totalStatValue = $scope.item.stats['_' + $scope.stat];
        }

        //Watch for when either my stats change or the total stats change
        return myStatValue + '-' + totalStatValue;

    }, function(tally) {

        var myStatValue = $scope.myStats['_' + $scope.stat];
        var totalStatValue = 0;

        if ($scope.item.stats && $scope.item.stats['_' + $scope.stat]) {
            totalStatValue = $scope.item.stats['_' + $scope.stat];
        }


        //Update my total
        $scope.myTotal = myStatValue;

        //Update the total of everybody
        $scope.total = totalStatValue
    });


    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.toggle = function() {

        //We can't do anything if we don't know what the item or the stat is
        if (!$scope.item || !$scope.stat) {
            return;
        }

        ////////////////////////////////////

        //If theres no stats yet then create the object
        if (!$scope.item.stats) {
            $scope.item.stats = {};
        }

        ////////////////////////////////////

        //Get the simple variables we need
        var statName = $scope.stat;
        var itemID = $scope.item._id;

        ////////////////////////////////////

        var prevCount;

        if ($scope.myStats) {
            //Check unique count for current stats
            prevCount = $scope.myStats['_' + statName];
        }

        ////////////////////////////////////

        //Hit the toggle endpoint
        var request = FluroContent.endpoint('stat/' + itemID + '/' + statName).update({}).$promise;

        $scope.processing = true;
        ////////////////////////////////////

        //Once the request has completed
        request.then(function(res) {

            $scope.processing = false;
            $timeout(function() {

                //If previously we had a value for this stat
                if ($scope.myStats) {

                    //If it was 1
                    if (prevCount) {
                        //Change the current users stats to 0
                        $scope.myStats['_' + statName] = 0;
                    } else {
                        //Change the current users stats to 1
                        $scope.myStats['_' + statName] = 1;
                    }
                }


                //Update the stats on the item object itself
                $scope.item.stats['_' + statName] = res.total;
            });

            ////////////////////////////////////

        }, function(err) {
            $scope.processing = false;
            console.log('Error updating stat', err);
        })
    }
});
app.directive('hamburger', function() {
    return {
        restrict: 'E', 
        replace:true, 
        template:'<div class="hamburger"> \
		  <span></span> \
		  <span></span> \
		  <span></span> \
		  <span></span> \
		</div>', 
        link: function($scope, $elem, $attr) {
        } 
    } 
});
app.directive('compileHtml', function($compile) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.$watch(function() {
                return scope.$eval(attrs.compileHtml);
            }, function(value) {



                element.html(value);
                $compile(element.contents())(scope);
            });
        }
    };
});
app.directive('googleMap', function($window, $timeout) {
    return {
        restrict: "E",
        replace: true,
        template: '<div class="map-outer"><div class="map-inner"></div></div>',
        scope: {
            model: "=ngModel",
            selectCallback: "=ngSelectCallback",
            styleOptions: "=ngStyleOptions",
        },
        link: function($scope, $element, $attrs) {

            var mapOptions;
            var map;
            var marker;
            var geocoder;
            var markers = [];

            ////////////////////////////////////////////////////////////
            
            styleOptions = [];
            
            if($scope.styleOptions) {
                styleOptions = $scope.styleOptions;
            } 
            
            ////////////////////////////////////////////////////////////
            
            //Nullify all markers
            function clearMarkers() {
                console.log('clear all markers');
                _.each(markers, function(marker) {
                    marker.setMap(null);
                });
              
                markers = [];
            }
            
            ////////////////////////////////////////////////////////////
            
            function initialize() {
                console.log('Map starting')
                
                mapOptions = {
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: false,
                    maxZoom: 15,
                    scrollwheel: false,
                    draggable: true,
                    styles: styleOptions,
                };
                
                //Create the map itself
                map = new google.maps.Map($element.find('.map-inner').get(0), mapOptions);
                
                //Create the Geocoder
                geocoder = new google.maps.Geocoder();
                
                //refit the map whenever the window is resized
                angular.element($window).bind('resize', fitMap);
            };
            
            ////////////////////////////////////////////////////////////////

            function fitMap() {

                //Create a bounds object
                var bounds = new google.maps.LatLngBounds();

                //Loop through each marker and expand the boundaries
                _.each(markers, function(marker) {

                    var lat = marker.position.lat();
                    var lng = marker.position.lng();

                    var myLatLng = new google.maps.LatLng(lat, lng);
                    bounds.extend(myLatLng);
                });
                
                //Fit the bounds
                map.fitBounds(bounds);
            
            }
            
            ////////////////////////////////////////////////////////////////
            
            var openWindow;
            
            function createInfoWindow(map, infowindow, contentString, marker) {
            
                if(openWindow) {
                    openWindow.close();
                }
                
                  google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                    openWindow = infowindow;
                  });
            }
            
            ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////
            
            //Create a marker representing the location
            var createMarker = function(location) {
                if (location.latitude && location.longitude) {
                    
                    var position = new google.maps.LatLng(location.latitude, location.longitude);
                        
                    map.setCenter(position);
                        
                    marker = new google.maps.Marker({
                        map: map,
                        position: position,
                        title: location.title
                    });
                    
                    google.maps.event.addListener(marker, 'click', function() {
                    
                        $timeout(function() {
                            if ($scope.selectCallback) {
                                $scope.selectCallback(location)
                            }
                        })
                    
                    });
                    
                    //Add the marker to the array
                    markers.push(marker);
                }
            };
            
            /////////////////////////////////////
            
            $scope.$watch("model", function(model) {
                // if (model) {


                //     _.each(model, function(model) {
                        createMarker(model)
                    // });
                // }

                fitMap();
            });

            ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////
            
            //Initialize the map
            initialize();
        },
    };
});
app.directive('imgOnload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                scope.$eval(attrs.imgOnload);
            });
        }
    };
});

/**
 * sample usage 
<any-tag infinite-pager items="items" per-page="18" ng-params="{nextPageOnScroll:true}">
    <any-tag ng-repeat="page in pages"></any-tag>

    <div ng-if="pages.length <= totalPages">
        <i class="fa fa-spin fa-spinner"></i>
        <button ng-click="nextPage()">next page</button>
    </div>

</any-tag>
 */

app.directive('infinitePager', function($timeout, $sessionStorage, $state, $window) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attr) {
            var perPage = 16;
            
            if($attr.perPage) {
                perPage = parseInt($attr.perPage);
            }

            if($attr.ngParams) {
                var ngParams = $scope.$eval($attr.ngParams);
                if(ngParams.nextPageOnScroll) {
                    bindNextPageOnScroll();
                }
            }
            
            $scope.pager = {
                currentPage: 0,
                limit: perPage,
            };

            $scope.pages = [];

            // session storage to identify previous page
            if ($sessionStorage.currentPage) {
                if($sessionStorage.currentPage[$state.current.name]) {
                    // make sure pages is created after back button is pressed, before scrolling down
                    $scope.$on('back-resume-scroll', function(event, args) {
                        // load pages
                        var start = $scope.pager.limit;
                        var end = ($sessionStorage.currentPage[$state.current.name] + 1) * $scope.pager.limit;
                        // console.log('on-before-scroll', start, end);
                        
                        //Slice into seperate chunks
                        var sliced = _.slice($scope.allItems, start, end);
                        $scope.pages.push(sliced);

                        // only scroll and assign page when element is ready
                        $element.ready(function(){
                            document.body.scrollTop = args.previousScrollPosition;
                            $scope.pager.currentPage = $sessionStorage.currentPage[$state.current.name];
                            // console.log('scrolled and currentPage updated');
                        })
                    });
                }
            } else {
                $sessionStorage.currentPage = {};
            }

            /////////////////////////////////////////////////////
            
            $scope.$watch($attr.items, function(items) {
                $scope.allItems = items;
                
                if($scope.allItems) {
                    $scope.pages.length = 0;
                    $scope.pager.currentPage = 0;

                    $scope.totalPages = Math.ceil($scope.allItems.length / $scope.pager.limit) - 1;

                    $scope.updateCurrentPage();                    
                }
            });
    
            /////////////////////////////////////////////////////
           
            //Update the current page
            $scope.updateCurrentPage = function() {

                if ($scope.allItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
                    $scope.pager.currentPage = 0;
                }
                
                var start = ($scope.pager.currentPage * $scope.pager.limit);
                var end = start + $scope.pager.limit;

                // console.log('page updated', start, end);

                //Slice into seperate chunks
                var sliced = _.slice($scope.allItems, start, end);
                $scope.pages.push(sliced);
            }
    
            /////////////////////////////////////////////////////
            var timer;
            
            $scope.nextPage = function() {
                if ($scope.pager.currentPage < $scope.totalPages) {
                    $timeout.cancel(timer);
                    timer = $timeout(function() {
                        $scope.pager.currentPage = ($scope.pager.currentPage + 1);
                        $sessionStorage.currentPage[$state.current.name] = $scope.pager.currentPage;
                        $scope.updateCurrentPage();
                    });
                } else {
                    // $scope.updateCurrentPage();
                }
            }
            
            /////////////////////////////////////////////////////

            function bindNextPageOnScroll() {
                angular.element($window).bind("scroll", function() {
                    var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
                    var body = document.body, html = document.documentElement;
                    var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
                    windowBottom = windowHeight + window.pageYOffset;
                    if (windowBottom >= docHeight) {
                        $scope.nextPage();
                    }
                });
            }


        }
    };
    
})
app.directive('longTextWrap', function() {


	return {
		restrict:'E',
		transclude:true,
		template:'<div class="long-text-wrap" ng-class="{expanded:ltSettings.expanded}" ><div class="long-text-wrap-inner" ng-transclude></div><a class="long-text-wrap-link" ng-click="ltSettings.expanded = !ltSettings.expanded"><div><span>Read more<span><i class="fa fa-angle-down"></i></div></a></div>',
		controller:function($scope) {
			$scope.ltSettings = {};
		}
	}
})

app.directive('ontaphold', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {

				var timer;
				var touchduration = 500; //default touch duration

				element[0].addEventListener('touchstart', function() {
				    timer = $timeout(onlongtouch, touchduration); 
				}, false);

				element[0].addEventListener('touchend', function() {
				    //stops short touches from firing the event
				    if (timer) {
				        return $timeout.cancel(timer);
				    } else {
				    	return onlongtouch();
				    }
				}, false);

				function onlongtouch() { 
					scope.$eval(attrs.ontaphold);
				};
                
            });
        }
    };
});
app.directive('selectOnFocus', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var focusedElement = null;

            element.on('focus', function() {
                var self = this;
                if (focusedElement != self) {
                    focusedElement = self;
                    $timeout(function() {


                       
                            self.select()

                    }, 10);
                }
            });

            element.on('blur', function() {
                focusedElement = null;
            });
        }
    }
});

app.filter('commaSummary', function() {
    return function (arrayOfObjects, limit) {

        if(!limit) {
            limit = 20;
        }

        var names = _.chain(arrayOfObjects)
        .map(function(object) {
            return object.title;
        })
        .compact()
        .value();

        var string = names.join(', ');

        if(string.length >=limit) {
            string = string.substr(0,limit) + '...';
        }

        return string;
    }
})

app.filter('filesize', function() {


     /*function calculateAge(birthday) { // birthday is a date
         var ageDifMs = Date.now() - birthday.getTime();
         var ageDate = new Date(ageDifMs); // miliseconds from epoch
         return Math.abs(ageDate.getUTCFullYear() - 1970);
     }
     */

     return function(bytes) { 
           var sizes = ['Bytes', 'kb', 'mb', 'gb', 'tb'];
		   if (bytes == 0) return '0 Byte';
		   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		   return Math.round(bytes / Math.pow(1024, i), 2) + '' + sizes[i];
     }; 
     
});


app.filter('readableDate', function() {
    return function(event, style) {

        if(!event) {
            return;
        }

        if(!event.startDate) {
            return;
        }

        var startDate = new Date(event.startDate);
        var endDate = new Date(event.endDate);

       

        ///////////////////////////////////////////////

        var noEndDate = startDate.format('g:ia j M Y') == endDate.format('g:ia j M Y');
        var sameYear = (startDate.format('Y') == endDate.format('Y'));
        var sameMonth = (startDate.format('M Y') == endDate.format('M Y'));
        var sameDay = (startDate.format('j M Y') == endDate.format('j M Y'));

        switch (style) {
            case 'short':
                // console.log('SHORT', startDate, endDate);
                if (noEndDate) {
                    return startDate.format('j M')
                }

                if (sameDay) {
                    //8am - 9am Thursday 21 May 2016
                    return startDate.format('j M')
                }

                if (sameMonth) {
                    //20 - 21 May 2016
                    return startDate.format('j') + '-' + endDate.format('j M')
                }

                if (sameYear) {
                    //20 Aug - 21 Sep 2016
                    return startDate.format('j') + '-' + endDate.format('j M')
                }

                //20 Aug 2015 - 21 Sep 2016
                return startDate.format('j M Y') + ' - ' + endDate.format('j M Y')

                break;
            default:
                if (noEndDate) {
                    return startDate.format('g:ia l j M Y')
                }

                if (sameDay) {
                    //8am - 9am Thursday 21 May 2016
                    return startDate.format('g:ia') + ' - ' + endDate.format('g:ia l j M Y')
                }

                if (sameMonth) {
                    //20 - 21 May 2016
                    return startDate.format('j') + '-' + endDate.format('j M Y')
                }

                if (sameYear) {
                    //20 Aug - 21 Sep 2016
                    return startDate.format('j M') + ' - ' + endDate.format('j M Y')
                }

                //20 Aug 2015 - 21 Sep 2016
                return startDate.format('j M Y') + ' - ' + endDate.format('j M Y')

                break;
        }

    };
});
// app.filter('reword', function($rootScope) {
// 	return function (str) {

// 		// console.log('TEST', str, $rootScope.applicationData);

// 		var translations = _.get($rootScope.applicationData, 'translations');
// 		var replacement = _.find(translations, {from:str});

// 		if(replacement) {

// 			console.log('Replace', replacement.from, replacement.to);
// 			return str.replace(new RegExp(replacement.from, 'g'), replacement.to);
// 		} else {
// 			return str;
// 		}
// 	}
// });



 app.filter('reword', function($rootScope) {

 	console.log('Get Translations')
	var translations = _.get($rootScope.applicationData, 'translations');


      return function(string) {


      	//Loop through each translatedWord
   			_.each(translations, function(set) {

   				// console.log('REPLACE', set.from, string);
   				string = string.replace(new RegExp(set.from, 'g'), set.to);
   			});

   			// console.log('Replaced', string);

   			return string
    		
    		
		};





    });

app.filter('timeago', function(){
  return function(date){

  	if(_.isString(date)) {
  		date = new Date(date);
  	}
    return moment(date).fromNow();
  };
});

app.service('pastTimeService', function(){
	
	var list = [
		{ label: 'Last hour', value: 'hours' },
		{ label: 'Last 24 hours', value: 'days' },
		{ label: 'Last week', value: 'weeks' },
		{ label: 'Last month', value: 'months' },
		{ label: 'Last year', value: 'years' },
	];
    var service = {
        list: list,
        getMoment: getMoment,
        getLabel: getLabel
    };

    return service;

    ////////////

    function getMoment(listValue) {
    	return moment().subtract(1, listValue);
    };

    function getLabel(listValue) {
    	var found = _.find(list, { value: listValue });
    	if(!found) { 
    		return; 
    	}
    	return found.label;
    };


})
EventFormController.resolve = {
	event: function($http, $stateParams, Fluro) {
		var url = Fluro.apiURL + '/content/_query/5987dc3dcf544d199f545833?variables[slugID]=' + $stateParams.id;
    
    	return $http.get(url);
	},
	
	seo: function(FluroSEOService, Asset, $filter, event) {

		event = _.first(event.data);

        FluroSEOService.pageTitle = event.title;


        var imageID = _.get(event, 'mainImage._id');

        if(imageID) {
        	//Use the poster of the video
        	FluroSEOService.imageURL = Asset.imageUrl(imageID, 640, null);
        } else {
        	FluroSEOService.imageURL = null;
        }
        


        // var shortDescription = _.get(event, 'data.publicData.body');


        // shortDescription = shortDescription.replace(/<[^>]+>/g, '').slice(0, 100);
        // if (shortDescription.length == 100) {
        //     shortDescription += '...';
        // }

        FluroSEOService.description = $filter('readableDate')(event);//shortDescription;


        return true;
    },
}

function EventFormController($scope, event, $http, Fluro) {

// 	
	// console.log('TESTING', event);
	
	$scope.event = _.get(event, 'data[0]');
	$scope.form = _.get(event, 'data[0].forms[0]');


	////////////////////////////////

	// function populateForms() {

	// 	// run through each of the forms on the event  
	// 	_.each($scope.event.forms, function(form) {

	// 		// then go get the form from the server
	// 		var url = Fluro.apiURL + '/content/_query/5892a5369985ce5ee279eeb6?variables[slugID]=' + form._id;
	    
	//     	$http.get(url).then(function success(res) {
	//     		console.log('Got form',res.data[0].title)

	//     		// then put into $scope.forms[].
	//     		$scope.forms.push(res.data[0])

	//     		console.log('forms', $scope.forms)
	//     	});

    		
	// 	});
	// }
	// populateForms();
}


FormViewController.resolve = {
    // publicForms: function($stateParams, FluroContentRetrieval) {
    // 	return FluroContentRetrieval.query(null, null, 5892a5369985ce5ee279eeb6, null, {
    // 		slugID: 
    // 	};
    // },
    form: function($http, Fluro, $stateParams, $q) {

        var deferred = $q.defer();

        // var url = Fluro.apiURL + '/content/_query/5892a5369985ce5ee279eeb6?variables[slugID]=' + $stateParams.id; // + formID;
        var url = Fluro.apiURL + '/form/' + $stateParams.id; // + formID;

        $http.get(url).then(function(res) {
            console.log('GOT FORM', res.data)
            deferred.resolve(res.data);
        }, function(err) {
            console.log('ERROR',err);
            deferred.reject(err);
        });


        return deferred.promise;
    },
    seo: function(FluroSEOService, Asset, $filter, form) {

    	// console.log('FORM DATA', _.first(form.data));

        console.log('FORM', form);

    	// form = _.first(form.data);

        var displayTitle = _.get(form, 'data.publicData.title');
        if (!displayTitle) {
            displayTitle = form.title;
        }

        FluroSEOService.pageTitle = displayTitle;

        // console.log('SEO', form)

        var imageID = _.get(form, 'data.publicData.mainImage');

        if(imageID) {
        	//Use the poster of the video
        	FluroSEOService.imageURL = Asset.imageUrl(imageID, 640, null);
        } else {
        	FluroSEOService.imageURL = null;
        }


        var shortDescription = _.get(form, 'data.publicData.body');

        if(!shortDescription) {
        	shortDescription = '';
        }
        shortDescription = shortDescription.replace(/<[^>]+>/g, '').slice(0, 100);
        if (shortDescription.length == 100) {
            shortDescription += '...';
        }

        FluroSEOService.description = shortDescription;


        return true;
    },
}

function FormViewController($scope, $stateParams, form) {

    // console.log('form', form)
    $scope.form = form;

    $scope.linkedProcess = $stateParams.process;
    console.log('Linked Process', $scope.linkedProcess);

    $scope.done = function(response) {
        console.log('Callback done');
    }

}
app.directive('shortUrl', function(FluroContent, $location, ShareModalService) {

    return {
        restrict: 'E',
        replace: true,
        scope:{
            form:'=ngModel',
        },
        template: '<a class="btn btn-sm btn-outline" ng-click="open()"><span>Share</span><i class="fa fa-share"></i></a>', 
        link: function($scope, $element, $attr) {


            $scope.$watch('form', function(form) {
                if(!form) {
                    return;
                }
                
                if(form && form._id) {

                    var longURL = $location.absUrl();
                    FluroContent.endpoint('url/shorten').save({url:longURL}).$promise.then(function(res) {

                        console.log('Got short url', res);
                        var shortURL = res.url;
                        $scope.shortURL = 'http://' + shortURL;
                    })
                }
            }) 

            ///////////////////////////////////////////////

            $scope.open = function() {
                console.log('happy days')
                ShareModalService.form = $scope.form;
            }
        }
    };
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////

app.service('ShareModalService', function() {
    var service = {};

    service.remove = function() {
        service.form = null;
        service.copied = false;
    }

    return service;
})


///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////


function clipboard(shortURL){

    var element = angular.element('<input id="clipper">');
    element.css({
        position:'fixed',
        top:999999999,
        left:99999999999,
        'z-index':-999,
    });
    element[0].value = shortURL;
    // console.log(shortURL, element[0].value);
    angular.element('body').append(element);
    element[0].select();

    var copied;

    try {
        copied = document.execCommand('copy');

        if(!copied) {
            console.error("Cannot copy text");
        } else {
            console.log("Copied text", copied);
        }
    } catch (err) {
        console.log('Unable to copy', err);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

app.directive('shareModal', function($location, FluroContent) {

	return {
        restrict: 'E',
        replace: true,
        scope:true,
        templateUrl: 'share-modal/share-modal.html', 
        controller: function($scope, ShareModalService) {

	    	// $scope.$watch(function() {
	    	// 	return ShareModalService.form;
	    	// }, function(form) {
	    	// 	// console.log('Formy form', form)
	    	// })

            $scope.service = ShareModalService;
            console.log('Service', $scope.service)

            $scope.processing = true;



            var longURL = $location.absUrl();

            FluroContent.endpoint('url/shorten').save({url:longURL}).$promise.then(function(res) {

                $scope.processing = false;

                $scope.url = 'http://' + res.url;

            })

            $scope.close = function() {

                angular.element('#clipper').remove();
                ShareModalService.remove()
            }

            $scope.copyUrl = function() {
                console.log('starting to copy');
                clipboard($scope.url);

                

                ShareModalService.copied = true;
               
                
            }



        }
    };
})
