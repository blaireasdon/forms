function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName("meta");
    for (i = 0; i < metas.length; i++) if (metas[i].getAttribute("property") == stringKey) return metas[i].getAttribute("content");
    return "";
}

function EventFormController($scope, event, $http, Fluro) {
    $scope.event = _.get(event, "data[0]"), $scope.form = _.get(event, "data[0].forms[0]");
}

function FormViewController($scope, $stateParams, form) {
    $scope.form = form, $scope.linkedProcess = $stateParams.process, console.log("Linked Process", $scope.linkedProcess), 
    $scope.done = function(response) {
        console.log("Callback done");
    };
}

function clipboard(shortURL) {
    var element = angular.element('<input id="clipper">');
    element.css({
        position: "fixed",
        top: 999999999,
        left: 99999999999,
        "z-index": -999
    }), element[0].value = shortURL, angular.element("body").append(element), element[0].select();
    var copied;
    try {
        copied = document.execCommand("copy"), copied ? console.log("Copied text", copied) : console.error("Cannot copy text");
    } catch (err) {
        console.log("Unable to copy", err);
    }
}

_.contains = _.includes;

var app = angular.module("fluro", [ "ngAnimate", "ngResource", "ui.router", "ngTouch", "fluro.config", "fluro.content", "fluro.asset", "fluro.access", "fluro.validate", "fluro.interactions", "formly", "formlyBootstrap", "angulartics", "angulartics.google.analytics", "signature" ]);

app.config(function($stateProvider, $httpProvider, $compileProvider, FluroProvider, $urlRouterProvider, $locationProvider, $analyticsProvider) {
    $analyticsProvider.settings.ga.additionalAccountNames = [ "fluro" ], $analyticsProvider.settings.ga.additionalAccountHitTypes.setUserProperties = !0, 
    $compileProvider.debugInfoEnabled(!1);
    var accessToken = getMetaKey("fluro_application_key"), apiURL = getMetaKey("fluro_url"), initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: !1,
        backupToken: accessToken
    }, appDevelopmentURL = getMetaKey("app_dev_url");
    appDevelopmentURL && appDevelopmentURL.length ? initialConfig.appDevelopmentURL = appDevelopmentURL : $locationProvider.html5Mode(!0), 
    FluroProvider.set(initialConfig), accessToken || ($httpProvider.defaults.withCredentials = !0), 
    $httpProvider.interceptors.push("FluroAuthentication"), $stateProvider.state("form", {
        url: "/form/:id?process",
        templateUrl: "routes/form/form-view.html",
        controller: FormViewController,
        resolve: FormViewController.resolve
    }), $stateProvider.state("event", {
        url: "/event/:id",
        templateUrl: "routes/event/event.html",
        controller: EventFormController,
        resolve: EventFormController.resolve
    }), $urlRouterProvider.otherwise("/");
}), app.run(function($rootScope, $timeout, ShareModalService, $sessionStorage, DateTools, Asset, FluroSEOService, FluroContent, FluroBreadcrumbService, FluroScrollService, $location, $timeout, $state, $analytics) {
    if ($rootScope.avatarCache = {
        buster: 1
    }, $rootScope.asset = Asset, $rootScope.$state = $state, $rootScope.session = $sessionStorage, 
    $rootScope.breadcrumb = FluroBreadcrumbService, $rootScope.scroll = FluroScrollService, 
    $rootScope.seo = FluroSEOService, $rootScope.modal = ShareModalService, $rootScope.date = DateTools, 
    applicationData) {
        $rootScope.applicationData = applicationData, applicationData.sandbox && console.log("ENVIRONMENT: Sandbox"), 
        applicationData.staging && console.log("ENVIRONMENT: Staging"), applicationUser && applicationUser._id && ($rootScope.user = applicationUser, 
        $analytics.setUsername(applicationUser._id), $analytics.setAlias(applicationUser.email), 
        $analytics.setUserProperties({
            dimension1: applicationUser.account._id
        }));
        var defaultImageID = null, applicationID = _.get(applicationData, "_application._id");
        defaultImageID && (defaultImageID._id && (defaultImageID = defaultImageID._id), 
        FluroSEOService.defaultImageURL = Asset.imageUrl(defaultImageID, 640, null, {
            from: applicationID,
            extension: "jpg"
        }));
        var defaultSEODescription = null;
        FluroSEOService.defaultDescription = defaultSEODescription, $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
            console.log("State change error", event);
        }), $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams, error) {
            $rootScope.currentState = toState.name;
        }), $rootScope.getTypeOrDefinition = function(item, defaultIfNoneProvided) {
            return item.definition && item.definition.length ? item.definition : !item._type && defaultIfNoneProvided ? defaultIfNoneProvided : item._type;
        }, FastClick.attach(document.body);
    }
}), app.directive("realmFolders", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            type: "@ngType",
            definition: "@?ngDefinition"
        },
        templateUrl: "admin-realm-folders/admin-realm-folders.html",
        controller: "RealmFoldersController"
    };
}), app.directive("realmFolderItem", function($compile, $templateCache) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            realm: "=ngModel",
            selection: "=ngSelection",
            searchTerms: "=ngSearchTerms"
        },
        templateUrl: "admin-realm-folders/admin-realm-folders-item.html",
        link: function($scope, $element, $attrs) {
            function hasActiveChild(realm) {
                var active = $scope.contains(realm);
                return active ? !0 : realm.children && realm.children.length ? _.some(realm.children, hasActiveChild) : void 0;
            }
            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    return _.isObject(i) ? i._id == realm._id : i == realm._id;
                });
            }, $scope.toggle = function(realm) {
                var matches = _.filter($scope.selection, function(existingRealmID) {
                    var searchRealmID = realm._id;
                    return existingRealmID._id && (existingRealmID = existingRealmID._id), searchRealmID == existingRealmID;
                });
                matches.length ? _.each(matches, function(match) {
                    _.pull($scope.selection, match);
                }) : $scope.selection.push(realm);
            }, $scope.isExpanded = function(realm) {
                return realm.expanded ? realm.expanded : $scope.activeTrail(realm);
            }, $scope.activeTrail = function(realm) {
                return realm.children ? _.some(realm.children, hasActiveChild) : void 0;
            };
            var template = '<realm-folder-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:\'title\' track by subRealm._id"></realm-folder-item>', newElement = angular.element(template);
            $compile(newElement)($scope), $element.find(".realm-select-children").append(newElement);
        }
    };
}), app.controller("RealmFoldersController", function($scope, $filter, $rootScope, FluroContent, FluroAccess) {
    $scope.settings = {
        show: !0
    }, $scope.model || ($scope.model = []), $scope.definition && $scope.definition.length || ($scope.definition = $scope.type), 
    $scope.tree = $rootScope.user.permissionSets, $scope.selected = function(realm) {
        var realmID = realm;
        return realmID._id && (realmID = realmID._id), _.some($scope.model, function(r) {
            return r._id && (r = r._id), r == realmID;
        });
    }, $scope.toggle = function(realm) {
        var realmID = realm._id;
        realmID._id && (realmID = realmID._id);
        var selected = $scope.selected(realm);
        selected ? $scope.model = _.reject($scope.model, function(rid) {
            return rid._id && (rid = rid._id), rid == realmID;
        }) : $scope.model.push(realm);
    };
}), app.directive("realmFolders", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            type: "@ngType",
            definition: "@?ngDefinition"
        },
        templateUrl: "admin-realm-folders/admin-realm-folders.html",
        controller: "RealmFoldersController"
    };
}), app.directive("realmFolderItem", function($compile, $templateCache) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            realm: "=ngModel",
            selection: "=ngSelection",
            searchTerms: "=ngSearchTerms"
        },
        templateUrl: "admin-realm-folders/admin-realm-folders-item.html",
        link: function($scope, $element, $attrs) {
            function hasActiveChild(realm) {
                var active = $scope.contains(realm);
                return active ? !0 : realm.children && realm.children.length ? _.some(realm.children, hasActiveChild) : void 0;
            }
            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    return _.isObject(i) ? i._id == realm._id : i == realm._id;
                });
            }, $scope.toggle = function(realm) {
                var matches = _.filter($scope.selection, function(existingRealmID) {
                    var searchRealmID = realm._id;
                    return existingRealmID._id && (existingRealmID = existingRealmID._id), searchRealmID == existingRealmID;
                });
                matches.length ? _.each(matches, function(match) {
                    _.pull($scope.selection, match);
                }) : $scope.selection.push(realm);
            }, $scope.isExpanded = function(realm) {
                return realm.expanded ? realm.expanded : $scope.activeTrail(realm);
            }, $scope.activeTrail = function(realm) {
                return realm.children ? _.some(realm.children, hasActiveChild) : void 0;
            };
            var template = '<realm-select-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:\'title\' track by subRealm._id"></realm-select-item>', newElement = angular.element(template);
            $compile(newElement)($scope), $element.find(".realm-select-children").append(newElement);
        }
    };
}), app.controller("RealmFoldersController", function($scope, $filter, $rootScope, FluroContent, FluroAccess) {
    $scope.settings = {
        show: !0
    }, $scope.model || ($scope.model = []), $scope.definition && $scope.definition.length || ($scope.definition = $scope.type), 
    FluroAccess.retrieveSelectableRealms("create", $scope.definition, $scope.type).then(function(realmTree) {
        $scope.tree = realmTree, 1 != realmTree.length || $scope.model.length || realmTree.children && realmTree.children.length || ($scope.model = [ realmTree[0] ]);
    }), $scope.selected = function(realm) {
        var realmID = realm;
        return realmID._id && (realmID = realmID._id), _.some($scope.model, function(r) {
            return r._id && (r = r._id), r == realmID;
        });
    }, $scope.toggle = function(realm) {
        var realmID = realm._id;
        realmID._id && (realmID = realmID._id);
        var selected = $scope.selected(realm);
        selected ? $scope.model = _.reject($scope.model, function(rid) {
            return rid._id && (rid = rid._id), rid == realmID;
        }) : $scope.model.push(realm);
    };
}), function() {
    Date.shortMonths = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ], 
    Date.longMonths = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ], 
    Date.shortDays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ], Date.longDays = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    var replaceChars = {
        d: function() {
            return (this.getDate() < 10 ? "0" : "") + this.getDate();
        },
        D: function() {
            return Date.shortDays[this.getDay()];
        },
        j: function() {
            return this.getDate();
        },
        l: function() {
            return Date.longDays[this.getDay()];
        },
        N: function() {
            return 0 == this.getDay() ? 7 : this.getDay();
        },
        S: function() {
            return this.getDate() % 10 == 1 && 11 != this.getDate() ? "st" : this.getDate() % 10 == 2 && 12 != this.getDate() ? "nd" : this.getDate() % 10 == 3 && 13 != this.getDate() ? "rd" : "th";
        },
        w: function() {
            return this.getDay();
        },
        z: function() {
            var d = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((this - d) / 864e5);
        },
        W: function() {
            var target = new Date(this.valueOf()), dayNr = (this.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var firstThursday = target.valueOf();
            return target.setMonth(0, 1), 4 !== target.getDay() && target.setMonth(0, 1 + (4 - target.getDay() + 7) % 7), 
            1 + Math.ceil((firstThursday - target) / 6048e5);
        },
        F: function() {
            return Date.longMonths[this.getMonth()];
        },
        m: function() {
            return (this.getMonth() < 9 ? "0" : "") + (this.getMonth() + 1);
        },
        M: function() {
            return Date.shortMonths[this.getMonth()];
        },
        n: function() {
            return this.getMonth() + 1;
        },
        t: function() {
            var d = new Date();
            return new Date(d.getFullYear(), d.getMonth(), 0).getDate();
        },
        L: function() {
            var year = this.getFullYear();
            return year % 400 == 0 || year % 100 != 0 && year % 4 == 0;
        },
        o: function() {
            var d = new Date(this.valueOf());
            return d.setDate(d.getDate() - (this.getDay() + 6) % 7 + 3), d.getFullYear();
        },
        Y: function() {
            return this.getFullYear();
        },
        y: function() {
            return ("" + this.getFullYear()).substr(2);
        },
        a: function() {
            return this.getHours() < 12 ? "am" : "pm";
        },
        A: function() {
            return this.getHours() < 12 ? "AM" : "PM";
        },
        B: function() {
            return Math.floor(1e3 * ((this.getUTCHours() + 1) % 24 + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) / 24);
        },
        g: function() {
            return this.getHours() % 12 || 12;
        },
        G: function() {
            return this.getHours();
        },
        h: function() {
            return ((this.getHours() % 12 || 12) < 10 ? "0" : "") + (this.getHours() % 12 || 12);
        },
        H: function() {
            return (this.getHours() < 10 ? "0" : "") + this.getHours();
        },
        i: function() {
            return (this.getMinutes() < 10 ? "0" : "") + this.getMinutes();
        },
        s: function() {
            return (this.getSeconds() < 10 ? "0" : "") + this.getSeconds();
        },
        u: function() {
            var m = this.getMilliseconds();
            return (10 > m ? "00" : 100 > m ? "0" : "") + m;
        },
        e: function() {
            return "Not Yet Supported";
        },
        I: function() {
            for (var DST = null, i = 0; 12 > i; ++i) {
                var d = new Date(this.getFullYear(), i, 1), offset = d.getTimezoneOffset();
                if (null === DST) DST = offset; else {
                    if (DST > offset) {
                        DST = offset;
                        break;
                    }
                    if (offset > DST) break;
                }
            }
            return this.getTimezoneOffset() == DST | 0;
        },
        O: function() {
            return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + "00";
        },
        P: function() {
            return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + ":00";
        },
        T: function() {
            return this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
        },
        Z: function() {
            return 60 * -this.getTimezoneOffset();
        },
        c: function() {
            return this.format("Y-m-d\\TH:i:sP");
        },
        r: function() {
            return this.toString();
        },
        U: function() {
            return this.getTime() / 1e3;
        }
    };
    Date.prototype.format = function(format) {
        var date = this;
        return format.replace(/(\\?)(.)/g, function(_, esc, chr) {
            return "" === esc && replaceChars[chr] ? replaceChars[chr].call(date) : chr;
        });
    };
}.call(this), function() {
    "use strict";
    function FastClick(layer, options) {
        function bind(method, context) {
            return function() {
                return method.apply(context, arguments);
            };
        }
        var oldOnClick;
        if (options = options || {}, this.trackingClick = !1, this.trackingClickStart = 0, 
        this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, 
        this.touchBoundary = options.touchBoundary || 10, this.layer = layer, this.tapDelay = options.tapDelay || 200, 
        !FastClick.notNeeded(layer)) {
            for (var methods = [ "onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel" ], context = this, i = 0, l = methods.length; l > i; i++) context[methods[i]] = bind(context[methods[i]], context);
            deviceIsAndroid && (layer.addEventListener("mouseover", this.onMouse, !0), layer.addEventListener("mousedown", this.onMouse, !0), 
            layer.addEventListener("mouseup", this.onMouse, !0)), layer.addEventListener("click", this.onClick, !0), 
            layer.addEventListener("touchstart", this.onTouchStart, !1), layer.addEventListener("touchmove", this.onTouchMove, !1), 
            layer.addEventListener("touchend", this.onTouchEnd, !1), layer.addEventListener("touchcancel", this.onTouchCancel, !1), 
            Event.prototype.stopImmediatePropagation || (layer.removeEventListener = function(type, callback, capture) {
                var rmv = Node.prototype.removeEventListener;
                "click" === type ? rmv.call(layer, type, callback.hijacked || callback, capture) : rmv.call(layer, type, callback, capture);
            }, layer.addEventListener = function(type, callback, capture) {
                var adv = Node.prototype.addEventListener;
                "click" === type ? adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
                    event.propagationStopped || callback(event);
                }), capture) : adv.call(layer, type, callback, capture);
            }), "function" == typeof layer.onclick && (oldOnClick = layer.onclick, layer.addEventListener("click", function(event) {
                oldOnClick(event);
            }, !1), layer.onclick = null);
        }
    }
    var deviceIsAndroid = navigator.userAgent.indexOf("Android") > 0, deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent), deviceIsIOS4 = deviceIsIOS && /OS 4_\d(_\d)?/.test(navigator.userAgent), deviceIsIOSWithBadTarget = deviceIsIOS && /OS ([6-9]|\d{2})_\d/.test(navigator.userAgent), deviceIsBlackBerry10 = navigator.userAgent.indexOf("BB10") > 0;
    FastClick.prototype.needsClick = function(target) {
        switch (target.nodeName.toLowerCase()) {
          case "button":
          case "select":
          case "textarea":
            if (target.disabled) return !0;
            break;

          case "input":
            if (deviceIsIOS && "file" === target.type || target.disabled) return !0;
            break;

          case "label":
          case "video":
            return !0;
        }
        return /\bneedsclick\b/.test(target.className);
    }, FastClick.prototype.needsFocus = function(target) {
        switch (target.nodeName.toLowerCase()) {
          case "textarea":
            return !0;

          case "select":
            return !deviceIsAndroid;

          case "input":
            switch (target.type) {
              case "button":
              case "checkbox":
              case "file":
              case "image":
              case "radio":
              case "submit":
                return !1;
            }
            return !target.disabled && !target.readOnly;

          default:
            return /\bneedsfocus\b/.test(target.className);
        }
    }, FastClick.prototype.sendClick = function(targetElement, event) {
        var clickEvent, touch;
        document.activeElement && document.activeElement !== targetElement && document.activeElement.blur(), 
        touch = event.changedTouches[0], clickEvent = document.createEvent("MouseEvents"), 
        clickEvent.initMouseEvent(this.determineEventType(targetElement), !0, !0, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, !1, !1, !1, !1, 0, null), 
        clickEvent.forwardedTouchEvent = !0, targetElement.dispatchEvent(clickEvent);
    }, FastClick.prototype.determineEventType = function(targetElement) {
        return deviceIsAndroid && "select" === targetElement.tagName.toLowerCase() ? "mousedown" : "click";
    }, FastClick.prototype.focus = function(targetElement) {
        var length;
        deviceIsIOS && targetElement.setSelectionRange && 0 !== targetElement.type.indexOf("date") && "time" !== targetElement.type && "month" !== targetElement.type ? (length = targetElement.value.length, 
        targetElement.setSelectionRange(length, length)) : targetElement.focus();
    }, FastClick.prototype.updateScrollParent = function(targetElement) {
        var scrollParent, parentElement;
        if (scrollParent = targetElement.fastClickScrollParent, !scrollParent || !scrollParent.contains(targetElement)) {
            parentElement = targetElement;
            do {
                if (parentElement.scrollHeight > parentElement.offsetHeight) {
                    scrollParent = parentElement, targetElement.fastClickScrollParent = parentElement;
                    break;
                }
                parentElement = parentElement.parentElement;
            } while (parentElement);
        }
        scrollParent && (scrollParent.fastClickLastScrollTop = scrollParent.scrollTop);
    }, FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {
        return eventTarget.nodeType === Node.TEXT_NODE ? eventTarget.parentNode : eventTarget;
    }, FastClick.prototype.onTouchStart = function(event) {
        var targetElement, touch, selection;
        if (event.targetTouches.length > 1) return !0;
        if (targetElement = this.getTargetElementFromEventTarget(event.target), touch = event.targetTouches[0], 
        deviceIsIOS) {
            if (selection = window.getSelection(), selection.rangeCount && !selection.isCollapsed) return !0;
            if (!deviceIsIOS4) {
                if (touch.identifier && touch.identifier === this.lastTouchIdentifier) return event.preventDefault(), 
                !1;
                this.lastTouchIdentifier = touch.identifier, this.updateScrollParent(targetElement);
            }
        }
        return this.trackingClick = !0, this.trackingClickStart = event.timeStamp, this.targetElement = targetElement, 
        this.touchStartX = touch.pageX, this.touchStartY = touch.pageY, event.timeStamp - this.lastClickTime < this.tapDelay && event.preventDefault(), 
        !0;
    }, FastClick.prototype.touchHasMoved = function(event) {
        var touch = event.changedTouches[0], boundary = this.touchBoundary;
        return Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary ? !0 : !1;
    }, FastClick.prototype.onTouchMove = function(event) {
        return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) && (this.trackingClick = !1, 
        this.targetElement = null), !0) : !0;
    }, FastClick.prototype.findControl = function(labelElement) {
        return void 0 !== labelElement.control ? labelElement.control : labelElement.htmlFor ? document.getElementById(labelElement.htmlFor) : labelElement.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea");
    }, FastClick.prototype.onTouchEnd = function(event) {
        var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;
        if (!this.trackingClick) return !0;
        if (event.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, 
        !0;
        if (this.cancelNextClick = !1, this.lastClickTime = event.timeStamp, trackingClickStart = this.trackingClickStart, 
        this.trackingClick = !1, this.trackingClickStart = 0, deviceIsIOSWithBadTarget && (touch = event.changedTouches[0], 
        targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement, 
        targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent), 
        targetTagName = targetElement.tagName.toLowerCase(), "label" === targetTagName) {
            if (forElement = this.findControl(targetElement)) {
                if (this.focus(targetElement), deviceIsAndroid) return !1;
                targetElement = forElement;
            }
        } else if (this.needsFocus(targetElement)) return event.timeStamp - trackingClickStart > 100 || deviceIsIOS && window.top !== window && "input" === targetTagName ? (this.targetElement = null, 
        !1) : (this.focus(targetElement), this.sendClick(targetElement, event), deviceIsIOS && "select" === targetTagName || (this.targetElement = null, 
        event.preventDefault()), !1);
        return deviceIsIOS && !deviceIsIOS4 && (scrollParent = targetElement.fastClickScrollParent, 
        scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) ? !0 : (this.needsClick(targetElement) || (event.preventDefault(), 
        this.sendClick(targetElement, event)), !1);
    }, FastClick.prototype.onTouchCancel = function() {
        this.trackingClick = !1, this.targetElement = null;
    }, FastClick.prototype.onMouse = function(event) {
        return this.targetElement ? event.forwardedTouchEvent ? !0 : event.cancelable && (!this.needsClick(this.targetElement) || this.cancelNextClick) ? (event.stopImmediatePropagation ? event.stopImmediatePropagation() : event.propagationStopped = !0, 
        event.stopPropagation(), event.preventDefault(), !1) : !0 : !0;
    }, FastClick.prototype.onClick = function(event) {
        var permitted;
        return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, 
        !0) : "submit" === event.target.type && 0 === event.detail ? !0 : (permitted = this.onMouse(event), 
        permitted || (this.targetElement = null), permitted);
    }, FastClick.prototype.destroy = function() {
        var layer = this.layer;
        deviceIsAndroid && (layer.removeEventListener("mouseover", this.onMouse, !0), layer.removeEventListener("mousedown", this.onMouse, !0), 
        layer.removeEventListener("mouseup", this.onMouse, !0)), layer.removeEventListener("click", this.onClick, !0), 
        layer.removeEventListener("touchstart", this.onTouchStart, !1), layer.removeEventListener("touchmove", this.onTouchMove, !1), 
        layer.removeEventListener("touchend", this.onTouchEnd, !1), layer.removeEventListener("touchcancel", this.onTouchCancel, !1);
    }, FastClick.notNeeded = function(layer) {
        var metaViewport, chromeVersion, blackberryVersion;
        if ("undefined" == typeof window.ontouchstart) return !0;
        if (chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [ , 0 ])[1]) {
            if (!deviceIsAndroid) return !0;
            if (metaViewport = document.querySelector("meta[name=viewport]")) {
                if (-1 !== metaViewport.content.indexOf("user-scalable=no")) return !0;
                if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) return !0;
            }
        }
        if (deviceIsBlackBerry10 && (blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), 
        blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3 && (metaViewport = document.querySelector("meta[name=viewport]")))) {
            if (-1 !== metaViewport.content.indexOf("user-scalable=no")) return !0;
            if (document.documentElement.scrollWidth <= window.outerWidth) return !0;
        }
        return "none" === layer.style.msTouchAction ? !0 : !1;
    }, FastClick.attach = function(layer, options) {
        return new FastClick(layer, options);
    }, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function() {
        return FastClick;
    }) : "undefined" != typeof module && module.exports ? (module.exports = FastClick.attach, 
    module.exports.FastClick = FastClick) : window.FastClick = FastClick;
}(), app.service("FluroBreadcrumbService", function($rootScope, $timeout, $state) {
    var backButtonPress, controller = {
        trail: []
    }, scrollPositions = {};
    return controller.trail = [], $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, error) {
        var path = $state.href(fromState, fromParams), previousScrollPosition = document.body.scrollTop;
        scrollPositions[path] = previousScrollPosition;
    }), $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams, error) {
        if ("home" == toState.name && (controller.trail.length = 0), backButtonPress) {
            var path = $state.href(toState, toParams), previousScrollPosition = scrollPositions[path];
            previousScrollPosition ? $timeout(function() {
                $rootScope.$broadcast("back-resume-scroll", {
                    previousScrollPosition: previousScrollPosition
                }), document.body.scrollTop = previousScrollPosition;
            }) : document.body.scrollTop = 0, controller.trail.pop(), controller.trail.pop(), 
            backButtonPress = !1;
        } else document.body.scrollTop = 0;
        toParams.noBreadcrumb || controller.trail.push({
            name: toState.name,
            params: toParams
        });
    }), controller.topState = _.find($state.get(), function(state) {
        return state.name && state.name.length;
    }), controller.top = function() {
        controller.trail.length = 0, controller.topState && $state.go(controller.topState);
    }, controller.clear = function() {
        var last = controller.trail.pop();
        last ? controller.trail = [ last ] : controller.trail.length = 0;
    }, controller.backTo = function(breadcrumbItem) {
        var index = controller.trail.indexOf(breadcrumbItem);
        return -1 != index ? (controller.trail.length = index, void $state.go(breadcrumbItem.name, breadcrumbItem.params)) : void 0;
    }, controller.back = function() {
        if (controller.trail.length) {
            backButtonPress = !0;
            var count = controller.trail.length, previousState = controller.trail[count - 2];
            previousState ? $state.go(previousState.name, previousState.params) : $state.$current.parent && $state.$current.parent.self.name.length ? $state.go("^") : $state.go(controller.topState);
        }
    }, controller;
}), app.factory("dateutils", function() {
    function validateDate(date) {
        var d = new Date(Date.UTC(date.year, date.month, date.day)), result = d && d.getMonth() === date.month && d.getDate() === Number(date.day);
        return result;
    }
    function changeDate(date) {
        return date.day > 28 ? (date.day--, date) : date.month > 11 ? (date.day = 31, date.month--, 
        date) : void 0;
    }
    var self = this;
    return this.days = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 ], 
    this.months = [ {
        value: 0,
        name: "January"
    }, {
        value: 1,
        name: "February"
    }, {
        value: 2,
        name: "March"
    }, {
        value: 3,
        name: "April"
    }, {
        value: 4,
        name: "May"
    }, {
        value: 5,
        name: "June"
    }, {
        value: 6,
        name: "July"
    }, {
        value: 7,
        name: "August"
    }, {
        value: 8,
        name: "September"
    }, {
        value: 9,
        name: "October"
    }, {
        value: 10,
        name: "November"
    }, {
        value: 11,
        name: "December"
    } ], {
        checkDate: function(date) {
            if (!date.day) return !1;
            if (0 === date.month) ; else if (!date.month) return !1;
            return date.year ? validateDate(date) ? date : this.checkDate(changeDate(date)) : !1;
        },
        get: function(name) {
            return self[name];
        }
    };
}), app.directive("dobSelect", [ "dateutils", function(dateutils) {
    return {
        restrict: "E",
        replace: !0,
        require: "ngModel",
        scope: {
            model: "=ngModel",
            hideAge: "=?",
            hideDates: "=?"
        },
        controller: [ "$scope", "dateutils", function($scope, dateutils) {
            function dateFieldsChanged(newDate, oldDate) {
                if (newDate != oldDate && (stopWatchingAge(), $scope.dateFields && $scope.dateFields.year)) {
                    if (0 === $scope.dateFields.month) ; else if (!$scope.dateFields.month) return;
                    if ($scope.dateFields.day) {
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        date.setHours(0, 0, 0, 0), $scope.model || ($scope.model = new Date()), $scope.model.setDate(date.getDate()), 
                        $scope.model.setMonth(date.getMonth()), $scope.model.setFullYear(date.getFullYear());
                        var diff = moment().diff($scope.model, "years");
                        $scope.age = parseInt(diff), startWatchingAge();
                    }
                }
            }
            function ageChanged(newAge, oldAge) {
                if (newAge != oldAge) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var currentYear = today.getFullYear(), diff = parseInt(currentYear) - parseInt(newAge);
                    if (diff) {
                        if (stopWatchingDate(), $scope.dateFields) $scope.dateFields.year = diff; else {
                            var dateFields = {};
                            dateFields.year = diff, $scope.dateFields = dateFields;
                        }
                        startWatchingDate();
                    }
                }
            }
            function startWatchingAge() {
                ageWatcher = $scope.$watch("age", ageChanged);
            }
            function stopWatchingAge() {
                ageWatcher && ageWatcher();
            }
            function startWatchingDate() {
                dateWatcher = $scope.$watch("dateFields", dateFieldsChanged, !0);
            }
            function stopWatchingDate() {
                dateWatcher && dateWatcher();
            }
            $scope.days = dateutils.get("days"), $scope.months = dateutils.get("months");
            for (var currentYear = parseInt(new Date().getFullYear()), numYearsInPast = 100, oldestYear = currentYear - numYearsInPast, yearOptions = [], i = currentYear; i >= oldestYear; i--) yearOptions.push(i);
            $scope.years = yearOptions, $scope.model && (_.isDate($scope.model) || ($scope.model = new Date($scope.model)));
            var dateFields = {};
            $scope.model && (dateFields.day = new Date($scope.model).getDate(), dateFields.month = new Date($scope.model).getMonth(), 
            dateFields.year = new Date($scope.model).getFullYear(), $scope.dateFields = dateFields, 
            $scope.age = moment().diff($scope.model, "years")), $scope.checkDate = function() {
                var date = dateutils.checkDate($scope.dateFields);
                date && ($scope.dateFields = date);
            };
            var ageWatcher, dateWatcher;
            startWatchingAge(), startWatchingDate();
        } ],
        templateUrl: "fluro-dob-select/fluro-dob-select.html",
        link: function(scope, element, attrs, ctrl) {
            attrs.yearText && (scope.yearText = !0), attrs.dayDivClass && (angular.element(element[0].children[0]).removeClass("form-group col-xs-3"), 
            angular.element(element[0].children[0]).addClass(attrs.dayDivClass)), attrs.dayClass && (angular.element(element[0].children[0].children[0]).removeClass("form-control"), 
            angular.element(element[0].children[0].children[0]).addClass(attrs.dayClass)), attrs.monthDivClass && (angular.element(element[0].children[1]).removeClass("form-group col-xs-5"), 
            angular.element(element[0].children[1]).addClass(attrs.monthDivClass)), attrs.monthClass && (angular.element(element[0].children[1].children[0]).removeClass("form-control"), 
            angular.element(element[0].children[1].children[0]).addClass(attrs.monthClass)), 
            attrs.yearDivClass && (angular.element(element[0].children[2]).removeClass("form-group col-xs-4"), 
            angular.element(element[0].children[2]).addClass(attrs.yearDivClass)), attrs.yearClass && (angular.element(element[0].children[2].children[0]).removeClass("form-control"), 
            angular.element(element[0].children[2].children[0]).addClass(attrs.yearClass)), 
            scope.$parent.$watch(attrs.ngDisabled, function(newVal) {
                scope.disableFields = newVal;
            });
        }
    };
} ]), app.controller("FluroInteractionButtonSelectController", function($scope, FluroValidate) {
    function checkValidity() {
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    function setModel() {
        $scope.multiple ? $scope.model[opts.key] = angular.copy($scope.selection.values) : $scope.model[opts.key] = angular.copy($scope.selection.value), 
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }
    var opts = ($scope.to, $scope.options);
    $scope.selection = {
        values: [],
        value: null
    };
    var definition = $scope.to.definition, minimum = definition.minimum, maximum = definition.maximum;
    if (minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum, 
    $scope.dragControlListeners = {
        orderChanged: function(event) {
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        }
    }, $scope.selectBox = {}, $scope.selectUpdate = function() {
        $scope.selectBox.item && ($scope.selection.values.push($scope.selectBox.item), $scope.model[opts.key] = angular.copy($scope.selection.values));
    }, $scope.canAddMore = function() {
        return maximum ? $scope.multiple ? $scope.selection.values.length < maximum : $scope.selection.value ? void 0 : !0 : !0;
    }, $scope.contains = function(value) {
        return $scope.multiple ? _.includes($scope.selection.values, value) : $scope.selection.value == value;
    }, $scope.select = function(value) {
        if ($scope.multiple) {
            if (!$scope.canAddMore()) return;
            $scope.selection.values.push(value);
        } else $scope.selection.value = value;
    }, $scope.deselect = function(value) {
        $scope.multiple ? _.pull($scope.selection.values, value) : $scope.selection.value = null;
    }, $scope.toggle = function(reference) {
        $scope.contains(reference) ? $scope.deselect(reference) : $scope.select(reference), 
        setModel();
    }, $scope.$watch("model", function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {
            var modelValue;
            _.keys(newModelValue).length && (modelValue = newModelValue[opts.key], $scope.multiple ? modelValue && _.isArray(modelValue) ? $scope.selection.values = angular.copy(modelValue) : $scope.selection.values = [] : $scope.selection.value = angular.copy(modelValue));
        }
    }, !0), opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
});

var _nowYear = new Date().getFullYear(), _defaultYears = _.chain(_.range(-100, 12)).map(function(y) {
    var actual = _nowYear + y;
    return {
        name: actual,
        value: actual
    };
}).reverse().value();

app.directive("fluroDateSelect", [ function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "fluro-interaction-form/date-select/date-selector.html",
        controller: [ "$scope", "dateutils", function($scope, dateutils) {
            function modelChanged() {
                $scope.model && (_.isDate($scope.model) || ($scope.model = new Date($scope.model)), 
                dateFields.day = new Date($scope.model).getDate(), dateFields.month = new Date($scope.model).getMonth(), 
                dateFields.year = new Date($scope.model).getFullYear(), $scope.dateFields = dateFields);
            }
            function dateFieldsChanged(newDate, oldDate) {
                if (newDate != oldDate && $scope.dateFields && $scope.dateFields.year) {
                    if (0 === $scope.dateFields.month) ; else if (!$scope.dateFields.month) return;
                    if ($scope.dateFields.day) {
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        date.setHours(0, 0, 0, 0), $scope.model || ($scope.model = new Date()), $scope.model.setDate(date.getDate()), 
                        $scope.model.setMonth(date.getMonth()), $scope.model.setFullYear(date.getFullYear());
                    }
                }
            }
            $scope.days = dateutils.get("days"), $scope.months = dateutils.get("months"), $scope.years = _defaultYears;
            var dateFields = {};
            $scope.$watch("dateFields", dateFieldsChanged, !0), $scope.$watch("model", modelChanged), 
            $scope.checkDate = function() {
                var date = dateutils.checkDate($scope.dateFields);
                date && ($scope.dateFields = date);
            };
        } ]
    };
} ]), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "dob-select",
        templateUrl: "fluro-interaction-form/dob-select/fluro-dob-select.html",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "embedded",
        templateUrl: "fluro-interaction-form/embedded/fluro-embedded.html",
        controller: "FluroInteractionNestedController",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.directive("interactionForm", function($compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            integration: "=ngPaymentIntegration",
            linkedProcess: "=?linkedProcess",
            vm: "=?config",
            callback: "=?callback"
        },
        transclude: !0,
        controller: "InteractionFormController",
        template: '<div class="fluro-interaction-form" ng-transclude-here />',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.find("[ng-transclude-here]").append(clone);
            });
        }
    };
}), app.directive("webform", function($compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            integration: "=ngPaymentIntegration",
            vm: "=?config",
            debugMode: "=?debugMode",
            callback: "=?callback",
            linkedEvent: "=?linkedEvent",
            linkedProcess: "=?linkedProcess"
        },
        transclude: !0,
        controller: "InteractionFormController",
        templateUrl: "fluro-interaction-form/fluro-web-form.html",
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        }
    };
}), app.config(function(formlyConfigProvider) {
    formlyConfigProvider.setType({
        name: "currency",
        "extends": "input",
        overwriteOk: !0,
        controller: [ "$scope", function($scope) {} ],
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
        defaultOptions: {
            ngModelAttrs: {
                currencyDirective: {
                    attribute: "ng-currency"
                },
                fractionValue: {
                    attribute: "fraction",
                    bound: "fraction"
                },
                minimum: {
                    attribute: "min",
                    bound: "min"
                },
                maximum: {
                    attribute: "max",
                    bound: "max"
                }
            },
            templateOptions: {
                customAttrVal: "",
                required: !0,
                fraction: 2
            },
            validators: {
                validInput: {
                    expression: function($viewValue, $modelValue, scope) {
                        var numericValue = Number($modelValue);
                        if (isNaN(numericValue)) return !1;
                        var minimumAmount = scope.options.data.minimumAmount, maximumAmount = scope.options.data.maximumAmount;
                        return minimumAmount && minimumAmount > numericValue ? !1 : maximumAmount && numericValue > maximumAmount ? !1 : !0;
                    }
                }
            }
        }
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.templateManipulators.postWrapper.push(function(template, options, scope) {
        if (!scope.to.errorMessage || !scope.to.errorMessage.length) {
            var viewValue = _.get(scope, "fc.$viewValue"), fieldDefinition = _.get(options, "templateOptions.definition");
            !viewValue || _.isArray(viewValue) ? fieldDefinition && 1 == fieldDefinition.maximum ? scope.to.errorMessage = "This field is required" : fieldDefinition && 1 != fieldDefinition.minimum ? scope.to.errorMessage = "Please select at least " + fieldDefinition.minimum + " options" : scope.to.errorMessage = "Please select at least 1 option" : scope.to.errorMessage = "'" + viewValue + "' is not a valid answer";
        }
        var fluroErrorTemplate = $templateCache.get("fluro-interaction-form/field-errors.html");
        return "<div>" + template + fluroErrorTemplate + "</div>";
    }), formlyConfig.setType({
        name: "multiInput",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/multi.html",
        defaultOptions: {
            noFormControl: !0,
            wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: [ "$scope", function($scope) {
            function copyItemOptions() {
                return angular.copy($scope.to.inputOptions);
            }
            $scope.copyItemOptions = copyItemOptions;
        } ]
    }), formlyConfig.setType({
        name: "custom",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/custom.html",
        controller: "CustomInteractionFieldController",
        wrapper: [ "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "button-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/button-select/fluro-button-select.html",
        controller: "FluroInteractionButtonSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "date-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/date-select/fluro-date-select.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
        controller: function($scope, FluroValidate) {
            function checkValidity() {
                $scope.model || ($scope.model = {});
                var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
                if ($scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
                $scope.fc) {
                    var formControl = $scope.fc;
                    formControl.length && (formControl = formControl[0]), formControl.$setValidity("required", validRequired), 
                    formControl.$setValidity("validInput", validInput);
                }
            }
            $scope.$watch("model", checkValidity, !0);
            var opts = $scope.options, definition = opts.templateOptions.definition;
        }
    }), formlyConfig.setType({
        name: "terms",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/fluro-terms.html",
        wrapper: [ "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "order-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/order-select/fluro-order-select.html",
        controller: "FluroInteractionButtonSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.controller("CustomInteractionFieldController", function($scope, FluroValidate) {
    $scope.$watch("model[options.key]", function(value) {
        value && $scope.fc && $scope.fc.$setTouched();
    }, !0);
}), app.controller("InteractionFormController", function($scope, $q, $timeout, $rootScope, DateTools, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {
    function getAge(dateInput, timezone) {
        var today = new Date(), birthDate = new Date(dateInput), age = today.getFullYear() - birthDate.getFullYear(), m = today.getMonth() - birthDate.getMonth();
        return (0 > m || 0 === m && today.getDate() < birthDate.getDate()) && age--, age;
    }
    function matchInArray(array, key, value, operator) {
        var matches = _.filter(array, function(entry) {
            var isMatch, retrievedValue = _.get(entry, key);
            switch (operator) {
              case ">":
                isMatch = retrievedValue > value;
                break;

              case "<":
                isMatch = value > retrievedValue;
                break;

              case ">=":
                isMatch = retrievedValue >= value;
                break;

              case "<=":
                isMatch = value >= retrievedValue;
                break;

              case "in":
                isMatch = _.includes(retrievedValue, value);
                break;

              default:
                isMatch = void 0 === value ? retrievedValue : retrievedValue == value;
            }
            return isMatch;
        });
        return matches;
    }
    function debugLog() {
        $scope.debugMode && console.log(_.map(arguments, function(v) {
            return v;
        }));
    }
    function validateAllFields() {
        timer && ($timeout.cancel(timer), timer = null), timer = $timeout(function() {
            function setValid(ready) {
                $scope.readyToSubmit = ready;
            }
            function mapRecursiveField(field) {
                if (field) {
                    var output = [ field ];
                    if ("_paymentCardDetails" == field.key) {
                        var calculatedAmount = _.get($formScope, "vm.model.calculatedTotal"), donationAmount = _.get($formScope, "vm.model._paymentAmount");
                        if (!calculatedAmount && !donationAmount) return void console.log("No calculated total");
                        var paymentMethod = _.get($formScope, "vm.model._paymentMethod");
                        if (paymentMethod && paymentMethod.length && "card" != paymentMethod) return void console.log("Alternative payment method");
                    }
                    var isDisabled = _.get(field, "templateOptions.definition.disableValidation");
                    if (!isDisabled) {
                        if (!field.hide && field.fields && field.fields.length && output.push(_.map(field.fields, mapRecursiveField)), 
                        !field.hide && field.data && field.data.fields && field.data.fields.length && output.push(_.map(field.data.fields, mapRecursiveField)), 
                        !field.hide && field.fieldGroup && field.fieldGroup.length && output.push(_.map(field.fieldGroup, mapRecursiveField)), 
                        !field.hide && field.data && field.data.replicatedFields && field.data.replicatedFields.length) {
                            var nestedForms = _.chain(field.data.replicatedFields).flatten().map(mapRecursiveField).value();
                            output.push(nestedForms);
                        }
                        if (!field.hide && field.data && field.data.replicatedDataFields && field.data.replicatedDataFields.length) {
                            var nestedForms = _.chain(field.data.replicatedDataFields).flatten().map(mapRecursiveField).value();
                            output.push(nestedForms);
                        }
                        if (!field.hide && field.data && field.data.replicatedDetailFields && field.data.replicatedDetailFields.length) {
                            var nestedForms = _.chain(field.data.replicatedDetailFields).flatten().map(mapRecursiveField).value();
                            output.push(nestedForms);
                        }
                        return output;
                    }
                }
            }
            $scope.errorList = _.chain($scope.vm.modelFields).map(mapRecursiveField).flattenDeep().compact().filter(function(field) {
                var required = _.get(field, "formControl.$error.required"), dateInvalid = _.get(field, "formControl.$error.date"), invalid = _.get(field, "formControl.$error.validInput"), uploadInProgress = _.get(field, "formControl.$error.uploadInProgress"), shouldwarning = required || invalid || dateInvalid || uploadInProgress;
                return shouldwarning;
            }).value();
            var interactionForm = $scope.vm.modelForm;
            if (!interactionForm) return console.log("INVALID - no form"), setValid(!1);
            if (!$scope.errorList || !$scope.errorList.length) {
                var invalidValues = _.get(interactionForm, "$error.validInput"), requiredValues = _.get(interactionForm, "$error.required");
                if ($scope.errorList = _.chain([ invalidValues, requiredValues ]).flatten().compact().map(function(f, index) {
                    return _.chain(f).keys().map(function(key) {
                        if (_.startsWith(key, "formly_")) {
                            var field = f[key];
                            return field.$invalid || field.$error ? field : void 0;
                        }
                    }).value();
                }).flatten().compact().map(function(f, index) {
                    var readableName = f.$name;
                    if (!f.$viewValue) {
                        var message = "Please check " + readableName;
                        return _.startsWith(readableName, "formly_") && f.$error.date && (message = "Invalid date provided"), 
                        {
                            id: index,
                            field: f,
                            message: message
                        };
                    }
                    return {
                        id: index,
                        field: f,
                        message: f.$viewValue + " is an invalid value for " + readableName
                    };
                }).compact().value(), $scope.errorList.length) return console.log("INVALID We have errors so set to false"), 
                setValid(!1);
            }
            if (interactionForm.$error) {
                if (console.log("Errors", interactionForm.$error), interactionForm.$error.required && interactionForm.$error.required.length) return setValid(!1);
                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) return console.log("INVALID valid input not provided", interactionForm.$error.validInput), 
                setValid(!1);
                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) return console.log("INVALID Upload has not finished"), 
                setValid(!1);
            }
            setValid(!0);
        }, 200);
    }
    function injectScript(scriptURL, callback) {
        if (injectedScripts[scriptURL]) return callback();
        injectedScripts[scriptURL] = !0;
        var script = document.createElement("script");
        script.type = "text/javascript", script.async = !0, script.onload = callback, script.src = scriptURL, 
        document.getElementsByTagName("head")[0].appendChild(script), console.log("Appended script dependency", scriptURL);
    }
    function integrationChanged(integration) {
        function tithelyLoaded() {
            console.log("TITHELY HAS LOADED");
            var key, liveKey = integration.publicDetails.publicKey, sandboxKey = integration.publicDetails.publicKey;
            key = SANDBOX_ENV ? sandboxKey : liveKey, tithelyController = new Tithely(key);
        }
        if (integration) {
            var SANDBOX_ENV = _.get(integration, "publicDetails.sandbox") || _.get(window, "applicationData.sandbox");
            switch (integration.module) {
              case "stripe":
                injectScript("https://js.stripe.com/v2/", function() {
                    console.log("Stripe has been included on page");
                });
                break;

              case "eway":
                injectScript("https://secure.ewaypayments.com/scripts/eCrypt.js", function() {
                    console.log("Eway has been included on page");
                });
                break;

              case "tithely":
                SANDBOX_ENV ? (console.log("Inject SANDBOX token script"), injectScript("https://tithelydev.com/api-js/v1/tithely.js", tithelyLoaded)) : (console.log("Inject LIVE token script"), 
                injectScript("https://tithe.ly/api-js/v1/tithely.js", tithelyLoaded));
            }
        }
    }
    function definitionModelChanged() {
        function addFieldDefinition(array, fieldDefinition) {
            if (!fieldDefinition.params || !fieldDefinition.params.disableWebform) {
                var newField = {};
                newField.key = fieldDefinition.key, fieldDefinition.minimum = parseInt(fieldDefinition.minimum), 
                fieldDefinition.maximum = parseInt(fieldDefinition.maximum), fieldDefinition.className && (newField.className = fieldDefinition.className);
                var templateOptions = {};
                switch (templateOptions.type = "text", templateOptions.label = fieldDefinition.title, 
                templateOptions.description = fieldDefinition.description, templateOptions.params = fieldDefinition.params, 
                fieldDefinition.errorMessage && (templateOptions.errorMessage = fieldDefinition.errorMessage), 
                templateOptions.definition = fieldDefinition, fieldDefinition.placeholder && fieldDefinition.placeholder.length && (templateOptions.placeholder = fieldDefinition.placeholder), 
                templateOptions.required = fieldDefinition.minimum > 0, templateOptions.onBlur = "to.focused=false", 
                templateOptions.onFocus = "to.focused=true", fieldDefinition.type) {
                  case "reference":
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                        return {
                            name: ref.title,
                            value: ref._id
                        };
                    }); else if (templateOptions.options = [], fieldDefinition.sourceQuery) {
                        var queryId = fieldDefinition.sourceQuery;
                        queryId._id && (queryId = queryId._id);
                        var options = {};
                        fieldDefinition.queryTemplate && (options.template = fieldDefinition.queryTemplate, 
                        options.template._id && (options.template = options.template._id));
                        var promise = FluroContentRetrieval.getQuery(queryId, options);
                        promise.then(function(res) {
                            templateOptions.options = _.map(res, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id
                                };
                            });
                        });
                    } else "embedded" != fieldDefinition.directive && fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length && FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                        templateOptions.options = _.map(res, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id
                            };
                        });
                    });
                    break;

                  default:
                    fieldDefinition.options && fieldDefinition.options.length ? templateOptions.options = fieldDefinition.options : templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                        return {
                            name: val,
                            value: val
                        };
                    });
                }
                switch (fieldDefinition.directive) {
                  case "reference-select":
                  case "value-select":
                    switch (window.DEVICE_PLATFORM) {
                      case "ios":
                      case "android":
                        newField.type = "reference-select";
                        break;

                      default:
                        templateOptions.options && templateOptions.options.length > 10 ? newField.type = "select" : newField.type = "button-select";
                    }
                    break;

                  case "select":
                    newField.type = "select";
                    break;

                  case "wysiwyg":
                    newField.type = "textarea";
                    break;

                  default:
                    newField.type = fieldDefinition.directive || "input";
                }
                if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length && (newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = "customAttr" + key;
                    return results[customKey] = {
                        attribute: key
                    }, templateOptions[customKey] = attr, results;
                }, {})), "select" == newField.type && (newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                1 != fieldDefinition.maximum && (newField.ngModelAttrs.multiple = {
                    value: "multiple"
                })), "select" == newField.type && (newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                1 != fieldDefinition.maximum && (newField.ngModelAttrs.multiple = "true")), "custom" != fieldDefinition.directive && "value" != fieldDefinition.directive) switch (fieldDefinition.type) {
                  case "boolean":
                    fieldDefinition.params && fieldDefinition.params.storeCopy ? newField.type = "terms" : newField.type = "checkbox";
                    break;

                  case "date":
                    "dob-select" != newField.type && (newField.type = "date-select");
                    break;

                  case "number":
                  case "float":
                  case "integer":
                  case "decimal":
                    templateOptions.type = "input", newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                    newField.ngModelAttrs.customAttrpattern = {
                        attribute: "pattern"
                    }, templateOptions.customAttrpattern = "[0-9.]*", "integer" == fieldDefinition.type && (templateOptions.type = "number", 
                    templateOptions.baseDefaultValue = 0, templateOptions.customAttrpattern = "[0-9]*", 
                    newField.ngModelAttrs.customAttrinputmode = {
                        attribute: "inputmode"
                    }, templateOptions.customAttrinputmode = "numeric", fieldDefinition.params && (0 !== parseInt(fieldDefinition.params.maxValue) && (templateOptions.max = fieldDefinition.params.maxValue), 
                    0 !== parseInt(fieldDefinition.params.minValue) ? templateOptions.min = fieldDefinition.params.minValue : templateOptions.min = 0));
                }
                if (1 == fieldDefinition.maximum ? "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length && ("search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0] : templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id) : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0]) : "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length ? "search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences : templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                    return ref._id;
                }) : templateOptions.baseDefaultValue = [] : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                    return Number(val);
                }) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues), newField.templateOptions = templateOptions, 
                newField.validators = {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        if (!value) return !0;
                        var valid = FluroValidate.validate(value, fieldDefinition);
                        return valid;
                    }
                }, "date" == fieldDefinition.type && (newField.validators.date = function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;
                    if (!value) return fieldDefinition.minimum ? !1 : !0;
                    var parsedDate = Date.parse(value), invalid = isNaN(parsedDate);
                    return !invalid;
                }), "embedded" == fieldDefinition.directive) {
                    if (newField.type = "embedded", 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {
                        data: {}
                    }; else {
                        var askCount = 0;
                        fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                        fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                        var initialArray = [];
                        askCount && _.times(askCount, function() {
                            initialArray.push({});
                        }), templateOptions.baseDefaultValue = initialArray;
                    }
                    newField.data = {
                        fields: [],
                        dataFields: [],
                        detailFields: [],
                        replicatedFields: [],
                        replicatedDataFields: [],
                        replicatedDetailFields: []
                    };
                    var fieldContainer = newField.data.fields, dataFieldContainer = newField.data.dataFields, detailFieldContainer = newField.data.detailFields;
                    fieldDefinition.fields && fieldDefinition.fields.length && _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                    var embeddedContactDetails = _.get(fieldDefinition, "params.targetDetails");
                    _.each(embeddedContactDetails, function(contactDetailName) {
                        var formID = $formScope.model._id, detailPromise = FluroContent.endpoint("defined/" + contactDetailName + "?form=" + formID).get().$promise;
                        detailPromise.then(function(detailSheetDefinition) {
                            var detailDataObject = {
                                type: "group",
                                asObject: !0,
                                minimum: 1,
                                maximum: 1,
                                key: "data",
                                fields: detailSheetDefinition.fields
                            }, sheet = {
                                type: "group",
                                asObject: !0,
                                minimum: 1,
                                maximum: 1,
                                key: detailSheetDefinition.definitionName,
                                fields: [ detailDataObject ]
                            };
                            addFieldDefinition(detailFieldContainer, sheet);
                        }), $scope.promises.push(detailPromise);
                    });
                    var promise = FluroContent.endpoint("defined/" + fieldDefinition.params.restrictType).get().$promise;
                    promise.then(function(embeddedDefinition) {
                        if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                            var childFields = embeddedDefinition.fields;
                            fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length && (childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            })), _.each(childFields, function(sub) {
                                addFieldDefinition(dataFieldContainer, sub);
                            });
                        }
                    }), $scope.promises.push(promise);
                }
                if ("group" == fieldDefinition.type && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                    var fieldContainer;
                    if (fieldDefinition.asObject) {
                        if (newField.type = "nested", fieldDefinition.key && 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {}; else {
                            var askCount = 0;
                            fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                            fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                            var initialArray = [];
                            askCount && _.times(askCount, function() {
                                initialArray.push({});
                            }), templateOptions.baseDefaultValue = initialArray;
                        }
                        newField.data = {
                            fields: [],
                            replicatedFields: [],
                            replicatedDataFields: [],
                            replicatedDetailFields: []
                        }, fieldContainer = newField.data.fields;
                    } else newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className
                    }, fieldContainer = newField.fieldGroup;
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }
                if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length && (fieldDefinition.expressions || (fieldDefinition.expressions = {}), 
                fieldDefinition.expressions.hide = fieldDefinition.hideExpression, console.log("HIDE EXPRESSION", fieldDefinition.expressions)), 
                fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {
                    var allExpressions = _.reduce(fieldDefinition.expressions, function(memo, string) {
                        return string && string.length && (memo += "(" + string + ")"), memo;
                    }, "");
                    newField.watcher = {
                        expression: function(field, scope) {
                            var watchScope = {
                                model: scope.model,
                                data: $scope.vm.model,
                                interaction: $scope.vm.model,
                                date: new Date(),
                                matchInArray: matchInArray,
                                dateTools: DateTools,
                                getAge: getAge,
                                Date: Date,
                                Math: Math,
                                String: String,
                                Date: Date,
                                parseInt: parseInt,
                                parseFloat: parseFloat,
                                Boolean: Boolean,
                                Number: Number
                            }, val = $parse(allExpressions)(watchScope);
                            return console.log("EXPRESSION CHANGED", allExpressions, val), val;
                        },
                        listener: function(field, newValue, oldValue, scope, stopWatching) {
                            var checkScope = {
                                model: scope.model,
                                data: $scope.vm.model,
                                interaction: $scope.vm.model,
                                date: new Date(),
                                matchInArray: matchInArray,
                                dateTools: DateTools,
                                getAge: getAge,
                                Date: Date,
                                Math: Math,
                                String: String,
                                Date: Date,
                                parseInt: parseInt,
                                parseFloat: parseFloat,
                                Boolean: Boolean,
                                Number: Number
                            };
                            _.each(fieldDefinition.expressions, function(expression, key) {
                                var retrievedValue = $parse(expression)(checkScope);
                                console.log("Expression changed", expression, retrievedValue), debugLog("expression:", retrievedValue, checkScope);
                                var fieldKey = field.key;
                                switch (key) {
                                  case "defaultValue":
                                    if (!field.formControl || !field.formControl.$dirty) return scope.model[fieldKey] = retrievedValue;
                                    break;

                                  case "value":
                                    return scope.model[fieldKey] = retrievedValue;

                                  case "required":
                                    return field.templateOptions.required = Boolean(retrievedValue);

                                  case "hide":
                                    return field.hide = Boolean(retrievedValue);
                                }
                            });
                        }
                    };
                }
                newField.fieldGroup || (newField.defaultValue = angular.copy(templateOptions.baseDefaultValue)), 
                "pathlink" != newField.type && array.push(newField);
            }
        }
        if (!$scope.model || "interaction" != $scope.model.parentType) return void console.log("Model is not an interaction");
        var paymentSettings = $scope.model.paymentDetails;
        if (paymentSettings && (paymentSettings.required || paymentSettings.allow)) {
            var hasPublicDetails = _.get($scope, "integration.publicDetails"), hasModule = _.get($scope, "integration.module");
            if (!hasPublicDetails || !hasModule) return console.log("Form Requires Payment, but no payment gateway was provided"), 
            void alert("This form has not been configured properly. And no payment gateway was defined. Please notify the administrator of this website immediately.");
        }
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.vm.onSubmit = submitInteraction, 
        $scope.promises = [], $scope.submitLabel = "Submit", $scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length && ($scope.submitLabel = $scope.model.data.submitLabel);
        var interactionFormSettings = $scope.model.data;
        if (interactionFormSettings || (interactionFormSettings = {}), !interactionFormSettings.allowAnonymous && !interactionFormSettings.disableDefaultFields) switch (interactionFormSettings.requireFirstName = !0, 
        interactionFormSettings.requireLastName = !0, interactionFormSettings.requireGender = !0, 
        interactionFormSettings.requireEmail = !0, interactionFormSettings.identifier) {
          case "both":
            interactionFormSettings.requireEmail = interactionFormSettings.requirePhone = !0;
            break;

          case "email":
            interactionFormSettings.requireEmail = !0;
            break;

          case "phone":
            interactionFormSettings.requirePhone = !0;
            break;

          case "either":
            interactionFormSettings.askPhone = !0, interactionFormSettings.askEmail = !0;
        }
        var firstNameField, lastNameField, genderField;
        if ((interactionFormSettings.askGender || interactionFormSettings.requireGender) && (genderField = {
            key: "_gender",
            type: "select",
            templateOptions: {
                type: "email",
                label: "Title",
                placeholder: interactionFormSettings.placeholderGender,
                options: [ {
                    name: "Mr",
                    value: "male"
                }, {
                    name: "Ms / Mrs",
                    value: "female"
                } ],
                required: interactionFormSettings.requireGender,
                onBlur: "to.focused=false",
                onFocus: "to.focused=true"
            },
            validators: {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;
                    return "male" == value || "female" == value;
                }
            }
        }), (interactionFormSettings.askFirstName || interactionFormSettings.requireFirstName) && (firstNameField = {
            key: "_firstName",
            type: "input",
            templateOptions: {
                type: "text",
                label: "First Name",
                placeholder: interactionFormSettings.placeholderFirstName,
                required: interactionFormSettings.requireFirstName,
                onBlur: "to.focused=false",
                onFocus: "to.focused=true"
            }
        }), (interactionFormSettings.askLastName || interactionFormSettings.requireLastName) && (lastNameField = {
            key: "_lastName",
            type: "input",
            templateOptions: {
                type: "text",
                label: "Last Name",
                placeholder: interactionFormSettings.placeholderLastName,
                required: interactionFormSettings.requireLastName,
                onBlur: "to.focused=false",
                onFocus: "to.focused=true"
            }
        }), genderField && firstNameField && lastNameField ? (genderField.className = "col-sm-2", 
        firstNameField.className = lastNameField.className = "col-sm-5", $scope.vm.modelFields.push({
            fieldGroup: [ genderField, firstNameField, lastNameField ],
            className: "row"
        })) : firstNameField && lastNameField && !genderField ? (firstNameField.className = lastNameField.className = "col-sm-6", 
        $scope.vm.modelFields.push({
            fieldGroup: [ firstNameField, lastNameField ],
            className: "row"
        })) : (genderField && $scope.vm.modelFields.push(genderField), firstNameField && $scope.vm.modelFields.push(firstNameField), 
        lastNameField && $scope.vm.modelFields.push(lastNameField)), interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {
            var newField = {
                key: "_email",
                type: "input",
                templateOptions: {
                    type: "email",
                    label: "Email Address",
                    placeholder: interactionFormSettings.placeholderEmail,
                    required: interactionFormSettings.requireEmail,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return value && value.length ? validator.isEmail(value) : !0;
                    }
                }
            };
            "either" == interactionFormSettings.identifier && (newField.expressionProperties = {
                "templateOptions.required": function($viewValue, $modelValue, scope) {
                    return scope.model._phoneNumber && scope.model._phoneNumber.length ? !1 : !0;
                }
            }), $scope.vm.modelFields.push(newField);
        }
        if (interactionFormSettings.askPhone || interactionFormSettings.requirePhone) {
            var newField = {
                key: "_phoneNumber",
                type: "input",
                templateOptions: {
                    type: "tel",
                    label: "Contact Phone Number",
                    placeholder: interactionFormSettings.placeholderPhoneNumber,
                    required: interactionFormSettings.requirePhone,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            };
            "either" == interactionFormSettings.identifier && (newField.expressionProperties = {
                "templateOptions.required": function($viewValue, $modelValue, scope) {
                    return scope.model._email && scope.model._email.length ? !1 : !0;
                }
            }), $scope.vm.modelFields.push(newField);
        }
        if (interactionFormSettings.askDOB || interactionFormSettings.requireDOB) {
            var newField = {
                key: "_dob",
                type: "dob-select",
                templateOptions: {
                    label: "Date of birth",
                    placeholder: interactionFormSettings.placeholderDOB,
                    required: interactionFormSettings.requireDOB,
                    maxDate: new Date(),
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            };
            $scope.vm.modelFields.push(newField);
        }
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        }), $scope.model.paymentDetails || ($scope.model.paymentDetails = {});
        var paymentSettings = $scope.model.paymentDetails;
        if (paymentSettings.required || paymentSettings.allow) {
            var paymentWrapperFields = [], paymentCardFields = [];
            if (paymentSettings.required) debugLog("-- DEBUG -- Payment Settings Required"), 
            paymentWrapperFields.push({
                templateUrl: "fluro-interaction-form/payment/payment-summary.html",
                controller: [ "$scope", "$parse", function($scope, $parse) {
                    function calculateTotal(watchString) {
                        console.log("Recalculate!!", watchString), debugLog("Recalculate total", watchString), 
                        $scope.calculatedTotal = requiredAmount;
                        var date = new Date();
                        if ($scope.date = date.getTime(), $scope.modifications = [], !paymentSettings.modifiers || !paymentSettings.modifiers.length) return void debugLog("No payment modifiers set");
                        var modificationList = _.chain(paymentSettings.modifiers).map(function(modifier) {
                            var context = {
                                date: date,
                                calculatedTotal: $scope.calculatedTotal,
                                model: $scope.model,
                                data: $scope.data,
                                matchInArray: matchInArray,
                                getAge: getAge,
                                Date: Date,
                                Math: Math,
                                String: String,
                                Date: Date,
                                parseInt: parseInt,
                                parseFloat: parseFloat,
                                Boolean: Boolean,
                                Number: Number
                            }, parsedValue = $parse(modifier.expression)(context);
                            if (parsedValue = Number(parsedValue), isNaN(parsedValue)) return void debugLog("Payment modifier error", modifier.title, parsedValue);
                            var parsedCondition = !0;
                            if (modifier.condition && String(modifier.condition).length && (parsedCondition = $parse(modifier.condition)(context)), 
                            !parsedCondition) return void debugLog("inactive", modifier.title, modifier, $scope);
                            var currencySymbol = "$";
                            switch (paymentSettings.currency) {
                              case "gbp":
                                currencySymbol = "£";
                            }
                            var operator = "", operatingValue = currencySymbol + parseFloat(parsedValue / 100).toFixed(2);
                            switch (modifier.operation) {
                              case "add":
                                operator = "+", $scope.calculatedTotal = $scope.calculatedTotal + parsedValue;
                                break;

                              case "subtract":
                                operator = "-", $scope.calculatedTotal = $scope.calculatedTotal - parsedValue;
                                break;

                              case "divide":
                                operator = "/", operatingValue = parsedValue, $scope.calculatedTotal = $scope.calculatedTotal / parsedValue;
                                break;

                              case "multiply":
                                operator = "x", operatingValue = parsedValue, $scope.calculatedTotal = $scope.calculatedTotal * parsedValue;
                                break;

                              case "set":
                                $scope.calculatedTotal = parsedValue;
                            }
                            var readableOperator = operator + " " + operatingValue;
                            parsedValue || (readableOperator = "");
                            var resultModified = {
                                title: modifier.title,
                                total: $scope.calculatedTotal,
                                description: readableOperator,
                                operation: modifier.operation
                            };
                            return resultModified;
                        }).compact().value();
                        if (modificationList && modificationList.length) {
                            var lastSetIndex = _.findLastIndex(modificationList, function(mod) {
                                return "set" == mod.operation;
                            });
                            -1 == lastSetIndex ? $scope.hasSetModification = !1 : $scope.hasSetModification = !0, 
                            lastSetIndex && -1 != lastSetIndex ? $scope.modifications = _.slice(modificationList, lastSetIndex) : $scope.modifications = modificationList;
                        }
                        debugLog("Visible Modifications", modificationList), (!$scope.calculatedTotal || isNaN($scope.calculatedTotal) || $scope.calculatedTotal < 0) && ($scope.calculatedTotal = 0), 
                        $timeout(function() {
                            $formScope.vm.model.calculatedTotal = $scope.calculatedTotal;
                        });
                    }
                    switch ($scope.currencySymbol = "$", paymentSettings.currency) {
                      case "gbp":
                        $scope.currencySymbol = "£";
                    }
                    $scope.paymentDetails = paymentSettings;
                    var requiredAmount = paymentSettings.amount;
                    $formScope.vm.model.calculatedTotal = requiredAmount, $scope.calculatedTotal = requiredAmount;
                    var watchString = "";
                    $scope.matchInArray = matchInArray, $scope.data = $formScope.vm.model;
                    var modelVariables = _.chain(paymentSettings.modifiers).map(function(paymentModifier) {
                        var string = "";
                        return string = paymentModifier.condition && paymentModifier.condition.length ? "(" + paymentModifier.expression + ") + (" + paymentModifier.condition + ")" : "(" + paymentModifier.expression + ")";
                    }).flatten().compact().uniq().value();
                    modelVariables.length && (watchString = modelVariables.join(' + "a" + ')), watchString.length ? (debugLog("Watching changes", watchString), 
                    $scope.$watch(watchString, calculateTotal)) : ($scope.calculatedTotal = requiredAmount, 
                    $scope.modifications = []);
                } ]
            }); else {
                var amountDescription = "Please enter an amount (" + String(paymentSettings.currency).toUpperCase() + ") ", minimum = paymentSettings.minAmount, maximum = paymentSettings.maxAmount, paymentErrorMessage = (paymentSettings.amount, 
                "Invalid amount");
                minimum && (minimum = parseInt(minimum) / 100, paymentErrorMessage = "Amount must be a number at least " + $filter("currency")(minimum, "$"), 
                amountDescription += "Enter at least " + $filter("currency")(minimum, "$") + " " + String(paymentSettings.currency).toUpperCase()), 
                maximum && (maximum = parseInt(maximum) / 100, paymentErrorMessage = "Amount must be a number less than " + $filter("currency")(maximum, "$"), 
                amountDescription += "Enter up to " + $filter("currency")(maximum, "$") + " " + String(paymentSettings.currency).toUpperCase()), 
                minimum && maximum && (amountDescription = "Enter a numeric amount between " + $filter("currency")(minimum) + " and  " + $filter("currency")(maximum) + " " + String(paymentSettings.currency).toUpperCase(), 
                paymentErrorMessage = "Amount must be a number between " + $filter("currency")(minimum) + " and " + $filter("currency")(maximum));
                var fieldConfig = {
                    key: "_paymentAmount",
                    type: "currency",
                    templateOptions: {
                        type: "text",
                        label: "Amount",
                        description: amountDescription,
                        placeholder: "0.00",
                        required: !0,
                        errorMessage: paymentErrorMessage,
                        min: minimum,
                        max: maximum,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    },
                    data: {
                        customMaxLength: 8,
                        minimumAmount: minimum,
                        maximumAmount: maximum
                    }
                };
                minimum && (fieldConfig.defaultValue = minimum), paymentWrapperFields.push({
                    template: "<hr/><h3>Payment Details</h3>"
                }), paymentWrapperFields.push(fieldConfig);
            }
            var defaultCardName, defaultCardNumber, defaultCardExpMonth, defaultCardExpYear, defaultCardCVN;
            $scope.debugMode && (defaultCardName = "John Citizen", defaultCardNumber = "4242424242424242", 
            defaultCardExpMonth = "05", defaultCardExpYear = "2020", defaultCardCVN = "123"), 
            paymentCardFields.push({
                key: "_paymentCardName",
                type: "input",
                defaultValue: defaultCardName,
                templateOptions: {
                    type: "text",
                    label: "Name on Card",
                    placeholder: "eg. (John Smith)",
                    errorMessage: "Card Name is required",
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return String(value).length > 0;
                    }
                }
            }), paymentCardFields.push({
                key: "_paymentCardNumber",
                type: "input",
                defaultValue: defaultCardNumber,
                name: "PaymentCardNumber",
                templateOptions: {
                    type: "text",
                    label: "Card Number",
                    placeholder: "Card Number (No dashes or spaces)",
                    errorMessage: "Invalid Card Number",
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var luhnChk = function(a) {
                            return function(c) {
                                if (!c) return !1;
                                for (var v, l = c.length, b = 1, s = 0; l; ) v = parseInt(c.charAt(--l), 10), s += (b ^= 1) ? a[v] : v;
                                return s && 0 === s % 10;
                            };
                        }([ 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 ]), value = $modelValue || $viewValue, valid = luhnChk(value) && String(value).length > 15;
                        return valid;
                    }
                }
            }), paymentCardFields.push({
                className: "row clearfix",
                fieldGroup: [ {
                    key: "_paymentCardExpMonth",
                    className: "col-xs-6 col-sm-5",
                    type: "input",
                    defaultValue: defaultCardExpMonth,
                    templateOptions: {
                        type: "text",
                        label: "Expiry Month",
                        placeholder: "MM",
                        errorMessage: "Invalid Card Expiry Month",
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return 2 == String(value).length;
                        }
                    }
                }, {
                    key: "_paymentCardExpYear",
                    className: "col-xs-6 col-sm-5",
                    type: "input",
                    defaultValue: defaultCardExpYear,
                    templateOptions: {
                        type: "text",
                        label: "Expiry Year",
                        placeholder: "YYYY",
                        errorMessage: "Invalid Card Expiry Year",
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return 4 == String(value).length;
                        }
                    }
                }, {
                    key: "_paymentCardCVN",
                    className: "col-xs-4 col-sm-2",
                    type: "input",
                    defaultValue: defaultCardCVN,
                    templateOptions: {
                        type: "text",
                        label: "CVN",
                        placeholder: "CVN",
                        errorMessage: "Invalid Card CVN",
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return String(value).length > 0;
                        }
                    }
                } ]
            }), (interactionFormSettings.allowAnonymous || interactionFormSettings.askReceipt) && paymentCardFields.push({
                key: "_paymentEmail",
                type: "input",
                templateOptions: {
                    type: "email",
                    label: "Receipt Email Address",
                    placeholder: "Enter an email address for transaction receipt",
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            });
            var cardDetailsField = {
                className: "payment-details",
                key: "_paymentCardDetails",
                fieldGroup: paymentCardFields,
                hideExpression: function($viewValue, $modelValue, scope) {
                    return 0 === $formScope.vm.model.calculatedTotal ? (debugLog("HIDE PAYMENT NO AMOUNT DUE", $formScope.vm.model.calculatedTotal), 
                    !0) : paymentSettings.allowAlternativePayments && "card" != $formScope.vm.model._paymentMethod ? (debugLog("HIDE PAYMENT NOT CARD", $formScope.vm.model._paymentMethod), 
                    !0) : void 0;
                }
            }, alternativeMethodsAvailable = _.get(paymentSettings, "paymentMethods.length");
            if (paymentSettings.allowAlternativePayments && alternativeMethodsAvailable) {
                var methodSelection = {
                    className: "payment-method-select",
                    data: {
                        fields: [ cardDetailsField ],
                        settings: paymentSettings
                    },
                    controller: [ "$scope", function($scope) {
                        $scope.settings = paymentSettings, $scope.methodOptions = _.map(paymentSettings.paymentMethods, function(method) {
                            return method;
                        }), $scope.methodOptions.unshift({
                            title: "Pay with Card",
                            key: "card"
                        }), $scope.model._paymentMethod || (console.log("NO PAYMENT METHOD SO SET TO CARD"), 
                        $scope.model._paymentMethod = "card"), $scope.selected = {
                            method: $scope.methodOptions[0]
                        }, $scope.selectMethod = function(method) {
                            console.log("SELECT METHOD", method), $scope.settings.showOptions = !1, $scope.selected.method = method, 
                            $scope.model._paymentMethod = method.key;
                        };
                    } ],
                    templateUrl: "fluro-interaction-form/payment/payment-method.html",
                    hideExpression: function($viewValue, $modelValue, scope) {
                        return 0 === $formScope.vm.model.calculatedTotal ? (debugLog("HIDE BECAUSE CALCULATED TOTAL IS 0"), 
                        !0) : void 0;
                    }
                };
                paymentWrapperFields.push(methodSelection);
            } else paymentWrapperFields.push(cardDetailsField);
            $scope.vm.modelFields.push({
                fieldGroup: paymentWrapperFields
            });
        }
        $scope.promises.length ? ($scope.promisesResolved = !1, $q.all($scope.promises).then(function() {
            $scope.promisesResolved = !0;
        })) : $scope.promisesResolved = !0;
    }
    function submitInteraction() {
        function tokenCreated(status, response) {
            $timeout(function() {
                return response.error ? (console.log("Tithely token error", response), $scope.processErrorMessages = [ response.error.message ], 
                $scope.vm.state = "error", void 0) : (console.log("Got tithely token!", response), 
                paymentDetails.details = response, processRequest());
            });
        }
        function processRequest() {
            function submissionSuccess(res) {
                console.log("FORM -> Submission Success"), $scope.callback && $scope.callback(res), 
                $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
                $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), $formScope = $scope, 
                $scope.response = res, $scope.vm.state = "complete";
            }
            function submissionFail(res) {
                return console.log("FORM -> Submission Failed", res), $scope.vm.state = "error", 
                res.data ? res.data.error ? res.data.error.message ? $scope.processErrorMessages = [ res.error.message ] : $scope.processErrorMessages = [ res.error ] : res.data.errors ? $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                    return error.message;
                }) : _.isArray(res.data) ? $scope.processErrorMessages = res.data : void ($scope.processErrorMessages = [ res.data ]) : $scope.processErrorMessages = [ "Error: " + res ];
            }
            console.log("FORM -> Process Request");
            var receiptEmail = _.get(interactionDetails, "_paymentCardDetails._paymentEmail");
            receiptEmail && receiptEmail.length && (console.log("FORM -> Setting payment receipt email address", receiptEmail), 
            paymentDetails.email = receiptEmail), delete interactionDetails._paymentCardDetails, 
            interactionDetails._paymentAmount && (paymentDetails.amount = 100 * parseFloat(interactionDetails._paymentAmount));
            var definitionID = $scope.model._id;
            definitionID._id && (definitionID = definitionID._id);
            var params = {
                definition: definitionID
            };
            $scope.linkedProcess && (params.process = $scope.linkedProcess);
            var request = FluroInteraction.interact($scope.model.title, interactionKey, interactionDetails, paymentDetails, $scope.linkedEvent, params);
            request.then(submissionSuccess, submissionFail);
        }
        console.log("FORM -> Submit Interaction"), $scope.vm.state = "sending";
        var requiresPayment, allowsPayment, interactionKey = $scope.model.definitionName, interactionDetails = angular.copy($scope.vm.model), paymentConfiguration = $scope.model.paymentDetails;
        if (paymentConfiguration && (requiresPayment = paymentConfiguration.required, allowsPayment = paymentConfiguration.allow), 
        !requiresPayment && !allowsPayment) return console.log("FORM -> No payment required"), 
        processRequest();
        console.log("FORM -> Requires Payment", requiresPayment, allowsPayment);
        var paymentDetails = {};
        if (paymentConfiguration.allowAlternativePayments && paymentConfiguration.paymentMethods) {
            var selectedMethod = interactionDetails._paymentMethod;
            if (selectedMethod && "card" != selectedMethod) return paymentDetails.method = selectedMethod, 
            console.log("FORM -> Alternative Payment Method"), processRequest();
        }
        if (requiresPayment && !$formScope.vm.model.calculatedTotal) return console.log("FORM -> No Payment Total Due"), 
        processRequest();
        var paymentIntegration = $scope.integration;
        if (!paymentIntegration || !paymentIntegration.publicDetails) return alert("This form has not been configured properly. And no payment gateway was defined. Please notify the administrator of this website immediately."), 
        void ($scope.vm.state = "ready");
        paymentDetails.integration = paymentIntegration._id;
        var cardDetails = interactionDetails._paymentCardDetails, SANDBOX_ENV = _.get(paymentIntegration, "publicDetails.sandbox") || _.get(window, "applicationData.sandbox");
        switch (paymentDetails.sandbox = SANDBOX_ENV, console.log("CHECK PAYMENT MODULE", paymentIntegration), 
        paymentIntegration.module) {
          case "eway":
            if (!window.eCrypt) return console.log("ERROR: Eway is selected for payment but the eCrypt script has not been included in this application visit https://eway.io/api-v3/#encrypt-function for more information"), 
            void ($scope.vm.state = "ready");
            var key = paymentIntegration.publicDetails.publicKey, ewayCardDetails = {};
            ewayCardDetails.name = cardDetails._paymentCardName, ewayCardDetails.number = eCrypt.encryptValue(cardDetails._paymentCardNumber, key), 
            ewayCardDetails.cvc = eCrypt.encryptValue(cardDetails._paymentCardCVN, key);
            var expiryMonth = String(cardDetails._paymentCardExpMonth), expiryYear = String(cardDetails._paymentCardExpYear);
            return expiryMonth.length < 1 && (expiryMonth = "0" + expiryMonth), ewayCardDetails.exp_month = expiryMonth, 
            ewayCardDetails.exp_year = expiryYear.slice(-2), paymentDetails.details = ewayCardDetails, 
            processRequest();

          case "tithely":
            if (!window.Tithely) return console.log("ERROR: Tithely is selected for payment but the Tithly API has not been included in this application"), 
            void ($scope.vm.state = "ready");
            var tithelyCardDetails = {};
            tithelyCardDetails.card_name = cardDetails._paymentCardName, tithelyCardDetails.card_number = cardDetails._paymentCardNumber, 
            tithelyCardDetails.card_cvc = cardDetails._paymentCardCVN, tithelyCardDetails.card_expiry_month = cardDetails._paymentCardExpMonth, 
            tithelyCardDetails.card_expiry_year = cardDetails._paymentCardExpYear, tithelyController.add_payment_method(tithelyCardDetails, tokenCreated);
            break;

          case "stripe":
            if (!window.Stripe) return console.log("ERROR: Stripe is selected for payment but the Stripe API has not been included in this application"), 
            void ($scope.vm.state = "ready");
            console.log("FORM -> Send Request to Stripe");
            var liveKey = paymentIntegration.publicDetails.livePublicKey, sandboxKey = paymentIntegration.publicDetails.testPublicKey, key = liveKey;
            SANDBOX_ENV ? (key = sandboxKey, console.log("FORM -> Tokenizing using test key")) : console.log("FORM -> Tokenizing using live key"), 
            Stripe.setPublishableKey(key);
            var stripeCardDetails = {};
            stripeCardDetails.name = cardDetails._paymentCardName, stripeCardDetails.number = cardDetails._paymentCardNumber, 
            stripeCardDetails.cvc = cardDetails._paymentCardCVN, stripeCardDetails.exp_month = cardDetails._paymentCardExpMonth, 
            stripeCardDetails.exp_year = cardDetails._paymentCardExpYear, Stripe.card.createToken(stripeCardDetails, function(status, response) {
                $timeout(function() {
                    return response.error ? (console.log("FORM -> Stripe token error", response), $scope.processErrorMessages = [ response.error.message ], 
                    $scope.vm.state = "error", void 0) : (console.log("FORM -> Stripe tokenized"), paymentDetails.details = response, 
                    processRequest());
                });
            });
            break;

          default:
            console.log("FORM -> NO VALID PAYMENT MODULE PROVIDED");
        }
    }
    var tithelyController;
    _.get(window, "applicationData.staging") && ($scope.debugMode = !0), $scope.vm || ($scope.vm = {}), 
    $formScope = $scope, $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
    $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.correctPermissions = !0, 
    $scope.scrollToField = function(field) {
        field.field && (field = field.field);
        var element = _.get(field, "formControl.$$element[0]");
        element || (element = document.querySelector("#" + field.$name), console.log("ID", field, field.$name, element)), 
        element && element.scrollIntoView && element.scrollIntoView();
    }, $scope.readyToSubmit = !1, $scope.$watch("vm.modelFields", validateAllFields, !0), 
    $scope.$watch("vm.model", validateAllFields, !0), $scope.$watch("model", definitionModelChanged), 
    $scope.$watch("integration", integrationChanged);
    var timer;
    formlyValidationMessages.addStringMessage("required", "This field is required"), 
    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid value";
    }, formlyValidationMessages.messages.uploadInProgress = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is still processing";
    }, formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid date";
    }, $scope.reset = function() {
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), definitionModelChanged(), 
        $scope.response = null, $scope.vm.state = "ready", $scope.$broadcast("form-reset");
    };
    var injectedScripts = {};
}), app.directive("postForm", function($compile) {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel",
            host: "=hostId",
            reply: "=?reply",
            thread: "=?thread",
            userStore: "=?user",
            vm: "=?config",
            debugMode: "=?debugMode",
            callback: "=?callback"
        },
        transclude: !0,
        controller: "PostFormController",
        templateUrl: "fluro-interaction-form/fluro-web-form.html",
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        }
    };
}), app.directive("recaptchaRender", function($window) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs, $ctrl) {
            function activateRecaptcha(recaptcha) {
                console.log("Activate recaptcha!!"), cancelWatch && cancelWatch(), recaptcha && ($scope.vm.recaptchaID = recaptcha.render(element, {
                    sitekey: "6LelOyUTAAAAADSACojokFPhb9AIzvrbGXyd-33z"
                }));
            }
            if ($scope.model.data && $scope.model.data.recaptcha) {
                var cancelWatch, element = $element[0];
                window.grecaptcha ? activateRecaptcha(window.grecaptcha) : cancelWatch = $scope.$watch(function() {
                    return window.grecaptcha;
                }, activateRecaptcha);
            }
        }
    };
}), app.controller("PostFormController", function($scope, $rootScope, $timeout, $q, $http, Fluro, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {
    function validateAllFields() {
        timer && ($timeout.cancel(timer), timer = null), timer = $timeout(function() {
            function setValid(ready, list) {
                $scope.readyToSubmit = ready, ready ? $scope.errorList = null : $scope.errorList = list;
            }
            function mapRecursiveField(field) {
                if (field) {
                    var output = [ field ], isDisabled = _.get(field, "templateOptions.definition.disableValidation");
                    if (!isDisabled) {
                        if (field.fields && field.fields.length && output.push(_.map(field.fields, mapRecursiveField)), 
                        field.fieldGroup && field.fieldGroup.length && output.push(_.map(field.fieldGroup, mapRecursiveField)), 
                        field.data && field.data.replicatedFields && field.data.replicatedFields.length) {
                            var nestedForms = _.chain(field.data.replicatedFields).flatten().map(mapRecursiveField).value();
                            output.push(nestedForms);
                        } else field.data && field.data.fields && field.data.fields.length && output.push(_.map(field.data.fields, mapRecursiveField));
                        return output;
                    }
                }
            }
            timer = null;
            var errorList = _.chain($scope.vm.modelFields).map(mapRecursiveField).flattenDeep().compact().filter(function(field) {
                var required = _.get(field, "formControl.$error.required"), invalid = _.get(field, "formControl.$error.validInput"), uploadInProgress = _.get(field, "formControl.$error.uploadInProgress"), shouldwarning = required || invalid || uploadInProgress;
                return shouldwarning;
            }).value(), interactionForm = $scope.vm.modelForm;
            if (!interactionForm) return setValid(!1, errorList);
            if (interactionForm.$invalid) {
                if (!errorList || !errorList.length) {
                    var requiredValues = _.get(interactionForm, "$error.validInput");
                    errorList = _.map(requiredValues, function(f, index) {
                        return {
                            id: index,
                            message: f.$viewValue + " is an invalid value for " + f.$name
                        };
                    });
                }
                return setValid(!1, errorList);
            }
            if (interactionForm.$error) {
                if (interactionForm.$error.required && interactionForm.$error.required.length) return setValid(!1, errorList);
                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) return setValid(!1, errorList);
                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) return setValid(!1, errorList);
            }
            setValid(!0, errorList);
        }, 200);
    }
    function resetCaptcha() {
        var recaptchaID = $scope.vm.recaptchaID;
        console.log("Reset Captcha", recaptchaID), window.grecaptcha && recaptchaID && window.grecaptcha.reset(recaptchaID);
    }
    function submitPost() {
        function submissionSuccess(res) {
            $scope.callback && $scope.callback(res), $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {
                data: {}
            }, $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), resetCaptcha(), 
            $scope.response = res, $scope.thread && $scope.thread.push(res), $scope.vm.state = "complete";
        }
        function submissionFail(res) {
            return $scope.vm.state = "error", res.data ? res.data.error ? res.data.error.message ? $scope.processErrorMessages = [ res.error.message ] : $scope.processErrorMessages = [ res.error ] : res.data.errors ? $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                return error.message;
            }) : _.isArray(res.data) ? $scope.processErrorMessages = res.data : void ($scope.processErrorMessages = [ res.data ]) : $scope.processErrorMessages = [ "Error: " + res ];
        }
        $scope.vm.state = "sending";
        var submissionKey = $scope.model.definitionName, submissionModel = {
            data: angular.copy($scope.vm.model)
        }, hostID = $scope.host;
        if ($scope.reply && (submissionModel.reply = $scope.reply), "undefined" != typeof $scope.vm.recaptchaID) {
            var response = window.grecaptcha.getResponse($scope.vm.recaptchaID);
            submissionModel["g-recaptcha-response"] = response;
        }
        var request;
        $scope.userStore ? $scope.userStore.config().then(function(config) {
            var postURL = Fluro.apiURL + "/post/" + hostID + "/" + submissionKey;
            request = $http.post(postURL, submissionModel, config), request.then(function(res) {
                return submissionSuccess(res.data);
            }, function(res) {
                return submissionFail(res.data);
            });
        }) : (request = FluroContent.endpoint("post/" + hostID + "/" + submissionKey).save(submissionModel).$promise, 
        request.then(submissionSuccess, submissionFail));
    }
    $scope.thread || ($scope.thread = []), $scope.vm || ($scope.vm = {}), $scope.promisesResolved = !0, 
    $scope.correctPermissions = !0, $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
    $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.readyToSubmit = !1, 
    $scope.$watch("vm.modelFields", validateAllFields, !0), $scope.$watch("vm.model", validateAllFields, !0), 
    $scope.$watch("model", validateAllFields);
    var timer;
    formlyValidationMessages.addStringMessage("required", "This field is required"), 
    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid value";
    }, formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid date";
    }, $scope.reset = function() {
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), resetCaptcha(), 
        $scope.response = null, $scope.vm.state = "ready", console.log("Broadcast reset"), 
        $scope.$broadcast("form-reset");
    }, $scope.$watch("model", function(newData, oldData) {
        function addFieldDefinition(array, fieldDefinition) {
            if (!fieldDefinition.params || !fieldDefinition.params.disableWebform) {
                var newField = {};
                newField.key = fieldDefinition.key, fieldDefinition.className && (newField.className = fieldDefinition.className);
                var templateOptions = {};
                switch (templateOptions.type = "text", templateOptions.label = fieldDefinition.title, 
                templateOptions.description = fieldDefinition.description, templateOptions.params = fieldDefinition.params, 
                fieldDefinition.errorMessage && (templateOptions.errorMessage = fieldDefinition.errorMessage), 
                templateOptions.definition = fieldDefinition, fieldDefinition.placeholder && fieldDefinition.placeholder.length ? templateOptions.placeholder = fieldDefinition.placeholder : fieldDefinition.description && fieldDefinition.description.length ? templateOptions.placeholder = fieldDefinition.description : templateOptions.placeholder = fieldDefinition.title, 
                templateOptions.required = fieldDefinition.minimum > 0, templateOptions.onBlur = "to.focused=false", 
                templateOptions.onFocus = "to.focused=true", fieldDefinition.type) {
                  case "reference":
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                        return {
                            name: ref.title,
                            value: ref._id
                        };
                    }); else if (templateOptions.options = [], fieldDefinition.sourceQuery) {
                        var queryId = fieldDefinition.sourceQuery;
                        queryId._id && (queryId = queryId._id);
                        var options = {};
                        fieldDefinition.queryTemplate && (options.template = fieldDefinition.queryTemplate, 
                        options.template._id && (options.template = options.template._id));
                        var promise = FluroContentRetrieval.getQuery(queryId, options);
                        promise.then(function(res) {
                            templateOptions.options = _.map(res, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id
                                };
                            });
                        });
                    } else "embedded" != fieldDefinition.directive && fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length && FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                        templateOptions.options = _.map(res, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id
                            };
                        });
                    });
                    break;

                  default:
                    fieldDefinition.options && fieldDefinition.options.length ? templateOptions.options = fieldDefinition.options : templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                        return {
                            name: val,
                            value: val
                        };
                    });
                }
                switch (fieldDefinition.directive) {
                  case "reference-select":
                    switch (window.DEVICE_PLATFORM) {
                      case "ios":
                      case "android":
                        newField.type = "reference-select";
                        break;

                      default:
                        newField.type = "select";
                    }
                    break;

                  case "value-select":
                    newField.type = "button-select";
                    break;

                  case "select":
                    newField.type = "select";
                    break;

                  case "wysiwyg":
                    newField.type = "textarea";
                    break;

                  default:
                    newField.type = fieldDefinition.directive;
                }
                if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length && (newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = "customAttr" + key;
                    return results[customKey] = {
                        attribute: key
                    }, templateOptions[customKey] = attr, results;
                }, {})), "select" == newField.type && (newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                1 != fieldDefinition.maximum && (newField.ngModelAttrs.multiple = {
                    value: "multiple"
                })), "custom" != fieldDefinition.directive) switch (fieldDefinition.type) {
                  case "boolean":
                    fieldDefinition.params && fieldDefinition.params.storeCopy ? newField.type = "terms" : newField.type = "checkbox";
                    break;

                  case "number":
                  case "float":
                  case "integer":
                  case "decimal":
                    templateOptions.type = "input", newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                    "integer" == fieldDefinition.type && (templateOptions.type = "number", templateOptions.baseDefaultValue = 0, 
                    newField.ngModelAttrs.customAttrpattern = {
                        attribute: "pattern"
                    }, newField.ngModelAttrs.customAttrinputmode = {
                        attribute: "inputmode"
                    }, templateOptions.customAttrpattern = "[0-9]*", templateOptions.customAttrinputmode = "numeric", 
                    fieldDefinition.params && (0 !== parseInt(fieldDefinition.params.maxValue) && (templateOptions.max = fieldDefinition.params.maxValue), 
                    0 !== parseInt(fieldDefinition.params.minValue) ? templateOptions.min = fieldDefinition.params.minValue : templateOptions.min = 0));
                }
                if (1 == fieldDefinition.maximum ? "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length && ("search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0] : templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id) : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0]) : "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length ? "search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences : templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                    return ref._id;
                }) : templateOptions.baseDefaultValue = [] : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                    return Number(val);
                }) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues), newField.templateOptions = templateOptions, 
                newField.validators = {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        if (!value) return !0;
                        var valid = FluroValidate.validate(value, fieldDefinition);
                        return valid;
                    }
                }, "embedded" == fieldDefinition.directive) {
                    if (newField.type = "embedded", 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {
                        data: {}
                    }; else {
                        var askCount = 0;
                        fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                        fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                        var initialArray = [];
                        askCount && _.times(askCount, function() {
                            initialArray.push({});
                        }), templateOptions.baseDefaultValue = initialArray;
                    }
                    newField.data = {
                        fields: [],
                        dataFields: [],
                        replicatedFields: []
                    };
                    var fieldContainer = newField.data.fields, dataFieldContainer = newField.data.dataFields;
                    fieldDefinition.fields && fieldDefinition.fields.length && _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                    var promise = FluroContent.endpoint("defined/" + fieldDefinition.params.restrictType).get().$promise;
                    promise.then(function(embeddedDefinition) {
                        if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                            var childFields = embeddedDefinition.fields;
                            fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length && (childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            })), _.each(childFields, function(sub) {
                                addFieldDefinition(dataFieldContainer, sub);
                            });
                        }
                    }), $scope.promises.push(promise);
                }
                if ("group" == fieldDefinition.type && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                    var fieldContainer;
                    if (fieldDefinition.asObject) {
                        if (newField.type = "nested", fieldDefinition.key && 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {}; else {
                            var askCount = 0;
                            fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                            fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                            var initialArray = [];
                            askCount && _.times(askCount, function() {
                                initialArray.push({});
                            }), templateOptions.baseDefaultValue = initialArray;
                        }
                        newField.data = {
                            fields: [],
                            replicatedFields: []
                        }, fieldContainer = newField.data.fields;
                    } else newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className
                    }, fieldContainer = newField.fieldGroup;
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }
                if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {
                    fieldDefinition.hideExpression && fieldDefinition.hideExpression.length && (fieldDefinition.expressions.hide = fieldDefinition.hideExpression);
                    var allExpressions = _.values(fieldDefinition.expressions).join("+");
                    newField.watcher = {
                        expression: function(field, scope) {
                            return $parse(allExpressions)(scope);
                        },
                        listener: function(field, newValue, oldValue, scope, stopWatching) {
                            scope.interaction || (scope.interaction = $scope.vm.model), _.each(fieldDefinition.expressions, function(expression, key) {
                                var retrievedValue = $parse(expression)(scope), fieldKey = field.key;
                                switch (key) {
                                  case "defaultValue":
                                    if (!field.formControl || !field.formControl.$dirty) return scope.model[fieldKey] = retrievedValue;
                                    break;

                                  case "value":
                                    return scope.model[fieldKey] = retrievedValue;

                                  case "required":
                                    return field.templateOptions.required = retrievedValue;

                                  case "hide":
                                    return field.hide = retrievedValue;
                                }
                            });
                        }
                    };
                }
                fieldDefinition.hideExpression && (newField.hideExpression = fieldDefinition.hideExpression), 
                newField.fieldGroup || (newField.defaultValue = angular.copy(templateOptions.baseDefaultValue)), 
                "pathlink" != newField.type && array.push(newField);
            }
        }
        if ($scope.model && "post" == $scope.model.parentType) {
            $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
            $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.vm.onSubmit = submitPost, 
            $scope.promises = [], $scope.submitLabel = "Submit", $scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length && ($scope.submitLabel = $scope.model.data.submitLabel);
            var interactionFormSettings = $scope.model.data;
            interactionFormSettings || (interactionFormSettings = {}), _.each($scope.model.fields, function(fieldDefinition) {
                addFieldDefinition($scope.vm.modelFields, fieldDefinition);
            });
        }
    });
}), app.directive("postThread", function() {
    return {
        restrict: "E",
        transclude: !0,
        scope: {
            definitionName: "=?type",
            host: "=?hostId",
            thread: "=?thread"
        },
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.replaceWith(clone);
            });
        },
        controller: function($scope, $filter, $rootScope, FluroContent) {
            function reloadThread() {
                return console.log("Reload thread", "post." + hostID + "." + definitionName), FluroContent.endpoint("post/" + hostID + "/" + definitionName, !0, !0).query().$promise.then(threadLoaded, threadError);
            }
            function threadLoaded(res) {
                console.log("Thread reloaded", res);
                var allPosts = res;
                $scope.thread = _.chain(res).map(function(post) {
                    return post.thread = _.filter(allPosts, function(sub) {
                        return sub.reply == post._id;
                    }), post.reply ? void 0 : post;
                }).compact().value();
            }
            function threadError(err) {
                console.log("Thread Error", err), $scope.thread = [];
            }
            $scope.outer = $scope.$parent, $scope.thread || ($scope.thread = []);
            var hostID, definitionName;
            $scope.$watch("host + definitionName", function() {
                if (hostID = $scope.host, definitionName = $scope.definitionName, hostID && definitionName) {
                    var threadRefreshEvent = "post." + hostID + "." + definitionName;
                    return console.log("LISTENING FOR", threadRefreshEvent), $rootScope.$on(threadRefreshEvent, reloadThread), 
                    reloadThread();
                }
            });
        }
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "nested",
        templateUrl: "fluro-interaction-form/nested/fluro-nested.html",
        controller: "FluroInteractionNestedController"
    });
}), app.controller("FluroInteractionNestedController", function($scope, $timeout) {
    function resetDefaultValue() {
        var defaultValue = angular.copy($scope.to.baseDefaultValue);
        $scope.model && ($scope.model[$scope.options.key] = defaultValue);
    }
    function guid() {
        function s4() {
            return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1);
        }
        return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
    }
    var def = $scope.to.definition, minimum = def.minimum, maximum = def.maximum;
    $scope.$watch("model[options.key]", function(model) {
        model || resetDefaultValue();
    }), $scope.$on("form-reset", resetDefaultValue), $scope.addAnother = function() {
        console.log("Add another"), $scope.model[$scope.options.key].push({
            id: guid()
        });
    }, $scope.removeEntry = function($index) {
        var modelEntries = $scope.model[$scope.options.key], replicatedEntries = _.get($scope.options, "data.replicatedFields"), replicatedDataEntries = _.get($scope.options, "data.replicatedDataFields"), replicatedDetailEntries = _.get($scope.options, "data.replicatedDetailFields");
        modelEntries.splice($index, 1), replicatedEntries.splice($index, 1), replicatedDataEntries.splice($index, 1), 
        replicatedDetailEntries.splice($index, 1), console.log("REMOVE", $index, replicatedEntries);
    }, $scope.canRemove = function() {
        return minimum ? $scope.model[$scope.options.key].length > minimum ? !0 : void 0 : !0;
    }, $scope.canAdd = function() {
        return maximum ? $scope.model[$scope.options.key].length < maximum ? !0 : void 0 : !0;
    }, $scope.copyFields = function() {
        var copiedFields = angular.copy($scope.options.data.fields);
        return $scope.options.data.replicatedFields.push(copiedFields), copiedFields;
    }, $scope.copyDataFields = function() {
        var copiedFields = angular.copy($scope.options.data.dataFields);
        return $scope.options.data.replicatedDataFields.push(copiedFields), copiedFields;
    }, $scope.copyDetailFields = function() {
        var copiedFields = angular.copy($scope.options.data.detailFields);
        return $scope.options.data.replicatedDetailFields.push(copiedFields), copiedFields;
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "search-select",
        templateUrl: "fluro-interaction-form/search-select/fluro-search-select.html",
        controller: "FluroSearchSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.controller("FluroSearchSelectController", function($scope, $http, Fluro, $filter, FluroValidate) {
    function setModel() {
        $scope.multiple ? $scope.model[opts.key] = angular.copy($scope.selection.values) : $scope.model[opts.key] = angular.copy($scope.selection.value), 
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }
    function checkValidity() {
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    $scope.search = {}, $scope.proposed = {};
    var opts = ($scope.to, $scope.options);
    $scope.selection = {};
    var definition = $scope.to.definition;
    definition.params || (definition.params = {});
    var restrictType = definition.params.restrictType, searchLimit = definition.params.searchLimit;
    searchLimit || (searchLimit = 10);
    var minimum = definition.minimum, maximum = definition.maximum;
    if (minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum, 
    $scope.multiple ? $scope.model[opts.key] && _.isArray($scope.model[opts.key]) && ($scope.selection.values = angular.copy($scope.model[opts.key])) : $scope.model[opts.key] && ($scope.selection.value = $scope.model[opts.key]), 
    $scope.canAddMore = function() {
        return maximum ? $scope.multiple ? $scope.selection.values.length < maximum : $scope.selection.value ? void 0 : !0 : !0;
    }, $scope.contains = function(value) {
        return $scope.multiple ? _.includes($scope.selection.values, value) : $scope.selection.value == value;
    }, $scope.$watch("model", function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {
            var modelValue;
            _.keys(newModelValue).length && (modelValue = newModelValue[opts.key], $scope.multiple ? modelValue && _.isArray(modelValue) ? $scope.selection.values = angular.copy(modelValue) : $scope.selection.values = [] : $scope.selection.value = angular.copy(modelValue));
        }
    }, !0), opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
    $scope.select = function(value) {
        if ($scope.multiple) {
            if (!$scope.canAddMore()) return;
            $scope.selection.values.push(value);
        } else $scope.selection.value = value;
        $scope.proposed = {}, setModel();
    }, $scope.retrieveReferenceOptions = function(val) {
        var searchUrl = Fluro.apiURL + "/content";
        return restrictType && (searchUrl += "/" + restrictType), searchUrl += "/search", 
        $http.get(searchUrl + "/" + val, {
            ignoreLoadingBar: !0,
            params: {
                limit: searchLimit
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selection.values, {
                    _id: item._id
                });
                return exists || filtered.push(item), filtered;
            }, []);
        });
    }, $scope.getValueLabel = function(value) {
        if (definition.options && definition.options.length) {
            var match = _.find(definition.options, {
                value: value
            });
            if (match && match.name) return match.name;
        }
        return value;
    }, $scope.retrieveValueOptions = function(val) {
        if (definition.options && definition.options.length) {
            var options = _.reduce(definition.options, function(results, item) {
                var exists;
                return exists = $scope.multiple ? _.includes($scope.selection.values, item.value) : $scope.selection.value == item.value, 
                exists || results.push({
                    name: item.name,
                    value: item.value
                }), results;
            }, []);
            return $filter("filter")(options, val);
        }
        if (definition.allowedValues && definition.allowedValues.length) {
            var options = _.reduce(definition.allowedValues, function(results, allowedValue) {
                var exists;
                return exists = $scope.multiple ? _.includes($scope.selection.values, allowedValue) : $scope.selection.value == allowedValue, 
                exists || results.push({
                    name: allowedValue,
                    value: allowedValue
                }), results;
            }, []);
            return console.log("Options", options), $filter("filter")(options, val);
        }
    }, $scope.deselect = function(value) {
        $scope.multiple ? _.pull($scope.selection.values, value) : delete $scope.selection.value, 
        setModel();
    }, $scope.toggle = function(reference) {
        $scope.contains(reference) ? $scope.deselect(reference) : $scope.select(reference);
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "signature",
        templateUrl: "fluro-interaction-form/signature/signature.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "upload",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/upload/upload.html",
        defaultOptions: {
            noFormControl: !0,
            wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: "FluroInteractionFormUploadController"
    });
}), app.controller("FluroInteractionFormUploadController", function($scope, $http, FluroValidate) {
    function checkValidity() {
        console.log("CHECK VALIDTIY!");
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    $scope.ctrl = {
        fileArray: []
    };
    var opts = ($scope.to, $scope.options), definition = $scope.to.definition, minimum = definition.minimum, maximum = definition.maximum;
    minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum;
    var definition = _.get($scope, "to.definition");
    if ($scope.remove = function(item) {
        if (_.pull($scope.ctrl.fileArray, item), !$scope.ctrl.fileArray || !$scope.ctrl.fileArray.length) {
            var fileElement = angular.element("#" + $scope.options.id + " input");
            angular.element(fileElement).val(null);
        }
        $scope.ctrl.refresh();
    }, $scope.ctrl.refresh = function() {
        var maximum = definition.maximum;
        if (1 == maximum) if ($scope.ctrl.fileArray.length) {
            var firstFile = $scope.ctrl.fileArray[0];
            "complete" == firstFile.state ? $scope.model[$scope.options.key] = firstFile.attachmentID : $scope.model[$scope.options.key] = firstFile.state;
        } else $scope.model[$scope.options.key] = null; else $scope.model[$scope.options.key] = _.map($scope.ctrl.fileArray, function(fileItem) {
            return "complete" == fileItem.state ? fileItem.attachmentID : fileItem.state;
        });
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }, opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
    $scope.filesize = function(fileSizeInBytes) {
        var i = -1, byteUnits = [ " kB", " MB", " GB", " TB", "PB", "EB", "ZB", "YB" ];
        do fileSizeInBytes /= 1024, i++; while (fileSizeInBytes > 1024);
        return Math.max(fileSizeInBytes, .1).toFixed(1) + byteUnits[i];
    };
}), app.directive("fluroFileInput", function(Fluro, $http) {
    return {
        scope: {
            model: "=fileArray",
            definition: "=",
            callback: "="
        },
        link: function($scope, $element, $attrs) {
            function filesSelected(changeEvent) {
                var files = changeEvent.target.files, realmID = _.get($scope.definition, "params.realm");
                realmID && (_.isArray(realmID) && (realmID = realmID[0]), realmID._id && (realmID = realmID._id)), 
                console.log("FILE SELECTED", realmID);
                var uploadURL = Fluro.apiURL + "/file/attach/" + realmID;
                $scope.model = _.map(files, function(file) {
                    var output = {};
                    output.state = "processing";
                    var formData = new FormData();
                    formData.append("file", file);
                    var request = $http({
                        url: uploadURL,
                        method: "POST",
                        data: formData,
                        headers: {
                            "Content-Type": void 0
                        },
                        uploadEventHandlers: {
                            progress: function(e) {
                                $scope.callback(), e.lengthComputable && (output.progress = Math.round(e.loaded / e.total * 100));
                            }
                        }
                    });
                    return output.file = file, output.request = request, request.then(function(res) {
                        output.state = "complete", output.attachmentID = res.data._id, $scope.callback();
                    }, function(err) {
                        output.state = "error", $scope.callback();
                    }), output;
                }), $scope.callback();
            }
            $scope.model || ($scope.model = []), $element.bind("change", filesSelected);
        }
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "value",
        templateUrl: "fluro-interaction-form/value/value.html",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.directive("preloadImage", function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            scope.aspect = angular.isDefined(attrs.aspect) ? scope.$parent.$eval(attrs.aspect) : 0, 
            scope.aspect ? element.wrap('<div class="preload-image-outer aspect-ratio" style="padding-bottom:' + scope.aspect + '%"></div>') : element.wrap('<div class="preload-image-outer"></div>');
            var preloader = angular.element('<span class="image-preloader"><i class="fa fa-spinner fa-spin"/></span>');
            element.on("load", function() {
                element.removeClass("preload-hide"), element.addClass("preload-show"), preloader.remove();
            }), element.on("error", function(err) {
                element.removeClass("preload-hide"), element.addClass("preload-show"), preloader.remove();
            }), attrs.ngSrc && attrs.ngSrc.length && (element.addClass("preload-hide"), element.parent().append(preloader));
        }
    };
}), app.directive("preloadBackground", function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            var image = new Image(), preloader = angular.element('<span class="image-preloader"><i class="fa fa-spinner fa-spin"/></span>'), div = angular.element('<div class="preload-bg-image"></div>');
            element.addClass("preload-bg-outer").append(div), attrs.preloadBackground && attrs.preloadBackground.length && (image.src = attrs.preloadBackground, 
            element.addClass("preload-hide"), element.append(preloader)), image.onload = function() {
                div.css({
                    backgroundImage: "url(" + image.src + ")"
                }), element.addClass("preload-show"), element.removeClass("preload-hide"), preloader.remove();
            };
        }
    };
}), app.directive("fluroPreloader", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {},
        templateUrl: "fluro-preloader/fluro-preloader.html",
        controller: "FluroPreloaderController",
        link: function(scope, element, attrs) {}
    };
}), app.controller("FluroPreloaderController", function($scope, $urlRouter, $state, $rootScope, $timeout) {
    function hidePreloader(event, toState, toParams, fromState, fromParams, error) {
        preloadTimer && ($timeout.cancel(preloadTimer), preloadTimer = null), "loading" == $scope.preloader["class"] && $timeout(function() {
            $scope.preloader["class"] = "loaded";
        }, 600);
    }
    var preloadTimer;
    $scope.preloader = {
        "class": "reset"
    }, $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, error) {
        return $rootScope.stateChangeBypass ? void ($rootScope.stateChangeBypass = !1) : (event.preventDefault(), 
        $rootScope.stateChangeBypass = !0, $scope.preloader["class"] = "reset", preloadTimer = $timeout(function() {
            $scope.preloader["class"] = "loading";
        }), console.log("WAIT"), $timeout(function() {
            console.log("GO"), $state.go(toState, toParams);
        }, 500), void 0);
    }), $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
        preloadTimer && ($timeout.cancel(preloadTimer), preloadTimer = null), $scope.preloader["class"] = "reset";
    }), $rootScope.$on("$preloaderHide", hidePreloader), $rootScope.$on("$stateChangeSuccess", hidePreloader);
}), app.directive("scrollActive", function($compile, $timeout, $window, FluroScrollService) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs) {
            function setScrollContext(context) {
                currentContext != context && (currentContext = context, $timeout(function() {
                    switch (context) {
                      case "active":
                        $element.removeClass("scroll-after"), $element.removeClass("scroll-before"), $element.addClass("scroll-active"), 
                        $scope.scrollActive = !0, $scope.scrollBefore = !1, $scope.scrollAfter = !1, onActive && onActive();
                        break;

                      case "before":
                        $element.removeClass("scroll-after"), $element.addClass("scroll-before"), $element.removeClass("scroll-active"), 
                        $scope.scrollActive = !1, $scope.scrollBefore = !0, $scope.scrollAfter = !1, onBefore && onBefore();
                        break;

                      case "after":
                        $element.addClass("scroll-after"), $element.removeClass("scroll-before"), $element.removeClass("scroll-active"), 
                        $scope.scrollActive = !1, $scope.scrollBefore = !1, $scope.scrollAfter = !0, onAfter && onAfter();
                    }
                }));
            }
            function updateParentScroll() {
                var viewportHeight = (parent.scrollTop(), parent.height()), contentHeight = parent.get(0).scrollHeight, maxScroll = contentHeight - viewportHeight, startView = 0, endView = startView + viewportHeight, halfView = endView - viewportHeight / 2, elementHeight = $element.outerHeight(), elementStart = $element.position().top, elementEnd = elementStart + elementHeight, elementHalf = elementStart + elementHeight / 4;
                if (onAnchor) {
                    var start = parseInt(startView), rangeStart = parseInt(elementStart), rangeEnd = parseInt(elementHalf);
                    console.log(rangeStart, start, rangeEnd), start >= rangeStart && rangeEnd > start ? anchored || (anchored = !0, 
                    anchored && onAnchor()) : anchored = !1;
                }
                var entirelyViewable = elementStart >= startView && endView >= elementEnd;
                return setScrollContext(entirelyViewable ? "active" : halfView >= elementEnd ? "after" : halfView >= elementStart ? "active" : startView >= maxScroll - 200 ? "active" : "before");
            }
            function updateFromMainScroll(scrollValue) {
                var windowHeight = $window.innerHeight, documentHeight = body.height(), maxScroll = documentHeight - windowHeight, startView = scrollValue;
                startView || (startView = 0);
                var endView = startView + windowHeight, halfView = endView - windowHeight / 2, elementHeight = $element.outerHeight(), elementStart = $element.offset().top, elementEnd = elementStart + elementHeight, elementHalf = elementStart + elementHeight / 4;
                if (onAnchor) {
                    var start = parseInt(startView), rangeStart = parseInt(elementStart), rangeEnd = parseInt(elementHalf);
                    console.log(rangeStart, start, rangeEnd), start >= rangeStart && rangeEnd > start ? anchored || (anchored = !0, 
                    anchored && onAnchor()) : anchored = !1;
                }
                var entirelyViewable = elementStart >= startView && endView >= elementEnd;
                return setScrollContext(entirelyViewable ? "active" : halfView >= elementEnd ? "after" : halfView >= elementStart ? "active" : startView >= maxScroll - 200 ? "active" : "before");
            }
            var onActive, onBefore, onAfter, onAnchor, anchored, currentContext = "";
            $attrs.onActive && (onActive = function() {
                $scope.$eval($attrs.onActive);
            }), $attrs.onAnchor && (onAnchor = function() {
                $scope.$eval($attrs.onAnchor);
            }), $attrs.onAfter && (onAfter = function() {
                $scope.$eval($attrs.onAfter);
            }), $attrs.onBefore && (onBefore = function() {
                $scope.$eval($attrs.onBefore);
            });
            var parent = $element.closest("[scroll-active-parent]"), body = angular.element("body");
            parent.length ? (parent.bind("scroll", updateParentScroll), $timeout(updateParentScroll, 10)) : ($scope.$watch(function() {
                return FluroScrollService.getScroll();
            }, updateFromMainScroll), $timeout(updateFromMainScroll, 10));
        }
    };
}), app.service("FluroScrollService", function($window, $location, $timeout) {
    function updateScroll() {
        var v = this.pageYOffset;
        _value != this.pageYOffset && (_value > v ? controller.direction = "up" : controller.direction = "down", 
        $timeout(function() {
            _value = this.pageYOffset;
        }));
    }
    var controller = {};
    controller.cache = {}, controller.direction = "down";
    var _value = 0;
    angular.element("html,body");
    return controller.setAnchor = function(id) {
        $location.hash("jump-to-" + id);
    }, controller.getAnchor = function() {
        var hash = $location.hash();
        return _.startsWith(hash, "jump-to-") ? hash.substring(8) : hash;
    }, controller.scrollToID = controller.scrollToId = function(id, speed, selector, offset) {
        0 == speed || speed || (speed = "fast");
        var $target = angular.element("#" + id);
        if ($target && $target.offset && $target.offset()) {
            selector || (selector = "body,html");
            var pos = $target.offset().top;
            offset && (pos += Number(offset)), angular.element(selector).animate({
                scrollTop: pos
            }, speed);
        }
    }, controller.scrollToPosition = controller.scrollTo = function(pos, speed, selector, offset) {
        0 == speed || speed || (speed = "fast"), selector || (selector = "body,html"), offset && (pos += Number(offset)), 
        angular.element(selector).animate({
            scrollTop: pos
        }, speed);
    }, controller.get = controller.getScroll = function() {
        return _value;
    }, controller.getMax = function(selector) {
        selector || (selector = "body,html");
        var bodyHeight = angular.element(selector).height(), windowHeight = $window.innerHeight;
        return bodyHeight - windowHeight;
    }, controller.getHalfPoint = function() {
        return $window.innerHeight / 2;
    }, controller.getWindowHeight = function() {
        return $window.innerHeight;
    }, angular.element($window).bind("scroll", updateScroll), updateScroll(), controller;
}), app.service("FluroSEOService", function($rootScope, $location) {
    var controller = {};
    return $rootScope.$watch(function() {
        return controller.siteTitle + " | " + controller.pageTitle;
    }, function() {
        controller.headTitle = "", controller.siteTitle && controller.siteTitle.length ? (controller.headTitle += controller.siteTitle, 
        controller.pageTitle && controller.pageTitle.length && (controller.headTitle += " | " + controller.pageTitle)) : controller.pageTitle && controller.pageTitle.length && (controller.headTitle = controller.pageTitle);
    }), controller.getImageURL = function() {
        var url = controller.defaultImageURL;
        return controller.imageURL && controller.imageURL.length && (url = controller.imageURL), 
        url;
    }, controller.getDescription = function() {
        var description = controller.defaultDescription;
        return controller.description && (description = controller.description), description && description.length ? description : "";
    }, $rootScope.$on("$stateChangeSuccess", function() {
        controller.url = $location.$$absUrl;
    }), $rootScope.$on("$stateChangeStart", function() {
        controller.description = null, controller.imageURL = null, console.log("REset SEO");
    }), controller;
}), app.directive("statWidget", function() {
    return {
        restrict: "E",
        scope: {
            item: "=",
            stat: "@",
            myStats: "=?myStats"
        },
        controller: "FluroStatWidgetController",
        transclude: !0,
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.replaceWith(clone);
            });
        }
    };
}), app.controller("FluroStatWidgetController", function($scope, $timeout, FluroContent) {
    $scope.myStats || ($scope.myStats = {}), $scope.processing = !1, $scope.$watch(function() {
        var myStatValue = $scope.myStats["_" + $scope.stat], totalStatValue = 0;
        return $scope.item.stats && $scope.item.stats["_" + $scope.stat] && (totalStatValue = $scope.item.stats["_" + $scope.stat]), 
        myStatValue + "-" + totalStatValue;
    }, function(tally) {
        var myStatValue = $scope.myStats["_" + $scope.stat], totalStatValue = 0;
        $scope.item.stats && $scope.item.stats["_" + $scope.stat] && (totalStatValue = $scope.item.stats["_" + $scope.stat]), 
        $scope.myTotal = myStatValue, $scope.total = totalStatValue;
    }), $scope.toggle = function() {
        if ($scope.item && $scope.stat) {
            $scope.item.stats || ($scope.item.stats = {});
            var prevCount, statName = $scope.stat, itemID = $scope.item._id;
            $scope.myStats && (prevCount = $scope.myStats["_" + statName]);
            var request = FluroContent.endpoint("stat/" + itemID + "/" + statName).update({}).$promise;
            $scope.processing = !0, request.then(function(res) {
                $scope.processing = !1, $timeout(function() {
                    $scope.myStats && (prevCount ? $scope.myStats["_" + statName] = 0 : $scope.myStats["_" + statName] = 1), 
                    $scope.item.stats["_" + statName] = res.total;
                });
            }, function(err) {
                $scope.processing = !1, console.log("Error updating stat", err);
            });
        }
    };
}), app.directive("hamburger", function() {
    return {
        restrict: "E",
        replace: !0,
        template: '<div class="hamburger"> 		  <span></span> 		  <span></span> 		  <span></span> 		  <span></span> 		</div>',
        link: function($scope, $elem, $attr) {}
    };
}), app.directive("compileHtml", function($compile) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            scope.$watch(function() {
                return scope.$eval(attrs.compileHtml);
            }, function(value) {
                element.html(value), $compile(element.contents())(scope);
            });
        }
    };
}), app.directive("googleMap", function($window, $timeout) {
    return {
        restrict: "E",
        replace: !0,
        template: '<div class="map-outer"><div class="map-inner"></div></div>',
        scope: {
            model: "=ngModel",
            selectCallback: "=ngSelectCallback",
            styleOptions: "=ngStyleOptions"
        },
        link: function($scope, $element, $attrs) {
            function initialize() {
                console.log("Map starting"), mapOptions = {
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: !1,
                    maxZoom: 15,
                    scrollwheel: !1,
                    draggable: !0,
                    styles: styleOptions
                }, map = new google.maps.Map($element.find(".map-inner").get(0), mapOptions), geocoder = new google.maps.Geocoder(), 
                angular.element($window).bind("resize", fitMap);
            }
            function fitMap() {
                var bounds = new google.maps.LatLngBounds();
                _.each(markers, function(marker) {
                    var lat = marker.position.lat(), lng = marker.position.lng(), myLatLng = new google.maps.LatLng(lat, lng);
                    bounds.extend(myLatLng);
                }), map.fitBounds(bounds);
            }
            var mapOptions, map, marker, geocoder, markers = [];
            styleOptions = [], $scope.styleOptions && (styleOptions = $scope.styleOptions);
            var createMarker = function(location) {
                if (location.latitude && location.longitude) {
                    var position = new google.maps.LatLng(location.latitude, location.longitude);
                    map.setCenter(position), marker = new google.maps.Marker({
                        map: map,
                        position: position,
                        title: location.title
                    }), google.maps.event.addListener(marker, "click", function() {
                        $timeout(function() {
                            $scope.selectCallback && $scope.selectCallback(location);
                        });
                    }), markers.push(marker);
                }
            };
            $scope.$watch("model", function(model) {
                createMarker(model), fitMap();
            }), initialize();
        }
    };
}), app.directive("imgOnload", function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            element.bind("load", function() {
                scope.$eval(attrs.imgOnload);
            });
        }
    };
}), app.directive("infinitePager", function($timeout, $sessionStorage, $state, $window) {
    return {
        restrict: "A",
        link: function($scope, $element, $attr) {
            function bindNextPageOnScroll() {
                angular.element($window).bind("scroll", function() {
                    var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight, body = document.body, html = document.documentElement, docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
                    windowBottom = windowHeight + window.pageYOffset, windowBottom >= docHeight && $scope.nextPage();
                });
            }
            var perPage = 16;
            if ($attr.perPage && (perPage = parseInt($attr.perPage)), $attr.ngParams) {
                var ngParams = $scope.$eval($attr.ngParams);
                ngParams.nextPageOnScroll && bindNextPageOnScroll();
            }
            $scope.pager = {
                currentPage: 0,
                limit: perPage
            }, $scope.pages = [], $sessionStorage.currentPage ? $sessionStorage.currentPage[$state.current.name] && $scope.$on("back-resume-scroll", function(event, args) {
                var start = $scope.pager.limit, end = ($sessionStorage.currentPage[$state.current.name] + 1) * $scope.pager.limit, sliced = _.slice($scope.allItems, start, end);
                $scope.pages.push(sliced), $element.ready(function() {
                    document.body.scrollTop = args.previousScrollPosition, $scope.pager.currentPage = $sessionStorage.currentPage[$state.current.name];
                });
            }) : $sessionStorage.currentPage = {}, $scope.$watch($attr.items, function(items) {
                $scope.allItems = items, $scope.allItems && ($scope.pages.length = 0, $scope.pager.currentPage = 0, 
                $scope.totalPages = Math.ceil($scope.allItems.length / $scope.pager.limit) - 1, 
                $scope.updateCurrentPage());
            }), $scope.updateCurrentPage = function() {
                $scope.allItems.length < $scope.pager.limit * ($scope.pager.currentPage - 1) && ($scope.pager.currentPage = 0);
                var start = $scope.pager.currentPage * $scope.pager.limit, end = start + $scope.pager.limit, sliced = _.slice($scope.allItems, start, end);
                $scope.pages.push(sliced);
            };
            var timer;
            $scope.nextPage = function() {
                $scope.pager.currentPage < $scope.totalPages && ($timeout.cancel(timer), timer = $timeout(function() {
                    $scope.pager.currentPage = $scope.pager.currentPage + 1, $sessionStorage.currentPage[$state.current.name] = $scope.pager.currentPage, 
                    $scope.updateCurrentPage();
                }));
            };
        }
    };
}), app.directive("longTextWrap", function() {
    return {
        restrict: "E",
        transclude: !0,
        template: '<div class="long-text-wrap" ng-class="{expanded:ltSettings.expanded}" ><div class="long-text-wrap-inner" ng-transclude></div><a class="long-text-wrap-link" ng-click="ltSettings.expanded = !ltSettings.expanded"><div><span>Read more<span><i class="fa fa-angle-down"></i></div></a></div>',
        controller: function($scope) {
            $scope.ltSettings = {};
        }
    };
}), app.directive("ontaphold", function($timeout) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            element.bind("load", function() {
                function onlongtouch() {
                    scope.$eval(attrs.ontaphold);
                }
                var timer, touchduration = 500;
                element[0].addEventListener("touchstart", function() {
                    timer = $timeout(onlongtouch, touchduration);
                }, !1), element[0].addEventListener("touchend", function() {
                    return timer ? $timeout.cancel(timer) : onlongtouch();
                }, !1);
            });
        }
    };
}), app.directive("selectOnFocus", function($timeout) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            var focusedElement = null;
            element.on("focus", function() {
                var self = this;
                focusedElement != self && (focusedElement = self, $timeout(function() {
                    self.select();
                }, 10));
            }), element.on("blur", function() {
                focusedElement = null;
            });
        }
    };
}), app.filter("commaSummary", function() {
    return function(arrayOfObjects, limit) {
        limit || (limit = 20);
        var names = _.chain(arrayOfObjects).map(function(object) {
            return object.title;
        }).compact().value(), string = names.join(", ");
        return string.length >= limit && (string = string.substr(0, limit) + "..."), string;
    };
}), app.filter("filesize", function() {
    return function(bytes) {
        var sizes = [ "Bytes", "kb", "mb", "gb", "tb" ];
        if (0 == bytes) return "0 Byte";
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + "" + sizes[i];
    };
}), app.filter("readableDate", function() {
    return function(event, style) {
        if (event && event.startDate) {
            var startDate = new Date(event.startDate), endDate = new Date(event.endDate), noEndDate = startDate.format("g:ia j M Y") == endDate.format("g:ia j M Y"), sameYear = startDate.format("Y") == endDate.format("Y"), sameMonth = startDate.format("M Y") == endDate.format("M Y"), sameDay = startDate.format("j M Y") == endDate.format("j M Y");
            switch (style) {
              case "short":
                return noEndDate ? startDate.format("j M") : sameDay ? startDate.format("j M") : sameMonth ? startDate.format("j") + "-" + endDate.format("j M") : sameYear ? startDate.format("j") + "-" + endDate.format("j M") : startDate.format("j M Y") + " - " + endDate.format("j M Y");

              default:
                return noEndDate ? startDate.format("g:ia l j M Y") : sameDay ? startDate.format("g:ia") + " - " + endDate.format("g:ia l j M Y") : sameMonth ? startDate.format("j") + "-" + endDate.format("j M Y") : sameYear ? startDate.format("j M") + " - " + endDate.format("j M Y") : startDate.format("j M Y") + " - " + endDate.format("j M Y");
            }
        }
    };
}), app.filter("reword", function($rootScope) {
    console.log("Get Translations");
    var translations = _.get($rootScope.applicationData, "translations");
    return function(string) {
        return _.each(translations, function(set) {
            string = string.replace(new RegExp(set.from, "g"), set.to);
        }), string;
    };
}), app.filter("timeago", function() {
    return function(date) {
        return _.isString(date) && (date = new Date(date)), moment(date).fromNow();
    };
}), app.service("pastTimeService", function() {
    function getMoment(listValue) {
        return moment().subtract(1, listValue);
    }
    function getLabel(listValue) {
        var found = _.find(list, {
            value: listValue
        });
        if (found) return found.label;
    }
    var list = [ {
        label: "Last hour",
        value: "hours"
    }, {
        label: "Last 24 hours",
        value: "days"
    }, {
        label: "Last week",
        value: "weeks"
    }, {
        label: "Last month",
        value: "months"
    }, {
        label: "Last year",
        value: "years"
    } ], service = {
        list: list,
        getMoment: getMoment,
        getLabel: getLabel
    };
    return service;
}), EventFormController.resolve = {
    event: function($http, $stateParams, Fluro) {
        var url = Fluro.apiURL + "/content/_query/5987dc3dcf544d199f545833?variables[slugID]=" + $stateParams.id;
        return $http.get(url);
    },
    seo: function(FluroSEOService, Asset, $filter, event) {
        event = _.first(event.data), FluroSEOService.pageTitle = event.title;
        var imageID = _.get(event, "mainImage._id");
        return imageID ? FluroSEOService.imageURL = Asset.imageUrl(imageID, 640, null) : FluroSEOService.imageURL = null, 
        FluroSEOService.description = $filter("readableDate")(event), !0;
    }
}, FormViewController.resolve = {
    form: function($http, Fluro, $stateParams, $q) {
        var deferred = $q.defer(), url = Fluro.apiURL + "/form/" + $stateParams.id;
        return $http.get(url).then(function(res) {
            console.log("GOT FORM", res.data), deferred.resolve(res.data);
        }, function(err) {
            console.log("ERROR", err), deferred.reject(err);
        }), deferred.promise;
    },
    seo: function(FluroSEOService, Asset, $filter, form) {
        console.log("FORM", form);
        var displayTitle = _.get(form, "data.publicData.title");
        displayTitle || (displayTitle = form.title), FluroSEOService.pageTitle = displayTitle;
        var imageID = _.get(form, "data.publicData.mainImage");
        imageID ? FluroSEOService.imageURL = Asset.imageUrl(imageID, 640, null) : FluroSEOService.imageURL = null;
        var shortDescription = _.get(form, "data.publicData.body");
        return shortDescription || (shortDescription = ""), shortDescription = shortDescription.replace(/<[^>]+>/g, "").slice(0, 100), 
        100 == shortDescription.length && (shortDescription += "..."), FluroSEOService.description = shortDescription, 
        !0;
    }
}, app.directive("shortUrl", function(FluroContent, $location, ShareModalService) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            form: "=ngModel"
        },
        template: '<a class="btn btn-sm btn-outline" ng-click="open()"><span>Share</span><i class="fa fa-share"></i></a>',
        link: function($scope, $element, $attr) {
            $scope.$watch("form", function(form) {
                if (form && form && form._id) {
                    var longURL = $location.absUrl();
                    FluroContent.endpoint("url/shorten").save({
                        url: longURL
                    }).$promise.then(function(res) {
                        console.log("Got short url", res);
                        var shortURL = res.url;
                        $scope.shortURL = "http://" + shortURL;
                    });
                }
            }), $scope.open = function() {
                console.log("happy days"), ShareModalService.form = $scope.form;
            };
        }
    };
}), app.service("ShareModalService", function() {
    var service = {};
    return service.remove = function() {
        service.form = null, service.copied = !1;
    }, service;
}), app.directive("shareModal", function($location, FluroContent) {
    return {
        restrict: "E",
        replace: !0,
        scope: !0,
        templateUrl: "share-modal/share-modal.html",
        controller: function($scope, ShareModalService) {
            $scope.service = ShareModalService, console.log("Service", $scope.service), $scope.processing = !0;
            var longURL = $location.absUrl();
            FluroContent.endpoint("url/shorten").save({
                url: longURL
            }).$promise.then(function(res) {
                $scope.processing = !1, $scope.url = "http://" + res.url;
            }), $scope.close = function() {
                angular.element("#clipper").remove(), ShareModalService.remove();
            }, $scope.copyUrl = function() {
                console.log("starting to copy"), clipboard($scope.url), ShareModalService.copied = !0;
            };
        }
    };
}), angular.module("fluro").run([ "$templateCache", function($templateCache) {
    "use strict";
    $templateCache.put("admin-realm-folders/admin-realm-folder-item.html", '<div class="realm-folder-item expanded" ng-class="{\'active\':contains(realm)}"><div class=realm-folder-item-link ng-class="{\'active\':contains(realm), \'active-trail\':activeTrail(realm)}"><a ng-click=toggle(realm)><i class="fa dot fa-circle" style="color: {{realm.bgColor}}"></i> {{realm.title}} <i class="fa fa-check tick"></i></a></div><div class=realm-folder-children></div></div>'), 
    $templateCache.put("admin-realm-folders/admin-realm-folders.html", '<div><div class=form-group><div class=input-group><input class=form-control ng-model=search.terms placeholder="Search realms"> <span class=input-group-addon ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></span></div></div><div class=realm-folder-list><div ng-if=tree.length><div class=realm-folder-items ng-class="{\'has-selection\':model.length}"><realm-folder-item ng-model=realm ng-search-terms=search.terms ng-selection=model ng-repeat="realm in tree | filter:search.terms | orderBy:\'title\' track by realm._id"></realm-folder-item></div></div></div><ul class=list-group ng-if=!tree.length><li class="list-group-item disabled" ng-repeat="realm in model track by realm._id">{{realm.title}}</li></ul></div>'), 
    $templateCache.put("fluro-dob-select/fluro-dob-select.html", '<div><div class=form-group ng-hide=hideAge><label>Age</label><input type=number select-on-focus ng-model-options="{debounce: { \'default\': 400, \'blur\': 0 } }" class=form-control pattern=[0-9]* placeholder=Age ng-model=age inputmode=numeric></div><div class=form-group ng-hide=hideDates><label>Date of Birth {{$parent.to.required ? \'*\' : \'\'}}</label><div class=row><div class=col-xs-3><select name=dateFields.day data-ng-model=dateFields.day placeholder=Day class=form-control ng-options="day for day in days" ng-change=checkDate() ng-disabled=disableFields><option value="">Day</option></select></div><div class=col-xs-5><select name=dateFields.month data-ng-model=dateFields.month placeholder=Month class=form-control ng-options="month.value as month.name for month in months" value={{dateField.month}} ng-change=checkDate() ng-disabled=disableFields><option value="">Month</option></select></div><div class=col-xs-4><select name=dateFields.year data-ng-model=dateFields.year placeholder=Year class=form-control ng-options="year for year in years" ng-change=checkDate() ng-disabled=disableFields><option value="">Year</option></select></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/button-select/fluro-button-select.html", '<div id={{options.id}} class="button-select {{to.definition.directive}}-buttons" ng-model=model[options.key]><a ng-repeat="(key, option) in to.options" ng-class={active:contains(option.value)} class="btn btn-default" id="{{id + \'_\'+ $index}}" ng-click=toggle(option.value)><span>{{option.name}}</span><i class="fa fa-check"></i></a></div>'), 
    $templateCache.put("fluro-interaction-form/custom.html", "<div ng-model=model[options.key] compile-html=to.definition.template></div>"), 
    $templateCache.put("fluro-interaction-form/date-select/date-selector.html", '<div class=row><div class=col-xs-3><select name=dateFields.day data-ng-model=dateFields.day placeholder=Day class=form-control ng-options="day for day in days" ng-change=checkDate()><option value="" disabled selected>Day</option></select></div><div class=col-xs-5><select name=dateFields.month data-ng-model=dateFields.month placeholder=Month class=form-control ng-options="month.value as month.name for month in months" value={{dateField.month}} ng-change=checkDate()><option value="" disabled selected>Month</option></select></div><div class=col-xs-4><select name=dateFields.year data-ng-model=dateFields.year placeholder=Year class=form-control ng-options="year.value as year.name for year in years" value={{dateField.year}} ng-change=checkDate()><option value="" disabled selected>Year</option></select></div></div>'), 
    $templateCache.put("fluro-interaction-form/date-select/fluro-date-select.html", "<div><div class=visible-xs-block><input type=date class=form-control ng-model=model[options.key] ng-required=to.required></div><div class=hidden-xs><fluro-date-select formly-skip-ng-model-attrs-manipulator ng-model=model[options.key]></fluro-date-select></div></div>"), 
    $templateCache.put("fluro-interaction-form/dob-select/fluro-dob-select.html", "<div class=fluro-interaction-dob-select><dob-select ng-model=model[options.key] hide-age=to.params.hideAge hide-dates=to.params.hideDates></dob-select></div>"), 
    $templateCache.put("fluro-interaction-form/embedded/fluro-embedded.html", '<div class=fluro-embedded-form><div class="form-multi-group multi-count-{{model[options.key].length}}" ng-if="(to.definition.maximum != 1) || (to.definition.minimum != 1)"><div class="panel panel-default" ng-init="fields = copyFields(); dataFields = copyDataFields(); detailFields = copyDetailFields(); " ng-repeat="entry in model[options.key] track by $index"><div class="panel-heading clearfix"><div class=row><div class="col-xs-9 col-sm-8 col-lg-9"><h5 class=multi-group-title>{{to.label}} {{$index + 1}}</h5></div><div class="col-xs-3 col-sm-4 col-lg-3 text-left text-sm-right"><a ng-if=canRemove() class="btn btn-danger btn-multi-group-remove btn-sm" ng-click=removeEntry($index)><span class=hidden-xs>Remove</span><i class="fa fa-times"></i></a></div></div></div><div class=panel-body><formly-form model=entry fields=fields></formly-form><formly-form model=entry.data fields=dataFields></formly-form><formly-form model=entry.details fields=detailFields></formly-form></div></div><a class="btn btn-primary btn-sm btn-multi-group-add" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class="fa fa-plus"></i></a></div><div ng-if="to.definition.maximum == 1 && to.definition.minimum == 1 && options.key"><formly-form model=model[options.key] fields=options.data.fields></formly-form><formly-form model=model[options.key].data fields=options.data.dataFields></formly-form><formly-form model=model[options.key].details fields=options.data.detailFields></formly-form></div></div>'), 
    $templateCache.put("fluro-interaction-form/field-errors.html", '<div class=field-errors ng-if="fc.$touched && fc.$invalid"><div ng-show=fc.$error.required class="alert alert-danger" role=alert><span class="fa fa-exclamation" aria-hidden=true></span> <span class=sr-only>Error:</span> {{to.label}} is required.</div><div ng-show=fc.$error.validInput class="alert alert-danger" role=alert><span class="fa fa-exclamation" aria-hidden=true></span> <span class=sr-only>Error:</span> {{to.errorMessage}}</div><div ng-show=fc.$error.email class="alert alert-danger" role=alert><span class="fa fa-exclamation" aria-hidden=true></span> <span class=sr-only>Error:</span> <span>\'{{fc.$viewValue}}\' is not a valid email address</span></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-interaction-input.html", '<div class="fluro-input form-group" scroll-active ng-class="{\'fluro-valid\':isValid(), \'fluro-dirty\':isDirty, \'fluro-invalid\':!isValid()}"><label><i class="fa fa-check" ng-if=isValid()></i><i class="fa fa-exclamation" ng-if=!isValid()></i><span>{{field.title}}</span></label><div class="error-message help-block"><span ng-if=field.errorMessage>{{field.errorMessage}}</span> <span ng-if=!field.errorMessage>Please provide valid input for this field</span></div><span class=help-block ng-if="field.description && field.type != \'boolean\'">{{field.description}}</span><div class=fluro-input-wrapper></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-terms.html", '<div class=terms-checkbox><div class=checkbox><label><input type=checkbox ng-model="model[options.key]"> <strong>{{to.definition.title}}</strong><div class=terms-info>{{to.definition.params.storeData}}</div></label></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-web-form.html", '<div class="fluro-interaction-form fluro-webform"><div ng-if=!correctPermissions class=form-permission-warning><div class="alert alert-warning small"><i class="fa fa-warning"></i> <span>You do not have permission to post {{model.plural}}</span></div></div><div ng-if=promisesResolved><div ng-if=debugMode><div class="btn-group btn-group-justified"><a ng-click="vm.state = \'ready\'" class="btn btn-default">State to ready</a> <a ng-click="vm.state = \'complete\'" class="btn btn-default">State to complete</a> <a ng-click=reset() class="btn btn-default">Reset</a></div><hr></div><div ng-show="vm.state != \'complete\'"><form novalidate ng-submit=vm.onSubmit()><formly-form model=vm.model fields=vm.modelFields form=vm.modelForm options=vm.options><div class=fluro-webform-recaptcha ng-if=model.data.recaptcha><div recaptcha-render></div></div><div class="form-error-summary form-client-error alert alert-warning" ng-if=errorList.length><strong>Please check the messages below before submitting this form</strong><br><div class=form-error-summary-item ng-click=scrollToField(field) ng-repeat="field in errorList"><i class="fa fa-exclamation"></i> <span ng-if=field.templateOptions.definition.errorMessage.length>{{::field.templateOptions.definition.errorMessage}}</span> <span ng-if=!field.templateOptions.definition.errorMessage.length><span ng-hide=field.message>{{::field.templateOptions.label}} has not been provided.</span> <span ng-show=field.message>{{::field.message}}</span></span></div></div><div ng-switch=vm.state class=fluro-webform-submit><div ng-switch-when=sending class=fluro-webform-submit-processing><a class="btn btn-primary" ng-disabled=true><span>Processing</span> <i class="fa fa-spinner fa-spin"></i></a></div><div ng-switch-when=error class=fluro-webform-submit-error><div class="form-error-summary form-server-error alert alert-danger" ng-if=processErrorMessages.length><div class=form-error-summary-item ng-repeat="error in processErrorMessages track by $index"><i class="fa fa-exclamation"></i> <span>Error processing your submission: {{::error}}</span></div></div><button type=submit class="btn btn-primary" ng-disabled=!readyToSubmit><span>Try Again</span> <i class="fa fa-angle-right"></i></button></div><div ng-switch-default class=fluro-webform-submit-buttons><button type=submit class="btn btn-primary" ng-disabled=!readyToSubmit><span>{{submitLabel}}</span> <i class="fa fa-angle-right"></i></button></div></div></formly-form></form></div><div ng-show="vm.state == \'complete\'"><div compile-html=transcludedContent></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/nested/fluro-nested.html", '<div class=form-group><div class="form-multi-group multi-count-{{model[options.key].length}}" ng-if="(to.definition.maximum != 1) || (to.definition.minimum != 1)"><div class="panel panel-default" ng-init="fields = copyFields()" ng-repeat="entry in model[options.key] track by $index"><div class="panel-heading clearfix"><div class=row><div class="col-xs-9 col-sm-8 col-lg-9"><h5 class=multi-group-title>{{to.label}} {{$index + 1}}</h5></div><div class="col-xs-3 col-sm-4 col-lg-3 text-left text-sm-right"><a ng-if=canRemove() class="btn btn-danger btn-multi-group-remove btn-sm" ng-click=removeEntry($index)><span class=hidden-xs>Remove</span><i class="fa fa-times"></i></a></div></div></div><div class=panel-body><formly-form model=entry fields=fields></formly-form></div></div><a class="btn btn-primary btn-sm btn-multi-group-add" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class="fa fa-plus"></i></a></div><div ng-if="to.definition.maximum == 1 && to.definition.minimum == 1 && options.key"><formly-form model=model[options.key] fields=options.data.fields></formly-form></div></div>'), 
    $templateCache.put("fluro-interaction-form/order-select/fluro-order-select.html", '<div id={{options.id}} class=fluro-order-select><div ng-if=selection.values.length><p class=help-block>Drag to reorder your choices</p></div><div class=list-group as-sortable=dragControlListeners formly-skip-ng-model-attrs-manipulator ng-model=selection.values><div class="list-group-item clearfix" as-sortable-item ng-repeat="item in selection.values"><div class=pull-left as-sortable-item-handle><i class="fa fa-arrows order-select-handle"></i> <span class="order-number text-muted">{{$index+1}}</span> <span>{{item}}</span></div><div class="pull-right order-select-remove" ng-click=deselect(item)><i class="fa fa-times"></i></div></div></div><div ng-if=canAddMore()><p class=help-block>Choose by selecting options below</p><select class=form-control ng-model=selectBox.item ng-change=selectUpdate()><option ng-repeat="(key, option) in to.options | orderBy:\'value\'" ng-if=!contains(option.value) value={{option.value}}>{{option.value}}</option></select></div></div>'), 
    $templateCache.put("fluro-interaction-form/payment/payment-method.html", '<hr><div class=payment-method-select><div ng-if=!settings.showOptions><h3 class=clearfix>{{selected.method.title}} <em class="pull-right small" ng-click="settings.showOptions = !settings.showOptions">Other payment options <i class="fa fa-angle-right"></i></em></h3></div><div ng-if=settings.showOptions><h3 class=clearfix>Select payment method <em ng-click="settings.showOptions = false" class="pull-right small">Back <i class="fa fa-angle-up"></i></em></h3><div class="payment-method-list list-group"><div class="payment-method-list-item list-group-item" ng-class="{active:method == selected.method}" ng-click=selectMethod(method) ng-repeat="method in methodOptions"><h5 class=title>{{method.title}}</h5></div></div></div><div ng-if=!settings.showOptions><div ng-if="selected.method.key == \'card\'"><formly-form model=model fields=options.data.fields></formly-form></div><div ng-if="selected.method == method && selected.method.description.length" ng-repeat="method in methodOptions"><div compile-html=method.description></div></div></div></div><hr>'), 
    $templateCache.put("fluro-interaction-form/payment/payment-summary.html", '<hr><div class=payment-summary><h3>Payment details</h3><div class=form-group><div ng-if=modifications.length class=payment-running-total><div class="row payment-base-row"><div class=col-xs-6><strong>Amount</strong></div><div class="col-xs-3 col-xs-offset-3" ng-if=paymentDetails.amount>{{paymentDetails.amount / 100 | currency:currencySymbol}}</div></div><div class="row text-muted" ng-repeat="mod in modifications"><div class=col-xs-6><em>{{mod.title}}</em></div><div class="col-xs-3 text-right"><em>{{mod.description}}</em></div><div class=col-xs-3><em class=text-muted>{{mod.total / 100 | currency:currencySymbol}}</em></div></div><div class="row payment-total-row"><div class=col-xs-3><h4>Total</h4></div><div class="col-xs-9 text-right"><h4>{{calculatedTotal /100 | currency:currencySymbol}} <span class="text-uppercase text-muted">{{paymentDetails.currency}}</span></h4></div></div></div><div class=payment-amount ng-if=!modifications.length>{{calculatedTotal /100 | currency:currencySymbol}} <span class=text-uppercase>({{paymentDetails.currency}})</span></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/person/person.html", '<div class=fluro-embedded-form><div class=form-multi-group ng-if="to.definition.maximum != 1"><div class="panel panel-default" ng-init="fields = copyFields(); dataFields = copyDataFields(); " ng-repeat="entry in model[options.key] track by $index"><div class="panel-heading clearfix"><a ng-if=canRemove() class="btn btn-danger btn-sm pull-right" ng-click="model[options.key].splice($index, 1)"><span>Remove {{to.label}}</span><i class="fa fa-times"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><div class=row><div class="form-group col-xs-12 col-sm-6"><label>First Name</label><input class=form-control placeholder="First Name" ng-model="entry.firstName"></div><div class="form-group col-xs-12 col-sm-6"><label>Last Name</label><input class=form-control placeholder="Last Name" ng-model="entry.lastName"></div></div><div class=row><div class="form-group col-xs-12 col-sm-6"><label>Email Address</label><input class=form-control placeholder="Email Address" ng-model="entry.email"></div><div class="form-group col-xs-12 col-sm-6"><label>Phone Number</label><input class=form-control placeholder="Phone Number" ng-model="entry.phoneNumber"></div></div><formly-form model=entry fields=fields></formly-form><formly-form model=entry.data fields=dataFields></formly-form></div></div><a class="btn btn-primary btn-sm" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class="fa fa-plus"></i></a></div><div ng-if="to.definition.maximum == 1 && options.key"><formly-form model=model[options.key] fields=options.data.fields></formly-form><formly-form model=model[options.key].data fields=options.data.dataFields></formly-form></div></div>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select-item.html", '<a class=clearfix><i class="fa fa-{{match.model._type}}"></i> <span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span> <span ng-if="match.model._type == \'event\' || match.model._type == \'plan\'" class="small text-muted">// {{match.model.startDate | formatDate:\'jS F Y - g:ia\'}}</span></a>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select-value.html", '<a class=clearfix><span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span></a>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select.html", '<div class=fluro-search-select><div ng-if="to.definition.type == \'reference\'"><div class=list-group ng-if="multiple && selection.values.length"><div class=list-group-item ng-repeat="item in selection.values"><i class="fa fa-times pull-right" ng-click=deselect(item)></i> {{item.title}}</div></div><div class=list-group ng-if="!multiple && selection.value"><div class="list-group-item clearfix"><i class="fa fa-times pull-right" ng-click=deselect(selection.value)></i> {{selection.value.title}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-item.html typeahead-on-select=select($item) placeholder=Search typeahead="item.title for item in retrieveReferenceOptions($viewValue)" typeahead-loading="search.loading"><div class=input-group-addon ng-if=!search.loading ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></div><div class=input-group-addon ng-if=search.loading><i class="fa fa-spin fa-spinner"></i></div></div></div></div><div ng-if="to.definition.type != \'reference\'"><div class=list-group ng-if="multiple && selection.values.length"><div class=list-group-item ng-repeat="value in selection.values"><i class="fa fa-times pull-right" ng-click=deselect(value)></i> {{getValueLabel(value)}}</div></div><div class=list-group ng-if="!multiple && selection.value"><div class="list-group-item clearfix"><i class="fa fa-times pull-right" ng-click=deselect(selection.value)></i> {{getValueLabel(selection.value)}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-value.html typeahead-on-select=select($item.value) placeholder=Search typeahead="item.name for item in retrieveValueOptions($viewValue)" typeahead-loading="search.loading"><div class=input-group-addon ng-if=!search.loading ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></div><div class=input-group-addon ng-if=search.loading><i class="fa fa-spin fa-spinner"></i></div></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/signature/signature.html", '<div><signature-pad dataurl=model[options.key] style="border:2px solid #ddd" clear=clear height=220 width=550 disabled></signature-pad><a class="btn btn-sm btn-default" ng-click=clear()>Clear signature</a></div>'), 
    $templateCache.put("fluro-interaction-form/upload/upload.html", '<div id={{options.id}} name="\'{{to.definition.title}}\'" class=fluro-file-input ng-model=model[options.key]><div class=list-group ng-if=ctrl.fileArray.length><div class="list-group-item upload-input-item clearfix" ng-switch=item.state ng-repeat="item in ctrl.fileArray"><div ng-switch-when=processing><i ng-click=remove(item) class="fa fa-fw fa-times pull-right"></i> <strong>{{::item.file.name}}</strong> <em class="small text-muted">{{::filesize(item.file.size)}}</em><div class="small text-muted"><i class="fa fa-spinner fa-spin"></i> Uploading {{item.progress}}%</div></div><div ng-switch-when=error><i ng-click=remove(item) class="fa fa-fw fa-times pull-right"></i> <strong>{{::item.file.name}}</strong> <em class="small text-muted">{{::filesize(item.file.size)}}</em><div class="small brand-warning"><i class="fa fa-warning brand-danger"></i> {{item.state}}</div></div><div ng-switch-when=complete><i ng-click=remove(item) class="fa fa-fw fa-times pull-right"></i> <strong>{{::item.file.name}}</strong> <em class="small text-muted">{{::filesize(item.file.size)}}</em><div class="small brand-success"><i class="fa fa-check"></i> Upload Complete</div></div></div></div><div ng-if="to.definition.maximum == 1 && !ctrl.fileArray.length"><input type=file fluro-file-input definition=to.definition callback=ctrl.refresh formly-skip-ng-model-attrs-manipulator file-array=ctrl.fileArray></div><div ng-if="to.definition.maximum != 1 && (!to.definition.maximum || ctrl.fileArray.length < to.definition.maximum)"><input type=file fluro-file-input definition=to.definition callback=ctrl.refresh formly-skip-ng-model-attrs-manipulator file-array=ctrl.fileArray multiple></div></div>'), 
    $templateCache.put("fluro-interaction-form/value/value.html", "<div class=fluro-interaction-value style=display:none><pre>{{model[options.key] | json}}</pre></div>"), 
    $templateCache.put("fluro-preloader/fluro-preloader.html", '<div class="preloader {{preloader.class}}"><div class=preloader-bg></div><div class=preloader-fg><div class=spinner><svg version=1.1 id=loader-1 xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink x=0px y=0px width=40px height=40px viewbox="0 0 50 50" style="enable-background:new 0 0 50 50" xml:space=preserve><path fill=#000 d=M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z transform="rotate(170 25 25)"><animatetransform attributetype=xml attributename=transform type=rotate from="0 25 25" to="360 25 25" dur=0.6s repeatcount=indefinite></animatetransform></path></svg></div></div></div>'), 
    $templateCache.put("routes/event/event.html", '<div class="top-banner bg-image" ng-if=event.mainImage style="background-image: url( {{$root.asset.imageUrl(event.mainImage._id)}} )"><div class=shader><div class=wrapper></div><div class=text><div class=container><div class=text-wrap><div class=wrapper-xs><div class="row row-flex"><div class="col-xs-12 col-sm-8"><h6 class="account-title text-muted">{{event.account.title}}</h6><h1 class=title>{{event.title}}</h1></div><div class="col-xs-12 col-sm-4"><div class=share><short-url ng-model=event></short-url></div></div></div></div></div></div></div></div></div><div class=top-banner ng-if=!event.mainImage style=background-color:{{event.account.color}}><div class=shader><div class=wrapper-sm></div><div class=text><div class=container><div class=text-wrap><div class=wrapper-xs><div class="row row-flex"><div class="col-xs-12 col-sm-8"><h6 class="account text-muted">{{event.account.title}}</h6><h1 class=title>{{event.title}}</h1></div><div class="col-xs-12 col-sm-4"><div class=share><short-url ng-model=event></short-url></div></div></div></div></div></div></div></div></div><div class="wrapper bg-white border-top"><div class=container><div class=text-wrap><div class=row><div class="form-group col-sm-6"><label>Date and Time</label><h4 class=normal>{{ event | readableDate }}</h4></div><div class="form-group col-sm-6" ng-if=event.locations.length><label>Location</label><h4 class=normal>{{event.locations[0].title}}</h4></div></div><hr class=border-top ng-if="event.body.length"><div ng-if=event.body.length><div ng-if="event.body.length > 500" class=text-wrap><long-text-wrap><div compile-html=event.body></div></long-text-wrap></div><div ng-if="event.body.length < 500" class="text-wrap paragraph-spacer" compile-html=event.body></div></div></div></div></div><div class="wrapper-sm bg-light border-top" ng-if=event.locations.length><div class=container><div class=text-wrap><div class=form-group><label>Location</label><div ng-repeat="location in event.locations track by location._id"><div class="row row-flex"><div class=col-sm-6><h5 class=normal>{{ location.title }}</h5><div><div ng-show=location.addressLine1.length>{{location.addressLine1}}</div><div ng-show=location.addressLine2.length>{{location.addressLine2}}</div><div>{{location.suburb}} {{location.state}} {{location.postalCode}}</div><div ng-show=location.country>{{location.country}}</div><p></p><a class="btn btn-outline" target=_blank ng-href="https://maps.google.com/maps?saddr=Current+Location&daddr={{location.title}},{{location.addressLine1}},+{{location.suburb}}+{{location.state}}+{{location.postalCode}}&dirflg=r"><span>Get Directions</span> <i class="fa fa-map-marker"></i></a></div></div><div class=col-sm-6><google-map ng-model=location></google-map></div></div></div></div></div></div></div><div class="wrapper border-top bg-white"><div class=container><div class=text-wrap ng-if="form.status != \'active\'"><h3 class=wrapper-title>Registration is closed</h3><p>Registrations have closed for this event.</p></div><div class=text-wrap ng-if="form.status == \'active\'"><h3 class=wrapper-title>Register Online</h3><webform ng-model=form ng-payment-integration=form.data.publicData.paymentGateways[0] linked-event=event._id><div ng-show=model.data.publicData.thankyou.length><div compile-html=model.data.publicData.thankyou></div></div><div ng-hide=model.data.publicData.thankyou.length><h4>Registration Complete!</h4><p>You have successfully submitted "{{model.title}}".</p></div><a class="btn btn-default" ng-click=reset()><i class="fa fa-angle-left"></i> <span>Back to form</span></a></webform></div></div></div>'), 
    $templateCache.put("routes/form/form-view.html", '<div class="top-banner bg-image" ng-if=form.data.publicData.mainImage style="background-image: url( {{$root.asset.imageUrl(form.data.publicData.mainImage, null, null, {from:form._id})}} )"><div class="wrapper-lg shader" style="padding-bottom: 0"><div class=text><div class=container><div class=text-wrap><div class=wrapper-xs><div class="row row-flex"><div class="col-xs-12 col-sm-8"><h6 class="account-title text-muted">{{form.account.title}}</h6><h1 class=title>{{form.data.publicData.title || form.title}}</h1></div><div class="col-xs-12 col-sm-4"><div class=share><short-url ng-model=form></short-url></div></div></div></div></div></div></div></div></div><div class=top-banner ng-if=!form.data.publicData.mainImage style=background-color:{{form.account.color}}><div class=shader><div class=wrapper-sm></div><div class=text><div class=container><div class=text-wrap><div class=wrapper-xs><div class="row row-flex"><div class="col-xs-12 col-sm-8"><h6 class="account-title text-muted">{{form.account.title}}</h6><h1 class=title>{{form.data.publicData.title || form.title}}</h1></div><div class="col-xs-12 col-sm-4"><div class=share><short-url ng-model=form></short-url></div></div></div></div></div></div></div></div></div><div class="wrapper-sm border-top" ng-if=form.data.publicData.body.length><div class=container><div class=text-wrap><div class=form-group><label>{{form.data.publicData.title || form.title}}</label><div ng-if="form.data.publicData.body.length > 500"><long-text-wrap><div compile-html=form.data.publicData.body></div></long-text-wrap></div><div ng-if="form.data.publicData.body.length < 500" class=paragraph-spacer compile-html=form.data.publicData.body></div></div></div></div></div><div class="wrapper-sm border-top bg-white"><div class=container><div class=text-wrap><webform ng-model=form linked-process=linkedProcess ng-payment-integration=form.gateways[0] callback=done><div ng-show=$parent.$parent.form.data.publicData.thankyou.length compile-html=$parent.$parent.form.data.publicData.thankyou></div><div ng-hide=$parent.$parent.form.data.publicData.thankyou.length><h4>Thank you!</h4><p>Your submission was successful.</p></div><a class="btn btn-default" ng-click=reset()><i class="fa fa-angle-left"></i> <span>Back to form</span></a></webform></div></div></div>'), 
    $templateCache.put("share-modal/share-modal.html", '<div class=share-modal ng-show=service.form><div class=share-modal-bg ng-click=close()></div><div class=share-modal-fg><div class=share-modal-wrap ng-switch=service.status><div class="panel panel-default" ng-switch-when=processing><div class=panel-body><i class="fa fa-spinner fa-spin"></i></div></div><div class="panel panel-default" ng-switch-default><div class=panel-body><i class="fa fa-times pull-right" ng-click=close()></i><h5 class=title>Share this form</h5><p class=help-block>Copy and share this short url with others</p><div class=input-group><input select-on-focus readonly class=form-control ng-model="url"><div class=input-group-addon ng-class={active:service.copied} ng-click=copyUrl()><span ng-show=!service.copied>Copy</span> <span ng-show=service.copied>Copied!</span></div></div></div></div></div></div></div>');
} ]);